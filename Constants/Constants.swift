//
//  Constants.swift
//  Coupon Book
//
//  Created by apple  on 10/21/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import Foundation

import UIKit

let SWIDTH = UIScreen.main.bounds.width
let SHEIGHT = UIScreen.main.bounds.height
let STATUSBARHEIGHT = UIApplication.shared.statusBarFrame.size.height

let BASE_URL = "http://couplescouponbook.com/ccbadminnew/Coupon_api/"
//"https://adarshtated.com/dev/ccb/Coupon_api/"
//"http://couplescouponbook.com/ccbadmin/Coupon_api/"
// http://couplescouponbook.com/ccbadmin/Coupon_api/delete_favorite_coupon  
let GIFTCHARGE = 2.99
var Contact_Email = "thecouplescouponbook@gmail.com"

//let BASE_URL = "http://thevikasenterprises.com/thecouplecouponbook/Coupon_api/"
//let BASE_URL = "http://globalitrentals.com/ccb/Coupon_api/"

let USER_DEFAULTS = UserDefaults.standard

let APPPINKCOLOR = UIColor(red: 255/255, green: 100/255, blue: 104/255, alpha: 1.0)
let APPLIGHTPINKCOLOR = UIColor(red: 231/255, green: 81/255, blue: 86/255, alpha: 1.0)
let APPLIGHTPINKCOLORSECOND = UIColor(red: 243/255, green: 133/255, blue: 135/255, alpha: 1.0)
let FACEBOOKBLUECOLOR = UIColor(red: 59/255, green: 90/255, blue: 148/255, alpha: 1.0)
let APPLIGHTREDCOLOR = APPLIGHTGRAYCOLOR//UIColor(red: 209/255, green: 0/255, blue: 16/255, alpha: 1.0)
let APPREDCOLOR = APPGRAYCOLOR//UIColor(red: 162/255, green: 6/255, blue: 20/255, alpha: 1.0)
let APPGREENCOLOR = UIColor(red: 102/255, green: 199/255, blue: 113/255, alpha: 1.0)
let APPSKYBLUECOLOR = UIColor(red: 101/255, green: 172/255, blue: 221/255, alpha: 1.0)
let APPYELLOWCOLOR = UIColor(red: 255/255, green: 164/255, blue: 55/255, alpha: 1.0)
let APPGRAYCOLOR = UIColor(red: 48/255, green: 48/255, blue: 48/255, alpha: 1.0)
let APPLIGHTGRAYCOLOR = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
let APPLIGHTGREENCOLOR = UIColor(red: 0/255, green: 176/255, blue: 80/255, alpha: 1.0)
let APPRedCOLOR = APPGRAYCOLOR//UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
