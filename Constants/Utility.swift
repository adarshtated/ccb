//
//  Utility.swift
//  Coupon Book
//
//  Created by apple  on 10/22/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import Foundation
import UIKit

//import Alamofire
//import RappleProgressHUD

class Utility
{
    static let sharedInstance = Utility()
    fileprivate init() {}
    
    
    
    func showAlert(_ title:String, msg:String, controller:UIViewController) {
        
        
        let alertController = UIAlertController(title: title,
                                                message: msg,
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    func showDismissAlert(_ title:String, msg:String, controller:UIViewController) {
        let alertController = UIAlertController(title: title,
                                                message: msg,
                                                preferredStyle: .alert)
        
        let actionOK = UIAlertAction.init(title: "", style: .default) { (alert: UIAlertAction!) in
        }
        alertController.addAction(actionOK)
        
        controller.present(alertController, animated: true, completion: {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak controller] in
                guard controller?.presentedViewController == alertController else { return }
                
                controller?.dismiss(animated: true, completion: nil)
            }
        })
    }
    
}
