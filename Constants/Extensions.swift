//
//  Extensions.swift
//  Coupon Book
//
//  Created by apple  on 10/21/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import Foundation

import UIKit


extension UIImageView {
    
    func setImageView(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,img:UIImage) {
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.image = img
        self.clipsToBounds = true
        self.contentMode = UIViewContentMode.scaleAspectFit
    }
}

extension SkyFloatingLabelTextFieldWithIcon {
    
    func setSkyLabel(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Title:String,PlaceHolder:String,TitleColor:UIColor,selectedTitleColor:UIColor,LineHeight:CGFloat,img:UIImage,lineColorr:UIColor,SelectedlineColorr:UIColor,tagg:Int){
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.textColor = TextColor
        self.backgroundColor = BackColor
        self.title = Title
        self.placeholder = PlaceHolder
        
        self.titleColor = TitleColor
        self.selectedTitleColor = selectedTitleColor
        self.lineHeight = LineHeight
        self.selectedLineHeight = LineHeight
        self.lineColor = lineColorr
        self.selectedLineColor = SelectedlineColorr
        self.tag = tagg
        self.iconImage = img
        self.iconImageview.contentMode = UIViewContentMode.scaleAspectFit
        
    }
}
extension SkyFloatingLabelTextField {
    
    func setSkyLabelwithoutIcon(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Title:String,PlaceHolder:String,TitleColor:UIColor,selectedTitleColor:UIColor,LineHeight:CGFloat,lineColorr:UIColor,SelectedlineColorr:UIColor,tagg:Int){
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.textColor = TextColor
        self.backgroundColor = BackColor
        self.title = Title
        self.placeholder = PlaceHolder
        self.titleColor = TitleColor
        self.selectedTitleColor = selectedTitleColor
        self.lineHeight = LineHeight
        self.selectedLineHeight = LineHeight
        self.lineColor = lineColorr
        self.selectedLineColor = SelectedlineColorr
        self.tag = tagg
        
    }
}
extension UIButton {
    func setButton(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Title:String) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.setTitleColor(TextColor, for: .normal)
        self.setTitle(Title, for: .normal)
        self.backgroundColor = BackColor
        self.clipsToBounds = true
    }
    func setButtonDesboard(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Title:String) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.layer.cornerRadius = Height/3
        self.layer.masksToBounds = true
        self.layer.borderWidth = 3
        self.layer.borderColor = TextColor.cgColor
        self.setTitleColor(TextColor, for: .normal)
        self.setTitle(Title, for: .normal)
        self.backgroundColor = BackColor
        self.clipsToBounds = true
    }
}

extension UILabel {
    func setLabel(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Text:String,TextAlignment:NSTextAlignment,Font:UIFont) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.backgroundColor = BackColor
        self.text = Text
        self.textColor = TextColor
        self.font = Font
        self.textAlignment = TextAlignment
        self.clipsToBounds = true
    }
}

extension UITextField {
    func setTextField(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.textColor = TextColor
        self.backgroundColor = BackColor
    }
    
    class func connectTextFields(fields: [UITextField]) -> Void{
            guard let last = fields.last else {
                return
            }
            for i in 0 ..< fields.count - 1 {
                fields[i].returnKeyType = .next
                fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
            }
            last.returnKeyType = .done
            last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
    }
    
}

extension String {
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
   }
}


extension Date {
    func app_stringFromDate() -> String{
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        let strdt = dateFormatter.string(from: self as Date)
        if let dtDate = dateFormatter.date(from: strdt){
            return dateFormatter.string(from: dtDate)
        }
        return "--"
    }
}


