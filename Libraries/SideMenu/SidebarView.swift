


import Foundation
import UIKit


protocol SidebarViewDelegate: class {
    func sidebarDidSelectRowSectionFirst(row: Row)
    func sidebarDidSelectRowSectionSecond(row: Roww)
    func sidebarDidSelectRowSectionThird(row: Privacy)
    func GoToProfileView()
    func GoToNotificationView()
    func copounAction()
    func sendGiftAction()
    func rebuyCopounAction()
    func submitCopounAction()
    func sentCopounAction()
    func recivedCopounAction()
    func favorateCopounAction()
}


var UserNamme = String()
var UserImage = UIImage()

enum Row: String {
    case home
    case sendcoupon
    case redeemed
    case referral
    case submitcoupon
    case mycoupon
    case favouritecoupons
    case topcoupons
    case repurchasedcoupons
    case none
    
    init(row: Int) {
        switch row {
        case 0: self = .home
        case 1: self = .sendcoupon
        case 2: self = .referral
        case 3: self = .favouritecoupons
        case 4: self = .topcoupons
        case 5: self = .submitcoupon
        case 6: self = .redeemed
        case 7: self = .mycoupon
        case 8: self = .repurchasedcoupons
        default: self = .none
        }
    }
}

enum Roww: String {
    case wallet
    case sendgift
    case logout
    case none
    
    init(row: Int) {
        switch row {
        case 0: self = .wallet
        case 1: self = .sendgift
        case 2: self = .logout
        default: self = .none
        }
    }
}
enum Privacy: String{
    case privacypolicy
    case none
    
    init(row: Int){
        switch row {
        case 0:
            self = .privacypolicy
        default:
            self = .none
        }
    }
}

class SidebarView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var sectionFirstTitleArray = [String]()
    var sectionFirstImageArray = [UIImage]()
    var sectionSelectedFirstImageArray = [UIImage]()
    
    var sectionSecondTitleArray = [String]()
    var sectionSecondImageArray = [UIImage]()
    var sectionSelectedSecondImageArray = [UIImage]()
    
    var sectionThirdTitleArray = [String]()
    var UserNme = "User Name"
    
    weak var delegate: SidebarViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        sectionFirstTitleArray = []//["DASHBOARD","SENT COUPONS","COUPONS RECEIVED","FAVORITE COUPONS"]//,"TOP10 RATING COUPONS","SUBMIT A COUPON","REDEEMED COUPONS","SUBMITTED COUPONS", "REPURCHASED COUPONS"
        sectionFirstImageArray = []//[#imageLiteral(resourceName: "dashboard"),#imageLiteral(resourceName: "email"),#imageLiteral(resourceName: "giftcoupon"),#imageLiteral(resourceName: "like-5"),UIImage(named: "top10.png")!,#imageLiteral(resourceName: "send"),#imageLiteral(resourceName: "redeem"),#imageLiteral(resourceName: "mycoupon"),#imageLiteral(resourceName: "reuse")]
        sectionSelectedFirstImageArray = [#imageLiteral(resourceName: "dashboard_red"), #imageLiteral(resourceName: "email_red"),#imageLiteral(resourceName: "giftcoupon_red"),#imageLiteral(resourceName: "like-4"),UIImage(named: "top10red.png")!,UIImage(named: "sent-mail")!,#imageLiteral(resourceName: "redeem_red"),#imageLiteral(resourceName: "mycoupon_red"),#imageLiteral(resourceName: "reuse_red")]
        
        sectionSecondTitleArray = ["LOGOUT"]//"MY WALLET","SEND AS VOUCHER",
        sectionSecondImageArray = [#imageLiteral(resourceName: "wallet"),#imageLiteral(resourceName: "cart"),#imageLiteral(resourceName: "logout")]
        sectionSelectedSecondImageArray = [#imageLiteral(resourceName: "wallet_red"),#imageLiteral(resourceName: "cart"),#imageLiteral(resourceName: "logout_red")]
        
        sectionThirdTitleArray = ["Privacy Policy | Disclaimer"]
        
        self.backgroundColor=UIColor(red: 54/255, green: 55/255, blue: 56/255, alpha: 1.0)
        self.clipsToBounds=true
        
//        self.addSubview(BackGroundView)
//        BackGroundView.frame = CGRect(x: 0, y: 0, width: SWIDTH*3/4, height: SHEIGHT)
//        BackGroundView.contentMode =  UIViewContentMode.scaleAspectFit
//        BackGroundView.clipsToBounds = true
//        BackGroundView.image = #imageLiteral(resourceName: "Side_Menu.png")
        
        CCBImage()
        CCBimageVCiew.contentMode = UIViewContentMode.scaleAspectFit
        CCBimageVCiew.image = #imageLiteral(resourceName: "CCB Logo for avatar copy")
        CCBimageVCiew.isUserInteractionEnabled = true
        CCBimageVCiew.backgroundColor = .clear
        CCBimageVCiew.clipsToBounds = true
        
        setupUpperSep()
        UpperSepLabel.backgroundColor = .lightGray
        
        setupImageView()
        userImageView.contentMode = UIViewContentMode.scaleToFill
        userImageView.image = #imageLiteral(resourceName: "user")
        userImageView.isUserInteractionEnabled = true
        userImageView.backgroundColor = .clear
        userImageView.layer.borderColor = UIColor.lightGray.cgColor
        userImageView.layer.borderWidth = 0.8
        userImageView.layer.cornerRadius = (SHEIGHT/11)/2
        userImageView.clipsToBounds = true
        
        if let image = USER_DEFAULTS.string(forKey: "user_image") {
            userImageView.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "user"))
        } else {
            userImageView.image = #imageLiteral(resourceName: "user")
        }
        BellImage()
        BellImageView.contentMode = UIViewContentMode.scaleToFill
        BellImageView.image = #imageLiteral(resourceName: "bell_icon")
        BellImageView.isUserInteractionEnabled = true
        BellImageView.backgroundColor = .clear
        BellImageView.clipsToBounds = true
        
        /***************************************************/
        setupNotLabel()
        countLabel.backgroundColor = .yellow
        countLabel.textColor = .black
        countLabel.adjustsFontSizeToFitWidth = true
        countLabel.textAlignment = .center
        countLabel.font = UIFont.boldSystemFont(ofSize: 11)
        countLabel.layer.cornerRadius = (SHEIGHT/31)/2
        countLabel.clipsToBounds = true
        if let countt = USER_DEFAULTS.value(forKey: "all_noti_count") as? String {
            if countt == "" {
                countLabel.isHidden = true
                countLabel.text = "0"
            } else if countt == "0" {
                countLabel.isHidden = true
            } else if countt.count > 2 {
                countLabel.isHidden = false
                countLabel.text = "99+"
            } else {
                countLabel.isHidden = false
                countLabel.text = countt
            }
            
        } else {
            countLabel.isHidden = true
            countLabel.text = "0"
        }
        /***************************************************/
        
        setupViews()
        TableView.delegate=self
        TableView.dataSource=self
        TableView.register(SidemenuuTableViewCell.self, forCellReuseIdentifier: "SideMenuCell")
        TableView.tableFooterView=UIView()
        TableView.separatorStyle = UITableViewCellSeparatorStyle.none
        TableView.allowsSelection = true
        TableView.bounces=false
        TableView.showsVerticalScrollIndicator=false
        TableView.backgroundColor = UIColor.clear//(red: 236/255, green: 236/255, blue: 236/255, alpha: 0.9)
        
        setupUserNameLabel()
        UserNameLabel.backgroundColor = UIColor.clear
        UserNameLabel.titleLabel?.adjustsFontSizeToFitWidth = true
        UserNameLabel.backgroundColor = .clear
        UserNameLabel.titleLabel?.textColor = UIColor.white
        UserNameLabel.setTitle(USER_DEFAULTS.string(forKey: "user_name") ?? "", for: .normal)
        //UserNameLabel.contentHorizontalAlignment = .left
        UserNameLabel.addTarget(self, action: #selector(UserNameAction), for: .touchUpInside)
        
        if SWIDTH > 500 {
            UserNameLabel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
            
        }
        else {
            UserNameLabel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 1
        } else if section == 1{
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 1
        } else if section == 1{
            return 0
        }
        else{
            return 0
        }
    }
    var footerView : UIView!
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            footerView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH*3/4, height: 2))
            footerView.backgroundColor = UIColor(red: 140/255, green: 134/255, blue: 125/255, alpha: 1.0)
        } else if section == 1 {
            footerView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH*3/4, height: 2))
            footerView.backgroundColor = UIColor(red: 140/255, green: 134/255, blue: 125/255, alpha: 1.0)
        }
        else {
            
        }
        return footerView
    }
    var commonHeight = 0
    var sentLabel1 = "0"
    var recivedLabel = "0"
    var favoriteLabel = "0"
    var scrollView = UIScrollView()
    let couponBtn = UIButton()
    let viewButtons = UIView()
    let sentBtn = UIButton()
    let sentLabel = UILabel()
    let lineLabel = UILabel()
    let receivedBtn = UIButton()
    let receivedLabel = UILabel()
    let lineLabel1 = UILabel()
    let favouiteBtn = UIButton()
    let favouiteLabel = UILabel()
    let sendGiftBtn = UIButton()
    let lineLabel2 = UILabel()
    let sendGiftLabel = UILabel()
    let reBuyBtn = UIButton()
    let submitBtn = UIButton()
    let lineLabel3 = UILabel()
    var trmsSwitch = UISwitch()
    let termsLabel = UILabel()
    let acceptLabel = UILabel()
    let lineLabel4 = UILabel()
    let YesLabel1 = UILabel()
    var privcySwitch = UISwitch()
    let privcyLabel = UILabel()
    let NoLabel0 = UILabel()
    let YesLabel0 = UILabel()
    let NoLabel2 = UILabel()
    let YesLabel2 = UILabel()
    var eulaSwitch = UISwitch()
    let eulaLabel = UILabel()
    let NoLabel1 = UILabel()
    
    
    @objc func AgeSwitchAction(){
        
//        let AlertController = UIAlertController(title: "", message: "Sorry!! You are not eligible to use this app.", preferredStyle: .alert)
//        let OkAction = UIAlertAction(title: "OK", style: .destructive) { (UIAlertAction) in
//            exit(0)
//        }
//        AlertController.addAction(OkAction)
//        present(AlertController, animated: true, completion: nil)
    }
    var flag = false
    
    @objc func couposAction(){
        print("clickHello")
        flag.toggle()

        guard !flag else {
            viewButtons.isHidden = false
            commonHeight = 145
//            self.onToggleView()
            return }
        commonHeight = 0
        viewButtons.isHidden = true
//        self.onToggleView()
    }
    
    
    @objc func sentCouponAction(){
        delegate?.sentCopounAction()
        print("sentCouponAction")
//        navigationController?.pushViewController(SendCouponsViewController(), animated: false)
    }
    @objc func receivedAction(){
        delegate?.recivedCopounAction()
        print("receivedAction")
//        navigationController?.pushViewController(ReferralCouponViewController(), animated: false)
    }
    @objc func favoriteAction(){
        delegate?.favorateCopounAction()
        print("favoriteAction")
//        navigationController?.pushViewController(FavouriteCouponsViewController(), animated: false)
    }
    @objc func sendGiftAction(){
        delegate?.sendGiftAction()
        print("sendGiftAction")
//        navigationController?.pushViewController(SendGiftViewController(), animated: false)
    }
    @objc func reBuyAction(){
        delegate?.rebuyCopounAction()
        print("reBuyAction")
//        navigationController?.pushViewController(RepurchasedCouponsViewController(), animated: false)
    }
    @objc func submitCoupontAction(){
        delegate?.submitCopounAction()
        print("sentCouponAction")
//        navigationController?.pushViewController(SubmitCouponViewController(), animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView : UIView!
        if section == 0 {
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH*3/4, height: 1))
            headerView.backgroundColor = UIColor(red: 140/255, green: 134/255, blue: 125/255, alpha: 1.0)
            
        } else if section == 1 {
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH*3/4, height: 1))
            headerView.backgroundColor = UIColor(red: 140/255, green: 134/255, blue: 125/255, alpha: 1.0)
        }
        else{
            headerView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH*3/4, height: (SHEIGHT-SHEIGHT/4)/9.5))
            headerView.backgroundColor = UIColor.clear
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return sectionFirstTitleArray.count
        }
        else if section == 1{
            return sectionSecondTitleArray.count
        }
        else {
            return sectionThirdTitleArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SidemenuuTableViewCell
        
        if SWIDTH > 500 {
            cell.Namelabel.font = UIFont.boldSystemFont(ofSize: 15)
            
        }
        else {
            cell.Namelabel.font = UIFont.boldSystemFont(ofSize: 15)
            
        }
        /*********************--NJ--********************/
        /*********************--NJ--********************/
        if indexPath.section == 0 {
            cell.imageview.isHidden = false
            cell.countLabel.backgroundColor = APPREDCOLOR
            if indexPath.row == 1 {
                if let Sentcount = USER_DEFAULTS.value(forKey: "sent_coupon") as? String {
                    if Sentcount == "" {
                        cell.countLabel.text = "0"
                    }
                    else if Sentcount.count > 2 {
                        cell.countLabel.text = "99+"
                    } else {
                        cell.countLabel.text = Sentcount
                    }
                } else {
                    cell.countLabel.text = "0"
                }
                cell.countLabel.isHidden = false
            } else if indexPath.row == 2 {
                if let Rec_count = USER_DEFAULTS.value(forKey: "count") as? String {
                    if Rec_count == "" {
                        cell.countLabel.text = "0"
                    }
                    else if Rec_count.count > 2 {
                        cell.countLabel.text = "99+"
                    } else {
                        cell.countLabel.text = Rec_count
                    }
                } else {
                    cell.countLabel.text = "0"
                }
                cell.countLabel.backgroundColor = APPREDCOLOR
                cell.countLabel.isHidden = false
            } else if indexPath.row == 3 {
                if let fav_count = USER_DEFAULTS.value(forKey: "favorite_coupon_count") as? String {
                    if fav_count == "" {
                        cell.countLabel.text = "0"
                    }
                    else if fav_count.count > 2 {
                        cell.countLabel.text = "99+"
                    } else {
                        cell.countLabel.text = fav_count
                    }
                } else {
                    cell.countLabel.text = "0"
                }
                cell.countLabel.backgroundColor = APPREDCOLOR
                cell.countLabel.isHidden = false
            } else if indexPath.row == 7 {
                if let countt = USER_DEFAULTS.value(forKey: "redeem_count") as? String {
                    if countt == "" {
                        cell.countLabel.text = "0"
                    }
                    else if countt.count > 2 {
                        cell.countLabel.text = "99+"
                    } else {
                        cell.countLabel.text = countt
                    }
                    
                } else {
                    cell.countLabel.text = "0"
                }
                cell.countLabel.backgroundColor = .clear
                cell.countLabel.isHidden = false
            } else {
                cell.countLabel.isHidden = true
            }
            cell.Namelabel.text = sectionFirstTitleArray[indexPath.row]
            cell.imageview.image = sectionFirstImageArray[indexPath.row]
        }
        else if indexPath.section == 1{
            cell.countLabel.isHidden = true
            cell.imageview.isHidden = false
            cell.Namelabel.text = sectionSecondTitleArray[indexPath.row]
            cell.imageview.image = sectionSecondImageArray[indexPath.row]
        }
        else{
            cell.countLabel.isHidden = true
            cell.imageview.isHidden = true
            cell.Namelabel.text = sectionThirdTitleArray[indexPath.row]
        }
        /*********************--NJ--********************/
        if USER_DEFAULTS.integer(forKey: "section") == indexPath.section && USER_DEFAULTS.integer(forKey: "row") == indexPath.row {
            if USER_DEFAULTS.integer(forKey: "section") == 0 {
                cell.imageview.image = sectionSelectedFirstImageArray[indexPath.row]
                cell.Namelabel.textColor = APPREDCOLOR
            } else if USER_DEFAULTS.integer(forKey: "section") == 1 {
                cell.imageview.image = sectionSelectedSecondImageArray[indexPath.row]
                cell.Namelabel.textColor = APPREDCOLOR
            } else {
                cell.Namelabel.textColor = .white
            }
            
        } else {
            cell.Namelabel.textColor = .white
        }
        
        cell.backgroundColor = .clear
        cell.selectionStyle = UITableViewCellSelectionStyle.gray
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            USER_DEFAULTS.set(0, forKey: "section")
            USER_DEFAULTS.set(indexPath.row, forKey: "row")
            self.delegate?.sidebarDidSelectRowSectionFirst(row: Row(row: indexPath.row))
        } else  if indexPath.section == 1 {
            USER_DEFAULTS.set(1, forKey: "section")
            USER_DEFAULTS.set(indexPath.row, forKey: "row")
            self.delegate?.sidebarDidSelectRowSectionSecond(row: Roww(row: indexPath.row))
        }
        else{
            USER_DEFAULTS.set(2, forKey: "section")
            USER_DEFAULTS.set(indexPath.row, forKey: "row")
            self.delegate?.sidebarDidSelectRowSectionThird(row: Privacy(row: indexPath.row))
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((SHEIGHT-SHEIGHT/4)/10.5)*3/4
    }
    
    @objc func UserNameAction(){
        self.delegate?.GoToProfileView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first!
        if touch.view == userImageView {
            self.delegate?.GoToProfileView()
        } else if touch.view == BellImageView {
            self.delegate?.GoToNotificationView()
        }
    }
    var btn:UIButton!
    func setupViews() {
        self.addSubview(TableView)
        TableView.topAnchor.constraint(equalTo: userImageView.bottomAnchor, constant:10).isActive = true
        TableView.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
        TableView.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
        TableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive=true
        
    }
    func BellImage(){
        self.addSubview(BellImageView)
        BellImageView.topAnchor.constraint(equalTo: UpperSepLabel.bottomAnchor, constant: 10).isActive = true
        BellImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -SHEIGHT/25-10).isActive = true
      //  BellImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: SWIDTH*3/4-15-SHEIGHT/25).isActive=true
        BellImageView.widthAnchor.constraint(equalToConstant: SHEIGHT/25).isActive=true
        BellImageView.heightAnchor.constraint(equalToConstant: SHEIGHT/25).isActive=true
    }
    func setupNotLabel() {
        self.addSubview(countLabel)
        countLabel.topAnchor.constraint(equalTo: BellImageView.topAnchor, constant: -3).isActive = true
        countLabel.leftAnchor.constraint(equalTo: BellImageView.rightAnchor, constant: -(SHEIGHT/31)/2).isActive=true
       // UserNameLabel.rightAnchor.constraint(equalTo: BellImageView.leftAnchor, constant: -5).isActive=true
        countLabel.widthAnchor.constraint(equalToConstant: SHEIGHT/31).isActive=true
        countLabel.heightAnchor.constraint(equalToConstant: SHEIGHT/31).isActive=true
    }
    func CCBImage(){
       self.addSubview(CCBimageVCiew)
        CCBimageVCiew.topAnchor.constraint(equalTo: topAnchor, constant: STATUSBARHEIGHT+5).isActive = true
        CCBimageVCiew.leftAnchor.constraint(equalTo: leftAnchor, constant: SWIDTH/4).isActive=true
        CCBimageVCiew.widthAnchor.constraint(equalToConstant: SWIDTH/2).isActive=true
        CCBimageVCiew.heightAnchor.constraint(equalToConstant: SHEIGHT/7.5).isActive=true
    }
    func setupImageView() {
        self.addSubview(userImageView)
        userImageView.topAnchor.constraint(equalTo: UpperSepLabel.bottomAnchor, constant: 8).isActive = true
        userImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive=true
        userImageView.widthAnchor.constraint(equalToConstant: SHEIGHT/11).isActive=true
        userImageView.heightAnchor.constraint(equalToConstant: SHEIGHT/11).isActive=true
    }
    func setupUserNameLabel() {
        self.addSubview(UserNameLabel)
        UserNameLabel.topAnchor.constraint(equalTo: userImageView.topAnchor, constant: (SHEIGHT/11)/3).isActive = true
        UserNameLabel.leftAnchor.constraint(equalTo: userImageView.rightAnchor, constant: 5).isActive=true
        UserNameLabel.rightAnchor.constraint(equalTo: BellImageView.leftAnchor, constant: -5).isActive=true
        //UserNameLabel.widthAnchor.constraint(equalToConstant: SWIDTH*3/4-(SHEIGHT/11+SHEIGHT/15+20)).isActive=true
        UserNameLabel.heightAnchor.constraint(equalToConstant: (SHEIGHT/11)/3).isActive=true
    }
    func setupUpperSep() {
        self.addSubview(UpperSepLabel)
        UpperSepLabel.topAnchor.constraint(equalTo: CCBimageVCiew.bottomAnchor, constant: 2).isActive = true
        UpperSepLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive=true
        UpperSepLabel.widthAnchor.constraint(equalToConstant: SWIDTH).isActive=true
        UpperSepLabel.heightAnchor.constraint(equalToConstant: 1).isActive=true
    }
//    func setBAckImage(){
//        self.addSubview(BackGroundView)
//        BackGroundView.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        BackGroundView.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
//        BackGroundView.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
//        BackGroundView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive=true
//    }
    
    
    let TableView: UITableView = {
        let table=UITableView()
        table.translatesAutoresizingMaskIntoConstraints=false
        return table
    }()
    let BellImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints=false
        return image
    }()
    let userImageView: UIImageView = {
        let image=UIImageView()
        image.translatesAutoresizingMaskIntoConstraints=false
        return image
    }()
    let CCBimageVCiew: UIImageView = {
        let image=UIImageView()
        image.translatesAutoresizingMaskIntoConstraints=false
        return image
    }()
    let UserNameLabel: UIButton = {
        let name=UIButton()
        name.translatesAutoresizingMaskIntoConstraints=false
        return name
    }()
    let UpperSepLabel: UILabel = {
        let label=UILabel()
        label.translatesAutoresizingMaskIntoConstraints=false
        return label
    }()
    
    let countLabel: UILabel = {
        let label=UILabel()
        label.translatesAutoresizingMaskIntoConstraints=false
        return label
    }()
//    let BackGroundView: UIImageView = {
//        let backimage=UIImageView()
//        backimage.translatesAutoresizingMaskIntoConstraints = true
//        return backimage
//    }()
//
//    let BlurBackGround: UIView = {
//        let Blurbackimage=UIView()
//        Blurbackimage.translatesAutoresizingMaskIntoConstraints = true
//        return Blurbackimage
//    }()
//
//
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
        
}

