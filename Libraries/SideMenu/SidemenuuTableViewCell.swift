



import UIKit

class SidemenuuTableViewCell: UITableViewCell {

    let sideMenucellView = UIView()
    let Namelabel = UILabel()
    let imageview = UIImageView()
    let countLabel = UILabel()
   // let countLabel1 = UILabel()
   // let SentLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        sideMenucellView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: ((SHEIGHT-SHEIGHT/4)/10.5)*3/4)
        sideMenucellView.backgroundColor = .clear//UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 0.9)
       contentView.addSubview(sideMenucellView)
        /***************************************************/
        imageview.frame = CGRect(x: sideMenucellView.frame.size.height/3, y: sideMenucellView.frame.size.height/3, width: sideMenucellView.frame.size.height/3, height: sideMenucellView.frame.size.height/3)
        imageview.contentMode = UIViewContentMode.scaleAspectFit
        imageview.clipsToBounds = true
        imageview.backgroundColor = UIColor.clear
        sideMenucellView.addSubview(imageview)
        /***************************************************/
        Namelabel.setLabel(X: 10+imageview.frame.origin.x+imageview.frame.size.width, Y: 5, Width: SWIDTH*3/4-(imageview.frame.origin.x+imageview.frame.size.width)-sideMenucellView.frame.size.height+30, Height: sideMenucellView.frame.size.height-10, TextColor: UIColor.white, BackColor: UIColor.clear, Text: "", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 2))
        Namelabel.text = "title"
      //  Namelabel.adjustsFontSizeToFitWidth = true
        sideMenucellView.addSubview(Namelabel)
        /***************************************************/
        countLabel.setLabel(X: sideMenucellView.frame.width-sideMenucellView.frame.size.height+5, Y: sideMenucellView.frame.size.height/4, Width: sideMenucellView.frame.size.height/2, Height: sideMenucellView.frame.size.height/2, TextColor: UIColor.white, BackColor: APPREDCOLOR, Text: "0", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 11))
        countLabel.adjustsFontSizeToFitWidth = true
        countLabel.layer.cornerRadius = countLabel.frame.height/2
        countLabel.clipsToBounds = true
        sideMenucellView.addSubview(countLabel)
        /***************************************************/
//        countLabel1.setLabel(X: sideMenucellView.frame.width-sideMenucellView.frame.size.height+5, Y: sideMenucellView.frame.size.height/4, Width: sideMenucellView.frame.size.height/2, Height: sideMenucellView.frame.size.height/2, TextColor: UIColor.white, BackColor: APPREDCOLOR, Text: "0", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 11))
//        countLabel1.adjustsFontSizeToFitWidth = true
//        countLabel1.layer.cornerRadius = countLabel1.frame.height/2
//        countLabel1.clipsToBounds = true
//        sideMenucellView.addSubview(countLabel1)
//        /***************************************************/
//        SentLabel.setLabel(X: sideMenucellView.frame.width-sideMenucellView.frame.size.height+5, Y: sideMenucellView.frame.size.height/4, Width: sideMenucellView.frame.size.height/2, Height: sideMenucellView.frame.size.height/2, TextColor: UIColor.white, BackColor: APPREDCOLOR, Text: "0", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 11))
//        SentLabel.adjustsFontSizeToFitWidth = true
//        SentLabel.layer.cornerRadius = countLabel1.frame.height/2
//        SentLabel.clipsToBounds = true
//        sideMenucellView.addSubview(SentLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

