import UIKit
import RappleProgressHUD
import SDWebImage

class SideMenuViewController: UIViewController {
    
    var Hidebutton: UIButton!
    
    var sidebarView: SidebarView!
    var blackScreen: UIView!
    
    var swipeLeftt : UISwipeGestureRecognizer!
    var swipeLeftt1 : UISwipeGestureRecognizer!
    
    var btnMenu: UIBarButtonItem!
    var topbbarHeight = CGFloat()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topbbarHeight = UIApplication.shared.statusBarFrame.size.height+(self.navigationController?.navigationBar.frame.height ?? 0.0)
        self.navigationItem.title = ""
        /******************--NJ--******************/
        btnMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-button"), style: .plain, target: self, action: #selector(btnMenuAction))
        btnMenu.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = btnMenu
        /******************--NJ--******************/
        sidebarView=SidebarView(frame: CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.view.frame.height))
        sidebarView.delegate = self
        
        sidebarView.layer.zPosition=100
        self.view.isUserInteractionEnabled=true
        sidebarView.backgroundColor = .black
        self.navigationController?.view.addSubview(sidebarView)
        /******************--NJ--******************/
        blackScreen=UIView(frame: self.view.bounds)
        blackScreen.backgroundColor=UIColor(white: 0, alpha: 0.5)
        blackScreen.isHidden=true
        self.navigationController?.view.addSubview(blackScreen)
        blackScreen.layer.zPosition=99
        let tapGestRecognizer = UITapGestureRecognizer(target: self, action: #selector(blackScreenTapAction(sender:)))
        blackScreen.addGestureRecognizer(tapGestRecognizer)
        
        swipeLeftt = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesturee))
        swipeLeftt.direction = UISwipeGestureRecognizerDirection.left
        blackScreen.addGestureRecognizer(self.swipeLeftt)
        
        swipeLeftt1 = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesturee))
        swipeLeftt1.direction = UISwipeGestureRecognizerDirection.left
        
        Hidebutton = UIButton(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        Hidebutton.addTarget(self, action: #selector(HidebuttonAction), for: .touchUpInside)
        Hidebutton.backgroundColor = UIColor(white: 0, alpha: 0.6)
        Hidebutton.isHidden = true
        navigationController?.view.addSubview(Hidebutton)        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCount()
    }
    
    @objc func HidebuttonAction(){
        
        Hidebutton.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
        })
    }
    
    
    
    var Sidemenuuushowung : Bool = false
    
    @objc func respondToSwipeGesturee(gesturee: UIGestureRecognizer) {
        
        if Sidemenuuushowung == true {
            
            Sidemenuuushowung = false
            
            if let swipeGesture = gesturee as? UISwipeGestureRecognizer {
                
                switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.right:
                    print("Swiped right")
                    
                    
                    
                case UISwipeGestureRecognizerDirection.down:
                    print("Swiped down")
                case UISwipeGestureRecognizerDirection.left:
                    print("Swiped left")
                    
                    blackScreen.isHidden=true
                    blackScreen.frame=self.view.bounds
                    
                    
                    UIView.animate(withDuration: 0.3) {
                        self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
                    }
                    
                case UISwipeGestureRecognizerDirection.up:
                    print("Swiped up")
                default:
                    break
                }
            }
        }
    }
    
    @objc func btnMenuAction() {
        
        getCount()
        
        blackScreen.isHidden=false
        Sidemenuuushowung = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.sidebarView.frame=CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            self.sidebarView.addGestureRecognizer(self.swipeLeftt1)
            //self.sidebarView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "Side_Menu.jpg"))
        }) { (complete) in
            self.blackScreen.frame=CGRect(x: SWIDTH, y: 0, width: SWIDTH/4, height: self.view.bounds.height+100)
        }
    }
    
    @objc func blackScreenTapAction(sender: UITapGestureRecognizer) {
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
        
    }
}


extension SideMenuViewController: SidebarViewDelegate {
    func ReloadTable() {
        
    }
    
    func ReloadSideTableView() {
    }
    func sidebarDidSelectRowSectionFirst(row: Row) {
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
        switch row {
        case .home:
            navigationController?.pushViewController(HomeViewController(), animated: false)
            break
        case .redeemed:
            navigationController?.pushViewController(RedeemedViewController(), animated: false)
            break
        case .referral:
            navigationController?.pushViewController(ReferralCouponViewController(), animated: false)
            break
        case .submitcoupon:
            navigationController?.pushViewController(SubmitCouponViewController(), animated: false)
            break
        case .mycoupon:
            navigationController?.pushViewController(MyCouponViewController(), animated: false)
            break
        case .sendcoupon:
            navigationController?.pushViewController(SendCouponsViewController(), animated: false)
        case .favouritecoupons: navigationController?.pushViewController(FavouriteCouponsViewController(), animated: false)
        case .repurchasedcoupons: navigationController?.pushViewController(RepurchasedCouponsViewController(), animated: false)
            
        case .none:
            break
        case .topcoupons:
            navigationController?.pushViewController(TopCouponsViewController(), animated: false)
        }
    }
    func sidebarDidSelectRowSectionSecond(row: Roww) {
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
            switch row {
        case .wallet:
            navigationController?.pushViewController(MyWalletViewController(), animated: false)
            break
        case .sendgift:
            navigationController?.pushViewController(SendGiftViewController(), animated: false)
            break
        case .logout:
            
            let AlertController = UIAlertController(title: "", message: "Are you sure!! you want to logout?.", preferredStyle: .alert)
            let OkAction = UIAlertAction(title: "Logout", style: .destructive) { (UIAlertAction) in
                self.Logout()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (UIAlertAction) in
                USER_DEFAULTS.set(0, forKey: "section")
                USER_DEFAULTS.set(0, forKey: "row")
            }
            AlertController.addAction(OkAction)
            AlertController.addAction(cancelAction)
            self.present(AlertController, animated: true, completion: nil)
            
            break
        case .none:
            break
        }
    }
    
    func copounAction() {
        print("copounAction")
    }
    func sentCopounAction() {
        print("sentCopounAction")
    }
    func recivedCopounAction() {
        print("recivedCopounAction")
    }
    func favorateCopounAction() {
        print("favorateCopounAction")
        
        
    }
    func sendGiftAction() {
        print("sendGiftAction")
    }
    func rebuyCopounAction() {
        print("rebuyCopounAction")
    }
    func submitCopounAction() {
        print("submitCopounAction")
    }
    
    
    
    func sidebarDidSelectRowSectionThird(row: Privacy) {
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
        switch row {
        case .privacypolicy:
            navigationController?.pushViewController(PrivacyPolicyViewController(), animated: false)
            break
        case .none:
            break
        }
    }
//    func NotificationsView() {
//        blackScreen.isHidden=true
//        blackScreen.frame=self.view.bounds
//        UIView.animate(withDuration: 0.3) {
//            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
//        }
//        navigationController?.pushViewController(NotificationsViewController(), animated: true)
//    }
    func GoToProfileView() {
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
        navigationController?.pushViewController(MyProfileViewController(), animated: true)
    }
    func GoToNotificationView(){
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
        navigationController?.pushViewController(NotificationsViewController(), animated: true)
    }
    
    func Logout(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"logout")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(String(describing: USER_DEFAULTS.value(forKey: "user_id")))".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        USER_DEFAULTS.set("no", forKey: "isloggedin")
                        USER_DEFAULTS.set(0, forKey: "section")
                        USER_DEFAULTS.set(0, forKey: "row")
                        USER_DEFAULTS.set("", forKey: "user_id")
                        USER_DEFAULTS.set("", forKey: "user_name")
                        USER_DEFAULTS.set("", forKey: "user_image")
                        USER_DEFAULTS.set("", forKey: "user_password")
                        USER_DEFAULTS.set("", forKey: "user_email")
                        self.navigationController?.pushViewController(ViewController(), animated: false)
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func getCount(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            // Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"count_refer_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&mobile=\(USER_DEFAULTS.value(forKey: "user_mobile") ?? "")&email=\(USER_DEFAULTS.value(forKey: "user_email") ?? "")".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        
                        if let counnt = dataDictionary.object(forKey: "count") as? NSNumber {
                            
                            USER_DEFAULTS.set(counnt.stringValue, forKey: "count")
                        } else {
                            USER_DEFAULTS.set(dataDictionary.object(forKey: "count") as! String, forKey: "count")
                        }
                        if let Reredeemcount = dataDictionary.object(forKey: "redeem_count") as? NSNumber {

                            USER_DEFAULTS.set(Reredeemcount.stringValue, forKey: "redeem_count")
                        } else {
                            USER_DEFAULTS.set(dataDictionary.object(forKey: "redeem_count") as! String, forKey: "redeem_count")
                       }
                        if let Sentcount = dataDictionary.object(forKey: "sent_coupon") as? NSNumber {
                            
                            USER_DEFAULTS.set(Sentcount.stringValue, forKey: "sent_coupon")
                        } else {
                            USER_DEFAULTS.set(dataDictionary.object(forKey: "sent_coupon") as! String, forKey: "sent_coupon")
                        }
                        if let allcount = dataDictionary.object(forKey: "all_noti_count") as? NSNumber {
                            
                            USER_DEFAULTS.set(allcount.stringValue, forKey: "all_noti_count")
                        } else {
                            USER_DEFAULTS.set(dataDictionary.object(forKey: "all_noti_count") as! String, forKey: "sent_coupon")
                        }
                        if let favcount = dataDictionary.object(forKey: "favorite_coupon_count") as? NSNumber {
                            
                            USER_DEFAULTS.set(favcount.stringValue, forKey: "favorite_coupon_count")
                        } else {
                            USER_DEFAULTS.set(dataDictionary.object(forKey: "favorite_coupon_count") as! String, forKey: "favorite_coupon_count")
                        }
                        
                    })
                }
                else {
                    
                }
            }
            else {
                
            }
            }.resume()
    }
    
    
}

