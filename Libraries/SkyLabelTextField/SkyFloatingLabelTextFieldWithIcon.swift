





import UIKit

/**
 A beautiful and flexible textfield implementation with support for icon, title label, error message and placeholder.
 */
open class SkyFloatingLabelTextFieldWithIcon: SkyFloatingLabelTextField {
    
    /// A UILabel value that identifies the label used to display the icon
    open var iconImageview: UIImageView!
    open var iconRightImageview: UIImageView!
    
    /// A UIFont value that determines the font that the icon is using
    @objc dynamic open var iconImage: UIImage? {
        didSet {
            iconImageview?.image = iconImage
            iconRightImageview?.image = iconImage
        }
    }
    
    /// A String value that determines the text used when displaying the icon
    //    @IBInspectable
    //    open var iconText: String? {
    //        didSet {
    //            iconLabel?.text = iconText
    //        }
    //    }
    
    /// A UIColor value that determines the color of the icon in the normal state
    //    @IBInspectable
    //    dynamic open var iconColor: UIColor = UIColor.gray {
    //        didSet {
    //            updateIconbackColor()
    //        }
    //    }
    
    /// A UIColor value that determines the color of the icon when the control is selected
    //    @IBInspectable
    //    dynamic open var selectedIconColor: UIColor = UIColor.gray {
    //        didSet {
    //            updateIconbackColor()
    //        }
    //    }
    
    /// A float value that determines the width of the icon
    @IBInspectable
    dynamic open var iconWidth: CGFloat = 20 {
        didSet {
            updateFrame()
        }
    }
    @IBInspectable
    dynamic open var iconHeight: CGFloat = 20 {
        didSet {
            updateFrame()
        }
    }
    
    /**
     A float value that determines the left margin of the icon.
     Use this value to position the icon more precisely horizontally.
     */
    @IBInspectable
    dynamic open var iconMarginLeft: CGFloat = 4 {
        didSet {
            updateFrame()
        }
    }
    
    /**
     A float value that determines the bottom margin of the icon.
     Use this value to position the icon more precisely vertically.
     */
    @IBInspectable
    dynamic open var iconMarginBottom: CGFloat = 6 {
        didSet {
            updateFrame()
        }
    }
    
    /**
     A float value that determines the rotation in degrees of the icon.
     Use this value to rotate the icon in either direction.
     */
    @IBInspectable
    open var iconRotationDegrees: Double = 0 {
        didSet {
            iconImageview.transform = CGAffineTransform(rotationAngle: CGFloat(iconRotationDegrees * .pi / 180.0))
            
            iconRightImageview.transform = CGAffineTransform(rotationAngle: CGFloat(iconRotationDegrees * .pi / 180.0))
        }
    }
    
    // MARK: Initializers
    
    /**
     Initializes the control
     - parameter frame the frame of the control
     */
    override public init(frame: CGRect) {
        super.init(frame: frame)
        createIconImageview()
    }
    
    /**
     Intialzies the control by deserializing it
     - parameter coder the object to deserialize the control from
     */
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createIconImageview()
    }
    
    // MARK: Creating the icon label
    
    /// Creates the icon label
    fileprivate func createIconImageview() {
        let iconImageview = UIImageView()
        iconImageview.backgroundColor = UIColor.clear
        iconImageview.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin]
        self.iconImageview = iconImageview
        addSubview(iconImageview)
        
        let iconRightImageview = UIImageView()
        iconRightImageview.backgroundColor = UIColor.clear
        iconRightImageview.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin]
        self.iconRightImageview = iconRightImageview
        addSubview(iconRightImageview)
        
        // updateIconbackColor()
    }
    
    // MARK: Handling the icon color
    
    /// Update the colors for the control. Override to customize colors.
    //    override open func updateColors() {
    //        super.updateColors()
    //       // updateIconbackColor()
    //    }
    
    //    fileprivate func updateIconbackColor() {
    //        if self.hasErrorMessage {
    //            iconImageview?.backgroundColor = errorColor
    //        } else {
    //            iconImageview?.backgroundColor = editingOrSelected ? selectedIconColor : iconColor
    //        }
    //    }
    
    // MARK: Custom layout overrides
    
    /**
     Calculate the bounds for the textfield component of the control.
     Override to create a custom size textbox in the control.
     - parameter bounds: The current bounds of the textfield component
     - returns: The rectangle that the textfield component should render in
     */
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        if isLTRLanguage {
            rect.origin.x += CGFloat(iconWidth + iconMarginLeft)
        } else {
            rect.origin.x -= CGFloat(iconWidth + iconMarginLeft)
        }
        rect.size.width -= CGFloat(iconWidth + iconMarginLeft)
        return rect
    }
    
    /**
     Calculate the rectangle for the textfield when it is being edited
     - parameter bounds: The current bounds of the field
     - returns: The rectangle that the textfield should render in
     */
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.editingRect(forBounds: bounds)
        if isLTRLanguage {
            rect.origin.x += CGFloat(iconWidth + iconMarginLeft)
        } else {
            // don't change the editing field X position for RTL languages
        }
        rect.size.width -= CGFloat(iconWidth + iconMarginLeft)
        return rect
    }
    
    /**
     Calculates the bounds for the placeholder component of the control.
     Override to create a custom size textbox in the control.
     - parameter bounds: The current bounds of the placeholder component
     - returns: The rectangle that the placeholder component should render in
     */
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.placeholderRect(forBounds: bounds)
        if isLTRLanguage {
            rect.origin.x += CGFloat(iconWidth + iconMarginLeft)
        } else {
            // don't change the editing field X position for RTL languages
        }
        rect.size.width -= CGFloat(iconWidth + iconMarginLeft)
        return rect
    }
    
    /// Invoked by layoutIfNeeded automatically
    override open func layoutSubviews() {
        super.layoutSubviews()
        updateFrame()
    }
    
    fileprivate func updateFrame() {
        let textWidth: CGFloat = bounds.size.width
        if isLTRLanguage {
            iconImageview.frame = CGRect(
                x: 0,
                y: bounds.size.height - textHeight() - iconMarginBottom,
                width: iconWidth,
                height: textHeight()
            )
        } else {
            iconImageview.frame = CGRect(
                x: textWidth - iconWidth,
                y: bounds.size.height - textHeight() - iconMarginBottom,
                width: iconWidth,
                height: iconWidth
            )
        }
    }
}

