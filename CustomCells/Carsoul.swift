//
//  Carsoul.swift
//  Coupon Book
//
//  Created by apple  on 12/3/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class Carsoul: UITableViewCell,iCarouselDelegate,iCarouselDataSource {
    var carouselView: iCarousel!
    var gameImages = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
    func numberOfItems(in carousel: iCarousel) -> Int {
        return gameImages.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let cellWidth = SWIDTH-20
        let cellheight = SHEIGHT/3-15
        let logView = UIView(frame: CGRect(x: 0, y: 0, width: cellWidth, height: cellheight))
        logView.layer.backgroundColor = UIColor.red.cgColor
        logView.layer.borderColor = UIColor.black.cgColor
        logView.layer.borderWidth = 3.0
        return logView
    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.spacing{
            return value * 1.1
        }
        return value
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        carouselView.type = .coverFlow2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
