//
//  MyCouponsTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 10/27/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class MyCouponsTableViewCell: UITableViewCell {

    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = UILabel()
    let PostedByLabel = UILabel()
    let CategaryLabel = UILabel()
    let RedView = UIView()
    let StatusLabel = UILabel()
    let LowerTextLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        cellView.frame = CGRect(x: 10, y: 8, width: SWIDTH-20, height: SHEIGHT/3-15)
        cellView.backgroundColor = UIColor.white
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 2
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        /******************--NJ--********************/
        ImageView.setImageView(X: 0, Y: 0, Width: cellView.frame.width, Height: cellView.frame.height, img: #imageLiteral(resourceName: "CCB Logo for avatar.png"))
        ImageView.contentMode = UIViewContentMode.scaleAspectFill
        ImageView.clipsToBounds = true
        cellView.addSubview(ImageView)
        /******************--NJ--********************/
       PostedByLabel.setLabel(X: CategaryLabel.frame.origin.x+17, Y: c_height/6+3, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .right, Font: UIFont.boldSystemFont(ofSize: 11))
        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: c_height/3, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+10, Width: c_width-c_height/2, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Raining outside !!", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 15))
        HeadingLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        HeadingLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        CategaryLabel.setLabel(X: 5, Y: c_height/6, Width: c_width-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Categary : Accessories", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 11))
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height-7, width: c_width, height: c_height+7)
        RedView.backgroundColor = APPLIGHTREDCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        StatusLabel.setLabel(X: 0, Y: 0, Width: RedView.frame.width, Height: RedView.frame.height, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        StatusLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        StatusLabel.adjustsFontSizeToFitWidth = true
        RedView.addSubview(StatusLabel)
        /******************--NJ--********************/
        LowerTextLabel.setLabel(X: c_height/3, Y: RedView.frame.origin.y-c_height-10, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
        LowerTextLabel.numberOfLines = 2
        LowerTextLabel.lineBreakMode = .byTruncatingTail
        LowerTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(LowerTextLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
