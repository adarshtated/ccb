//
//  submittedCollectionViewCell.swift
//  Coupon Book
//
//  Created by apple  on 12/31/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import MarqueeLabel

class submittedCollectionViewCell: UICollectionViewCell {
    
    
    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = UILabel()
    let PostedByLabel = UILabel()
    var CategaryLabel = MarqueeLabel()
    let LowerTextLabel = UILabel()
    let CountLabel = UILabel()
    let RedView = UIView()
    let DaysLabel = UILabel()
    let currentStatusLabel = UILabel()
    let CategaryLabelStatic = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(){
        
        cellView.frame = CGRect(x: 5, y: 5, width: SWIDTH-10, height: SHEIGHT/3-10)
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 10
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        ImageView.setImageView(X: 0, Y: 0, Width: cellView.frame.width, Height: cellView.frame.height-RedView.frame.height, img: #imageLiteral(resourceName: "CCB Logo for avatar.png"))
        ImageView.contentMode = UIViewContentMode.scaleAspectFill
        ImageView.clipsToBounds = true
       // cellView.addSubview(ImageView)
        /******************--NJ--********************/
        CategaryLabelStatic.setLabel(X: c_height/3, Y: c_height/6, Width: c_width/4-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Category:", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabelStatic.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabelStatic.adjustsFontSizeToFitWidth = true
       // cellView.addSubview(CategaryLabelStatic)
        /******************--NJ--********************/
        //        CategaryLabel.setLabel(X: , Y: , Width: , Height: , TextColor: .white, BackColor: .clear, Text: "Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel = MarqueeLabel.init(frame: CGRect(x: CategaryLabelStatic.frame.origin.x+CategaryLabelStatic.frame.size.width+2, y: c_height/6, width: c_width-SWIDTH/3.4-CategaryLabelStatic.frame.origin.x-CategaryLabelStatic.frame.width-4, height: c_height), duration: 8.0, fadeLength: 10.0)
        // CategaryLabel.frame =
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabel.backgroundColor = .clear
        CategaryLabel.numberOfLines = 0
        CategaryLabel.lineBreakMode = .byTruncatingTail
        CategaryLabel.adjustsFontSizeToFitWidth = true
        CategaryLabel.type = .continuous
        CategaryLabel.speed = .rate(80)
        CategaryLabel.fadeLength = 80.0
        CategaryLabel.labelWillBeginScroll()
       // cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        PostedByLabel.setLabel(X: 15, Y: c_height/8, Width: cellView.frame.width-30-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        /******************--NJ--********************/
        DaysLabel.setLabel(X: 15, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Hi", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        DaysLabel.font = UIFont(name: "Electrolize-Regular", size: 22)
        DaysLabel.attributedText = NSAttributedString(string: "Hi", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        DaysLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        DaysLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(DaysLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: 15, Y: DaysLabel.frame.origin.y+DaysLabel.frame.height+2, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "DayLabel", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        HeadingLabel.font = UIFont(name: "Electrolize-Regular", size: 21)
        HeadingLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        CountLabel.setLabel(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height-5, width: c_width, height: c_height+5)
        RedView.backgroundColor = APPLIGHTREDCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        currentStatusLabel.setLabel(X: RedView.frame.width/6, Y: 4, Width: (RedView.frame.width/6)*4, Height: RedView.frame.height-8, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        currentStatusLabel.font = UIFont(name: "Electrolize-Regular", size: 18)
        currentStatusLabel.adjustsFontSizeToFitWidth = true
        RedView.addSubview(currentStatusLabel)
        /******************--NJ--***********************/
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        /******************--NJ--********************/
        LowerTextLabel.setLabel(X: 15, Y: RedView.frame.origin.y-c_height-20, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        LowerTextLabel.numberOfLines = 0
        LowerTextLabel.lineBreakMode = .byTruncatingTail
        LowerTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        
    }
    
}


