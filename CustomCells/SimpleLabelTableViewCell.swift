//
//  SimpleLabelTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 10/28/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class SimpleLabelTableViewCell: UITableViewCell {

    let cellView = UIView()
    var GenderTextLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        cellView.frame = CGRect(x: 10, y: 0, width: SWIDTH/2-20, height: SHEIGHT/14)
        cellView.backgroundColor = UIColor.white
        cellView.clipsToBounds = true
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        /******************--NJ--********************/
        GenderTextLabel.setLabel(X: 10, Y: cellView.frame.height/6, Width: cellView.frame.width-30, Height: (cellView.frame.height/6)*4, TextColor: .black, BackColor: .clear, Text: "", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
        GenderTextLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        GenderTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(GenderTextLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
