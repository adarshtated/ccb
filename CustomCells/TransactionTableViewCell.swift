//
//  TransactionTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 10/27/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    let cellView = UIView()
    let UpperDescriptionLAbel = UILabel()
    let MiddleLabel = UILabel()
    let DateTimeLabel = UILabel()
    var AmountLabel = UILabel()
    let sep = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        cellView.frame = CGRect(x: 8, y: 5, width: SWIDTH-16, height: SHEIGHT/7-9)
        cellView.backgroundColor = UIColor.black
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 10
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/4
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        UpperDescriptionLAbel.setLabel(X: 10, Y: c_height/2, Width: c_width-SWIDTH/4, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Money Added", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 16))
        UpperDescriptionLAbel.font = UIFont(name: "Electrolize-Regular", size: 16)
        UpperDescriptionLAbel.adjustsFontSizeToFitWidth = true
        UpperDescriptionLAbel.numberOfLines = 0
        UpperDescriptionLAbel.lineBreakMode = .byTruncatingTail
        cellView.addSubview(UpperDescriptionLAbel)
        /******************--NJ--********************/
        MiddleLabel.setLabel(X: 10, Y: UpperDescriptionLAbel.frame.origin.y+UpperDescriptionLAbel.frame.height, Width: c_width-SWIDTH/3.5, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 14))
        MiddleLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        MiddleLabel.adjustsFontSizeToFitWidth = true
        MiddleLabel.numberOfLines = 0
        MiddleLabel.lineBreakMode = .byTruncatingTail
        cellView.addSubview(MiddleLabel)
        /******************--NJ--********************/
        DateTimeLabel.setLabel(X: 10, Y: MiddleLabel.frame.origin.y+MiddleLabel.frame.height, Width: c_width-SWIDTH/4, Height: c_height, TextColor: .white, BackColor: .clear, Text: "0000-00-00 00:00:00", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 14))
        DateTimeLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        DateTimeLabel.adjustsFontSizeToFitWidth = true
        DateTimeLabel.numberOfLines = 0
        DateTimeLabel.lineBreakMode = .byTruncatingTail
        cellView.addSubview(DateTimeLabel)
        /******************--NJ--********************/
        AmountLabel.setLabel(X: cellView.frame.width-SWIDTH/4, Y: cellView.frame.height/3, Width: SWIDTH/4.5, Height: cellView.frame.height/3, TextColor: .white, BackColor: APPSKYBLUECOLOR, Text: "00", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 16))
        AmountLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        AmountLabel.adjustsFontSizeToFitWidth = true
        AmountLabel.layer.cornerRadius = AmountLabel.frame.height/2
        cellView.addSubview(AmountLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        sep.setLabel(X: 10, Y: cellView.frame.height-1, Width: cellView.frame.width, Height: 1, TextColor: .clear, BackColor: .lightGray, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 10))
        sep.font = UIFont(name: "Electrolize-Regular", size: 10)
        cellView.addSubview(sep)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
