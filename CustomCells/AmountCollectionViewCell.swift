//
//  AmountCollectionViewCell.swift
//  Coupon Book
//
//  Created by apple  on 10/28/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class AmountCollectionViewCell: UICollectionViewCell {
    
    let cellview = UIView()
    let AmountLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        cellview.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        cellview.layer.cornerRadius = 10
        cellview.backgroundColor = .black
        cellview.layer.borderColor = UIColor.lightGray.cgColor
        cellview.layer.borderWidth = 1
        //cellview.layer.shadowOpacity = 0.2
        //cellview.layer.shadowRadius = 3
        contentView.addSubview(cellview)
        /******************--NJ--******************/
        AmountLabel.setLabel(X: cellview.frame.width/4, Y: cellview.frame.height/4, Width: cellview.frame.width/2, Height: cellview.frame.height/2, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 19))
        AmountLabel.font = UIFont(name: "Electrolize-Regular", size: 19)
        AmountLabel.adjustsFontSizeToFitWidth = true
        cellview.addSubview(AmountLabel)
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
