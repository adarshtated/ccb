//
//  DashBoardTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 10/24/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class DashBoardTableViewCell: UITableViewCell {
let CategaryLabelStatic = UILabel()
    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let CategaryLabel = UILabel()
    let DownloadImage = UIImageView()
    let DownLoadLabel = UILabel()
    let DownLoadButton = UIButton()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        //cellView.frame = CGRect(x: 10, y: 98, width: SWIDTH-20, height: SHEIGHT/3-130)
        cellView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT/12)
        cellView.layer.backgroundColor = UIColor.clear.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 0
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/3
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        CategaryLabelStatic.setLabel(X: c_height*1.2, Y: c_height/2, Width: c_width/2.5, Height: c_height, TextColor: .blue, BackColor: .clear, Text: "Category :", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        CategaryLabelStatic.font = UIFont(name: "Electrolize-Regular", size: 17)
        CategaryLabelStatic.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        CategaryLabelStatic.attributedText = NSAttributedString(string: "Category :", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        CategaryLabelStatic.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CategaryLabelStatic)
         /******************--NJ--********************/
        CategaryLabel.setLabel(X: c_height*1.2, Y: CategaryLabelStatic.frame.origin.y+CategaryLabelStatic.frame.height+2, Width: c_width/3, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Accessories", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        CategaryLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        DownloadImage.setImageView(X: c_width-c_width/2.6, Y: c_height+3, Width: c_height-6, Height: c_height-6, img: #imageLiteral(resourceName: "download-button"))
        DownloadImage.contentMode = UIViewContentMode.scaleAspectFit
        DownloadImage.clipsToBounds = true
        cellView.addSubview(DownloadImage)
        /******************--NJ--********************/
        DownLoadLabel.setLabel(X: DownloadImage.frame.origin.x+DownloadImage.frame.width+4, Y: c_height, Width: c_width/3, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Show All Coupons", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 12))
        DownLoadLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        DownLoadLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(DownLoadLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        DownLoadButton.setButton(X: DownloadImage.frame.origin.x, Y: DownLoadLabel.frame.origin.y, Width: DownLoadLabel.frame.width+DownloadImage.frame.width, Height: c_height, TextColor: .clear, BackColor: .clear, Title: "")
        cellView.addSubview(DownLoadButton)
        /******************--NJ--********************/
        let sep = UILabel()
        sep.setLabel(X: 0, Y: cellView.frame.height-2, Width: SWIDTH, Height: 1.3, TextColor: .clear, BackColor: .lightGray, Text: "", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 12))
        cellView.addSubview(sep)
        /******************--NJ--********************/
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
