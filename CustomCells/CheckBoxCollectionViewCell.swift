//
//  CheckBoxCollectionViewCell.swift
//  Coupon Book
//
//  Created by apple  on 1/1/19.
//  Copyright © 2019 Scientific web solution. All rights reserved.
//

import UIKit

class CheckBoxCollectionViewCell: UICollectionViewCell {
    
    let cellView = UIView()
    
    let CheckBoxImageView = UIImageView()
    let genderButton = UIButton()
    let genderLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(){
        
        cellView.frame = CGRect(x: 5, y: 5, width: (SWIDTH-30)/2-10, height: SHEIGHT/16-10)
        cellView.clipsToBounds = true
        cellView.backgroundColor = .clear
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        /******************--NJ--*********************/
        CheckBoxImageView.setImageView(X: 5, Y: c_height/3, Width: c_height/3, Height: c_height/3, img: UIImage(named:"unchecked_box.png")!)
        CheckBoxImageView.contentMode = UIViewContentMode.scaleAspectFit
        cellView.addSubview(CheckBoxImageView)
        /******************--NJ--*********************/
        genderLabel.setLabel(X: CheckBoxImageView.frame.origin.x+CheckBoxImageView.frame.width+c_height/6, Y: c_height/8, Width: c_width-CheckBoxImageView.frame.height-10-c_height/6, Height: c_height-(c_height/8)*2, TextColor: .white, BackColor: .clear, Text: "gender", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
        genderLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(genderLabel)
        /******************--NJ--*********************/
//        gayButton = UIButton()
//        gayButton.setButton(X: gayRadioImage.frame.origin.x, Y: gayLabel.frame.origin.y, Width: gayLabel.frame.width+gayRadioImage.frame.width+TextFieldHeight/6, Height: gayLabel.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        gayButton.addTarget(self, action: #selector(gayButtonAction), for: .touchUpInside)
//        scrollView.addSubview(gayButton)
        /******************--NJ--*********************/
    }
    
}




