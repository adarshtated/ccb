//
//  ReferralCouponCollectionViewCell.swift
//  Coupon Book
//
//  Created by apple  on 12/31/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import MarqueeLabel
import MGStarRatingView
class ReferralCouponCollectionViewCell: UICollectionViewCell {
    
    let cellView = UIView()
    let backView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = MarqueeLabel()
    let PostedByLabel = UILabel()
    var CategaryLabel = MarqueeLabel()
    let LowerTextLabel = UILabel()
    let CountLabel = UILabel()
    let RedView = UIView()
    let DaysLabel = MarqueeLabel()
    let currentStatusLabel = UILabel()
    let CategaryLabelStatic = UILabel()

    let ReferralByLabel = UILabel()
    let NameLabel = UILabel()
    let EmailLabel = UILabel()
    let RejectButton = UIButton()
    let AcceptButton = UIButton()
    
    var likeButton:UIButton!
    var bottomView:UIView!
    var sendButton:UIButton!
    let starRatingView = StarRatingView()
    
    let CouponStatus = UILabel()
    
    var ViolationImage = UIButton()
    var buttonViolation = UIButton()
    
    var acceptButton = UIButton()
    var rejectButton = UIButton()
    var infoButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(){
        
        backView.frame = CGRect(x: 5, y: 5, width: SWIDTH-10, height: SHEIGHT/2.5)
        backView.backgroundColor = UIColor.clear
        contentView.addSubview(backView)
        
        
        
        cellView.frame = CGRect(x: 0, y: 0, width: SWIDTH-10, height: SHEIGHT/2.8-10)
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 10
        cellView.clipsToBounds = true
        cellView.backgroundColor = UIColor.black
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        backView.addSubview(cellView)
        
        
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        ImageView.setImageView(X: (cellView.frame.width - 80)/2, Y: 0, Width: 80, Height: 80, img: UIImage(named: "Logo.png")!)
        ImageView.contentMode = UIViewContentMode.scaleAspectFit
        //ImageView.alpha = 0.5
        ImageView.clipsToBounds = true
        //cellView.addSubview(ImageView)
        /******************--NJ--********************/
        CategaryLabelStatic.setLabel(X: c_height/3, Y: c_height/6, Width: c_width/4-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Category:", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabelStatic.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabelStatic.adjustsFontSizeToFitWidth = true
       // cellView.addSubview(CategaryLabelStatic)
        /******************--NJ--********************/
        //        CategaryLabel.setLabel(X: , Y: , Width: , Height: , TextColor: .white, BackColor: .clear, Text: "Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel = MarqueeLabel.init(frame: CGRect(x: CategaryLabelStatic.frame.origin.x+CategaryLabelStatic.frame.size.width+2, y: c_height/6, width: c_width-SWIDTH/3.4-CategaryLabelStatic.frame.origin.x-CategaryLabelStatic.frame.width-4, height: c_height), duration: 8.0, fadeLength: 10.0)
        // CategaryLabel.frame =
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabel.backgroundColor = .clear
        CategaryLabel.numberOfLines = 0
        CategaryLabel.lineBreakMode = .byTruncatingTail
        CategaryLabel.adjustsFontSizeToFitWidth = true
        CategaryLabel.type = .continuous
        CategaryLabel.speed = .rate(80)
        CategaryLabel.fadeLength = 80.0
        CategaryLabel.labelWillBeginScroll()
       // cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        
        
        likeButton = UIButton()
        likeButton.setButton(X: 10, Y: 10, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        likeButton.setImage(UIImage(named: "like-5"), for: .normal)
        cellView.addSubview(likeButton)
        
        PostedByLabel.setLabel(X: 15, Y: c_height/8, Width: cellView.frame.width-30-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        /******************--NJ--********************/
        DaysLabel.setLabel(X: 15, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+15, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Hi", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        DaysLabel.font = UIFont(name: "VerminVibes2Nightclub", size: 40) //VerminVibes2Nightclub
        DaysLabel.attributedText = NSAttributedString(string: "Hi", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        DaysLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        DaysLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(DaysLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: 15, Y: DaysLabel.frame.origin.y+35, Width: c_width-30, Height: c_height*2.3, TextColor: .white, BackColor: .clear, Text: "DayLabel", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        HeadingLabel.font = UIFont(name: "PerpetuaTitlingMTLight2", size: 23)
        HeadingLabel.layer.borderColor = UIColor.white.cgColor
        HeadingLabel.layer.borderWidth = 0.5
        HeadingLabel.layer.cornerRadius = 10
        HeadingLabel.adjustsFontSizeToFitWidth = true
        HeadingLabel.lineBreakMode = .byWordWrapping
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        CountLabel.setLabel(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height-5, width: c_width, height: c_height+5)
        RedView.backgroundColor = APPLIGHTREDCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        currentStatusLabel.setLabel(X: RedView.frame.width/6, Y: 4, Width: (RedView.frame.width/6)*4, Height: RedView.frame.height-8, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        currentStatusLabel.font = UIFont(name: "Electrolize-Regular", size: 18)
        currentStatusLabel.adjustsFontSizeToFitWidth = true
        RedView.addSubview(currentStatusLabel)
        /******************--NJ--***********************/
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        /******************--NJ--********************/
//        LowerTextLabel.setLabel(X: c_height/3, Y: RedView.frame.origin.y-c_height-20, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
//        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 13)
//        LowerTextLabel.numberOfLines = 2
//        LowerTextLabel.lineBreakMode = .byTruncatingTail
//        LowerTextLabel.adjustsFontSizeToFitWidth = true
//        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        ReferralByLabel.setLabel(X: 10, Y: RedView.frame.height/4, Width: RedView.frame.width/4, Height: RedView.frame.height/2, TextColor: UIColor.white, BackColor: .clear, Text: "Referred by", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        ReferralByLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        RedView.addSubview(ReferralByLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        NameLabel.setLabel(X: ReferralByLabel.frame.width+ReferralByLabel.frame.origin.x, Y: (RedView.frame.height/3)/2-2, Width: RedView.frame.width/2, Height: RedView.frame.height/3, TextColor: UIColor.white, BackColor: .clear, Text: "Name", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 12))
        NameLabel.font = UIFont(name: "Electrolize-Regular", size: 13)
        RedView.addSubview(NameLabel)
        /******************--NJ--********************/
        EmailLabel.setLabel(X: NameLabel.frame.origin.x, Y: NameLabel.frame.height+NameLabel.frame.origin.y+4, Width: RedView.frame.width*2, Height: RedView.frame.height/3, TextColor: UIColor.white, BackColor: .clear, Text: "Email", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 12))
        EmailLabel.font = UIFont(name: "Electrolize-Regular", size: 13)
        RedView.addSubview(EmailLabel)
        /******************--NJ--********************/
//        LowerTextLabel.setLabel(X: c_height/3, Y: HeadingLabel.frame.origin.y+HeadingLabel.frame.height+2, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
//        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
//        LowerTextLabel.numberOfLines = 2
//        LowerTextLabel.lineBreakMode = .byTruncatingTail
//        LowerTextLabel.adjustsFontSizeToFitWidth = true
//        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        AcceptButton.setButton(X: 20, Y: RedView.frame.origin.y-c_height-20, Width: c_width/2-30, Height: c_height*3/4, TextColor: .white, BackColor: APPLIGHTGREENCOLOR, Title: "ACCEPT")
        AcceptButton.layer.cornerRadius = 5
        cellView.addSubview(AcceptButton)
        /******************--NJ--********************/
        /******************--NJ--********************/
        RejectButton.setButton(X: c_width/2+10, Y: RedView.frame.origin.y-c_height-20, Width: c_width/2-30, Height: c_height*3/4, TextColor: .white, BackColor: APPRedCOLOR, Title: "DECLINE")
        RejectButton.layer.cornerRadius = 5
        cellView.addSubview(RejectButton)
        /******************--NJ--********************/
        CouponStatus.setLabel(X: c_width/3, Y: AcceptButton.frame.origin.y, Width: c_width/3, Height: AcceptButton.frame.height, TextColor: .red, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 15))
        CouponStatus.adjustsFontSizeToFitWidth = true
        CouponStatus.isHidden = true
        cellView.addSubview(CouponStatus)
        /******************--NJ--********************/
        ViolationImage = UIButton()
        buttonViolation.setButton(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y, Width: c_height*3/4, Height: c_height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
        buttonViolation.setImage(UIImage(named:"flag"), for: .normal)
        cellView.addSubview(buttonViolation)
        /******************--NJ--********************/
//        buttonViolation = UIButton()
//        buttonViolation.setButton(X: cellView.frame.width-c_height-5, Y: PostedByLabel.frame.origin.y-5, Width: c_height+10, Height: c_height+10, TextColor: .clear, BackColor: .clear, Title: "")
//        cellView.addSubview(buttonViolation)
        /******************--NJ--********************/
        CountLabel.setLabel(X: buttonViolation.frame.origin.x-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        /******************--NJ--********************/
        
        /******************--KD--********************/
        bottomView = UIView()
        bottomView.frame = CGRect(x: 0, y: cellView.frame.height-50, width: cellView.frame.width, height: 50)
        bottomView.backgroundColor = .red
        bottomView.layer.cornerRadius = 6
        bottomView.layer.masksToBounds = true
        cellView.addSubview(bottomView)

        sendButton = UIButton()
        sendButton.setButton(X: 10, Y: 8, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        sendButton.setImage(UIImage(named: "sent-mail"), for: .normal)
       // sendButton.addTarget(self, action: #selector(SendGiftButtonAction), for: .touchUpInside)
        bottomView.addSubview(sendButton)
        
        
        

        starRatingView.frame = CGRect(x: bottomView.frame.width-160, y: 10, width: 30, height: bottomView.frame.height/2)
        let attribute = StarRatingAttribute(type: .rate, point: bottomView.frame.height/2, spacing: 4, emptyColor: UIColor(white: 0, alpha: 0.5), fillColor: .white, emptyImage: nil, fillImage: nil)
        starRatingView.configure(attribute, current: 5, max: 5)
        starRatingView.clipsToBounds = true
        bottomView.addSubview(starRatingView)
        
        acceptButton = UIButton()
        acceptButton.setButton(X: backView.frame.width/2-17+60, Y: cellView.frame.maxY+8, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        acceptButton.setImage(UIImage(named: "accept.png"), for: .normal)
        backView.addSubview(acceptButton)
        
        infoButton = UIButton()
        infoButton.setButton(X: backView.frame.width/2-17, Y: cellView.frame.maxY+8, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        infoButton.setImage(UIImage(named: "info.png"), for: .normal)
        backView.addSubview(infoButton)
        
        rejectButton = UIButton()
        rejectButton.setButton(X: backView.frame.width/2-77, Y: cellView.frame.height+8, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        rejectButton.setImage(UIImage(named: "decline.png"), for: .normal)
        backView.addSubview(rejectButton)
        
    }
    
}



