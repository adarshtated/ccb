//
//  couponCollectionViewCell.swift
//  Coupon Book
//
//  Created by apple  on 12/30/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import MarqueeLabel
import MGStarRatingView

class couponCollectionViewCell: UICollectionViewCell {
    
    let btnlikeImage = UIImageView()
    let FruitIcon = UIImageView()
    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = UILabel()
    let TitleLabel = MarqueeLabel()
    let PostedByLabel = UILabel()
    
    var CategaryLabel = MarqueeLabel()
    
    let DownloadImage = UIImageView()
    let RedeemLabel = UILabel()
    let RedeemButton = UIButton()
    let LowerTextLabel = UILabel()
    let CountLabel = UILabel()
    let RedView = UIView()
    let CategaryLabelStatic = UILabel()
    let DisclameLabel = UILabel()
    let CheckIcon = UIImageView()
    let starRatingView = StarRatingView()
    let btnlike = UIButton()
    
    var ViolationImage = UIButton()
    var buttonViolation = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(){
        
        cellView.frame = CGRect(x: 5, y: 5, width: SWIDTH-10, height: SHEIGHT/3-10)
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 10
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        ImageView.setImageView(X: 0, Y: 0, Width: cellView.frame.width, Height: cellView.frame.height-RedView.frame.height, img: #imageLiteral(resourceName: "CCB Logo for avatar.png"))
        ImageView.contentMode = UIViewContentMode.scaleAspectFill
        ImageView.clipsToBounds = true
        cellView.addSubview(ImageView)
        /******************--NJ--********************/
        CategaryLabelStatic.setLabel(X: c_height/3, Y: c_height/6, Width: c_width/4-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Category:", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabelStatic.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabelStatic.adjustsFontSizeToFitWidth = true
       // cellView.addSubview(CategaryLabelStatic)
        /******************--NJ--********************/
        //        CategaryLabel.setLabel(X: , Y: , Width: , Height: , TextColor: .white, BackColor: .clear, Text: "Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel = MarqueeLabel.init(frame: CGRect(x: CategaryLabelStatic.frame.origin.x+CategaryLabelStatic.frame.size.width+2, y: c_height/6, width: c_width-SWIDTH/3.4-CategaryLabelStatic.frame.origin.x-CategaryLabelStatic.frame.width-4, height: c_height), duration: 8.0, fadeLength: 10.0)
        // CategaryLabel.frame =
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabel.backgroundColor = .clear
        CategaryLabel.numberOfLines = 0
        CategaryLabel.lineBreakMode = .byTruncatingTail
        CategaryLabel.adjustsFontSizeToFitWidth = true
        CategaryLabel.type = .continuous
        CategaryLabel.speed = .rate(80)
        CategaryLabel.fadeLength = 80.0
        CategaryLabel.labelWillBeginScroll()
       // cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        PostedByLabel.setLabel(X: 15, Y: c_height/8, Width: cellView.frame.width-30-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: 15, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "DayLabel", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        HeadingLabel.font = UIFont(name: "Electrolize-Regular", size: 21)
        HeadingLabel.attributedText = NSAttributedString(string: "DayLabel", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        HeadingLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        HeadingLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        TitleLabel.setLabel(X: 15, Y: HeadingLabel.frame.origin.y+HeadingLabel.frame.height+2, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Raining outside !!", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        TitleLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        TitleLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(TitleLabel)
        /******************--NJ--********************/
        
        DisclameLabel.setLabel(X: c_width/2-(c_width/6)/2, Y: c_height*4+LowerTextLabel.frame.height, Width: c_width/6, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Disclaimer", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 8))
        DisclameLabel.font = UIFont(name: "Electrolize-Regular", size: 10)
        DisclameLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(DisclameLabel)
        /******************--NJ--********************/
        CheckIcon.setImageView(X: DisclameLabel.frame.origin.x-1-c_height*3/9, Y: c_height*4.3+LowerTextLabel.frame.height, Width: c_height*3/9, Height: c_height*3/9, img: #imageLiteral(resourceName: "White Checkbox"))
        CheckIcon.contentMode = UIViewContentMode.scaleAspectFit
        CheckIcon.clipsToBounds = true
        cellView.addSubview(CheckIcon)
        /******************--NJ--********************/
        FruitIcon.setImageView(X: DisclameLabel.frame.origin.x+DisclameLabel.frame.width+1, Y: c_height*4.3+LowerTextLabel.frame.height, Width: c_height*3/9, Height: c_height*3/9, img: #imageLiteral(resourceName: "apple"))
        FruitIcon.contentMode = UIViewContentMode.scaleAspectFit
        FruitIcon.clipsToBounds = true
        cellView.addSubview(FruitIcon)
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height-5, width: c_width, height: c_height+5)
        RedView.backgroundColor = APPLIGHTREDCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        DownloadImage.setImageView(X: HeadingLabel.frame.origin.x, Y: cellView.frame.height-c_height+(c_height*3/4)/4, Width: (c_height*3/4)/2, Height: (c_height*3/4)/2, img: #imageLiteral(resourceName: "download-button"))
        DownloadImage.contentMode = UIViewContentMode.scaleAspectFit
        DownloadImage.clipsToBounds = true
        cellView.addSubview(DownloadImage)
        /******************--NJ--********************/
        RedeemLabel.setLabel(X: DownloadImage.frame.origin.x+c_height*3/4, Y: cellView.frame.height-c_height, Width: c_width/3, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "REDEEM", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 16))
        RedeemLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        RedeemLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(RedeemLabel)
        /******************--NJ--********************/
        //       btnlikeImage.setImageView(X: RedeemLabel.frame.origin.x+RedeemLabel.frame.size.width+20, Y: cellView.frame.height-c_height+(c_height*3/4)/4, Width: (c_height*3/4)/2, Height: (c_height*3/4)/2, img: #imageLiteral(resourceName: "like-5"))
        //        btnlikeImage.contentMode = UIViewContentMode.scaleAspectFit
        //        btnlikeImage.clipsToBounds = true
        //        cellView.addSubview(btnlikeImage)
        /******************--NJ--********************/
        btnlike.setButton(X: RedView.frame.width/2-RedeemLabel.frame.height/2, Y: RedeemLabel.frame.origin.y, Width: RedeemLabel.frame.height, Height: RedeemLabel.frame.height, TextColor: .white, BackColor: .clear, Title: "")
        cellView.addSubview(btnlike)
        /******************--NJ--********************/
        RedeemButton.setButton(X: DownloadImage.frame.origin.x, Y: RedeemLabel.frame.origin.y, Width: RedeemLabel.frame.width+DownloadImage.frame.width, Height: c_height, TextColor: .clear, BackColor: .clear, Title: "")
        cellView.addSubview(RedeemButton)
        /******************--NJ--********************/
        /******************--NJ--********************/
        LowerTextLabel.setLabel(X: 15, Y: TitleLabel.frame.origin.y+TitleLabel.frame.height+5, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        LowerTextLabel.numberOfLines = 0
        LowerTextLabel.lineBreakMode = .byTruncatingTail
        LowerTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        starRatingView.frame = CGRect(x: RedView.frame.width-RedView.frame.width/2.5-4, y: RedView.frame.height/4, width: RedView.frame.width/2.5, height: RedView.frame.height/2)
        let attribute = StarRatingAttribute(type: .rate,
                                            point: RedView.frame.height/2,
                                            spacing: 4,
                                            emptyColor: UIColor(white: 0, alpha: 0.5),
                                            fillColor: .white,
                                            emptyImage: nil,
                                            fillImage: nil)
        starRatingView.configure(attribute, current: 0, max: 5)
        starRatingView.clipsToBounds = true
        RedView.addSubview(starRatingView)
        /******************--NJ--********************/
        ViolationImage = UIButton()
        buttonViolation.setButton(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y, Width: c_height*3/4, Height: c_height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
        buttonViolation.setImage(UIImage(named:"flag"), for: .normal)
        cellView.addSubview(buttonViolation)
        /******************--NJ--********************/
        buttonViolation = UIButton()
        buttonViolation.setButton(X: cellView.frame.width-c_height-5, Y: PostedByLabel.frame.origin.y-5, Width: c_height+10, Height: c_height+10, TextColor: .clear, BackColor: .clear, Title: "")
        cellView.addSubview(buttonViolation)
        /******************--NJ--********************/
        CountLabel.setLabel(X: buttonViolation.frame.origin.x-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        /******************--NJ--********************/
        
    }
    
}
