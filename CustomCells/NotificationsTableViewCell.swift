//
//  NotificationsTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 12/25/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    let cellView = UIView()
    let upperText = UILabel()
    let bellIcon = UIImageView()
    let lowerText = UILabel()
    let dateLabel = UILabel()
    let timeLabel = UILabel()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        //cellView.frame = CGRect(x: 10, y: 98, width: SWIDTH-20, height: SHEIGHT/3-130)
        cellView.frame = CGRect(x: 10, y: 8, width: SWIDTH-20, height: SHEIGHT/11-15)
        cellView.backgroundColor = UIColor.white
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 5
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/3
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        bellIcon.setImageView(X: 10, Y: c_height, Width: c_height, Height: c_height, img: UIImage(named: "notification.png")!)
        bellIcon.contentMode = UIViewContentMode.scaleAspectFit
        bellIcon.clipsToBounds = true
        cellView.addSubview(bellIcon)
        /******************--NJ--********************/
        upperText.setLabel(X: bellIcon.frame.origin.x+bellIcon.frame.width+c_height/5, Y: c_height/2, Width: c_width/1.5, Height: c_height, TextColor: .black, BackColor: .white, Text: "notification", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 15))
        upperText.font = UIFont(name: "Electrolize-Regular", size: 15)
        cellView.addSubview(upperText)
        /******************--NJ--********************/
        lowerText.setLabel(X: upperText.frame.origin.x, Y: upperText.frame.height+upperText.frame.origin.y, Width: c_width/1.5, Height: c_height, TextColor: .darkGray, BackColor: .white, Text: "notification", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        cellView.addSubview(lowerText)
        /******************--NJ--********************/
        /******************--NJ--********************/
        dateLabel.setLabel(X: upperText.frame.width+4+upperText.frame.origin.x, Y: c_height/2, Width: c_width-(upperText.frame.width+10+upperText.frame.origin.x), Height: c_height, TextColor: .black, BackColor: .white, Text: "date", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        dateLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        dateLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(dateLabel)
        /******************--NJ--********************/
        timeLabel.setLabel(X: dateLabel.frame.origin.x, Y: dateLabel.frame.height+dateLabel.frame.origin.y, Width: c_width-dateLabel.frame.width, Height: c_height, TextColor: .black, BackColor: .white, Text: "time", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        timeLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        timeLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(timeLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
