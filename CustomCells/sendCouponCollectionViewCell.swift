//
//  sendCouponCollectionViewCell.swift
//  Coupon Book
//
//  Created by apple  on 12/30/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import MarqueeLabel
import MGStarRatingView

class sendCouponCollectionViewCell: UICollectionViewCell {
    
    let btnlike = UIButton()
    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = MarqueeLabel()
    let PostedByLabel = UIButton()
    let LabelByPost = UILabel()
    var CategaryLabel = MarqueeLabel()
    let LowerTextLabel = UILabel()
    let CountLabel = UILabel()
    let RedView = UIView()
    let DaysLabel = UILabel()
    let starRatingView = StarRatingView()
    let CategaryLabelStatic = UILabel()
    let shareButton = UIButton()
    
    let DownloadImage = UIImageView()
    let RedeemLabel = UILabel()
    let RedeemButton = UIButton()
    
    let buttonViolation123 = UIButton()
    
    var ViolationImage = UIButton()
    var buttonViolation = UIButton()
    
    let lblUserName = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(){
        
        cellView.frame = CGRect(x: 5, y: 5, width: SWIDTH-10, height: SHEIGHT/3-10)
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 10
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        cellView.backgroundColor = UIColor.black
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        ImageView.setImageView(X: (cellView.frame.width - 80)/2, Y: 0, Width: 80, Height: 80, img: UIImage(named: "Logo.png")!)
        ImageView.contentMode = UIViewContentMode.scaleAspectFit
        ImageView.alpha = 0.5
        ImageView.clipsToBounds = true
        ImageView.backgroundColor = UIColor.clear
        cellView.addSubview(ImageView)
        /******************--NJ--********************/
        CategaryLabelStatic.setLabel(X: c_height/3, Y: c_height/6, Width: c_width/4-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Category:", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabelStatic.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabelStatic.adjustsFontSizeToFitWidth = true
       // cellView.addSubview(CategaryLabelStatic)
        
        //~~~~~~~~~~~~~~~~~COOL~~~~~~~~~~~~~~~~~~
        lblUserName.setLabel(X: 15, Y: 5, Width: c_width-30, Height: c_height, TextColor: .lightGray, BackColor: .clear, Text: "UserName", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 13))
        lblUserName.font = UIFont(name: "Electrolize-Regular", size: 13)
        lblUserName.adjustsFontSizeToFitWidth = true
       // cellView.addSubview(lblUserName)
        //~~~~~~~~~~~~~~~~~COOL~~~~~~~~~~~~~~~~~~
        
        /******************--NJ--********************/
        //        CategaryLabel.setLabel(X: , Y: , Width: , Height: , TextColor: .white, BackColor: .clear, Text: "Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel = MarqueeLabel.init(frame: CGRect(x: CategaryLabelStatic.frame.origin.x+CategaryLabelStatic.frame.size.width+2, y: c_height/6, width: c_width-SWIDTH/3.4-CategaryLabelStatic.frame.origin.x-CategaryLabelStatic.frame.width-4, height: c_height), duration: 8.0, fadeLength: 10.0)
        // CategaryLabel.frame =
        CategaryLabel.font = UIFont(name: "VerminVibes2Nightclub", size: 11)
        CategaryLabel.backgroundColor = .clear
        CategaryLabel.numberOfLines = 0
        CategaryLabel.lineBreakMode = .byTruncatingTail
        CategaryLabel.adjustsFontSizeToFitWidth = true
        CategaryLabel.type = .continuous
        CategaryLabel.speed = .rate(80)
        CategaryLabel.fadeLength = 80.0
        CategaryLabel.labelWillBeginScroll()
       // cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        PostedByLabel.setButton(X: 10, Y: 10, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        PostedByLabel.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
        
        //(X: 15, Y: c_height/8, Width: cellView.frame.width-30-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
//        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
//        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        
        LabelByPost.setLabel(X: 15, Y: c_height/8, Width: cellView.frame.width-30-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        cellView.addSubview(LabelByPost)
        /******************--NJ--********************/
        DaysLabel.setLabel(X: 15, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+15, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Hi", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        DaysLabel.font = UIFont(name: "VerminVibes2Nightclub", size: 40) //VerminVibes2Nightclub
        DaysLabel.attributedText = NSAttributedString(string: "Hi", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        DaysLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        DaysLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(DaysLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: 15, Y: DaysLabel.frame.origin.y+DaysLabel.frame.height-5, Width: c_width-30, Height: c_height, TextColor: .white, BackColor: .clear, Text: "DayLabel", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        HeadingLabel.font = UIFont(name: "PerpetuaTitlingMTLight2", size: 21)
        HeadingLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        CountLabel.setLabel(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height-5, width: c_width, height: c_height+5)
        RedView.backgroundColor = APPLIGHTREDCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        btnlike.setButton(X: 15, Y: RedView.frame.height/4, Width: RedView.frame.height/2, Height: RedView.frame.height/2, TextColor: .white, BackColor: .clear, Title: "")
        btnlike.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
        btnlike.isHidden = true
        RedView.addSubview(btnlike)
        /******************--NJ--********************/
        /******************--NJ--********************/
        DownloadImage.setImageView(X: HeadingLabel.frame.origin.x, Y: cellView.frame.height-c_height+(c_height*3/4)/4, Width: (c_height*3/4)/2, Height: (c_height*3/4)/2, img: #imageLiteral(resourceName: "download-button"))
        DownloadImage.contentMode = UIViewContentMode.scaleAspectFit
        DownloadImage.clipsToBounds = true
        DownloadImage.isHidden = true
        cellView.addSubview(DownloadImage)
        /******************--NJ--********************/
        RedeemLabel.setLabel(X: DownloadImage.frame.origin.x+c_height*3/4, Y: cellView.frame.height-c_height, Width: c_width/3, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "REDEEM", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 16))
        RedeemLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        RedeemLabel.adjustsFontSizeToFitWidth = true
        RedeemLabel.isHidden = true
        cellView.addSubview(RedeemLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedeemButton.setButton(X: DownloadImage.frame.origin.x, Y: RedeemLabel.frame.origin.y, Width: RedeemLabel.frame.width+DownloadImage.frame.width, Height: c_height, TextColor: .clear, BackColor: .clear, Title: "")
        RedeemButton.isHidden = true
        cellView.addSubview(RedeemButton)
        /******************--NJ--********************/
        /******************--NJ--***********************/
        //       btnlikeImage.setImageView(X: RedeemLabel.frame.origin.x+RedeemLabel.frame.size.width+20, Y: cellView.frame.height-c_height+(c_height*3/4)/4, Width: (c_height*3/4)/2, Height: (c_height*3/4)/2, img: #imageLiteral(resourceName: "like-5"))
        //        btnlikeImage.contentMode = UIViewContentMode.scaleAspectFit
        //        btnlikeImage.clipsToBounds = true
        //        cellView.addSubview(btnlikeImage)
        /******************--NJ--********************/
        /******************--NJ--********************/
        /******************--NJ--********************/
        LowerTextLabel.setLabel(X: 15, Y: DaysLabel.frame.maxY+10, Width: c_width-30, Height: cellView.frame.height/4.5, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
        LowerTextLabel.font = UIFont(name: "PerpetuaTitlingMTLight2", size: 18)
        LowerTextLabel.numberOfLines = 0
        LowerTextLabel.lineBreakMode = .byClipping
        LowerTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        shareButton.setButton(X: 10, Y: (RedView.frame.height - 35)/2, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        shareButton.setImage(UIImage(named: "sent-mail"), for: .normal)
        
        RedView.addSubview(shareButton)
        starRatingView.frame = CGRect(x: RedView.frame.width-RedView.frame.width/2.5-4, y: RedView.frame.height/4, width: RedView.frame.width/2.5, height: RedView.frame.height/2)
        let attribute = StarRatingAttribute(type: .rate,
                                            point: RedView.frame.height/2,
                                            
                                            spacing: 4,
                                            emptyColor: UIColor(white: 0, alpha: 0.5),
                                            fillColor: .white,
                                            emptyImage: nil,
                                            fillImage: nil)
        starRatingView.configure(attribute, current: 0, max: 5)
        starRatingView.clipsToBounds = true
        RedView.addSubview(starRatingView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        ViolationImage = UIButton()
//        buttonViolation.setButton(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y, Width: c_height*3/4, Height: c_height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
//        buttonViolation.setImage(UIImage(named:"flag"), for: .normal)
//
//        cellView.addSubview(buttonViolation)
        /******************--NJ--********************/
        buttonViolation = UIButton()
        buttonViolation.setButton(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y, Width: c_height*3/4, Height: c_height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
        buttonViolation.setImage(UIImage(named:"flag"), for: .normal)
        cellView.addSubview(buttonViolation)
        
        /******************--NJ--********************/
        CountLabel.setLabel(X: buttonViolation.frame.origin.x-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        /******************--NJ--********************/
    }
    
}

