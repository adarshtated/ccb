//
//  CouponsTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 10/24/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import MGStarRatingView

class CouponsTableViewCell: UITableViewCell {
    

    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = UILabel()
    let PostedByLabel = UILabel()
    let CategaryLabel = UILabel()
    let DownloadImage = UIImageView()
    let RedeemLabel = UILabel()
    let RedeemButton = UIButton()
    let LowerTextLabel = UILabel()
    let CountLabel = UILabel()
    let RedView = UIView()
    let starRatingView = StarRatingView()
let DayLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        /******************--NJ--********************/
        /******************--NJ--********************/
        cellView.frame = CGRect(x: 10, y: 8, width: SWIDTH-20, height: SHEIGHT/3-15)
        //cellView.backgroundColor = UIColor.white
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 10
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        /******************--NJ--********************/
//ImageView.setImageView(X: 0, Y: 0, Width: cellView.frame.width, Height: cellView.frame.height, img: #imageLiteral(resourceName: "CCB Logo for avatar"))
//        ImageView.contentMode = UIViewContentMode.scaleAspectFill
//        ImageView.clipsToBounds = true
//        cellView.addSubview(ImageView)
        /******************--NJ--********************/
     //   CategaryLabel.setLabel(X: HeadingLabel.frame.origin.x, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_width-c_height*2, Height: c_height*3/4, TextColor: .darkGray, BackColor: .clear, Text: "Categary : Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel.setLabel(X: c_height/3, Y: c_height/6, Width: c_width-c_height*2, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Categary : Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        PostedByLabel.setLabel(X: CategaryLabel.frame.origin.x+8, Y: c_height/6+4, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 11))
        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        /******************--NJ--********************/
        DayLabel.setLabel(X: c_height/3, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_width-c_height/2, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Hi", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        DayLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        DayLabel.attributedText = NSAttributedString(string: "Hi", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        DayLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        DayLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(DayLabel)
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: c_height/3, Y: DayLabel.frame.origin.y+DayLabel.frame.height+2, Width: c_width-c_height/2, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Raining outside !!", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 15))
        HeadingLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        HeadingLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        CountLabel.setLabel(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)       
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height-5, width: c_width, height: c_height+5)
        RedView.backgroundColor = APPLIGHTREDCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        DownloadImage.setImageView(X: HeadingLabel.frame.origin.x, Y: cellView.frame.height-c_height+(c_height*3/4)/4, Width: (c_height*3/4)/2, Height: (c_height*3/4)/2, img: #imageLiteral(resourceName: "download-button"))
        DownloadImage.contentMode = UIViewContentMode.scaleAspectFit
        DownloadImage.clipsToBounds = true
        cellView.addSubview(DownloadImage)
        /******************--NJ--********************/
        RedeemLabel.setLabel(X: DownloadImage.frame.origin.x+c_height*3/4, Y: cellView.frame.height-c_height, Width: c_width/3, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "REDEEM", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 16))
        RedeemLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        RedeemLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(RedeemLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedeemButton.setButton(X: DownloadImage.frame.origin.x, Y: RedeemLabel.frame.origin.y, Width: RedeemLabel.frame.width+DownloadImage.frame.width, Height: c_height, TextColor: .clear, BackColor: .clear, Title: "")
        cellView.addSubview(RedeemButton)
        /******************--NJ--********************/
        /******************--NJ--********************/
        LowerTextLabel.setLabel(X: c_height/3, Y: RedView.frame.origin.y-c_height-10, Width: c_width-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        LowerTextLabel.numberOfLines = 2
        LowerTextLabel.lineBreakMode = .byWordWrapping
        LowerTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        starRatingView.frame = CGRect(x: RedView.frame.width-RedView.frame.width/2.2, y: RedView.frame.height/4, width: RedView.frame.width/2.3, height: RedView.frame.height/2)
        let attribute = StarRatingAttribute(type: .rate,
                                            point: RedView.frame.height/2,
                                            spacing: 4,
                                            emptyColor: UIColor(white: 0, alpha: 0.5),
                                            fillColor: .white,
                                            emptyImage: nil,
                                            fillImage: nil)
        starRatingView.configure(attribute, current: 0, max: 5)
        starRatingView.clipsToBounds = true
        RedView.addSubview(starRatingView)
        /******************--NJ--********************/
    }
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        backgroundColor = .red
//    }
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        backgroundColor = .white
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
