//
//  ReferralCouponTableViewCell.swift
//  Coupon Book
//
//  Created by apple  on 11/14/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit

class ReferralCouponTableViewCell: UITableViewCell {

    let cellView = UIView()
    let ImageView = UIImageView()
    let BlurView = UIView()
    let HeadingLabel = UILabel()
    let PostedByLabel = UILabel()
    let CategaryLabel = UILabel()
    let DownloadImage = UIImageView()
    let RedeemLabel = UILabel()
    let RedeemButton = UIButton()
    let LowerTextLabel = UILabel()
    let CountLabel = UILabel()
    let RedView = UIView()
    let ReferralByLabel = UILabel()
    let NameLabel = UILabel()
    let EmailLabel = UILabel()
    let RejectButton = UIButton()
    let AcceptButton = UIButton()
  
    let CouponStatus = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        cellView.frame = CGRect(x: 10, y: 8, width: SWIDTH-20, height: SHEIGHT/2.9-15)
        cellView.backgroundColor = UIColor.white
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
        cellView.layer.cornerRadius = 7
        cellView.clipsToBounds = true
        cellView.layer.shadowColor = UIColor.white.cgColor
        cellView.layer.shadowRadius = 5
        cellView.layer.shadowOpacity = 0.3
        contentView.addSubview(cellView)
        /******************--NJ--********************/
        /******************--NJ--********************/
        let c_height = cellView.frame.height/6
        let c_width = cellView.frame.width
        /******************--NJ--********************/
        ImageView.setImageView(X: 0, Y: 0, Width: cellView.frame.width, Height: cellView.frame.height, img: UIImage(named: "logo1.png")!)
        ImageView.contentMode = UIViewContentMode.scaleAspectFill
        ImageView.alpha = 0.2
        ImageView.clipsToBounds = true
        cellView.addSubview(ImageView)
        /******************--NJ--********************/
        HeadingLabel.setLabel(X: c_height/3, Y: CategaryLabel.frame.origin.y+CategaryLabel.frame.height+25, Width: c_width-c_height/2, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Raining outside !!", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize:  20))
        HeadingLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        HeadingLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(HeadingLabel)
        /******************--NJ--********************/
        CountLabel.setLabel(X: cellView.frame.width-c_height, Y: PostedByLabel.frame.origin.y+PostedByLabel.frame.height+2, Width: c_height*3/4, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 11))
        CountLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CountLabel.isHidden = true
        CountLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CountLabel)
        /******************--NJ--********************/
        PostedByLabel.setLabel(X: CategaryLabel.frame.origin.x+17, Y: c_height/6+3, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "posted by : Admin", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 11))
        PostedByLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        PostedByLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(PostedByLabel)
        /******************--NJ--********************/
        CategaryLabel.setLabel(X: HeadingLabel.frame.origin.x, Y: c_height/6, Width: c_width-c_height, Height: c_height, TextColor: .white, BackColor: .clear, Text: "Categary : Accessories", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 11))
        CategaryLabel.font = UIFont(name: "Electrolize-Regular", size: 11)
        CategaryLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(CategaryLabel)
        /******************--NJ--********************/
        /******************--NJ--********************/
        RedView.frame = CGRect(x: 0, y: cellView.frame.height-c_height*1.2-5, width: c_width, height: c_height*1.2+5)
        RedView.backgroundColor = APPYELLOWCOLOR
        RedView.clipsToBounds = true
        cellView.addSubview(RedView)
        /******************--NJ--********************/
        ReferralByLabel.setLabel(X: 10, Y: RedView.frame.height/3, Width: RedView.frame.width/2-10-c_height, Height: RedView.frame.height/2, TextColor: UIColor.white, BackColor: .clear, Text: "Referred by", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        ReferralByLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        RedView.addSubview(ReferralByLabel)
        /******************--NJ--********************/
        NameLabel.setLabel(X: RedView.frame.width/2.8, Y: (RedView.frame.height/3)/2-2, Width: RedView.frame.width/2, Height: RedView.frame.height/3, TextColor: UIColor.white, BackColor: .clear, Text: "Name", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 12))
        NameLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
        RedView.addSubview(NameLabel)
        /******************--NJ--********************/
        EmailLabel.setLabel(X: RedView.frame.width/2.8, Y: NameLabel.frame.height+NameLabel.frame.origin.y+4, Width: RedView.frame.width*2, Height: RedView.frame.height/3, TextColor: UIColor.white, BackColor: .clear, Text: "Email", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 12))
        EmailLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
        RedView.addSubview(EmailLabel)
        /******************--NJ--********************/
        LowerTextLabel.setLabel(X: c_height/3, Y: HeadingLabel.frame.origin.y+HeadingLabel.frame.height+2, Width: c_width-c_height, Height: c_height*3/4, TextColor: .white, BackColor: .clear, Text: "Awesome wheater today. thanks..", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
        LowerTextLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
        LowerTextLabel.numberOfLines = 2
        LowerTextLabel.lineBreakMode = .byTruncatingTail
        LowerTextLabel.adjustsFontSizeToFitWidth = true
        cellView.addSubview(LowerTextLabel)
        /******************--NJ--********************/
        AcceptButton.setButton(X: 20, Y: RedView.frame.origin.y-c_height-20, Width: c_width/2-30, Height: c_height*3/4, TextColor: .white, BackColor: APPLIGHTGREENCOLOR, Title: "ACCEPT")
        AcceptButton.layer.cornerRadius = 5
        cellView.addSubview(AcceptButton)
        /******************--NJ--********************/
        /******************--NJ--********************/
        RejectButton.setButton(X: c_width/2+10, Y: RedView.frame.origin.y-c_height-20, Width: c_width/2-30, Height: c_height*3/4, TextColor: .white, BackColor: APPRedCOLOR, Title: "DECLINE")
        RejectButton.layer.cornerRadius = 5
        cellView.addSubview(RejectButton)
        /******************--NJ--********************/
        CouponStatus.setLabel(X: c_width/3, Y: AcceptButton.frame.origin.y, Width: c_width/3, Height: AcceptButton.frame.height, TextColor: .red, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 15))
        CouponStatus.adjustsFontSizeToFitWidth = true
        CouponStatus.isHidden = true
        cellView.addSubview(CouponStatus)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
