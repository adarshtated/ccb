//
//  ELUAClass.swift
//  Coupon Book
//
//  Created by Adarsh on 05/04/19.
//  Copyright © 2019 Scientific web solution. All rights reserved.
//

import UIKit

class ELUAClass: UIView {

    @IBOutlet weak var textViewElua: UITextView!
    @IBOutlet weak var eluaAction: UIButton!
    @IBAction func action_Eula(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    override func awakeFromNib() {
        textViewElua.setContentOffset(CGPoint.zero, animated: false)
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
