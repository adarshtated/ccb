//
//  ViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/21/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import RappleProgressHUD
import Alamofire
import CoreLocation
import MapKit

class ViewController: UIViewController,UITextFieldDelegate,LoginButtonDelegate,CLLocationManagerDelegate {
    var AcceptTextField: UITextField!
    var DeclineTextField: SkyFloatingLabelTextField!
    var CancelButton : UIButton!
    var settingButton = UIButton()
    var CancelImage : UIImageView!
    var CouponBookImage: UIImageView!
    var CouponBookImage1: UIImageView!
    var CouponBackGoundImage: UIImageView!
    var EmailTextField: SkyFloatingLabelTextFieldWithIcon!
    var PasswordTextfield: SkyFloatingLabelTextFieldWithIcon!
    var PasswordHideShowButton: UIButton!
    var EyeImageView: UIImageView!
    var LoginButton: UIButton!
    var FaceBookButton: UIButton!
    var FaceBookImage: UIImageView!
    var FaCeBookTitleLabel: UILabel!
    var ForGotPasswordButton: UIButton!
    var CreateAccountButton: UIButton!
    var BlackBackGround: UIView!
    var DisclaimerPopUp: UIView!
    var PopUpLabelHeight = CGFloat()
    //ravi
    var fristView:UIView!
    var fristAgeLabel:UILabel!
    var fristSwitch:UISwitch!
    var fristSwitchYes:UILabel!
    var fristSwitchNo:UILabel!
    var fristFaceBookButton: UIButton!
    var fristFaceBookImage: UIButton!
    var fristSkip: UIButton!
    var fristDownloadBtn: UIButton!
    
    
    //Variable for checking password text type
    var TextType = ""
    var EnterDetailsLabel: UILabel!
    var EnterDetailsLabel2: UILabel!
    var EnterDetailsLabel3: UILabel!
    var EnterDetailsLabel4: UILabel!
    var EnterDetailsLabel1: UILabel!
    let TextFieldHeight = SHEIGHT/14
    var AcceptButton:UIButton!
    var AcceptRadioImage: UIImageView!
    var CancelRadioImage:UIImageView!
    var accept = ""
    var acceptButton = UIButton()
    var termView:UITextView!
    let EndUserAgreementView = EndUserAgreementViewController()
    
    var locationManager:CLLocationManager!
    var state = ""
    var country = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        SetView()
        //  setPopUp()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
        
        let locale = Locale.current
        print("currencyCode~~~~~~~",locale.regionCode)
        
       
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)
                self.state = placemark.administrativeArea!
                self.country = placemark.country!
                let dispatchAfter = DispatchTimeInterval.seconds(Int(2.0))
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                    self.getPlayStoreUser()
                })
                
                
            }
        }
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        if error != nil {
            Utility.sharedInstance.showAlert("", msg: error?.localizedDescription ?? "", controller: self)
        }  else if result?.isCancelled ?? false {
            print("Cancelled")
        } else {
            print(result)
            
            fetchUserData()
        }
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
    
    @objc func FaceBookButtonAction(){
        if accept == "" {
            Utility.sharedInstance.showAlert("", msg: "Accept Terms Of Services", controller: self)
        }else{
            let fbLoginManager : LoginManager = LoginManager()
            fbLoginManager.logIn(permissions: ["public_profile","email","user_friends"], from: self) { (result, error) -> Void in
                if (error == nil){
                    
                    
                    let fbloginresult : LoginManagerLoginResult = result!
                    // if user cancel the login
                    if (result?.isCancelled)!{
                        return
                    }
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.fetchUserData()
                    }
                }
            }
        }
    }
    
    func fetchUserData() {
        
        RappleActivityIndicatorView.startAnimating()
        
        let graphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
        graphRequest.start(completionHandler: { (connection, result, error) in
            if error != nil {
                print("Error",error!.localizedDescription)
            }
            else{
                print(result!)
                let field = result! as? [String:Any]
                //self.userNameLabel.text = field!["name"] as? String
                
                var idd = ""
                var imgurll = ""
                
                if let imageURL = ((field!["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                    print(imageURL)
                    imgurll = imageURL
                    //                    let url = URL(string: imageURL)
                    //                    let data = NSData(contentsOf: url!)
                    //                    let image = UIImage(data: data! as Data)
                    //                    fbImage = image!
                } else {
                    imgurll = ""
                }
                
                if let iddd = field!["id"] as? NSNumber {
                    idd = iddd.stringValue
                } else {
                    idd = field!["id"] as! String
                }
                //print("fetchUserData~~~~~~~~~",field!["name"] as? String,field!["email"] as? String)
                
                self.signUp(name: (field!["name"] as? String)!, email: (field!["email"] as? String ?? ""), imgUrll: imgurll,f_iD:idd)
            }
        })
    }
    
    
    
    
    
    //Set UI for Screen
    
    func SetView(){
        
        
        
        
        /******************--NJ--*********************/
        CouponBookImage = UIImageView()
        CouponBookImage.setImageView(X: SWIDTH/2-TextFieldHeight*3, Y: STATUSBARHEIGHT*1.2, Width: TextFieldHeight*6, Height: 200, img: UIImage(named: "Logo")!)
//        CouponBookImage.alpha = 0.5
        self.view.addSubview(CouponBookImage)
        
        /******************--NJ--*********************/
        EmailTextField = SkyFloatingLabelTextFieldWithIcon()
        EmailTextField.setSkyLabel(X: 25, Y: CouponBookImage.frame.origin.y+CouponBookImage.frame.height+TextFieldHeight/4, Width: SWIDTH-50, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Email", PlaceHolder: "Email", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "email"),lineColorr:.white,SelectedlineColorr:.white, tagg: 0)
        //EmailTextField.text = "xyz@xyz.com"
        EmailTextField.keyboardType = .emailAddress
        EmailTextField.delegate = self
        self.view.addSubview(EmailTextField)
        /******************--NJ--*********************/ //EmailTextField.frame.origin.y+EmailTextField.frame.height+TextFieldHeight/2
        PasswordTextfield = SkyFloatingLabelTextFieldWithIcon()
        PasswordTextfield.setSkyLabel(X: 25, Y: EmailTextField.frame.maxY+10, Width: SWIDTH-50, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Password", PlaceHolder: "Password", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "key"),lineColorr:.white,SelectedlineColorr:.white, tagg: 1)
        PasswordTextfield.delegate = self
        //PasswordTextfield.text = "123456"
        PasswordTextfield.isSecureTextEntry = true
        self.view.addSubview(PasswordTextfield)
        /******************--NJ--*********************/
        UITextField.connectTextFields(fields: [EmailTextField,PasswordTextfield])
        /******************--NJ--*********************/
        PasswordHideShowButton = UIButton()
        PasswordHideShowButton.setButton(X: PasswordTextfield.frame.origin.x+PasswordTextfield.frame.width-PasswordTextfield.frame.height*3/4, Y: PasswordTextfield.frame.origin.y+PasswordTextfield.titleLabel.frame.height, Width: PasswordTextfield.frame.height*3/4, Height: PasswordTextfield.frame.height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
        PasswordHideShowButton.addTarget(self, action: #selector(PasswordHideShowButtonAction), for: .touchUpInside)
        self.view.addSubview(PasswordHideShowButton)
        /******************--NJ--*********************/
        EyeImageView = UIImageView()
        EyeImageView.setImageView(X: PasswordHideShowButton.frame.height/6, Y: PasswordHideShowButton.frame.height/8, Width: (PasswordHideShowButton.frame.height/6)*4, Height: (PasswordHideShowButton.frame.height/6)*4, img: #imageLiteral(resourceName: "Image - Global"))
        EyeImageView.contentMode = UIViewContentMode.scaleAspectFit
        PasswordHideShowButton.addSubview(EyeImageView)
        /******************--NJ--*********************/
        settingButton.frame = CGRect(x: SWIDTH/2 - 30, y:PasswordTextfield.frame.origin.y+PasswordTextfield.frame.height+TextFieldHeight*2/8, width: 22, height: 22)
        settingButton.setImage(UIImage(named: "unchecked.png"), for: .normal)
        settingButton.addTarget(self, action: #selector(settingView), for: .touchUpInside)
        self.view.addSubview(settingButton)
        let btnTermText = UIButton()
        btnTermText.frame = CGRect(x: SWIDTH/2 , y:PasswordTextfield.frame.origin.y+PasswordTextfield.frame.height+TextFieldHeight*2/8, width: SWIDTH/2, height: 22)
        btnTermText.titleLabel?.minimumScaleFactor = 0.5
        btnTermText.titleLabel?.adjustsFontSizeToFitWidth = true
        btnTermText.setTitle("Accept Terms of Services", for: .normal)
        btnTermText.addTarget(self, action: #selector(settingView), for: .touchUpInside)
        self.view.addSubview(btnTermText)
        
        /******************--NJ--*********************/
        LoginButton = UIButton()
        LoginButton.setButton(X: PasswordTextfield.frame.origin.x, Y: PasswordTextfield.frame.origin.y+PasswordTextfield.frame.height+TextFieldHeight*2/2, Width: PasswordTextfield.frame.width, Height: PasswordTextfield.frame.height, TextColor: .white, BackColor: APPPINKCOLOR, Title: "LOGIN")
        LoginButton.addTarget(self, action: #selector(LoginButtonAction), for: .touchUpInside)
        LoginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        LoginButton.layer.cornerRadius = LoginButton.frame.height/2
        self.view.addSubview(LoginButton)
        /******************--NJ--*********************/
        FaceBookButton = UIButton()
        FaceBookButton.setButton(X: LoginButton.frame.origin.x, Y: LoginButton.frame.origin.y+LoginButton.frame.height+TextFieldHeight*2/3, Width: LoginButton.frame.width, Height: LoginButton.frame.height, TextColor: .clear, BackColor: FACEBOOKBLUECOLOR, Title: "")
        FaceBookButton.layer.cornerRadius = FaceBookButton.frame.height/2
        FaceBookButton.addTarget(self, action: #selector(FaceBookButtonAction), for: .touchUpInside)
        self.view.addSubview(FaceBookButton)
        /******************--NJ--*********************/
        FaCeBookTitleLabel = UILabel()
        FaCeBookTitleLabel.setLabel(X: FaceBookButton.frame.width/4, Y: FaceBookButton.frame.height/4, Width: FaceBookButton.frame.width/2, Height: FaceBookButton.frame.height/2, TextColor: .white, BackColor: .clear, Text: "Continue with Facebook", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 18))
        FaCeBookTitleLabel.font = UIFont(name: "Electrolize-Regular", size: 18)
        FaCeBookTitleLabel.adjustsFontSizeToFitWidth = true
        FaceBookButton.addSubview(FaCeBookTitleLabel)
        /******************--NJ--*********************/
        FaceBookImage = UIImageView()
        FaceBookImage.setImageView(X: FaCeBookTitleLabel.frame.origin.x-10-FaceBookButton.frame.height/3, Y: FaceBookButton.frame.height/3, Width: FaceBookButton.frame.height/3, Height: FaceBookButton.frame.height/3, img: #imageLiteral(resourceName: "facebook"))
        FaceBookImage.contentMode = UIViewContentMode.scaleAspectFit
        FaceBookButton.addSubview(FaceBookImage)
        /******************--NJ--*********************/
        ForGotPasswordButton = UIButton()
        ForGotPasswordButton.setButton(X: SWIDTH/5, Y: FaceBookButton.frame.origin.y+FaceBookButton.frame.height+TextFieldHeight/4, Width: SWIDTH/1.5, Height: LoginButton.frame.height*3/4, TextColor: .white, BackColor: .clear, Title: "Forgot Your Password?")
        
        
        ForGotPasswordButton.addTarget(self, action: #selector(ForgotPasswordButtonAction), for: .touchUpInside)
        
        ForGotPasswordButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        ForGotPasswordButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.view.addSubview(ForGotPasswordButton)
        /******************--NJ--*********************/
        CreateAccountButton = UIButton()
        CreateAccountButton.setButton(X: ForGotPasswordButton.frame.origin.x, Y: ForGotPasswordButton.frame.origin.y+ForGotPasswordButton.frame.height+TextFieldHeight/6, Width: ForGotPasswordButton.frame.width, Height: ForGotPasswordButton.frame.height, TextColor: .white, BackColor: .clear, Title: "No Account? Create one")
        CreateAccountButton.addTarget(self, action: #selector(SignupButtonAction), for: .touchUpInside)
        CreateAccountButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        CreateAccountButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.view.addSubview(CreateAccountButton)
        
        /******************--NJ--*********************/
        termView = UITextView()
        termView.frame = CGRect(x: 0 , y:0, width: SWIDTH, height: SHEIGHT-50)
        termView.backgroundColor = .black
        termView.isEditable = false
        termView.font = .systemFont(ofSize: 20)
        termView.isHidden = true
        termView.textColor = .white
        let HtmlData = EndUserAgreementView.agreement
        let plainText = HtmlData.html2String
        termView.text  =  plainText
        self.view.addSubview(termView)
        /******************--NJ--*********************/
        acceptButton = UIButton()
        //acceptButton.frame = CGRect(x: SWIDTH/2 , y:SHEIGHT-50, width: SWIDTH, height: SHEIGHT-50)
        acceptButton.setButton(X: ForGotPasswordButton.frame.origin.x, Y: SHEIGHT-50, Width: ForGotPasswordButton.frame.width, Height: ForGotPasswordButton.frame.height, TextColor: .white, BackColor: .clear, Title: "Accept")
        acceptButton.addTarget(self, action: #selector(acceptConditions), for: .touchUpInside)
        acceptButton.backgroundColor = .blue
        acceptButton.isHidden = true
        self.view.addSubview(acceptButton)
        
        /******************--fristView  Frist Screen after Splace--*********************/
        fristView = UIView()
        fristView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        //fristView.backgroundColor = UIColor.black
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        //  imageView.image = background
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        fristView.addSubview(imageView)
        
        self.view.addSubview(fristView)
        /******************--NJ--*********************/
        CouponBookImage1 = UIImageView()
        CouponBookImage1.setImageView(X: 0, Y: SHEIGHT/6, Width: SWIDTH, Height: SHEIGHT/4, img: UIImage(named: "Logo.png")!)
        self.fristView.addSubview(CouponBookImage1)
        
        /******************--FristLabel Are You 18 or Older--*********************/
        fristAgeLabel = UILabel()
        fristAgeLabel.setLabel(X: SWIDTH/2-SWIDTH/4, Y: CouponBookImage1.frame.maxY+100, Width: SWIDTH/2, Height: 20, TextColor: .white, BackColor: .clear, Text: "Are You 18 or Older", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 22))
        fristAgeLabel.adjustsFontSizeToFitWidth = true
        //fristAgeLabel = buttomBorder(label: fristAgeLabel)
        self.fristView.addSubview(fristAgeLabel)
        /******************--FristSwitch Yes/No--*********************/
        fristSwitch = UISwitch(frame: CGRect(x:SWIDTH/2-20 , y: fristAgeLabel.frame.origin.y+25, width: 40, height: 30))
        fristSwitch.isOn = false
        fristSwitch.tintColor = .red
        fristSwitch.thumbTintColor = .red
        fristSwitch.addTarget(self, action: #selector(fristSwitchAction), for: .touchUpInside)
        self.fristView.addSubview(fristSwitch)
        /******************--FristSwitch Yes--*********************/
        fristSwitchYes = UILabel()
        fristSwitchYes.setLabel(X: SWIDTH/2-60, Y: fristSwitch.frame.origin.y+5, Width: 40, Height: 20, TextColor: .white, BackColor: .clear, Text: "YES", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 18))
        fristSwitchYes.adjustsFontSizeToFitWidth = true
        self.fristView.addSubview(fristSwitchYes)
        /******************--FristSwitch Yes--*********************/
        fristAgeLabel = UILabel()
        fristAgeLabel.setLabel(X: SWIDTH/2+30, Y: fristSwitch.frame.origin.y+5, Width: 40, Height: 20, TextColor: .white, BackColor: .clear, Text: "NO", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 18))
        fristAgeLabel.adjustsFontSizeToFitWidth = true
        self.fristView.addSubview(fristAgeLabel)
        
        /******************--facebook UIButton--*********************/
        fristFaceBookButton = UIButton()
        fristFaceBookButton.setButton(X: SWIDTH/2-SWIDTH/4-10, Y:fristSwitch.frame.origin.y+50 , Width: SWIDTH/2+20, Height: 20, TextColor: .white, BackColor: .clear, Title: "Sign In With Facebook")
        fristFaceBookButton.titleLabel?.adjustsFontSizeToFitWidth  = true
        fristFaceBookButton.addTarget(self, action: #selector(FaceBookButtonAction), for: .touchUpInside)
        
        self.fristView.addSubview(fristFaceBookButton)
        
        /******************--facebook image--*********************/
        fristFaceBookImage = UIButton()
        fristFaceBookImage.setButton(X: fristFaceBookButton.frame.maxX+5, Y:fristSwitch.frame.origin.y+50 , Width: 20, Height: 20, TextColor: .clear, BackColor: .clear, Title: "")
        fristFaceBookImage.setImage(#imageLiteral(resourceName: "facebook"), for: .normal)
        fristFaceBookImage.titleLabel?.adjustsFontSizeToFitWidth  = true
        fristFaceBookImage.addTarget(self, action: #selector(FaceBookButtonAction), for: .touchUpInside)
        self.fristView.addSubview(fristFaceBookImage)
        /******************--Skip button --*********************/
        fristSkip = UIButton()
        fristSkip.setButton(X: SWIDTH-80, Y:60 , Width: 50, Height: 20, TextColor: .red, BackColor: .clear, Title: "Skip")
        fristSkip.titleLabel?.adjustsFontSizeToFitWidth  = true
        fristSkip.addTarget(self, action: #selector(fristSkipButtonAction), for: .touchUpInside)
        self.fristView.addSubview(fristSkip)
        /******************--fristDownloadBtn button --*********************/
        fristDownloadBtn = UIButton()
        fristDownloadBtn.setButton(X: SWIDTH/2-95, Y:fristFaceBookImage.frame.origin.y+50 , Width: 190, Height: 60, TextColor: .red, BackColor: .clear, Title: "")
        fristDownloadBtn.setImage(#imageLiteral(resourceName: "Download"), for: .normal)
        fristDownloadBtn.titleLabel?.adjustsFontSizeToFitWidth  = true
        fristDownloadBtn.addTarget(self, action: #selector(fristSkipButtonAction), for: .touchUpInside)
        self.fristView.addSubview(fristDownloadBtn)
        
    }
    @objc func fristSwitchAction(){
        print("fristSwitchAction")
        let AlertController = UIAlertController(title: "", message: "Sorry!! You are not eligible to use this app.", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "OK", style: .destructive) { (UIAlertAction) in
            exit(0)
        }
        AlertController.addAction(OkAction)
        self.present(AlertController, animated: true, completion: nil)
    }
    @objc func fristSkipButtonAction(){
        print("fristSwitchAction")
        self.fristView.isHidden = true
    }
    //Login Button Action  settingView
    //    func setPopUp(){
    //
    //        /*********************--NJ--********************/
    //        BlackBackGround = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
    //        BlackBackGround.backgroundColor = UIColor(white: 0, alpha: 0.5)
    //        BlackBackGround.isHidden = true
    //        self.navigationController?.view.addSubview(BlackBackGround)
    //        /*********************--NJ--********************/
    //        DisclaimerPopUp = UIView(frame: CGRect(x: 20, y: SHEIGHT/6, width: SWIDTH-40, height: (SHEIGHT/6)*4))
    //       DisclaimerPopUp.layer.backgroundColor = UIColor.darkGray.cgColor
    //        DisclaimerPopUp.layer.cornerRadius = 8
    //        DisclaimerPopUp.layer.borderColor = UIColor.white.cgColor
    //        DisclaimerPopUp.layer.shadowColor = UIColor.white.cgColor
    //        BlackBackGround.addSubview(DisclaimerPopUp)
    //        /*********************--NJ--********************/
    //    }
    @objc func acceptConditions(){
        termView.isHidden = true
        acceptButton.isHidden = true
    }
    @objc func settingView(){
        print("abc")
        if settingButton.currentImage == UIImage(named: "unchecked.png"){
            settingButton.setImage(UIImage(named: "checked.png"), for: .normal)
            accept = "true"
            termView.isHidden = false
            acceptButton.isHidden = false
        } else {
            settingButton.setImage(UIImage(named: "unchecked.png"), for: .normal)
            accept = ""
        }
        //        let alert = UIAlertController(title: "Setting", message: "", preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "Terms of Service", style: UIAlertActionStyle.default, handler: { action in
        //            print("Terms of Service")
        //            let vc = TermAndConditionViewController()
        ////            vc.title = "Terms of Service"
        ////            let next = self.storyboard?.instantiateViewController(withIdentifier: "TermAndConditionViewController") as! TermAndConditionViewController
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }))
        //        alert.addAction(UIAlertAction(title: "Privacy Policy", style: UIAlertActionStyle.default, handler: { action in
        //            print("Privacy Policy")
        //            let vc = TermAndConditionViewController()
        ////            vc.title = "Privacy Policy"
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }))
        //        alert.addAction(UIAlertAction(title: "EULA", style: UIAlertActionStyle.default, handler: { action in
        //            let vc = TermAndConditionViewController()
        ////            vc.title = "Privacy Policy"
        //            self.navigationController?.pushViewController(vc, animated: true)
        //            print("EULA")
        //        }))
        //        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        //        self.present(alert, animated: true, completion: nil)
    }
    /*********************--NJ--********************/
    @objc func LoginButtonAction(sender: UIButton){
        if EmailTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter email", controller: self)
        } else if EmailTextField.text?.isValidEmail(testStr: EmailTextField.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email.", controller: self)
        } else if PasswordTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter password", controller: self)
            //        } else if (PasswordTextfield.text?.count)! < 6 {
            //            Utility.sharedInstance.showAlert("", msg: "Password must be at least 6 characters.", controller: self)
        } else if accept == ""{
            Utility.sharedInstance.showAlert("", msg: "Accept Terms Of Services", controller: self)
        }
        else {
            Login(emaill: EmailTextField.text!, passwordd: PasswordTextfield.text!)
        }
    }
    
    func Login(emaill:String,passwordd:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        print("EmailTextField.text!~~~~~~~",EmailTextField.text!,PasswordTextfield.text!)
        let url = URL(string: BASE_URL+"login")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "email=\(emaill)&password=\(passwordd)&device_id=\(USER_DEFAULTS.value(forKey: "device_token") ?? "")&device_type=Ios".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary~~~~~~~",dataDictionary)
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        if let verifyStatus = dataDictionary.object(forKey: "verified") as? String {
                            
                            if verifyStatus == "No" {
                                let AlertController = UIAlertController(title: "", message: "Your account is not verified, Please verify for Login.", preferredStyle: .alert)
                                let OkAction = UIAlertAction(title: "Verify", style: .default) { (UIAlertAction) in
                                    
                                    let vc = VerifyOTPViewController()
                                    vc.userEmail = emaill
                                    
                                    if let user_idd = dataDictionary.object(forKey: "user_id") as? String {
                                        vc.userId = user_idd
                                    }
                                    if let user_id = dataDictionary.object(forKey: "user_id") as? NSNumber {
                                        vc.userId = String(format: "%f",user_id.intValue)
                                    }
                                    
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }
                                AlertController.addAction(OkAction)
                                self.present(AlertController, animated: true, completion: nil)
                            } else {
                                //
                                USER_DEFAULTS.set(dataDictionary.object(forKey: "user_id") as? String, forKey: "user_id")
                                USER_DEFAULTS.set(dataDictionary.object(forKey: "name") as? String, forKey: "user_name")
                                USER_DEFAULTS.set(dataDictionary.object(forKey: "image") as? String, forKey: "user_image")
                                USER_DEFAULTS.set(dataDictionary.object(forKey: "mobile") as? String, forKey: "user_mobile")
                                USER_DEFAULTS.set(passwordd, forKey: "user_password")
                                USER_DEFAULTS.set(emaill, forKey: "user_email")
                                USER_DEFAULTS.set("yes", forKey: "isloggedin")
                                if dataDictionary.object(forKey: "privacy_status") as? String ?? "" == "Not Accept" {
                                    let vc = EndUserAgreementViewController()
                                    vc.privacyStatus = dataDictionary.object(forKey: "privacy_status") as? String ?? ""
                                    self.navigationController?.pushViewController(vc, animated: false)
                                } else {
                                    let vc = SettingViewController()
                                    //vc.privacyStatus = dataDictionary.object(forKey: "privacy_status") as? String ?? ""
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }
                            }
                        }
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var errorArr = [String : NSDictionary]()
                    let item1 = NSMutableDictionary()
                    item1.setObject("iOS", forKey: "From" as NSCopying)
                    item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                    item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                    item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                    errorArr["json"] = item1
                    var jsonData = NSData()
                    print(errorArr)
                    if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                        jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                        let jsondata = theJSONText
                        print("jsondata~~~~~~",jsondata)
                        callsaveExceptionLogApi(error: jsondata)
                    }
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func getPlayStoreUser(){
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        let deviceID = UserDefaults.standard.value(forKey: "deviceID") as? String ?? ""
        
        let url = URL(string: BASE_URL+"getPlayStoreUser")!
        print(USER_DEFAULTS.value(forKey: "device_token")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "country=\(self.country)&state=\(self.state)&device_id=\(deviceID)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                self.locationManager.stopUpdatingLocation()
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary~~~~getPlayStoreUser~~~~",dataDictionary)
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    
    @objc func ForgotPasswordButtonAction(){
        self.navigationController?.pushViewController(ForgotPasswordViewController(), animated: false)
    }
    @objc func SignupButtonAction(){
        self.navigationController?.pushViewController(SignUpViewController(), animated: false)
    }
    //Eye Button Action for showing or hiding password text
    @objc func PasswordHideShowButtonAction(){
        if TextType == "" {
            UIView.animate(withDuration: 0.3, animations: {
                self.PasswordTextfield.isSecureTextEntry = false
                self.EyeImageView.image = #imageLiteral(resourceName: "eye")
                self.TextType = "showpassword"
            })
        }
        else {
            UIView.animate(withDuration: 0.3, animations: {
                self.PasswordTextfield.isSecureTextEntry = true
                self.EyeImageView.image = #imageLiteral(resourceName: "Image - Global")
                self.TextType = ""
            })
        }
    }
    //Assign backGround
    
    func assignbackground(){
        // let background = #imageLiteral(resourceName: "bklogin")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        //  imageView.image = background
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    func signUp(name:String,email:String,imgUrll:String,f_iD:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"register")!
        
        print(USER_DEFAULTS.value(forKey: "device_token")!)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "name=\(name)&email=\(email)&mobile=&age=&password=&type=facebook&facebook_id=\(f_iD)&image=\(imgUrll)&device_id=\(USER_DEFAULTS.value(forKey: "device_token") ?? "")&device_type=Ios".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        let vc = HomeViewController()
                        
                        if let u_id = dataDictionary.object(forKey: "user_id") as? NSNumber {
                            USER_DEFAULTS.set(u_id, forKey: "user_id")
                        } else {
                            USER_DEFAULTS.set(dataDictionary.object(forKey: "user_id") as! String, forKey: "user_id")
                        }
                        
                        USER_DEFAULTS.set("yes", forKey: "isloggedin")
                        
                        USER_DEFAULTS.set(name, forKey: "user_name")
                        USER_DEFAULTS.set(imgUrll, forKey: "user_image")
                        USER_DEFAULTS.set(email, forKey: "user_email")
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                        print(userObject)
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func buttomBorder(label: UILabel) -> UILabel {
        let frame = label.frame
        let bottomLayer = CALayer()
        bottomLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width - 2, height: 1)
        bottomLayer.backgroundColor = UIColor.lightGray.cgColor
        label.layer.addSublayer(bottomLayer)
        //For Shadow
        label.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        label.layer.shadowOffset = CGSize(width: 0.0, height: 2.00)
        label.layer.shadowOpacity = 1.0
        label.layer.shadowRadius = 0.0
        label.layer.masksToBounds = false
        label.layer.cornerRadius = 4.0
        
        return label
        
    }
}
