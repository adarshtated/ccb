import Foundation
import StoreKit

import SwiftKeychainWrapper

enum IAPHandlerAlertType{
    case disabled
    case restored
    case purchased
    
    func message() -> String{
        switch self {
        case .disabled: return "Purchases are disabled in your device!"
        case .restored: return "You've successfully restored your purchase!"
        case .purchased: return "You've successfully bought this purchase!"
        }
    }
}


class IAPHandler: NSObject {
    static let shared = IAPHandler()
    
    fileprivate var productID = ""
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
    
    var purchaseStatusBlock: ((IAPHandlerAlertType) -> Void)?
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool { print("SKPaymentQueue.canMakePayments- ",SKPaymentQueue.canMakePayments())
        return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(index: Int){
        if iapProducts.count == 0 {
            print("no schemes available")
            let alertView = UIAlertController(title: "", message: "No Scheme Available", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            })
            alertView.addAction(action)
            //            selectQuizViewController.present(alertView, animated: true, completion: nil)
            return }
        if self.canMakePurchases() {
            print("iapProducts: \(iapProducts)")
            let product = iapProducts[index]
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
        } else {
            print("")
            let alertController = UIAlertController(title: "Alert", message: "Not Available", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            purchaseStatusBlock?(.disabled)
            return
        }
    }
    
    // MARK: - RESTORE PURCHASE
    func restorePurchase(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts(){
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: "RepurchaseCoupon")
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }
}

extension IAPHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver{// MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        print("response.products.count ----",response.products.count)
        if (response.products.count > 0) {
            iapProducts = response.products
            for product in iapProducts{
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
                let price1Str = numberFormatter.string(from: product.price)
                print(product.productIdentifier+product.localizedDescription + "\nfor just \(price1Str!)")
            }
        }
    }
    
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        //        print("paymentQueueRestoreCompletedTransactionsFinished")
//        selectQuizViewController.setArrays()
        purchaseStatusBlock?(.restored)
    }
    
    // MARK:- IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
//                print("product to purchase: \(trans.payment.productIdentifier)")
                switch trans.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    purchaseStatusBlock?(.purchased)
                    if trans.payment.productIdentifier == "SendAGift"{
                        
                        //use this func after iap successful.
                        //SendGift(name: RecipientNameTextField.text!, mobile: MobileTextField.text!, age: AgeTextField.text!, email: EmailTextField.text!)
                        
                        let saveSuccessful: Bool = KeychainWrapper.standard.set(0.99, forKey: "SendGift")
                        
                        objRepurchasedCouponsViewController.purchaseReredeemApi(payment_status: "Success")
                        //        Add a string value to keychain:
                        //
                        //        let saveSuccessful: Bool = KeychainWrapper.standard.set("Some String", forKey: "myKey")
                        //        Retrieve a string value from keychain:
                        //
                        //        let retrievedString: String? = KeychainWrapper.standard.string(forKey: "myKey")
                        //        Remove a string value from keychain:
                        //
                        //        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "myKey")
                    }
                    break
                case .failed:
                    print("failed")
                    let alertView = UIAlertController(title: "", message: "Transaction Failed", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    })
                    alertView.addAction(action)
//                    selectQuizViewController.present(alertView, animated: true, completion: nil)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .restored:
                    print("restored")
                    print(trans.payment.productIdentifier)
                    switch trans.payment.productIdentifier{
                    case "Full_key_package":
                        UserDefaults.standard.set(1, forKey: "quiz\(0)")
                        break
                    default:
                        break
                    }
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                default: break
                }}}
    }
    
}
