//
//  SratuouryWarning.swift
//  Coupon Book
//
//  Created by Adarsh on 09/04/19.
//  Copyright © 2019 Scientific web solution. All rights reserved.
//

import UIKit

class StatuouryWarning: UIView {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var showView: UIView!
   
    @IBAction func acceptAction(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    @IBAction func declineAction(_ sender: UIButton) {
        self.removeFromSuperview()
        
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
