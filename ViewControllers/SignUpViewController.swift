//
//  SignUpViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/22/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD

class SignUpViewController: UIViewController,UITextFieldDelegate {

    var scrollView: UIScrollView!
    var CouponBookImage: UIImageView!
    var SignUpLabel: UILabel!
    var AreYou18Label: UILabel!
    var YesLabel: UILabel!
    var NoLabel:UILabel!
    var AgeSwitch: UISwitch!
    var UserNameTextField: SkyFloatingLabelTextFieldWithIcon!
    var FullNameTextField: SkyFloatingLabelTextFieldWithIcon!
    var EmailTextField: SkyFloatingLabelTextFieldWithIcon!
    var MobileTextField: SkyFloatingLabelTextFieldWithIcon!
    var AgeTextField: SkyFloatingLabelTextFieldWithIcon!
    var PasswordTextfield: SkyFloatingLabelTextFieldWithIcon!
    var referCodeTextfield: SkyFloatingLabelTextFieldWithIcon!
    var PasswordHideShowButton: UIButton!
    var EyeImageView: UIImageView!
    var SignUpButton: UIButton!
    var SignInButton: UIButton!
    var CouponBackGoundImage: UIImageView!
    
    //Variable for checking password text type
    var TextType = ""
    
    let TextFieldHeight = SHEIGHT/14
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SIGNUP"
        assignbackground()
        SetView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func SetView(){
//        CouponBackGoundImage = UIImageView()
//        CouponBackGoundImage.setImageView(X: 0, Y: 0, Width: SWIDTH, Height: SHEIGHT, img: #imageLiteral(resourceName: "new_background.png"))
//        CouponBackGoundImage.contentMode = .scaleToFill
//        self.view.addSubview(CouponBackGoundImage)
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        scrollView.contentSize = CGSize(width: SWIDTH, height: SHEIGHT + 100)
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        /******************--NJ--*********************/
        CouponBookImage = UIImageView()
        CouponBookImage.setImageView(X: SWIDTH/2-TextFieldHeight*3, Y: STATUSBARHEIGHT*1.2, Width: TextFieldHeight*6, Height: 200, img: UIImage(named: "Logo")!)
        scrollView.addSubview(CouponBookImage)
         /******************--NJ--*********************/
        AreYou18Label = UILabel()
        AreYou18Label.setLabel(X: SWIDTH/4, Y: CouponBookImage.frame.origin.y+CouponBookImage.frame.height+10, Width: SWIDTH/2, Height: TextFieldHeight/2, TextColor: .white, BackColor: .clear, Text: "Are You 18 or Older", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 16))
        AreYou18Label.adjustsFontSizeToFitWidth = true
        scrollView.addSubview(AreYou18Label)
        /******************--NJ--*********************/
        AgeSwitch = UISwitch(frame: CGRect(x: SWIDTH/2-TextFieldHeight/2, y: AreYou18Label.frame.origin.y+AreYou18Label.frame.height+TextFieldHeight/6, width: TextFieldHeight, height: TextFieldHeight/4))
        AgeSwitch.isOn = false
        AgeSwitch.tintColor = .red
        AgeSwitch.thumbTintColor = .red
        AgeSwitch.addTarget(self, action: #selector(AgeSwitchAction), for: .touchUpInside)
        scrollView.addSubview(AgeSwitch)
        /******************--NJ--*********************/
        YesLabel = UILabel()
        YesLabel.setLabel(X: AgeSwitch.frame.origin.x-8-SWIDTH/8, Y: AreYou18Label.frame.origin.y+AreYou18Label.frame.height+TextFieldHeight/6, Width: SWIDTH/8, Height: TextFieldHeight/2, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
        YesLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        YesLabel.adjustsFontSizeToFitWidth = true
        scrollView.addSubview(YesLabel)
        /******************--NJ--*********************/
        NoLabel = UILabel()
        NoLabel.setLabel(X: AgeSwitch.frame.origin.x+AgeSwitch.frame.width+8, Y: AreYou18Label.frame.origin.y+AreYou18Label.frame.height+TextFieldHeight/6, Width: SWIDTH/8, Height: TextFieldHeight/2, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
        NoLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        NoLabel.adjustsFontSizeToFitWidth = true
        scrollView.addSubview(NoLabel)
        /******************--NJ--*********************/
//        UserNameTextField = SkyFloatingLabelTextFieldWithIcon()
//        UserNameTextField.setSkyLabel(X: 20, Y: CouponBookImage.frame.origin.y+CouponBookImage.frame.height+TextFieldHeight + 50
//            , Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Username", PlaceHolder: "Username", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "name"),lineColorr:.white,SelectedlineColorr:.white, tagg: 1)
//        UserNameTextField.delegate = self
//        scrollView.addSubview(UserNameTextField)
        /******************--NJ--*********************/
        FullNameTextField = SkyFloatingLabelTextFieldWithIcon()
        FullNameTextField.setSkyLabel(X: 20, Y: CouponBookImage.frame.origin.y+CouponBookImage.frame.height+TextFieldHeight + 50, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter Name", PlaceHolder: "Enter Name", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "name"),lineColorr:.white,SelectedlineColorr:.white, tagg: 1)
        FullNameTextField.delegate = self
        scrollView.addSubview(FullNameTextField)
        /******************--NJ--*********************/
        EmailTextField = SkyFloatingLabelTextFieldWithIcon()
        EmailTextField.setSkyLabel(X: 20, Y: FullNameTextField.frame.origin.y+FullNameTextField.frame.height+TextFieldHeight/4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter Email", PlaceHolder: "Enter Email", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "email"),lineColorr:.white,SelectedlineColorr:.white, tagg: 2)
        EmailTextField.delegate = self
        EmailTextField.keyboardType = .emailAddress
        scrollView.addSubview(EmailTextField)
        /******************--NJ--*********************/
        MobileTextField = SkyFloatingLabelTextFieldWithIcon()
        MobileTextField.setSkyLabel(X: 20, Y: EmailTextField.frame.origin.y+EmailTextField.frame.height+TextFieldHeight/4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter Mobile", PlaceHolder: "Enter Mobile", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "mobile"),lineColorr:.white,SelectedlineColorr:.white, tagg: 3)
        MobileTextField.delegate = self
        MobileTextField.keyboardType = .numberPad
        addDoneButtonOnKeyboard(textfieldd: MobileTextField)
        scrollView.addSubview(MobileTextField)
        /******************--NJ--*********************/
//        AgeTextField = SkyFloatingLabelTextFieldWithIcon()
//        AgeTextField.setSkyLabel(X: 20, Y: MobileTextField.frame.origin.y+MobileTextField.frame.height+TextFieldHeight/4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Age(Optional)", PlaceHolder: "Age(Optional)", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "age"),lineColorr:.white,SelectedlineColorr:.white, tagg: 4)
//        AgeTextField.delegate = self
//        AgeTextField.keyboardType = .numberPad
//        addDoneButtonOnKeyboard(textfieldd: AgeTextField)
//        scrollView.addSubview(AgeTextField)
        /******************--NJ--*********************/
        PasswordTextfield = SkyFloatingLabelTextFieldWithIcon()
        PasswordTextfield.setSkyLabel(X: 20, Y: MobileTextField.frame.origin.y+MobileTextField.frame.height+TextFieldHeight/4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter Password", PlaceHolder: "Enter Password", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1, img: #imageLiteral(resourceName: "key"),lineColorr:.white,SelectedlineColorr:.white, tagg: 5)
        PasswordTextfield.delegate = self
        PasswordTextfield.isSecureTextEntry = true
        scrollView.addSubview(PasswordTextfield)
        /******************--NJ--*********************/
        UITextField.connectTextFields(fields: [FullNameTextField,EmailTextField])
        UITextField.connectTextFields(fields: [PasswordTextfield])
        /******************--NJ--*********************/
        PasswordHideShowButton = UIButton()
        PasswordHideShowButton.setButton(X: PasswordTextfield.frame.origin.x+PasswordTextfield.frame.width-PasswordTextfield.frame.height*3/4, Y: PasswordTextfield.frame.origin.y+PasswordTextfield.titleLabel.frame.height, Width: PasswordTextfield.frame.height*3/4, Height: PasswordTextfield.frame.height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
        PasswordHideShowButton.addTarget(self, action: #selector(PasswordHideShowButtonAction), for: .touchUpInside)
        scrollView.addSubview(PasswordHideShowButton)
        /******************--NJ--*********************/
        EyeImageView = UIImageView()
        EyeImageView.setImageView(X: PasswordHideShowButton.frame.height/6, Y: PasswordHideShowButton.frame.height/8, Width: (PasswordHideShowButton.frame.height/6)*4, Height: (PasswordHideShowButton.frame.height/6)*4, img: #imageLiteral(resourceName: "Image - Global"))
        EyeImageView.contentMode = UIViewContentMode.scaleAspectFit
        PasswordHideShowButton.addSubview(EyeImageView)
        /******************--NJ--*********************/
        
        referCodeTextfield = SkyFloatingLabelTextFieldWithIcon()
        referCodeTextfield.setSkyLabel(X: 20, Y: PasswordTextfield.frame.origin.y+PasswordTextfield.frame.height+TextFieldHeight/4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter Referral Code", PlaceHolder: "Enter Referral Code", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1,img: #imageLiteral(resourceName: "coupon"),lineColorr:.white,SelectedlineColorr:.white, tagg: 9)
        referCodeTextfield.delegate = self
        //referCodeTextfield.isSecureTextEntry = true
        UITextField.connectTextFields(fields: [referCodeTextfield])
        scrollView.addSubview(referCodeTextfield)
        
        
        SignUpButton = UIButton()
        SignUpButton.setButton(X: referCodeTextfield.frame.origin.x, Y: referCodeTextfield.frame.origin.y+referCodeTextfield.frame.height+TextFieldHeight*2/3, Width: referCodeTextfield.frame.width, Height: referCodeTextfield.frame.height, TextColor: .white, BackColor: APPPINKCOLOR, Title: "Registration")
        SignUpButton.addTarget(self, action: #selector(SignupButtonAction), for: .touchUpInside)
        SignUpButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        SignUpButton.layer.cornerRadius = SignUpButton.frame.height/2
        scrollView.addSubview(SignUpButton)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        SignInButton = UIButton()
        SignInButton.setButton(X: PasswordTextfield.frame.origin.x, Y: SignUpButton.frame.origin.y+SignUpButton.frame.height+TextFieldHeight/6, Width: PasswordTextfield.frame.width, Height: TextFieldHeight*3/4, TextColor: .white, BackColor: .clear, Title: "Already have an Account? Login")
        SignInButton.addTarget(self, action: #selector(SigninButtonAction), for: .touchUpInside)
        SignInButton.titleLabel?.textAlignment = .center
        SignInButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        SignInButton.titleLabel?.adjustsFontSizeToFitWidth = true
        scrollView.addSubview(SignInButton)
        /******************--NJ--*********************/
    }
    
    //Login Button Action
    
    @objc func SignupButtonAction(){
        if FullNameTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter name", controller: self)
        } else if EmailTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter email", controller: self)
        } else if EmailTextField.text?.isValidEmail(testStr: EmailTextField.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email", controller: self)
        }
//        else if MobileTextField.text?.isEmpty == true {
//            Utility.sharedInstance.showAlert("", msg: "Please enter mobile number", controller: self)
//        } else if (MobileTextField.text?.count)! < 8 || (MobileTextField.text?.count)! > 12 {
//            Utility.sharedInstance.showAlert("", msg: "Please enter valid mobile number", controller: self)
//        } else if AgeTextField.text?.isEmpty == true {
//            Utility.sharedInstance.showAlert("", msg: "Please enter age", controller: self)
//        } else if Int(AgeTextField.text!)! < 18 {
//            Utility.sharedInstance.showAlert("", msg: "Please enter age above 18", controller: self)
//        }
        else if PasswordTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter password", controller: self)
        } else if (PasswordTextfield.text?.count)! < 6 {
            Utility.sharedInstance.showAlert("", msg: "Password must be at least 6 characters.", controller: self)
        } else {
            signUp(name: FullNameTextField.text!, email: EmailTextField.text!, mobileNo: MobileTextField.text!, age: "", password: PasswordTextfield.text!)
        }
    }
    
    // Sign Up Api
    
    func signUp(name:String,email:String,mobileNo:String,age:String,password:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        //params.put("name", s_name);
//        params.put("email", s_email);
//        params.put("mobile", s_number);
//        params.put("password", s_pass);
//        params.put("type", type_email);
//        params.put("device_id", device_id);
//        params.put("device_type", device_type);
//        params.put("age", s_age);
//        params.put("fullname", "");
//        params.put("facebook_id", "");
        
        let url = URL(string: BASE_URL+"register")!
        print(USER_DEFAULTS.value(forKey: "device_token")!)        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "name=\(name)&email=\(email)&mobile=\(mobileNo)&age=&password=\(password)&fullname=&type=email&facebook_id=&device_id=\(USER_DEFAULTS.value(forKey: "device_token") ?? "")&device_type=Ios&referred_code=\(referCodeTextfield.text ?? "")".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
           
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                 print("dataDictionary~~~~~~~~",dataDictionary)
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        let vc = VerifyOTPViewController()
                        vc.userEmail = email
                        let ottp = dataDictionary.object(forKey: "otp") as! NSNumber
                        vc.OTP = ottp.stringValue
                        if let user_idd = dataDictionary.object(forKey: "user_id") as? String {
                            vc.userId = user_idd
                        }
                        if let user_id = dataDictionary.object(forKey: "user_id") as? NSNumber {
                            vc.userId = user_id.stringValue
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                        print(userObject)
                    })//Pl9VE6A4mX$Pl9VE6A4mX$
                    

                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
            }
            
            }.resume()
    
//        let AlertController = UIAlertController(title: "", message: "Your account created successfully.", preferredStyle: .alert)
//        let OkAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
//            self.navigationController?.pushViewController(VerifyOTPViewController(), animated: true)
//        }
//        AlertController.addAction(OkAction)
//        self.present(AlertController, animated: true, completion: nil)
        
    }
    
    
    // getPlayStoreUser  https://adarshtated.com/dev/ccb/Coupon_api/getPlayStoreUser
    
    
    
    
    
    @objc func AgeSwitchAction(sender: UISwitch){
        if sender.isOn == true {
            let AlertController = UIAlertController(title: "", message: "Sorry!! You are not eligible to use this app.", preferredStyle: .alert)
            let OkAction = UIAlertAction(title: "OK", style: .destructive) { (UIAlertAction) in
                exit(0)
            }
            AlertController.addAction(OkAction)
            self.present(AlertController, animated: true, completion: nil)
        } else {
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            
        } else if textField.tag == 2 {
            
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*2), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*3.5), animated: true)
        }

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            
        } else if textField.tag == 2 {
            
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 4 {
        let maxLength = 2
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    @objc func SigninButtonAction(){
        self.navigationController?.popViewController(animated: false)
    }
    
    func assignbackground(){
     //   let background = #imageLiteral(resourceName: "bklogin")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
       // imageView.image = background
        imageView.image = UIImage(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    @objc func PasswordHideShowButtonAction(){
       // PasswordTextfield.resignFirstResponder()
        if TextType == "" {
            UIView.animate(withDuration: 0.3, animations: {
                
                self.PasswordTextfield.isSecureTextEntry = false
                self.EyeImageView.image = #imageLiteral(resourceName: "eye")
                self.TextType = "showpassword"
            })
        }
        else {
            UIView.animate(withDuration: 0.3, animations: {
                self.PasswordTextfield.isSecureTextEntry = true
                self.EyeImageView.image = #imageLiteral(resourceName: "Image - Global")
                self.TextType = ""
            })
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        MobileTextField.resignFirstResponder()
        //AgeTextField.resignFirstResponder()
    }

}
