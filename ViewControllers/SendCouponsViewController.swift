//
//  SendCouponsViewController.swift
//  Coupon Book
//
//  Created by apple  on 12/12/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SDWebImage
import MGStarRatingView
import MarqueeLabel
import UPCarouselFlowLayout


class SendCouponsViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource,StarRatingDelegate,UITextFieldDelegate {
    
    var couponCollectionView : UICollectionView!
    var NoCouponsLabel:UILabel!
    var SendCouponsTableview: UITableView!
    var imageArray = [UIImage]()
    
    var AllDataArray = [Any]()
    var WalletAmount = ""
    
    var navBarHeight = CGFloat()
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    var BlackBackGround: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sent Coupons"
        
        assignbackground()
        self.view.backgroundColor = .black
        
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
        SendCouponsTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
        let sharebtn1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))
        sharebtn1.tintColor=UIColor.white
        navigationItem.leftBarButtonItems = [sharebtn1]
       
        //SendCouponsTableview.delegate = self
       // SendCouponsTableview.dataSource = self
        SendCouponsTableview.backgroundColor = .clear
        SendCouponsTableview.bounces = false
        SendCouponsTableview.separatorStyle = .none
        SendCouponsTableview.isOpaque = false
        SendCouponsTableview.register(SendCouponsTableViewCell.self, forCellReuseIdentifier: "couponcell")
       // self.view.addSubview(SendCouponsTableview)
        
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(sendCouponCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
       // self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
      //  self.view.addSubview(FooterImage)
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        /***********************--NJ--*************************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
    }
    @objc func backAction(){
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
       // ReferDetails(otp:random(), message:shareText)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! sendCouponCollectionViewCell
        
        
        cell.CountLabel.isHidden = false
        cell.LabelByPost.isHidden = true
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        let title = dataDict["coupon_title"] as? String ?? ""
        cell.HeadingLabel.text = title.uppercased()
//        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        let des = dataDict["description"] as? String ?? ""
        cell.LowerTextLabel.text = des.uppercased()
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
//        cell.DaysLabel.text = (dataDict["default_title"] as?  String ?? "").uppercased()
        cell.DaysLabel.attributedText = NSAttributedString(string: (dataDict["default_title"] as?  String ?? "").uppercased(), attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        cell.starRatingView.isUserInteractionEnabled = false
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPREDCOLOR
        cell.backgroundColor = .clear
        
        cell.CategaryLabel.textColor = .white
        cell.CategaryLabel.type = .leftRight
        cell.CategaryLabel.speed = .rate(80)
        cell.CategaryLabel.fadeLength = 80.0
        cell.CategaryLabel.labelWillBeginScroll()
        cell.CategaryLabel.tag = indexPath.row
        cell.CategaryLabel.restartLabel()
        let like = dataDict["fav_status"] as?  String ?? ""
        if like == "Like"{
            cell.PostedByLabel.setImage(#imageLiteral(resourceName: "like-4"), for: .normal)
        }else{
            cell.PostedByLabel.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
        }
        
        cell.PostedByLabel.tag = indexPath.row
        cell.PostedByLabel.addTarget(self, action: #selector(newFavorateCopoun), for: .touchUpInside)
        cell.buttonViolation.tag = indexPath.row
        cell.buttonViolation.addTarget(self, action: #selector(buttonViolationAction), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? sendCouponCollectionViewCell else { return }
        cell.CategaryLabel.restartLabel()
    }
    @objc func newFavorateCopoun(sender: UIButton){
        var status = ""
        let dictData = AllDataArray[sender.tag] as! [String:Any]
        let Coupon_Id = dictData["coupon_id"] as! String
        if sender.currentImage == #imageLiteral(resourceName: "like-4") {
            status = "Unlike"
            UnFavoriteCoupon(couponId: Coupon_Id, status: status)
        }else{
            status = "Like"
            FavoriteCoupon(couponId: Coupon_Id,status:status)
        }  //PostedByLabel.setImage(#imageLiteral(resourceName: "like-4"), for: .normal)
        
        
    }
    func UnFavoriteCoupon(couponId:String,status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL + "delete_favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=\(status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.GetRedeemedCoupons()
                        Utility.sharedInstance.showDismissAlert("", msg: "\(msg)", controller: self)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func FavoriteCoupon(couponId:String,status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL + "favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=\(status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.GetRedeemedCoupons()
                        Utility.sharedInstance.showDismissAlert("", msg: "\(msg)", controller: self)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    @objc func buttonViolationAction(sender:UIButton){
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = self.AllDataArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            //  let Coupon_Title = dictData["coupon_title"] as! String
//            let RecEmail = Contact_Email
//            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
            self.GetRemoveCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String),Coupon_Id)
//
//            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
//            if let googleUrl = NSURL(string: googleUrlString) {
//                UIApplication.shared.openURL(googleUrl as URL)
//            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SHEIGHT/3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponcell", for: indexPath) as! SendCouponsTableViewCell
        
        cell.CountLabel.isHidden = false
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
//        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.DaysLabel.text = dataDict["default_title"] as?  String ?? ""
        cell.starRatingView.isUserInteractionEnabled = false
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        //cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPYELLOWCOLOR
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        
        let dataDict = AllDataArray[view.tag] as! [String:Any]
        
        
        submitRating(couponId: dataDict["coupon_id"] as! String, rating: value)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        WalletAmount = ""
        GetRedeemedCoupons()
        GetWalletAmount()
    }
    func GetRemoveCoupons(_ userId:String,_ couponId:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"delete_favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let parameters = "user_id= \(userId)&coupon_id=\(couponId)".data(using: .utf8)
        print("parameters :\(userId),\(couponId)")
        let postData = parameters
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {                
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary:\(dataDictionary)")
                let status = dataDictionary.object(forKey: "status") as! String
                print("status :\(status)")
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.GetRedeemedCoupons()
                         Utility.sharedInstance.showAlert("Alert", msg: "Post Blocked", controller: self)
//                        USER_DEFAULTS.set("0", forKey: "redeem_count")
//                        USER_DEFAULTS.set("0", forKey: "sent_coupon")
//                        RappleActivityIndicatorView.stopAnimation()
//                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
//                        print(self.AllDataArray)
//                        DispatchQueue.main.async(execute: {
//                            self.couponCollectionView.isHidden = false
//                            self.NoCouponsLabel.isHidden = true
//                        })
//                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
//                    DispatchQueue.main.async(execute: {
//                        self.couponCollectionView.isHidden = true
//                        self.NoCouponsLabel.isHidden = false
//                    })
                    RappleActivityIndicatorView.stopAnimation()
                    Utility.sharedInstance.showAlert("Alert", msg: dataDictionary.object(forKey: "message") as! String, controller: self)
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func GetRedeemedCoupons(){
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        let url = URL(string: BASE_URL+"get_re_redeem_coupons")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        USER_DEFAULTS.set("0", forKey: "redeem_count")
                        USER_DEFAULTS.set("0", forKey: "sent_coupon")
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print(self.AllDataArray)
                        DispatchQueue.main.async(execute: {
                            self.couponCollectionView.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.couponCollectionView.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func submitRating(couponId:String,rating:CGFloat){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_rating")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&rating=\(rating)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        Utility.sharedInstance.showDismissAlert("", msg: "Rating submitted.", controller: self)
                        //RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray.removeAll()
                        self.GetRedeemedCoupons()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func GetWalletAmount(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_wallet_amount")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.WalletAmount = dataDictionary.object(forKey: "amount") as! String
                    })
                }
                else {
                    // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }

}
