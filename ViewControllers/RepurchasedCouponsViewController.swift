//
//  RepurchasedCouponsViewController.swift
//  Coupon Book
//
//  Created by apple  on 12/12/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SDWebImage
import MGStarRatingView
import UPCarouselFlowLayout
import SwiftKeychainWrapper

var objRepurchasedCouponsViewController: RepurchasedCouponsViewController!

class RepurchasedCouponsViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource,StarRatingDelegate,UITextFieldDelegate{
    var PaymentCompletePopUp : UIView!
    var PaypalImage: UIImageView!
    var SepLabel: UILabel!
    var SuccessImage: UIImageView!
    var SuccessAmountLabel: UILabel!
    var SuccessMessageLabel: UILabel!
    var LowerDetailsView: UIView!
    var WalletTransactionIdNAmeLabel: UILabel!
    var WalletTransactionIdLabel : UILabel!
    var DateNameLabel : UILabel!
    var DateTimeLabel : UILabel!
    var PaymentApproveLabel : UILabel!
    var OkButton:UIButton!
    
    
    
    var couponCollectionView : UICollectionView!
    
    var NoCouponsLabel:UILabel!
    
//    var RepurchaseTableview: UITableView!
    var imageArray = [UIImage]()
    
    var AllDataArray = [Any]()
    var WalletAmount = ""
    
    var navBarHeight = CGFloat()
    
    var BlackBackGround: UIView!
    var ReferralPopUp: UIView!
    var GiftImageView: UIImageView!
    var CancelButton : UIButton!
    var CancelImage : UIImageView!
    var EnterDetailsLabel: UILabel!
    var EnterDetailsLabel1: UILabel!
    var EnterDetailsLabel2: UILabel!
    var EnterDetailsLabel3: UILabel!
    var EmailView: UIView!
    var EmailTextfield:UITextField!
    var MobileView: UIView!
    var MobileTextfield:UITextField!
    var SubmitButton:UIButton!
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    var PopUpLabelHeight = CGFloat()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Repurchase Coupons"
        GetRedeemedCoupons()
objRepurchasedCouponsViewController = self
        assignbackground()
        setUpNavigation()
        
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
//        RepurchaseTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
//        // RepurchaseTableview.delegate = self
//        // RepurchaseTableview.dataSource = self
//        RepurchaseTableview.backgroundColor = .clear
//        RepurchaseTableview.bounces = false
//        RepurchaseTableview.separatorStyle = .none
//        RepurchaseTableview.isOpaque = false
//        RepurchaseTableview.register(RepurchasedTableViewCell.self, forCellReuseIdentifier: "repurchasecell")
//        self.view.addSubview(RepurchaseTableview)
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
        // self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
        //  self.view.addSubview(FooterImage)
        
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(RepurchasedCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
        /***********************--NJ--*************************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        /******************--NJ--*********************/
        setPopUp()
        setUpPaymentCompletePopUp()
    }
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    func setUpNavigation(){
        
        self.view.backgroundColor = .black
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))
        backButton.tintColor=UIColor.white
        navigationItem.leftBarButtonItems = [backButton]
        
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    
    func setUpPaymentCompletePopUp(){
        PaymentCompletePopUp = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        PaymentCompletePopUp.backgroundColor = .black
        self.navigationController?.view.addSubview(PaymentCompletePopUp)
        /*****************--NJ--*****************/
        PaypalImage = UIImageView()
        PaypalImage.setImageView(X: SWIDTH/3, Y: 10, Width: SWIDTH/3, Height: SHEIGHT/14, img: #imageLiteral(resourceName: "paypal"))
        PaypalImage.contentMode = UIViewContentMode.scaleAspectFit
        PaypalImage.backgroundColor = .clear
        PaymentCompletePopUp.addSubview(PaypalImage)
        /*****************--NJ--*****************/
        SepLabel = UILabel()
        SepLabel.setLabel(X: 0, Y: PaypalImage.frame.origin.y+PaypalImage.frame.height+5, Width: SWIDTH, Height: 1.5, TextColor: .clear, BackColor: .lightGray, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 12))
        SepLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
        PaymentCompletePopUp.addSubview(SepLabel)
        /*****************--NJ--*****************/
        SuccessImage = UIImageView()
        SuccessImage.setImageView(X: SWIDTH/2-(SHEIGHT/10)/2, Y: PaypalImage.frame.origin.y+PaypalImage.frame.height+(SHEIGHT/10)*3/4, Width: SHEIGHT/10, Height: SHEIGHT/10, img: #imageLiteral(resourceName: "success"))
        SuccessImage.contentMode = UIViewContentMode.scaleAspectFit
        SuccessImage.backgroundColor = .clear
        PaymentCompletePopUp.addSubview(SuccessImage)
        /*****************--NJ--*****************/
        SuccessAmountLabel = UILabel()
        SuccessAmountLabel.setLabel(X: SWIDTH/3, Y: SuccessImage.frame.origin.y+SuccessImage.frame.height*1.2, Width: SWIDTH/3, Height: SHEIGHT/14, TextColor: .white, BackColor: .clear, Text: "$0", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 26))
        SuccessAmountLabel.font = UIFont(name: "Electrolize-Regular", size: 26)
        SuccessAmountLabel.adjustsFontSizeToFitWidth = true
        PaymentCompletePopUp.addSubview(SuccessAmountLabel)
        /*****************--NJ--*****************/
        SuccessMessageLabel = UILabel()
        SuccessMessageLabel.setLabel(X: SWIDTH/3, Y: SuccessAmountLabel.frame.origin.y+SuccessAmountLabel.frame.height, Width: SWIDTH/3, Height: SHEIGHT/14, TextColor: .white, BackColor: .clear, Text: "Payment Successful", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 24))
        SuccessMessageLabel.font = UIFont(name: "Electrolize-Regular", size: 24)
        SuccessMessageLabel.adjustsFontSizeToFitWidth = true
        PaymentCompletePopUp.addSubview(SuccessMessageLabel)
        /*****************--NJ--*****************/
        LowerDetailsView = UIView(frame: CGRect(x: 15, y: SuccessMessageLabel.frame.origin.y+SuccessMessageLabel.frame.height*1.4, width: SWIDTH-30, height: SHEIGHT/3.2))
        LowerDetailsView.layer.cornerRadius = 10
        LowerDetailsView.layer.borderColor = UIColor.lightGray.cgColor
        LowerDetailsView.layer.borderWidth = 1.5
        LowerDetailsView.backgroundColor = .clear
        LowerDetailsView.clipsToBounds = true
        PaymentCompletePopUp.addSubview(LowerDetailsView)
        OkButton = UIButton()
        OkButton.setButton(X: SWIDTH/3, Y: LowerDetailsView.frame.origin.y+LowerDetailsView.frame.height+SHEIGHT/15, Width: SWIDTH/3, Height: SHEIGHT/13, TextColor: .white, BackColor: APPSKYBLUECOLOR, Title: "OK")
        OkButton.layer.cornerRadius = OkButton.frame.height/2
        OkButton.addTarget(self, action: #selector(OkButtonAction), for: .touchUpInside)
        PaymentCompletePopUp.addSubview(OkButton)
        /*****************--NJ--*****************/
        self.PaymentCompletePopUp.alpha = 0

    }
    
    @objc func OkButtonAction(){
        self.PaymentCompletePopUp.alpha = 0
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        print("share action")
        for i in 0..<AllDataArray.count {
            let dataDict = AllDataArray[i] as! [String:Any]
            let payment_status = dataDict["payment_status"] as? String ?? ""
            if(payment_status == "Success"){
                let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
                let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
                activityvc.popoverPresentationController?.sourceView = self.view
                self.present(activityvc, animated: true, completion: nil)
            }else{
                let ac = UIAlertController(title: "Alert", message: "Please! Firstly buy this coupon then you can share with your loved ones.", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(ac, animated: false, completion: nil)
            }
        }
        

        
        

//        ReferDetails(otp:random(), message:shareText)
//        Toast.show(message: "Please! Firstly buy this coupon then you can share with your loved ones.", controller: self, left: UIScreen.main.bounds.width, colorSelect: .white)
        
    }
    
    @objc func backAction(){
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RepurchasedCollectionViewCell
        
        print("alldata:",AllDataArray[indexPath.row])
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = (dataDict["coupon_title"] as? String ?? "").uppercased()
        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        cell.LowerTextLabel.text = (dataDict["description"] as? String ?? "").uppercased()
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.starRatingView.isUserInteractionEnabled = true
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.ImageView.clipsToBounds = true
//        cell.RedView.backgroundColor = APPYELLOWCOLOR
        cell.backgroundColor = .clear
        cell.DaysLabel.text = (dataDict["default_title"] as? String ?? "").uppercased()
        
        cell.CategaryLabel.textColor = .white
        cell.CategaryLabel.type = .leftRight
        cell.CategaryLabel.speed = .rate(80)
        cell.CategaryLabel.fadeLength = 80.0
        cell.CategaryLabel.labelWillBeginScroll()
        cell.CategaryLabel.tag = indexPath.row
        cell.CategaryLabel.restartLabel()

        if let likeStatus = dataDict["fav_status"] as? String {
            if likeStatus == "Like"{
                cell.btnHeart.setImage(UIImage(named: "like-5"), for: .normal)
            }else{
                cell.btnHeart.setImage(UIImage(named: "like"), for: .normal)
            }
            cell.btnHeart.addTarget(self, action: #selector(like_unlike), for: .touchUpInside)
        }

        cell.RedeemLabel.isHidden = false
        cell.DownloadImage.isHidden = false
        cell.RedeemButton.isHidden = false
        cell.RedeemLabel.text = "$"
        
        cell.DownloadImage.tag = indexPath.row
        cell.DownloadImage.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        
        cell.RedeemButton.tag = indexPath.row
        cell.RedeemButton.addTarget(self, action: #selector(ReRedeemButtonAction), for: .touchUpInside)
        
        cell.buttonViolation.tag = indexPath.row
        cell.buttonViolation.addTarget(self, action: #selector(buttonViolationAction), for: .touchUpInside)
        
        return cell
    }
    
    @objc func like_unlike(_ sender: UIButton){
        if  Reachability.isConnectedToNetwork() == false{
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let dictData = self.AllDataArray[sender.tag] as! [String:Any]
        let Coupon_Id = dictData["coupon_id"] as! String
        let status = dictData["fav_status"] as! String
        var statusToSend = ""
        var strUrl = ""
        if status == "Like"{
            statusToSend = "Unlike"
            strUrl = "delete_favorite_coupon"
        }else{
            statusToSend = "Like"
            strUrl = "favorite_coupon"
        }
        
        let url = URL(string: BASE_URL+strUrl)!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let parameters = "user_id=\((USER_DEFAULTS.value(forKey: "user_id")as! String))&coupon_id=\(Coupon_Id)&status=\(statusToSend)".data(using: .utf8)
        print("parameters: \(Coupon_Id) \(statusToSend)")
        let postData = parameters
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary:\(dataDictionary)")
                let status = dataDictionary.object(forKey: "status") as! String
                print("status :\(status)")
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
//                        Utility.sharedInstance.showAlert("Alert", msg: "Post Liked", controller: self)
                        self.GetRedeemedCoupons()
                    })
                }else {
                    DispatchQueue.main.async(execute: {
                        Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                        RappleActivityIndicatorView.stopAnimation()
                    })
                }
            }else {
                DispatchQueue.main.async(execute: {
                    RappleActivityIndicatorView.stopAnimation()
                    Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                })
            }
            }.resume()
    }
    
    @objc func buttonViolationAction(sender:UIButton){
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = self.AllDataArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            self.GetRemoveCoupons((USER_DEFAULTS.value(forKey: "user_id")as! String), Coupon_Id)
//            //  let Coupon_Title = dictData["coupon_title"] as! String
//            let RecEmail = Contact_Email
//            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
//            if let googleUrl = NSURL(string: googleUrlString) {
//                UIApplication.shared.openURL(googleUrl as URL)
//            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    func GetRemoveCoupons(_ userId:String,_ couponId:String){
        
        if  Reachability.isConnectedToNetwork() == false{
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"delete_favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let parameters = "user_id= \(userId)&coupon_id=\(couponId)".data(using: .utf8)
        print("parameters :\(userId),\(couponId)")
        let postData = parameters
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary:\(dataDictionary)")
                let status = dataDictionary.object(forKey: "status") as! String
                print("status :\(status)")
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        //                        self.GetRedeemedCoupons()
                        
                        Utility.sharedInstance.showAlert("Alert", msg: "Post Blocked", controller: self)
                        
                    })
                }else {
                    Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                RappleActivityIndicatorView.stopAnimation()
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
            }
            }.resume()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? RepurchasedCollectionViewCell else { return }
        cell.CategaryLabel.restartLabel()
    }
    
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        
        let dataDict = AllDataArray[view.tag] as! [String:Any]
        
        
        submitRating(couponId: dataDict["coupon_id"] as! String, rating: value)
        
    }
    
    @objc func ReRedeemButtonAction(sender: UIButton) {
        
        
        purchase_product_popup()
//        UIView.animate(withDuration: 0.3, animations: {
//            self.BlackBackGround.isHidden = false
//        })
//        SubmitButton.tag = sender.tag
//        print(sender.tag)
    }
    
    func purchase(_:UIAlertAction){
        IAPHandler.shared.purchaseMyProduct(index: 0)
    }
    
    
    @objc func purchase_product_popup(){
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                print("purchased")
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
        let alertController = UIAlertController(title: "Make Purchase", message: "Ready To Re-Buy This Coupon Again?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: self.purchase)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(OKAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    func ShowAddmoneyPopup(){
        
        let alertController = UIAlertController(title: "", message: "Please add money to your account to ReRedeem Coupon.", preferredStyle: .alert)
        let AddAction = UIAlertAction(title: "Add Money", style: .default) { (UIAlertAction) in
            
            let vc = MyWalletViewController()
            vc.PrevViewStatus = "reredeem"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(AddAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        WalletAmount = ""
        GetWalletAmount()
    }
    func GetRedeemedCoupons(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_re_redeem_coupons")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    self.AllDataArray.removeAll()
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        let dataArray = dataDictionary.object(forKey: "data") as! [Any]
                        
                        DispatchQueue.main.async(execute: {
                            self.couponCollectionView.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
                        
                        
                        if dataArray.count > 0 {
                            for i in 0..<dataArray.count {
                                let dataDict = dataArray[i] as! [String:Any]
                                if let countt = dataDict["re_redeem_counts"] as? NSNumber {
                                    if countt.intValue > 0 {
                                        self.AllDataArray.append(dataDict)
                                    }
                                } else {
                                    let countt = dataDict["re_redeem_counts"] as? String
                                    if countt != "0" {
                                        self.AllDataArray.append(dataDict)
                                    }
                                }
                            }
                        }
                        
                        
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.couponCollectionView.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func submitRating(couponId:String,rating:CGFloat){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_rating")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&rating=\(rating)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        Utility.sharedInstance.showDismissAlert("", msg: "Rating submitted.", controller: self)
                        //RappleActivityIndicatorView.stopAnimation()
                       // self.AllDataArray.removeAll()
                        self.GetRedeemedCoupons()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func RedeemCoupon(couponId:String,payment_status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"re_redeem_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&payment_status=\(payment_status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                       // self.navigationController?.pushViewController(MyWalletViewController(), animated: true)
                        Utility.sharedInstance.showDismissAlert("", msg: "Coupon Repurchased successfully.", controller: self)
                        self.AllDataArray.removeAll()
                        self.GetRedeemedCoupons()
                        KeychainWrapper.standard.removeObject(forKey: "SendGift")
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func SendReferralCoupon(couponTitle:String,couponId:String,email:String,mobille:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"refer_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "coupon_title=\(couponTitle)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&email=\(email)&mobile=\(mobille)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.EmailTextfield.resignFirstResponder()
                        self.MobileTextfield.resignFirstResponder()
                        self.EmailTextfield.text = ""
                        self.MobileTextfield.text = ""
                        //RappleActivityIndicatorView.stopAnimation()
                        // Utility.sharedInstance.showDismissAlert("", msg: "Success", controller: self)
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func GetWalletAmount(){
        
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_wallet_amount")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                print("GetWalletAmount~~~~~~~~~")
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.WalletAmount = dataDictionary.object(forKey: "amount") as! String
                    })
                }
                else {
                    // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPopUp(){
        
        /*********************--NJ--********************/
        BlackBackGround = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        BlackBackGround.backgroundColor = UIColor(white: 0, alpha: 0.5)
        BlackBackGround.isHidden = true
        self.navigationController?.view.addSubview(BlackBackGround)
        /*********************--NJ--********************/
        ReferralPopUp = UIView(frame: CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2))
        ReferralPopUp.backgroundColor = UIColor.white
        ReferralPopUp.layer.cornerRadius = 8
        BlackBackGround.addSubview(ReferralPopUp)
        /*********************--NJ--********************/
        PopUpLabelHeight = ReferralPopUp.frame.height/9
        /*********************--NJ--********************/
        CancelButton = UIButton()
        CancelButton.setButton(X: ReferralPopUp.frame.width-PopUpLabelHeight, Y: 10, Width: PopUpLabelHeight, Height: PopUpLabelHeight, TextColor: .clear, BackColor: .clear, Title: "")
        CancelButton.addTarget(self, action: #selector(CancelButtonAction), for: .touchUpInside)
        ReferralPopUp.addSubview(CancelButton)
        /*********************--NJ--********************/
        CancelImage = UIImageView()
        CancelImage.setImageView(X: CancelButton.frame.height/4, Y: CancelButton.frame.height/4, Width: CancelButton.frame.height/2, Height: CancelButton.frame.height/2, img: #imageLiteral(resourceName: "cancel"))
        CancelImage.contentMode = UIViewContentMode.scaleAspectFit
        CancelButton.addSubview(CancelImage)
        /*********************--NJ--********************/
        //        GiftImageView = UIImageView()
        //GiftImageView.setImageView(X: ReferralPopUp.frame.width/2-PopUpLabelHeight*1.2, Y: CancelButton.frame.height, Width: PopUpLabelHeight*2.4, Height: PopUpLabelHeight*2.4, img: #imageLiteral(resourceName: "Image - Global"))
        //        GiftImageView.backgroundColor = .clear
        //        GiftImageView.contentMode = UIViewContentMode.scaleAspectFit
        //        ReferralPopUp.addSubview(GiftImageView)
        /*********************--NJ--********************/
        EnterDetailsLabel1 = UILabel()
        EnterDetailsLabel1.setLabel(X: 20, Y: CancelButton.frame.height+40, Width: ReferralPopUp.frame.width-40, Height: PopUpLabelHeight/3+CancelButton.frame.height/5+15, TextColor: .darkGray, BackColor: .clear, Text: "Re-Buy This Coupon", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        EnterDetailsLabel1.font = UIFont(name: "Electrolize-Regular", size: 17)
        EnterDetailsLabel1.attributedText = NSAttributedString(string: "Re-Buy This Coupon", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        EnterDetailsLabel1.adjustsFontSizeToFitWidth = true
        ReferralPopUp.addSubview(EnterDetailsLabel1)
        /*********************--NJ--********************/
        EmailView = UIView(frame: CGRect(x: 15, y: EnterDetailsLabel1.frame.origin.y+EnterDetailsLabel1.frame.height+PopUpLabelHeight/3+4, width: ReferralPopUp.frame.width-30, height: PopUpLabelHeight))
        EmailView.backgroundColor = UIColor.white
        EmailView.layer.cornerRadius = 10
        EmailView.layer.borderColor = APPLIGHTPINKCOLOR.cgColor
        EmailView.layer.borderWidth = 1.5
        ReferralPopUp.addSubview(EmailView)
        /*********************--NJ--********************/
        EmailTextfield = UITextField()
        EmailTextfield.setTextField(X: 10, Y: 2, Width: EmailView.frame.width-16, Height: EmailView.frame.height-4, TextColor: .darkGray, BackColor: .clear)
        EmailTextfield.keyboardType = .emailAddress
        EmailTextfield.delegate = self
        EmailTextfield.placeholder = "Enter Their Email"
        EmailView.addSubview(EmailTextfield)
        UITextField.connectTextFields(fields: [EmailTextfield])
        /*********************--NJ--********************/
        /*********************--NJ--********************/
        MobileView = UIView(frame: CGRect(x: 15, y: EmailView.frame.origin.y+EmailView.frame.height+PopUpLabelHeight/4, width: ReferralPopUp.frame.width-30, height: PopUpLabelHeight))
        MobileView.backgroundColor = UIColor.white
        MobileView.layer.cornerRadius = 10
        MobileView.layer.borderColor = APPLIGHTPINKCOLOR.cgColor
        MobileView.layer.borderWidth = 1.5
        ReferralPopUp.addSubview(MobileView)
        /*********************--NJ--********************/
        MobileTextfield = UITextField()
        MobileTextfield.setTextField(X: 10, Y: 2, Width: MobileView.frame.width-16, Height: MobileView.frame.height-4, TextColor: .darkGray, BackColor: .clear)
        MobileTextfield.delegate = self
        MobileTextfield.placeholder = "Enter Their Mobile No."
        MobileTextfield.keyboardType = .numberPad
        MobileView.addSubview(MobileTextfield)
        addDoneButtonOnKeyboard(textfieldd: MobileTextfield)
        /*********************--NJ--********************/
        /*********************--NJ--********************/
        SubmitButton = UIButton()
        SubmitButton.setButton(X: 15, Y: MobileView.frame.origin.y+MobileView.frame.height+PopUpLabelHeight/2, Width: ReferralPopUp.frame.width-30, Height: PopUpLabelHeight, TextColor: .white, BackColor: APPLIGHTPINKCOLOR, Title: "SUBMIT")
        SubmitButton.addTarget(self, action: #selector(SubmitButtonAction), for: .touchUpInside)
        SubmitButton.layer.cornerRadius = 10
        ReferralPopUp.addSubview(SubmitButton)
        /*********************--NJ--********************/
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == EmailTextfield {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5-self.PopUpLabelHeight*2, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5-self.PopUpLabelHeight*3, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == EmailTextfield {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        }
    }
    
    @objc func CancelButtonAction(){
        UIView.animate(withDuration: 0.3, animations: {
            self.BlackBackGround.isHidden = true
            self.EmailTextfield.text = ""
            self.MobileTextfield.text = ""
        })
    }
    @objc func SubmitButtonAction(){
        
        if EmailTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Email", controller: self)
        } else if EmailTextfield.text?.isValidEmail(testStr: EmailTextfield.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email", controller: self)
        } else if MobileTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Mobile", controller: self)
        } else if (MobileTextfield.text?.count)! < 8 || (MobileTextfield.text?.count)! > 12 {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid Mobile number", controller: self)
        } else {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.BlackBackGround.isHidden = true
                
            })
            
            
//            self.navigationController?.isNavigationBarHidden = true
//            if WalletAmount == "" {
//                WalletAmount = "0"
//                ShowAddmoneyPopup()
//            } else if Float(WalletAmount) ?? 0 < Float(1) {
//                ShowAddmoneyPopup()
//            } else {
//                let dictData = AllDataArray[SubmitButton.tag] as! [String:Any]
//                RedeemCoupon(couponId: dictData["coupon_id"] as! String)
//                SendReferralCoupon(couponTitle: dictData["coupon_title"] as! String, couponId: dictData["coupon_id"] as! String, email: EmailTextfield.text!, mobille: MobileTextfield.text!)
//
//            }
        }
    }
    
    func purchaseReredeemApi(payment_status:String){
        let dictData = AllDataArray[SubmitButton.tag] as! [String:Any]
        RedeemCoupon(couponId: dictData["coupon_id"] as! String,payment_status: payment_status)
    }
    
    
    var environment:String = PayPalEnvironmentNoNetwork {
        
        willSet(newEnvironment) {
            
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    
    
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        MobileTextfield.resignFirstResponder()
        
    }
}
