//
//  EditProfileViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/26/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import Alamofire
import RappleProgressHUD

class EditProfileViewController: UIViewController,UITextFieldDelegate {

    var TextType = ""
    
    //get data from Prev Screen
    var userId = ""
    var userName = ""
    var userEmail = ""
    var userMobile = ""
    var userImageStr = ""
    var userImage = UIImage()
    
    var scrollView: UIScrollView!
    var BackButton : UIButton!
    var CouponBookImage: UIImageView!
    var UserProfileImageView: UIImageView!
    var ChangeImageButton : UIButton!
    var EditIcon : UIImageView!
    var NameTextField: SkyFloatingLabelTextField!
    var EmailTextField: SkyFloatingLabelTextField!
    var MobileTextField: SkyFloatingLabelTextField!
    var PasswordTextfield: SkyFloatingLabelTextField!
    var PasswordHideShowButton: UIButton!
    var EyeImageView: UIImageView!
    var SaveButton: UIButton!
    
    let TextFieldHeight = SHEIGHT/15
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        SetView()
        self.UserProfileImageView.sd_setImage(with: URL(string: userImageStr), placeholderImage: #imageLiteral(resourceName: "user"))
        NameTextField.text = userName
        let trimmedString = userEmail.trimmingCharacters(in: .whitespaces)
        EmailTextField.text = trimmedString
        MobileTextField.text = userMobile
//        UserProfileImageView.image = userImage
        
       // PasswordTextfield.text = USER_DEFAULTS.string(forKey: "user_password") ?? ""
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: true)
    }
    
    func EditUserProfile(name:String,emaill:String,mobileNo:String,passwordd:String,image:UIImage){
            
            if  Reachability.isConnectedToNetwork() == false
            {
                Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
                return
            }
            
            RappleActivityIndicatorView.startAnimating()
            
            let url = BASE_URL + "edit_user_profile"
            
            // let imageData:Data = UIImagePNGRepresentation(pickedImage)!
            
            let parameters = [
                "user_id": USER_DEFAULTS.value(forKey: "user_id")!,
                "name":name,
                "email":emaill,
                "mobile":mobileNo,
                "password":passwordd
            ]
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(UIImageJPEGRepresentation(image, 1)!, withName: "image", fileName: "swift_file.jpeg", mimeType: "image/png")
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            }, usingThreshold:UInt64.init(),
               to: url, //URL Here
                method: .post,
                headers: nil, //pass header dictionary here
                encodingCompletion: { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        print("the status code is :")
                        
                        upload.uploadProgress(closure: { (progress) in
                            print("something")
                        })
                        
                        upload.responseJSON { response in
                            print("the resopnse code is : \(String(describing: response.response?.statusCode))")
                            print("the response is : \(response)")
                            
                            RappleActivityIndicatorView.stopAnimation()
                            
                            let AlertController = UIAlertController(title: "", message: "Profile updated successfully.", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            AlertController.addAction(alertAction)
                            self.present(AlertController, animated: true, completion: nil)
                            
                        }
                        break
                    case .failure(let encodingError):
                        print("the error is  : \(encodingError.localizedDescription)")
                        
                        Utility.sharedInstance.showAlert("", msg: "Something wrong.", controller: self)
                        let currentDate: Date = Date()
                        let stringDate: String = currentDate.app_stringFromDate()
                        var errorArr = [String : NSDictionary]()
                        let item1 = NSMutableDictionary()
                        item1.setObject("iOS", forKey: "From" as NSCopying)
                        item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                        item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                        item1.setObject(encodingError.localizedDescription, forKey: "error" as NSCopying)
                        errorArr["json"] = item1
                        var jsonData = NSData()
                        print(errorArr)
                        if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                            let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                            jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                            let jsondata = theJSONText
                            print("jsondata~~~~~~",jsondata)
                            callsaveExceptionLogApi(error: jsondata)
                        }
                        RappleActivityIndicatorView.stopAnimation()
                        
                        break
                    }
        })
    }
    
    
    @objc func ChangeImageButtonAction(){
        
//        let actionSheetController: UIAlertController = UIAlertController(title: "Change Photo", message: "", preferredStyle: .actionSheet)
//
//        let action0: UIAlertAction = UIAlertAction(title: "Open camera", style: .default) { action -> Void in
//
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .camera
//            self.present(imagePicker, animated: true, completion: nil)
//
//        }
//
//        let action1: UIAlertAction = UIAlertAction(title: "Open galary", style: .default) { action -> Void in
        
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType =  UIImagePickerControllerSourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
            
            
      //  }
        
        
//        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
//
//        }
//
//        actionSheetController.addAction(action0)
//        actionSheetController.addAction(action1)
//
//        actionSheetController.addAction(cancelAction)
//        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    func SetView(){
        
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        /******************--NJ--*********************/
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        scrollView.addSubview(BackButton)
        /*****************--NJ--*****************/
        /******************--NJ--*********************/
        CouponBookImage = UIImageView()
        CouponBookImage.setImageView(X: 15, Y: STATUSBARHEIGHT+TextFieldHeight, Width: SWIDTH-30, Height: SHEIGHT/6, img: UIImage(named: "Logo.png")!)
        self.view.addSubview(CouponBookImage)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        UserProfileImageView = UIImageView()
        UserProfileImageView.setImageView(X: SWIDTH/2-TextFieldHeight*1, Y:CouponBookImage.frame.maxY+10, Width: TextFieldHeight*2, Height: TextFieldHeight*2, img: #imageLiteral(resourceName: "user"))
        UserProfileImageView.contentMode = UIViewContentMode.scaleAspectFill
        UserProfileImageView.layer.cornerRadius = UserProfileImageView.frame.height/2
        UserProfileImageView.layer.borderColor = UIColor.lightGray.cgColor
        UserProfileImageView.layer.borderWidth = 0.5
        UserProfileImageView.clipsToBounds = true
        scrollView.addSubview(UserProfileImageView)
        /******************--NJ--*********************/
        ChangeImageButton = UIButton()
        ChangeImageButton.setButton(X: UserProfileImageView.frame.origin.x, Y: UserProfileImageView.frame.origin.y+UserProfileImageView.frame.height-UserProfileImageView.frame.height/3.7, Width: UserProfileImageView.frame.height/3.7, Height: UserProfileImageView.frame.height/3.7, TextColor: .clear, BackColor: .white, Title: "")
        ChangeImageButton.layer.cornerRadius = ChangeImageButton.frame.height/2
        ChangeImageButton.clipsToBounds = true
        ChangeImageButton.addTarget(self, action: #selector(ChangeImageButtonAction), for: .touchUpInside)
        scrollView.addSubview(ChangeImageButton)
        /******************--NJ--*********************/
        EditIcon = UIImageView()
        EditIcon.setImageView(X: ChangeImageButton.frame.height/4, Y: ChangeImageButton.frame.height/4, Width: ChangeImageButton.frame.height/2, Height: ChangeImageButton.frame.height/2, img: #imageLiteral(resourceName: "edit_blue"))
        EditIcon.contentMode = UIViewContentMode.scaleAspectFit
        EditIcon.clipsToBounds = true
        ChangeImageButton.addSubview(EditIcon)
        /******************--NJ--*********************/
        NameTextField = SkyFloatingLabelTextField()
        NameTextField.setSkyLabelwithoutIcon(X: 20, Y:ChangeImageButton.frame.maxY+25,Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "", PlaceHolder: "Name", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1, lineColorr:.lightGray,SelectedlineColorr:.white, tagg: 1)
        NameTextField.delegate = self
        NameTextField.selectedLineHeight = 1.5
        scrollView.addSubview(NameTextField)
        /******************--NJ--*********************/
        EmailTextField = SkyFloatingLabelTextField()
        EmailTextField.setSkyLabelwithoutIcon(X: 20, Y: NameTextField.frame.origin.y+NameTextField.frame.height+TextFieldHeight/3, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "", PlaceHolder: "Email", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1,lineColorr:.lightGray,SelectedlineColorr:.white, tagg: 2)
        EmailTextField.delegate = self
        EmailTextField.keyboardType = .emailAddress
        EmailTextField.selectedLineHeight = 1.5
        scrollView.addSubview(EmailTextField)
        /******************--NJ--*********************/
        MobileTextField = SkyFloatingLabelTextField()
        MobileTextField.setSkyLabelwithoutIcon(X: 20, Y: EmailTextField.frame.origin.y+EmailTextField.frame.height+TextFieldHeight/3, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "", PlaceHolder: "Mobile Number", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1,lineColorr:.lightGray,SelectedlineColorr:.white, tagg: 3)
        MobileTextField.delegate = self
        MobileTextField.keyboardType = .numberPad
        MobileTextField.selectedLineHeight = 1.5
        addDoneButtonOnKeyboard(textfieldd: MobileTextField)
        scrollView.addSubview(MobileTextField)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        PasswordTextfield = SkyFloatingLabelTextField()
        PasswordTextfield.setSkyLabelwithoutIcon(X: 20, Y: MobileTextField.frame.origin.y+MobileTextField.frame.height+TextFieldHeight/3, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "", PlaceHolder: "Password", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1,lineColorr:.lightGray,SelectedlineColorr:.white, tagg: 4)
        PasswordTextfield.delegate = self
        PasswordTextfield.isSecureTextEntry = true
        PasswordTextfield.selectedLineHeight = 1.5
        scrollView.addSubview(PasswordTextfield)
        /******************--NJ--*********************/
        UITextField.connectTextFields(fields: [NameTextField,EmailTextField])
        UITextField.connectTextFields(fields: [PasswordTextfield])
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        PasswordHideShowButton = UIButton()
        PasswordHideShowButton.setButton(X: PasswordTextfield.frame.origin.x+PasswordTextfield.frame.width-PasswordTextfield.frame.height*3/4, Y: PasswordTextfield.frame.origin.y+PasswordTextfield.titleLabel.frame.height, Width: PasswordTextfield.frame.height*3/4, Height: PasswordTextfield.frame.height*3/4, TextColor: .clear, BackColor: .clear, Title: "")
        PasswordHideShowButton.addTarget(self, action: #selector(PasswordHideShowButtonAction), for: .touchUpInside)
        scrollView.addSubview(PasswordHideShowButton)
        /******************--NJ--*********************/
        EyeImageView = UIImageView()
        EyeImageView.setImageView(X: PasswordHideShowButton.frame.height/6, Y: PasswordHideShowButton.frame.height/8, Width: (PasswordHideShowButton.frame.height/6)*4, Height: (PasswordHideShowButton.frame.height/6)*4, img: #imageLiteral(resourceName: "Image - Global"))
        EyeImageView.contentMode = UIViewContentMode.scaleAspectFit
        PasswordHideShowButton.addSubview(EyeImageView)
        /******************--NJ--*********************/
        SaveButton = UIButton()
        SaveButton.setButton(X: PasswordTextfield.frame.origin.x, Y:PasswordTextfield.frame.maxY+40, Width: PasswordTextfield.frame.width, Height: TextFieldHeight*1.1, TextColor: .white, BackColor: APPPINKCOLOR, Title: "SAVE")
        SaveButton.addTarget(self, action: #selector(SaveButtonAction), for: .touchUpInside)
        SaveButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        SaveButton.titleLabel?.adjustsFontSizeToFitWidth = true
        SaveButton.layer.cornerRadius = SaveButton.frame.height/2
        scrollView.addSubview(SaveButton)
        /******************--NJ--*********************/
        
    }
    
    @objc func PasswordHideShowButtonAction(){
        // PasswordTextfield.resignFirstResponder()
        if TextType == "" {
            UIView.animate(withDuration: 0.3, animations: {
                
                self.PasswordTextfield.isSecureTextEntry = false
                self.EyeImageView.image = #imageLiteral(resourceName: "eye")
                self.TextType = "showpassword"
            })
        }
        else {
            UIView.animate(withDuration: 0.3, animations: {
                self.PasswordTextfield.isSecureTextEntry = true
                self.EyeImageView.image = #imageLiteral(resourceName: "Image - Global")
                self.TextType = ""
            })
        }
    }
    
    //Login Button Action
    
    @objc func SaveButtonAction(){
        print("SaveButtonAction~~~~~~~~~",EmailTextField.text)
        if NameTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter name", controller: self)
        } else if EmailTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter email", controller: self)
        } else if EmailTextField.text?.isValidEmail(testStr: EmailTextField.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email", controller: self)
        } else if MobileTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter mobile number", controller: self)
        } else if (MobileTextField.text?.count)! < 8 || (MobileTextField.text?.count)! > 12 {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid mobile number", controller: self)
        }  else {
            
            var pswd = ""
            
            if PasswordTextfield.text == "" {
                pswd = USER_DEFAULTS.string(forKey: "user_password")!
            } else {
                pswd = PasswordTextfield.text!
            }
            
            EditUserProfile(name: NameTextField.text!, emaill: EmailTextField.text!, mobileNo: MobileTextField.text!, passwordd: pswd, image: UserProfileImageView.image!)
            
        }
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            
        } else if textField.tag == 2 {
            
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*2), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*3.5), animated: true)
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            
        } else if textField.tag == 2 {
            
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        
    }
    
    
    
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        MobileTextField.resignFirstResponder()
        
    }
    
}


extension EditProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
         UserProfileImageView.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
}


