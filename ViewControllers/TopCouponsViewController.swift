//
//  TopCouponsViewController.swift
//  Coupon Book
//
//  Created by apple  on 1/4/19.
//  Copyright © 2019 Scientific web solution. All rights reserved.
//


import UIKit
import SDWebImage
import RappleProgressHUD
import MarqueeLabel
import UPCarouselFlowLayout

class TopCouponsViewController: SideMenuViewController,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var couponCollectionView : UICollectionView!
    
    var NoCouponsLabel:UILabel!
    var ShowTableview: UITableView!
    var GenderTableView: UITableView!
    var GenderView: UIView!
    var HideGenderViewButton : UIButton!
    var imageArray = [UIImage]()
    var GenderArray = [String]()
    // var status = ""
    var AllDataArray = [Any]()
    
    var StatusChanged = ""
    //  let cellSpacingHeight: CGFloat = 5
    var navBarHeight = CGFloat()
    
    var BlackBackGround: UIView!
    var ReferralPopUp: UIView!
    var GiftImageView: UIImageView!
    var CancelButton : UIButton!
    var CancelImage : UIImageView!
    var EnterDetailsLabel: UILabel!
    var EmailView: UIView!
    var EmailTextfield:UITextField!
    
    var MobileView: UIView!
    var MobileTextfield:UITextField!
    var SubmitButton:UIButton!
    
    var PopUpLabelHeight = CGFloat()
    var BookId = ""
    var CategoryName = ""
    let ImageView = UIImageView()
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Coupons"
        // assignbackgroundd()
        self.view.backgroundColor = .black
        /***********************--NJ--*************************/
        GenderArray = ["Male","Female","Straight","Gay/BiTrans","Traditional","Extreme"]
        /***********************--NJ--*************************/
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        /***********************--NJ--*************************/
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        /***********************--NJ--*************************/
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
        /***********************--NJ--*************************/
        ShowTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
        //        ShowTableview.delegate = self
        //        ShowTableview.dataSource = self
        ShowTableview.backgroundColor = .clear
        ShowTableview.bounces = false
        ShowTableview.separatorStyle = .none
        ShowTableview.isOpaque = false
        // ShowTableview.layer.backgroundColor = UIColor.blue.cgColor
        ShowTableview.register(DashBoardTableViewCell.self.self, forCellReuseIdentifier: "couponcell")
        // ShowTableview.register(DashBoardTableViewCell.self, forCellReuseIdentifier: "dashcell")
        //self.view.addSubview(ShowTableview)
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(couponCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
        // self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
        // self.view.addSubview(FooterImage)
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        /***********************--NJ--*************************/
        HideGenderViewButton = UIButton()
        HideGenderViewButton.setButton(X: 0, Y: 0, Width: SWIDTH, Height: SHEIGHT, TextColor: .clear, BackColor: UIColor(white: 0, alpha: 0.5), Title: "")
        HideGenderViewButton.isHidden = true
        HideGenderViewButton.addTarget(self, action: #selector(HideGenderViewButtonAction), for: .touchUpInside)
        self.navigationController?.view.addSubview(HideGenderViewButton)
        /***********************--NJ--*************************/
        GenderView = UIView()
        GenderView.frame = CGRect(x: SWIDTH, y: STATUSBARHEIGHT+navBarHeight, width: 0, height: 0)
        GenderView.alpha = 0
        GenderView.backgroundColor = .white
        self.navigationController?.view.addSubview(GenderView)
        /***********************--NJ--*************************/
        GenderTableView = UITableView()
        GenderTableView.frame = CGRect(x: 0, y: 0, width:GenderView.frame.width, height: GenderView.frame.height)
        //        GenderTableView.delegate = self
        //        GenderTableView.dataSource = self
        GenderTableView.backgroundColor = .clear
        GenderTableView.bounces = false
        GenderTableView.separatorStyle = .none
        GenderTableView.isOpaque = false
        GenderTableView.register(SimpleLabelTableViewCell.self, forCellReuseIdentifier: "gendercell")
        GenderView.addSubview(GenderTableView)
        /******************--NJ--*********************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        /******************--NJ--*********************/
        setReferralPopUp()
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        backButton.tintColor=UIColor.white
       // self.navigationItem.leftBarButtonItem = backButton
        /*****************--NJ--*****************/
        
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: true)
    }
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
        ReferDetails(otp:random(), message:shareText)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let selectedRow: IndexPath? = ShowTableview.indexPathForSelectedRow
        if let selectedRowNotNill = selectedRow {
            ShowTableview.deselectRow(at: selectedRowNotNill, animated: true)
            self.ShowTableview.reloadData()
        }
        AllDataArray.removeAll()
        StatusChanged = "coupons"
        //   GetCoupons(bookId: BookId, genderr: "", bodyType: "", genderType: "")
        GetCoupons(Category: CategoryName)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! couponCollectionViewCell
        
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        if dataDict["status"] as? String != "" {
            if dataDict["status"] as? String != "Like"{
                cell.btnlike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
            else{
                cell.btnlike.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
            }
        }
        cell.TitleLabel.text = dataDict["coupon_title"] as? String ?? ""
        cell.PostedByLabel.text = "Posted by : \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        
        cell.TitleLabel.textColor = .white
        cell.TitleLabel.type = .leftRight
        cell.TitleLabel.speed = .rate(80)
        cell.TitleLabel.fadeLength = 80.0
        cell.TitleLabel.labelWillBeginScroll()
        cell.TitleLabel.tag = indexPath.row
        cell.TitleLabel.restartLabel()
        
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        cell.HeadingLabel.text = dataDict["coupon_title_default"] as? String ?? "";
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.starRatingView.isUserInteractionEnabled = false
        if indexPath.row < 10 {
            if ((dataDict["rating"] as? NSNumber)?.intValue) ?? 0 > 0 {
                cell.FruitIcon.isHidden = false
            } else {
                cell.FruitIcon.isHidden = true
            }
        } else {
            cell.FruitIcon.isHidden = true
        }
        cell.btnlike.tag = indexPath.row
        cell.btnlike.addTarget(self, action: #selector(checkboxClicked), for: .touchUpInside)
        cell.RedeemButton.tag = indexPath.row
        cell.RedeemButton.addTarget(self, action: #selector(RedeemButtonAction), for: .touchUpInside)
        cell.ImageView.clipsToBounds = true
        cell.backgroundColor = .clear
        cell.layer.cornerRadius = 15
        
        cell.buttonViolation.tag = indexPath.row
        cell.buttonViolation.addTarget(self, action: #selector(buttonViolationAction), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? couponCollectionViewCell else { return }
        cell.TitleLabel.restartLabel()
        cell.CategaryLabel.restartLabel()
    }
    
    @objc func buttonViolationAction(sender:UIButton){
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = self.AllDataArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            //  let Coupon_Title = dictData["coupon_title"] as! String
            let RecEmail = Contact_Email
            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
            
            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
            
            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
            if let googleUrl = NSURL(string: googleUrlString) {
                UIApplication.shared.openURL(googleUrl as URL)
            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == GenderTableView{
            return GenderArray.count
        }
        else {
            return AllDataArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == GenderTableView{
            return SHEIGHT/14
        }
        else {
            return SHEIGHT/3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponcell", for: indexPath) as! CouponssTableViewCell
        cell.cellView.frame = CGRect(x: 10, y: 8, width: SWIDTH-20, height: SHEIGHT/3-15)
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        if dataDict["status"] as? String != "" {
            if dataDict["status"] as? String != "Like"{
                cell.btnlike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
            else{
                cell.btnlike.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
            }
        }
        cell.TitleLabel.text = dataDict["coupon_title"] as? String ?? ""
        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        
        
        cell.CategaryLabel.textColor = .white
        cell.CategaryLabel.type = .leftRight
        cell.CategaryLabel.speed = .rate(80)
        cell.CategaryLabel.fadeLength = 80.0
        cell.CategaryLabel.labelWillBeginScroll()
        
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        cell.HeadingLabel.text = dataDict["coupon_title_default"] as? String ?? "";
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.starRatingView.isUserInteractionEnabled = false
        if indexPath.row < 10 {
            cell.FruitIcon.isHidden = false
        } else {
            cell.FruitIcon.isHidden = true
        }
        cell.btnlike.tag = indexPath.row
        cell.btnlike.addTarget(self, action: #selector(checkboxClicked), for: .touchUpInside)
        cell.RedeemButton.tag = indexPath.row
        cell.RedeemButton.addTarget(self, action: #selector(RedeemButtonAction), for: .touchUpInside)
        cell.ImageView.clipsToBounds = true
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        cell.CategaryLabel.tag = indexPath.row
        cell.CategaryLabel.restartLabel()
        
        return cell
        
    }
    
    
    
    @objc func checkboxClicked(sender: UIButton) {
        print("buttonpressed")
        
        let dictData = AllDataArray[sender.tag] as! [String:Any]
        let Coupon_Id = dictData["coupon_id"] as! String
        let Status = dictData["status"] as! String
        
        if sender.isSelected{
            sender.isSelected = false
            sender.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
        }else {
            sender.isSelected = true
            sender.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        if Status == "Like"{
            //  GetCoupons(Category: CategoryName)
            favCoupon(couponId: Coupon_Id, statuss: "Unlike")
            //   FavoriteCoupon(couponId: Coupon_Id, status:"Unlike", tagg:sender.tag)
        } else {
            favCoupon(couponId: Coupon_Id, statuss: "Like")
            // FavoriteCoupon(couponId: Coupon_Id, status:"Like", tagg:sender.tag)
        }
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        if tableView == GenderTableView {
    //            StatusChanged = ""
    //            if StatusChanged == "coupons" {
    //                if indexPath.row == 0 || indexPath.row == 1 {
    //                    GetCoupons(Category: CategoryName)
    ////   GetCoupons(bookId: BookId, genderr: GenderArray[indexPath.row], bodyType: "", genderType: "")
    //                } else if indexPath.row == 2 || indexPath.row == 3 {
    //                    GetCoupons(Category: CategoryName)
    ////   GetCoupons(bookId: BookId, genderr: "", bodyType: GenderArray[indexPath.row], genderType: "")
    //                } else {
    //                    GetCoupons(Category: CategoryName)
    ////   GetCoupons(bookId: BookId, genderr: "", bodyType: "", genderType: GenderArray[indexPath.row])
    //                }
    //            }
    //            HideGenderViewButtonAction()
    //        }
    //    }
    
    func GetCoupons(Category: String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_coupons")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&category=".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //   let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        self.couponCollectionView.isHidden = false
                        self.NoCouponsLabel.isHidden = true
                        print(self.AllDataArray)
                        self.ShowTableview.reloadData()
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        self.NoCouponsLabel.isHidden = false
                        self.couponCollectionView.isHidden = true
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func RedeemCoupon(couponId:String,tagg:Int){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"redeem_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        //RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Coupon Redeemed successfully.", controller: self)
                        //                        let indexpath = IndexPath.init(row: tagg, section: 0)
                        //                        let cell = self.ShowTableview.cellForRow(at: indexpath) as! CouponssTableViewCell
                        //                        UIView.animate(withDuration: 0.3, animations: {
                        //                            cell.cellView.frame = CGRect(x: SWIDTH, y: 8, width: SWIDTH-20, height: SHEIGHT/3-15)
                        //
                        //                        })
                        self.AllDataArray.removeAll()
                        self.GetCoupons(Category: self.CategoryName)
                        //   self.GetCoupons(bookId: self.BookId, genderr: "", bodyType: "", genderType: "")
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    @objc func RedeemButtonAction(sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.BlackBackGround.isHidden = false
        })
        SubmitButton.tag = sender.tag
    }
    
    @objc func HideGenderViewButtonAction(){
        UIView.animate(withDuration: 0.3, animations: {
            self.HideGenderViewButton.isHidden = true
            self.GenderView.frame = CGRect(x: SWIDTH, y: STATUSBARHEIGHT+self.navBarHeight, width: 0, height: 0)
            self.GenderView.alpha = 0
            self.GenderTableView.frame = CGRect(x: 0, y: 0, width: self.GenderView.frame.width, height: self.GenderView.frame.height)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPopUp(){
        
        /*********************--NJ--********************/
        BlackBackGround = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        BlackBackGround.backgroundColor = UIColor(white: 0, alpha: 0.5)
        BlackBackGround.isHidden = true
        self.navigationController?.view.addSubview(BlackBackGround)
        /*********************--NJ--********************/
        ReferralPopUp = UIView(frame: CGRect(x: 20, y: SHEIGHT/6, width: SWIDTH-40, height: (SHEIGHT/6)*4))
        ReferralPopUp.backgroundColor = UIColor.white
        ReferralPopUp.layer.cornerRadius = 8
        BlackBackGround.addSubview(ReferralPopUp)
    }
    
    func SendReferralCoupon(couponTitle:String,couponId:String,email:String,mobille:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        let url = URL(string: BASE_URL+"refer_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "coupon_title=\(couponTitle)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&email=\(email)&mobile=\(mobille)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        self.EmailTextfield.resignFirstResponder()
                        self.MobileTextfield.resignFirstResponder()
                        self.EmailTextfield.text = ""
                        self.MobileTextfield.text = ""
                        //RappleActivityIndicatorView.stopAnimation()
                        // Utility.sharedInstance.showDismissAlert("", msg: "Success", controller: self)
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func setReferralPopUp(){
        
        /*********************--NJ--********************/
        BlackBackGround = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        BlackBackGround.backgroundColor = UIColor(white: 0, alpha: 0.5)
        BlackBackGround.isHidden = true
        self.navigationController?.view.addSubview(BlackBackGround)
        /*********************--NJ--********************/
        ReferralPopUp = UIView(frame: CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2))
        ReferralPopUp.backgroundColor = UIColor.white
        ReferralPopUp.layer.cornerRadius = 8
        BlackBackGround.addSubview(ReferralPopUp)
        /*********************--NJ--********************/
        PopUpLabelHeight = ReferralPopUp.frame.height/9
        /*********************--NJ--********************/
        CancelButton = UIButton()
        CancelButton.setButton(X: ReferralPopUp.frame.width-PopUpLabelHeight, Y: 10, Width: PopUpLabelHeight, Height: PopUpLabelHeight, TextColor: .clear, BackColor: .clear, Title: "")
        CancelButton.addTarget(self, action: #selector(CancelButtonAction), for: .touchUpInside)
        ReferralPopUp.addSubview(CancelButton)
        /*********************--NJ--********************/
        CancelImage = UIImageView()
        CancelImage.setImageView(X: CancelButton.frame.height/4, Y: CancelButton.frame.height/4, Width: CancelButton.frame.height/2, Height: CancelButton.frame.height/2, img: #imageLiteral(resourceName: "cancel"))
        CancelImage.contentMode = UIViewContentMode.scaleAspectFit
        CancelButton.addSubview(CancelImage)
        /*********************--NJ--********************/
        EnterDetailsLabel = UILabel()
        EnterDetailsLabel.setLabel(X: 15, Y: CancelButton.frame.height+20, Width: ReferralPopUp.frame.width-30, Height: PopUpLabelHeight, TextColor: .darkGray, BackColor: .clear, Text: "Redeem This Coupon To Your Lover :", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        EnterDetailsLabel.attributedText = NSAttributedString(string: "Redeem This Coupon To Your Lover :", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        EnterDetailsLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        EnterDetailsLabel.adjustsFontSizeToFitWidth = true
        ReferralPopUp.addSubview(EnterDetailsLabel)
        /*********************--NJ--********************/
        EmailView = UIView(frame: CGRect(x: 15, y: EnterDetailsLabel.frame.origin.y+EnterDetailsLabel.frame.height+PopUpLabelHeight/3, width: ReferralPopUp.frame.width-30, height: PopUpLabelHeight))
        EmailView.backgroundColor = UIColor.white
        EmailView.layer.cornerRadius = 10
        EmailView.layer.borderColor = APPLIGHTPINKCOLOR.cgColor
        EmailView.layer.borderWidth = 1.5
        ReferralPopUp.addSubview(EmailView)
        /*********************--NJ--********************/
        EmailTextfield = UITextField()
        EmailTextfield.setTextField(X: 10, Y: 2, Width: EmailView.frame.width-16, Height: EmailView.frame.height-4, TextColor: .darkGray, BackColor: .clear)
        EmailTextfield.delegate = self
        EmailTextfield.placeholder = "Enter Their Email"
        EmailView.addSubview(EmailTextfield)
        UITextField.connectTextFields(fields: [EmailTextfield])
        /*********************--NJ--********************/
        MobileView = UIView(frame: CGRect(x: 15, y: EmailView.frame.origin.y+EmailView.frame.height+PopUpLabelHeight/4, width: ReferralPopUp.frame.width-30, height: PopUpLabelHeight))
        MobileView.backgroundColor = UIColor.white
        MobileView.layer.cornerRadius = 10
        MobileView.layer.borderColor = APPLIGHTPINKCOLOR.cgColor
        MobileView.layer.borderWidth = 1.5
        ReferralPopUp.addSubview(MobileView)
        /*********************--NJ--********************/
        MobileTextfield = UITextField()
        MobileTextfield.setTextField(X: 10, Y: 2, Width: MobileView.frame.width-16, Height: MobileView.frame.height-4, TextColor: .darkGray, BackColor: .clear)
        MobileTextfield.delegate = self
        MobileTextfield.placeholder = "Enter Their Mobile No."
        MobileTextfield.keyboardType = .numberPad
        MobileView.addSubview(MobileTextfield)
        addDoneButtonOnKeyboard(textfieldd: MobileTextfield)
        /*********************--NJ--********************/
        SubmitButton = UIButton()
        SubmitButton.setButton(X: 15, Y: MobileView.frame.origin.y+MobileView.frame.height+PopUpLabelHeight/2, Width: ReferralPopUp.frame.width-30, Height: PopUpLabelHeight, TextColor: .white, BackColor: APPLIGHTPINKCOLOR, Title: "SUBMIT")
        SubmitButton.addTarget(self, action: #selector(SubmitButtonAction), for: .touchUpInside)
        SubmitButton.layer.cornerRadius = 10
        ReferralPopUp.addSubview(SubmitButton)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == EmailTextfield {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5-self.PopUpLabelHeight*2, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5-self.PopUpLabelHeight*3, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == EmailTextfield {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        }
    }
    
    @objc func CancelButtonAction(){
        UIView.animate(withDuration: 0.3, animations: {
            self.BlackBackGround.isHidden = true
            self.EmailTextfield.text = ""
            self.MobileTextfield.text = ""
        })
    }
    @objc func SubmitButtonAction(){
        
        if EmailTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Email", controller: self)
        } else if EmailTextfield.text?.isValidEmail(testStr: EmailTextfield.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email", controller: self)
        } else if MobileTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Mobile", controller: self)
        } else if (MobileTextfield.text?.count)! < 8 || (MobileTextfield.text?.count)! > 12 {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid Mobile number", controller: self)
        } else {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.BlackBackGround.isHidden = true
                
            })
            let dictData = AllDataArray[SubmitButton.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            RedeemCoupon(couponId: Coupon_Id,tagg:SubmitButton.tag)
            SendReferralCoupon(couponTitle: dictData["coupon_title"] as! String, couponId: dictData["coupon_id"] as! String, email: EmailTextfield.text!, mobille: MobileTextfield.text!)
        }
    }
    func FavoriteCoupon(couponId:String, status:String,tagg:Int){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL + "favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=\(status) ".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        //RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Coupon ReRedeemed successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        MobileTextfield.resignFirstResponder()
        
    }
    
    func favCoupon(couponId:String,statuss:String) {
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"favorite_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=\(statuss)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        
                        // Utility.sharedInstance.showDismissAlert("", msg: msg, controller: self)
                        //RappleActivityIndicatorView.stopAnimation()
                        
                        //                        let indexpath = IndexPath.init(row: tagg, section: 0)
                        //                        let cell = self.ShowTableview.cellForRow(at: indexpath) as! CouponssTableViewCell
                        //                        UIView.animate(withDuration: 0.3, animations: {
                        //                            cell.cellView.frame = CGRect(x: SWIDTH, y: 8, width: SWIDTH-20, height: SHEIGHT/3-15)
                        //
                        //                        })
                        self.AllDataArray.removeAll()
                        self.GetCoupons(Category: self.CategoryName)
                        //   self.GetCoupons(bookId: self.BookId, genderr: "", bodyType: "", genderType: "")
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func assignbackgroundd(){
        //let background = #imageLiteral(resourceName: "drawerpic")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        // imageView.image = nil
        // imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
        imageView.image = UIImage(named: "background-1")!.resizableImage(withCapInsets: .zero)
        
        //        let BlurView = UIView()
        //        BlurView.frame = CGRect(x: imageView.frame.origin.x, y: imageView.frame.origin.y, width: imageView.frame.width, height: imageView.frame.height)
        //        BlurView.backgroundColor = UIColor(white: 0, alpha: 0.8)
        //        BlurView.clipsToBounds = true
        //        BlurView.layer.cornerRadius = 10
        //        imageView.addSubview(BlurView)
    }}
