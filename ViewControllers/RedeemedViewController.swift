//
//  RedeemedViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/25/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SDWebImage
import MGStarRatingView
import MarqueeLabel
import UPCarouselFlowLayout

class RedeemedViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource,StarRatingDelegate,UITextFieldDelegate {
    
    var couponCollectionView : UICollectionView!
    
//    iCarouselDelegate,iCarouselDataSource
    var NoCouponsLabel:UILabel!
   // var carsousel: iCarousel!
    var RedeemedTableview: UITableView!
    var imageArray = [UIImage]()
    
    var AllDataArray = [Any]()
    var WalletAmount = ""
    
    var navBarHeight = CGFloat()
    
    var BlackBackGround: UIView!
    var ReferralPopUp: UIView!
    var GiftImageView: UIImageView!
    var CancelButton : UIButton!
    var CancelImage : UIImageView!
    var EnterDetailsLabel: UILabel!
    var EnterDetailsLabel1: UILabel!
    var EnterDetailsLabel2: UILabel!
     var EnterDetailsLabel3: UILabel!
    var EmailView: UIView!
    var EmailTextfield:UITextField!
    var MobileView: UIView!
    var MobileTextfield:UITextField!
    var SubmitButton:UIButton!
    var PopUpLabelHeight = CGFloat()
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Redeemed Coupons"
        
        //assignbackgroundd()
        self.view.backgroundColor = .black
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
//        carsousel.delegate = self
//        carsousel.dataSource = self
//        self.carsousel.type = .coverFlow
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
        RedeemedTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
      //  RedeemedTableview.delegate = self
        //RedeemedTableview.dataSource = self
        RedeemedTableview.backgroundColor = .clear
        RedeemedTableview.bounces = false
        RedeemedTableview.separatorStyle = .none
        RedeemedTableview.isOpaque = false
        RedeemedTableview.register(CouponsTableViewCell.self, forCellReuseIdentifier: "couponcell")
      //  self.view.addSubview(RedeemedTableview)
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
      //   self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
       // self.view.addSubview(FooterImage)
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(sendCouponCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
       /******************--NJ--*********************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        /******************--NJ--*********************/
        setPopUp()
    }
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
        ReferDetails(otp:random(), message:shareText)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! sendCouponCollectionViewCell
        
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
//        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.DaysLabel.text = dataDict["default_title"] as?  String ?? ""
        cell.starRatingView.isUserInteractionEnabled = true
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPYELLOWCOLOR
        cell.backgroundColor = .clear
        cell.DaysLabel.text = dataDict["default_title"] as? String ?? ""
        
        cell.CategaryLabel.textColor = .white
        cell.CategaryLabel.type = .leftRight
        cell.CategaryLabel.speed = .rate(80)
        cell.CategaryLabel.fadeLength = 80.0
        cell.CategaryLabel.labelWillBeginScroll()
        cell.CategaryLabel.tag = indexPath.row
        cell.CategaryLabel.restartLabel()
        
        cell.RedeemLabel.isHidden = false
        cell.DownloadImage.isHidden = false
        cell.RedeemButton.isHidden = false
        cell.LabelByPost.isHidden = false
        cell.RedeemLabel.text = "REPURCHASE"
        cell.RedeemButton.tag = indexPath.row
        cell.RedeemButton.addTarget(self, action: #selector(ReRedeemButtonAction), for: .touchUpInside)
        
        cell.buttonViolation.tag = indexPath.row
        cell.buttonViolation.addTarget(self, action: #selector(buttonViolationAction), for: .touchUpInside)
        cell.PostedByLabel.isHidden = true
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? sendCouponCollectionViewCell else { return }
        cell.CategaryLabel.restartLabel()
    }
    
    @objc func buttonViolationAction(sender:UIButton){
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = self.AllDataArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            //let Coupon_Title = dictData["coupon_title"] as! String
            let RecEmail = Contact_Email
            
            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
            
            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
            if let googleUrl = NSURL(string: googleUrlString) {
                UIApplication.shared.openURL(googleUrl as URL)
            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SHEIGHT/3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponcell", for: indexPath) as! CouponsTableViewCell
        
        cell.CountLabel.isHidden = false
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "")"
        cell.CategaryLabel.text = "Category : \(dataDict["cat_name"] as? String ?? "")"
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
      // cell.CountLabel.text = dataDict["re_redeem_counts"] as? String ?? ""
        cell.DayLabel.text = dataDict["default_title"] as?  String ?? ""
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.starRatingView.isUserInteractionEnabled = true
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.RedeemLabel.text = "REPURCHASE"
        cell.RedeemButton.tag = indexPath.row
        cell.RedeemButton.addTarget(self, action: #selector(ReRedeemButtonAction), for: .touchUpInside)
        cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPYELLOWCOLOR
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.isUserInteractionEnabled = true
        return cell
    }
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        
        let dataDict = AllDataArray[view.tag] as! [String:Any]
        
        
        submitRating(couponId: dataDict["coupon_id"] as! String, rating: value.rounded())
        
    }
    
    @objc func ReRedeemButtonAction(sender: UIButton) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.BlackBackGround.isHidden = false
        })
        SubmitButton.tag = sender.tag
        print(sender.tag)
    }
    
    func ShowAddmoneyPopup(){
        
        let alertController = UIAlertController(title: "", message: "Please add money to your account to ReRedeem Coupon.", preferredStyle: .alert)
        let AddAction = UIAlertAction(title: "Add Money", style: .default) { (UIAlertAction) in
            
            let vc = MyWalletViewController()
            vc.PrevViewStatus = "reredeem"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(AddAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        WalletAmount = ""
        GetRedeemedCoupons()
        GetWalletAmount()
    }
    func GetRedeemedCoupons(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_re_redeem_coupons")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print(self.AllDataArray)
                        DispatchQueue.main.async(execute: {
                            self.couponCollectionView.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.couponCollectionView.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func submitRating(couponId:String,rating:CGFloat){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_rating")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&rating=\(rating)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        Utility.sharedInstance.showDismissAlert("", msg: "Rating submitted.", controller: self)
                        //RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray.removeAll()
                        self.GetRedeemedCoupons()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func RedeemCoupon(couponId:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"re_redeem_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        
                        Utility.sharedInstance.showDismissAlert("", msg: "Coupon Repurchased successfully.", controller: self)
                        self.AllDataArray.removeAll()
                        self.GetRedeemedCoupons()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func SendReferralCoupon(couponTitle:String,couponId:String,email:String,mobille:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
       // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"refer_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "coupon_title=\(couponTitle)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&email=\(email)&mobile=\(mobille)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                       
                        self.EmailTextfield.resignFirstResponder()
                        self.MobileTextfield.resignFirstResponder()
                        self.EmailTextfield.text = ""
                        self.MobileTextfield.text = ""
                        
                        self.navigationController?.pushViewController(MyWalletViewController(), animated: true)
                        //RappleActivityIndicatorView.stopAnimation()
                       // Utility.sharedInstance.showDismissAlert("", msg: "Success", controller: self)
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func GetWalletAmount(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_wallet_amount")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.WalletAmount = dataDictionary.object(forKey: "amount") as! String
                    })
                }
                else {
                    // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPopUp(){
        
        /*********************--NJ--********************/
        BlackBackGround = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        BlackBackGround.backgroundColor = UIColor(white: 0, alpha: 0.5)
        BlackBackGround.isHidden = true
        self.navigationController?.view.addSubview(BlackBackGround)
        /*********************--NJ--********************/
        ReferralPopUp = UIView(frame: CGRect(x: 20, y: SHEIGHT/3, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2.5))
        ReferralPopUp.backgroundColor = UIColor.white
        ReferralPopUp.layer.cornerRadius = 8
        BlackBackGround.addSubview(ReferralPopUp)
        /*********************--NJ--********************/
        PopUpLabelHeight = ReferralPopUp.frame.height/9
        /*********************--NJ--********************/
        CancelButton = UIButton()
        CancelButton.setButton(X: ReferralPopUp.frame.width-PopUpLabelHeight, Y: 10, Width: PopUpLabelHeight, Height: PopUpLabelHeight, TextColor: .clear, BackColor: .clear, Title: "")
        CancelButton.addTarget(self, action: #selector(CancelButtonAction), for: .touchUpInside)
        ReferralPopUp.addSubview(CancelButton)
        /*********************--NJ--********************/
        CancelImage = UIImageView()
        CancelImage.setImageView(X: CancelButton.frame.height/4, Y: CancelButton.frame.height/4, Width: CancelButton.frame.height/2, Height: CancelButton.frame.height/2, img: #imageLiteral(resourceName: "cancel"))
        CancelImage.contentMode = UIViewContentMode.scaleAspectFit
        CancelButton.addSubview(CancelImage)
        /*********************--NJ--********************/
        EnterDetailsLabel1 = UILabel()
        EnterDetailsLabel1.setLabel(X: 20, Y: CancelButton.frame.height+20, Width: ReferralPopUp.frame.width-40, Height: PopUpLabelHeight/3+CancelButton.frame.height/5+15, TextColor: .darkGray, BackColor: .clear, Text: "ReRedeem This Coupon To Your Lover:", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        EnterDetailsLabel1.attributedText = NSAttributedString(string: "ReRedeem This Coupon To Your Lover:", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        EnterDetailsLabel1.font = UIFont(name: "Electrolize-Regular", size: 17)
        EnterDetailsLabel1.adjustsFontSizeToFitWidth = true
        ReferralPopUp.addSubview(EnterDetailsLabel1)
        /*********************--NJ--********************/
        EmailView = UIView(frame: CGRect(x: 15, y: EnterDetailsLabel1.frame.origin.y+EnterDetailsLabel1.frame.height+PopUpLabelHeight/3+4, width: ReferralPopUp.frame.width-30, height: PopUpLabelHeight+10))
        EmailView.backgroundColor = UIColor.white
        EmailView.layer.cornerRadius = 10
        EmailView.layer.borderColor = APPLIGHTPINKCOLOR.cgColor
        EmailView.layer.borderWidth = 1.5
        ReferralPopUp.addSubview(EmailView)
        /*********************--NJ--********************/
        EmailTextfield = UITextField()
        EmailTextfield.setTextField(X: 10, Y: 2, Width: EmailView.frame.width-16, Height: EmailView.frame.height-4, TextColor: .darkGray, BackColor: .clear)
        EmailTextfield.delegate = self
        EmailTextfield.placeholder = "Enter Their Email"
        EmailView.addSubview(EmailTextfield)
        UITextField.connectTextFields(fields: [EmailTextfield])
        /*********************--NJ--********************/
        MobileView = UIView(frame: CGRect(x: 15, y: EmailView.frame.origin.y+EmailView.frame.height+PopUpLabelHeight/4, width: ReferralPopUp.frame.width-30, height: PopUpLabelHeight+10))
        MobileView.backgroundColor = UIColor.white
        MobileView.layer.cornerRadius = 10
        MobileView.layer.borderColor = APPLIGHTPINKCOLOR.cgColor
        MobileView.layer.borderWidth = 1.5
        ReferralPopUp.addSubview(MobileView)
        /*********************--NJ--********************/
        MobileTextfield = UITextField()
        MobileTextfield.setTextField(X: 10, Y: 2, Width: MobileView.frame.width-16, Height: MobileView.frame.height-4, TextColor: .darkGray, BackColor: .clear)
        MobileTextfield.delegate = self
        MobileTextfield.placeholder = "Enter Their Mobile No."
        MobileTextfield.keyboardType = .numberPad
        MobileView.addSubview(MobileTextfield)
        addDoneButtonOnKeyboard(textfieldd: MobileTextfield)
        /*********************--NJ--********************/
        /*********************--NJ--********************/
        SubmitButton = UIButton()
        SubmitButton.setButton(X: 15, Y: MobileView.frame.origin.y+MobileView.frame.height+PopUpLabelHeight/2+5, Width: ReferralPopUp.frame.width-30, Height: PopUpLabelHeight+10, TextColor: .white, BackColor: APPLIGHTPINKCOLOR, Title: "SUBMIT")
        SubmitButton.addTarget(self, action: #selector(SubmitButtonAction), for: .touchUpInside)
        SubmitButton.layer.cornerRadius = 10
        ReferralPopUp.addSubview(SubmitButton)
        /*********************--NJ--********************/
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == EmailTextfield {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5-self.PopUpLabelHeight*2, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5-self.PopUpLabelHeight*3, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == EmailTextfield {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.ReferralPopUp.frame = CGRect(x: 20, y: SHEIGHT/4.5, width: SWIDTH-40, height: SHEIGHT-(SHEIGHT/4.5)*2)
            })
        }
    }
    
    @objc func CancelButtonAction(){
        UIView.animate(withDuration: 0.3, animations: {
            self.BlackBackGround.isHidden = true
            self.EmailTextfield.text = ""
            self.MobileTextfield.text = ""
        })
    }
    @objc func SubmitButtonAction(){
        
        if EmailTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Email", controller: self)
        } else if EmailTextfield.text?.isValidEmail(testStr: EmailTextfield.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email", controller: self)
        } else if MobileTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Mobile", controller: self)
        } else if (MobileTextfield.text?.count)! < 8 || (MobileTextfield.text?.count)! > 12 {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid Mobile number", controller: self)
        } else {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.BlackBackGround.isHidden = true
            
        })
        
        if WalletAmount == "" {
            WalletAmount = "0"
            ShowAddmoneyPopup()
        } else if Float(WalletAmount) ?? 0 < Float(1) {
            ShowAddmoneyPopup()
        } else {
            let dictData = AllDataArray[SubmitButton.tag] as! [String:Any]
            RedeemCoupon(couponId: dictData["coupon_id"] as! String)
            SendReferralCoupon(couponTitle: dictData["coupon_title"] as! String, couponId: dictData["coupon_id"] as! String, email: EmailTextfield.text!, mobille: MobileTextfield.text!)
        }
    }
}
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        MobileTextfield.resignFirstResponder()
        
    }
    
    func assignbackgroundd(){
       // let background = #imageLiteral(resourceName: "drawerpic")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
       // imageView.image = nil
       /// imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
imageView.image = UIImage(named: "background-1")!.resizableImage(withCapInsets: .zero)
    }
}
