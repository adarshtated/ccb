//
//  SubmitCouponViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/25/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import Alamofire
import MGStarRatingView
class SubmitCouponViewController: SideMenuViewController,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource , UITextViewDelegate{

    let TextFieldHeight = SHEIGHT/15
    
    var AllDataArray = [Any]()
    
    var categoryCollectionView : UICollectionView!
    
    var SelectedgenderType = ""
    var OthergenderType = ""
    var GenderType = ""
    var scrollView: UIScrollView!
    var BackButton : UIButton!
    var imageViewBack : UIImageView!
    var SelectCouponTitleTextField: SkyFloatingLabelTextField!
    var ExtremeTextfield: UILabel!
    var TraditionalTextField: UILabel!
    var OtherTextField: UILabel!
    var OtherTextField1: UILabel!
    var TransTextField: UILabel!
    var GenderButton:UIButton!
    var OtherTypeButton:UIButton!
    var GenderTextField: UILabel!
    var DescriptionTextField: SkyFloatingLabelTextField!
    var FemaleTextField: UILabel!
    var BisexualTextField: UILabel!
    var UploadImageView: UIImageView!
    var ChangeImageButton : UIButton!
    var SubmitButton: UIButton!
    var ExtremeRadioImage: UIImageView!
    var TraditionalRadioImage:UIImageView!
    var FemaleRadioImage:UIImageView!
    var ExtremeButton: UIButton!
    var TraditionalButton:UIButton!
    var GenderDownImage: UIImageView!
    var OtherDownImage:UIImageView!
    var FemaleButton:UIButton!
    var HidePopUpButton : UIButton!
    var BitransButton: UIButton!
    var BitransRadioImage: UIImageView!
    var CouponImage: UIImageView!
    var SubmitcouponLabel: UILabel!
    var OnlySuggestionLabel: UILabel!
    var TransTypeButton: UIButton!
    var TransDownImage: UIImageView!
    var BisexualRadioImage: UIImageView!
    var BisexualButton: UIButton!
    
    // ravi create new view
    var homeView:UIView!
    var bottomView:UIView!
    let starRatingView = StarRatingView()
    var textView:UITextView!
    var descriptionTittle:UILabel!
    var tittleLabel:UILabel!
    var likeButton:UIButton!
    var flagButton:UIButton!
    var sendButton:UIButton!
    
    
    var straightLabel:UILabel!
    var straightRadioImage: UIImageView!
    var straightButton: UIButton!
    var gayLabel:UILabel!
    var gayRadioImage: UIImageView!
    var gayButton: UIButton!
    
    var selectedGenderArray = [String]() //["","","","","","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        SetView()
        //GetCategory()
        assignbackground()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func SetView(){
        
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        self.view.addSubview(BackButton)
        
        
        homeView = UIView()
        homeView.frame = CGRect(x: 30, y: SHEIGHT/3.5, width: SWIDTH-60, height: 380)//SHEIGHT/2.2
        homeView.backgroundColor = .black
        homeView.layer.cornerRadius = 6
        homeView.layer.masksToBounds = true
        homeView.layer.borderWidth = 1.5
        homeView.layer.borderColor = UIColor.white.cgColor
        self.view.addSubview(homeView)
        
        bottomView = UIView()
        bottomView.frame = CGRect(x: 0, y: homeView.frame.height-50, width: homeView.frame.width, height: 50)
        bottomView.backgroundColor = .red
        bottomView.layer.cornerRadius = 6
        bottomView.layer.masksToBounds = true
        self.homeView.addSubview(bottomView)
        
        sendButton = UIButton()
        sendButton.setButton(X: 10, Y: 8, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        sendButton.setImage(UIImage(named: "sent-mail"), for: .normal)
        sendButton.addTarget(self, action: #selector(SendGiftButtonAction), for: .touchUpInside)
        bottomView.addSubview(sendButton)
       
        
        starRatingView.frame = CGRect(x: bottomView.frame.width-160, y: 10, width: 30, height: bottomView.frame.height/2)
        let attribute = StarRatingAttribute(type: .rate, point: bottomView.frame.height/2, spacing: 4, emptyColor: UIColor(white: 0, alpha: 0.5), fillColor: .white, emptyImage: nil, fillImage: nil)
        starRatingView.configure(attribute, current: 5, max: 5)
        starRatingView.clipsToBounds = true
        bottomView.addSubview(starRatingView)
        
        
        
        
       
        likeButton = UIButton()
        likeButton.setButton(X: 10, Y: 10, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        likeButton.setImage(UIImage(named: "like-5"), for: .normal)
        homeView.addSubview(likeButton)
        
        
        flagButton = UIButton()
        flagButton.setButton(X: homeView.frame.width-45, Y: 10, Width: 35, Height: 35, TextColor: .white, BackColor: .clear, Title: "")
        flagButton.setImage(UIImage(named: "flag"), for: .normal)
//        homeView.addSubview(flagButton)
        
        
        tittleLabel = UILabel()
        tittleLabel.setLabel(X: homeView.frame.width/5.5, Y: flagButton.frame.maxY+10, Width: homeView.frame.width/1.5, Height: 40, TextColor: UIColor(displayP3Red: 27/255, green: 133/255, blue: 178/255, alpha: 1), BackColor: .clear, Text: "TODAY OR TONIGHT", TextAlignment: .center, Font:UIFont.boldSystemFont(ofSize: 23))
         //tittleLabel.font = UIFont(name: "Electrolize-Regular", size: 23)
        tittleLabel.attributedText = NSAttributedString(string: "TODAY OR TONIGHT", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        tittleLabel.font = UIFont(name: "VerminVibes2Nightclub", size: 40)
         tittleLabel.adjustsFontSizeToFitWidth = true
         tittleLabel.minimumScaleFactor = 0.1
         //tittleLabel = buttomBorder(label: tittleLabel)
          homeView.addSubview(tittleLabel)
        
        
//        descriptionTittle = UILabel()
//        descriptionTittle.setLabel(X: 10, Y: tittleLabel.frame.maxY+10, Width: homeView.frame.width-20, Height: homeView.frame.height*0.05, TextColor:.white, BackColor: .clear, Text: "Description", TextAlignment: .left, Font:UIFont.boldSystemFont(ofSize: 18))
//        descriptionTittle.adjustsFontSizeToFitWidth = true
//        descriptionTittle.minimumScaleFactor = 0.1
//        homeView.addSubview(descriptionTittle)
        textView = UITextView()
        textView.frame = CGRect(x: 10, y: tittleLabel.frame.maxY+10, width: homeView.frame.width-20, height: 130) //homeView.frame.height/3.5
        textView.backgroundColor = .clear
        textView.textColor = .white
       // textView.font = UIFont.systemFont(ofSize: 16)
        textView.font = UIFont(name: "PerpetuaTitlingMTLight2", size: 20)
        textView.layer.cornerRadius = 6
        textView.layer.masksToBounds = true
        textView.layer.borderWidth = 1.5
        textView.layer.borderColor = UIColor.white.cgColor
        textView.delegate = self
        textView.text = "Description"
        addDoneButtonOnKeyboard(textfieldd: textView)
        homeView.addSubview(textView)
        
        
//        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
//        scrollView.backgroundColor = .clear
//        scrollView.bounces = false
//        self.automaticallyAdjustsScrollViewInsets=false
//        self.view.addSubview(scrollView)
//        /******************--NJ--*********************/
       
//        /******************--NJ--*********************/
//        CouponImage = UIImageView()
//        CouponImage.setImageView(X: SWIDTH/4, Y: STATUSBARHEIGHT+5, Width: SWIDTH/2, Height: SHEIGHT/5.5, img: #imageLiteral(resourceName: "splash_screen1"))
//        CouponImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(CouponImage)
//        /*****************--NJ--*****************/
//        SubmitcouponLabel = UILabel()
//        SubmitcouponLabel.setLabel(X: 35, Y: CouponImage.frame.origin.y+CouponImage.frame.height+TextFieldHeight/8, Width: SWIDTH-70, Height: TextFieldHeight*3/4, TextColor: .white, BackColor: .clear, Text: "SUBMIT YOUR OWN COUPON", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 23))
//        SubmitcouponLabel.font = UIFont(name: "Electrolize-Regular", size: 23)
//        SubmitcouponLabel.adjustsFontSizeToFitWidth = true
//        scrollView.addSubview(SubmitcouponLabel)
//        /*****************--NJ--*****************/
//        OnlySuggestionLabel = UILabel()
//        OnlySuggestionLabel.setLabel(X: SubmitcouponLabel.frame.origin.x+5, Y: SubmitcouponLabel.frame.origin.y+SubmitcouponLabel.frame.height+TextFieldHeight/10, Width: SWIDTH-(SubmitcouponLabel.frame.origin.x)*2-10, Height: TextFieldHeight*3/4, TextColor: .white, BackColor: .clear, Text: "Only suggestion accepted by admin will be posted in the app.", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 14))
//        OnlySuggestionLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
//        OnlySuggestionLabel.adjustsFontSizeToFitWidth = true
//        OnlySuggestionLabel.numberOfLines = 0
//        OnlySuggestionLabel.lineBreakMode = .byTruncatingTail
//        scrollView.addSubview(OnlySuggestionLabel)
//        /*****************--NJ--*****************/
//        let sep = UILabel()
//        sep.setLabel(X: 0, Y: OnlySuggestionLabel.frame.origin.y+OnlySuggestionLabel.frame.height+4, Width: SWIDTH, Height: 1.2, TextColor: .clear, BackColor: .white, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 12))
//        scrollView.addSubview(sep)
//        /*****************--NJ--*****************/
//        SelectCouponTitleTextField = SkyFloatingLabelTextField()
//        SelectCouponTitleTextField.setSkyLabelwithoutIcon(X: 20, Y: sep.frame.origin.y+sep.frame.height+5, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter coupon title", PlaceHolder: "Enter coupon title", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1.2, lineColorr:.white,SelectedlineColorr:.white, tagg: 1)
//        SelectCouponTitleTextField.delegate = self
//        scrollView.addSubview(SelectCouponTitleTextField)
//        /*****************--NJ--*****************/
//        let selectCategory = UILabel()
//        selectCategory.setLabel(X: 20, Y: SelectCouponTitleTextField.frame.origin.y+SelectCouponTitleTextField.frame.height+TextFieldHeight/6, Width: SWIDTH/2, Height: TextFieldHeight/2, TextColor: .white, BackColor: .clear, Text: "Select Category :", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        selectCategory.adjustsFontSizeToFitWidth = true
//        scrollView.addSubview(selectCategory)
//        /******************--NJ--*********************/
//        GenderDownImage = UIImageView()
//        GenderDownImage.setImageView(X: 25, Y: selectCategory.frame.height+selectCategory.frame.origin.y+TextFieldHeight/5+TextFieldHeight/8, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        GenderDownImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(GenderDownImage)
//        /******************--NJ--*********************/
//        GenderTextField = UILabel()
//        GenderTextField.setLabel(X: GenderDownImage.frame.origin.x+GenderDownImage.frame.width+TextFieldHeight/6, Y: GenderDownImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Male", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(GenderTextField)
//        /******************--NJ--*********************/
//        GenderButton = UIButton()
//        GenderButton.setButton(X: GenderDownImage.frame.origin.x, Y: GenderTextField.frame.origin.y, Width: GenderTextField.frame.width+GenderDownImage.frame.width+TextFieldHeight/6, Height: GenderTextField.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        GenderButton.addTarget(self, action: #selector(GenderButtonAction), for: .touchUpInside)
//        scrollView.addSubview(GenderButton)
//        /******************--NJ--*********************/
//        FemaleRadioImage = UIImageView()
//        FemaleRadioImage.setImageView(X: SWIDTH/2+25, Y: selectCategory.frame.height+selectCategory.frame.origin.y+TextFieldHeight/5+TextFieldHeight/8, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        FemaleRadioImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(FemaleRadioImage)
//        /******************--NJ--*********************/
//        FemaleTextField = UILabel()
//        FemaleTextField.setLabel(X: FemaleRadioImage.frame.origin.x+FemaleRadioImage.frame.width+TextFieldHeight/6, Y: FemaleRadioImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Female", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(FemaleTextField)
//        /******************--NJ--*********************/
//        FemaleButton = UIButton()
//        FemaleButton.setButton(X: FemaleRadioImage.frame.origin.x, Y: FemaleTextField.frame.origin.y, Width: FemaleTextField.frame.width+FemaleRadioImage.frame.width+TextFieldHeight/6, Height: FemaleTextField.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        FemaleButton.addTarget(self, action: #selector(FemaleButtonAction), for: .touchUpInside)
//        scrollView.addSubview(FemaleButton)
//        /******************--NJ--*********************/
//        /******************--NJ--*********************/
//        straightRadioImage = UIImageView()
//        straightRadioImage.setImageView(X: 25, Y: FemaleTextField.frame.height+FemaleTextField.frame.origin.y+TextFieldHeight/10+TextFieldHeight/8, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        straightRadioImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(straightRadioImage)
//        /******************--NJ--*********************/
//        straightLabel = UILabel()
//        straightLabel.setLabel(X: straightRadioImage.frame.origin.x+straightRadioImage.frame.width+TextFieldHeight/6, Y: straightRadioImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Straight", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(straightLabel)
//        /******************--NJ--*********************/
//        straightButton = UIButton()
//        straightButton.setButton(X: straightRadioImage.frame.origin.x, Y: straightLabel.frame.origin.y, Width: straightLabel.frame.width+straightRadioImage.frame.width+TextFieldHeight/6, Height: straightLabel.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        straightButton.addTarget(self, action: #selector(straightButtonAction), for: .touchUpInside)
//        scrollView.addSubview(straightButton)
//        /******************--NJ--*********************/
//        gayRadioImage = UIImageView()
//        gayRadioImage.setImageView(X: SWIDTH/2+25, Y: straightRadioImage.frame.origin.y, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        gayRadioImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(gayRadioImage)
//        /******************--NJ--*********************/
//        gayLabel = UILabel()
//        gayLabel.setLabel(X: gayRadioImage.frame.origin.x+gayRadioImage.frame.width+TextFieldHeight/6, Y: gayRadioImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Gay", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(gayLabel)
//        /******************--NJ--*********************/
//        gayButton = UIButton()
//        gayButton.setButton(X: gayRadioImage.frame.origin.x, Y: gayLabel.frame.origin.y, Width: gayLabel.frame.width+gayRadioImage.frame.width+TextFieldHeight/6, Height: gayLabel.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        gayButton.addTarget(self, action: #selector(gayButtonAction), for: .touchUpInside)
//        scrollView.addSubview(gayButton)
//        /******************--NJ--*********************/
//        /******************--NJ--*********************/
//        TransDownImage = UIImageView()
//        TransDownImage.setImageView(X: 25, Y: gayLabel.frame.height+gayLabel.frame.origin.y+TextFieldHeight/10+TextFieldHeight/8, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        TransDownImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(TransDownImage)
//        /******************--NJ--*********************/
//        TransTextField = UILabel()
//        TransTextField.setLabel(X: TransDownImage.frame.origin.x+TransDownImage.frame.width+TextFieldHeight/6, Y: TransDownImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Trans", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(TransTextField)
//        /******************--NJ--*********************/
//        TransTypeButton = UIButton()
//        TransTypeButton.setButton(X: TransDownImage.frame.origin.x, Y: TransTextField.frame.origin.y, Width: TransTextField.frame.width+TransDownImage.frame.width+TextFieldHeight/6, Height: TransTextField.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        TransTypeButton.addTarget(self, action: #selector(TransTypeButtonAction), for: .touchUpInside)
//        scrollView.addSubview(TransTypeButton)
//        /******************--NJ--*********************/
//        BisexualRadioImage = UIImageView()
//        BisexualRadioImage.setImageView(X: SWIDTH/2+25, Y: TransDownImage.frame.origin.y, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        BisexualRadioImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(BisexualRadioImage)
//        /******************--NJ--*********************/
//        BisexualTextField = UILabel()
//        BisexualTextField.setLabel(X: BisexualRadioImage.frame.origin.x+BisexualRadioImage.frame.width+TextFieldHeight/6, Y: BisexualRadioImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Bisexual", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(BisexualTextField)
//        /******************--NJ--*********************/
//        BisexualButton = UIButton()
//        BisexualButton.setButton(X: BisexualRadioImage.frame.origin.x, Y: BisexualTextField.frame.origin.y, Width: BisexualTextField.frame.width+BisexualRadioImage.frame.width+TextFieldHeight/6, Height: gayLabel.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        BisexualButton.addTarget(self, action: #selector(BisexualButtonAction), for: .touchUpInside)
//        scrollView.addSubview(BisexualButton)
//        /******************--NJ--*********************/
//        /******************--NJ--*********************/
//        TraditionalRadioImage = UIImageView()
//        TraditionalRadioImage.setImageView(X: 25, Y: BisexualTextField.frame.height+BisexualTextField.frame.origin.y+TextFieldHeight/10+TextFieldHeight/8, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        TraditionalRadioImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(TraditionalRadioImage)
//        /******************--NJ--*********************/
//        TraditionalTextField = UILabel()
//        TraditionalTextField.setLabel(X: TraditionalRadioImage.frame.origin.x+TraditionalRadioImage.frame.width+TextFieldHeight/6, Y: TraditionalRadioImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Traditional", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(TraditionalTextField)
//        /******************--NJ--*********************/
//        TraditionalButton = UIButton()
//        TraditionalButton.setButton(X: TraditionalRadioImage.frame.origin.x, Y: TraditionalTextField.frame.origin.y, Width: TraditionalTextField.frame.width+TraditionalRadioImage.frame.width+TextFieldHeight/6, Height: TraditionalTextField.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        TraditionalButton.addTarget(self, action: #selector(TraditionalButtonAction), for: .touchUpInside)
//        scrollView.addSubview(TraditionalButton)
//        /******************--NJ--*********************/
//        ExtremeRadioImage = UIImageView()
//        ExtremeRadioImage.setImageView(X: SWIDTH/2+25, Y: TraditionalRadioImage.frame.origin.y, Width: TextFieldHeight/3, Height: TextFieldHeight/3, img: UIImage(named:"unchecked_box.png")!)
//        ExtremeRadioImage.contentMode = UIViewContentMode.scaleAspectFit
//        scrollView.addSubview(ExtremeRadioImage)
//        /******************--NJ--*********************/
//        ExtremeTextfield = UILabel()
//        ExtremeTextfield.setLabel(X: ExtremeRadioImage.frame.origin.x+ExtremeRadioImage.frame.width+TextFieldHeight/6, Y: ExtremeRadioImage.frame.origin.y-TextFieldHeight/8, Width: SWIDTH/3-30, Height: TextFieldHeight/3+(TextFieldHeight/8)*2, TextColor: .white, BackColor: .clear, Text: "Extreme", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 15))
//        scrollView.addSubview(ExtremeTextfield)
//        /******************--NJ--*********************/
//        ExtremeButton = UIButton()
//        ExtremeButton.setButton(X: ExtremeRadioImage.frame.origin.x, Y: ExtremeTextfield.frame.origin.y, Width: ExtremeTextfield.frame.width+ExtremeRadioImage.frame.width+TextFieldHeight/6, Height: ExtremeTextfield.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
//        ExtremeButton.addTarget(self, action: #selector(ExtremeButtonAction), for: .touchUpInside)
//        scrollView.addSubview(ExtremeButton)
        /******************--NJ--*********************/
//        /***********************--NJ--*************************/
//        let layout = UICollectionViewFlowLayout()
//        layout.itemSize = CGSize(width: (SWIDTH-30)/2, height: SHEIGHT/15)
//        layout.scrollDirection = .vertical
//        layout.minimumLineSpacing = 0
//        layout.minimumInteritemSpacing = 0
//        categoryCollectionView = UICollectionView(frame: CGRect(x: 15, y: selectCategory.frame.origin.y+selectCategory.frame.height+TextFieldHeight/5, width: SWIDTH-30, height: (SHEIGHT/15)*5), collectionViewLayout: layout)
//        categoryCollectionView.backgroundColor = .black
//        categoryCollectionView.delegate = self
//        categoryCollectionView.dataSource = self
//        categoryCollectionView.register(CheckBoxCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
//        scrollView.addSubview(categoryCollectionView)
//        /***********************--NJ--*************************/
//        /******************--NJ--*********************/
//        DescriptionTextField = SkyFloatingLabelTextField()
//        DescriptionTextField.setSkyLabelwithoutIcon(X: 20, Y: categoryCollectionView.frame.origin.y+categoryCollectionView.frame.height+4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Description", PlaceHolder: "Description", TitleColor: .white,selectedTitleColor:.white, LineHeight: 1.5, lineColorr:.white,SelectedlineColorr:.white, tagg: 2)
//        DescriptionTextField.delegate = self
//        scrollView.addSubview(DescriptionTextField)
//        /*****************--NJ--********************/
//        SubmitButton = UIButton()
//        SubmitButton.setButton(X: 20, Y: DescriptionTextField.frame.origin.y+DescriptionTextField.frame.height+TextFieldHeight/6, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: APPPINKCOLOR, Title: "SEND FOR APPROVAL")
//        SubmitButton.layer.cornerRadius = SubmitButton.frame.height/2
//        SubmitButton.addTarget(self, action: #selector(SendGiftButtonAction), for: .touchUpInside)
//        scrollView.addSubview(SubmitButton)
//        /******************--NJ--*********************/
//        /******************--NJ--*********************/
//        UITextField.connectTextFields(fields: [SelectCouponTitleTextField])
//        UITextField.connectTextFields(fields: [DescriptionTextField])
//        /******************--NJ--*********************/
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CheckBoxCollectionViewCell
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.genderLabel.text = dataDict["category_name"] as? String ?? ""
        return cell
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let indexPathh = IndexPath.init(row: indexPath.row, section: 0)
        let cell = categoryCollectionView.cellForItem(at: indexPathh) as! CheckBoxCollectionViewCell
        if cell.CheckBoxImageView.image == UIImage(named:"unchecked_box.png")! {
            cell.CheckBoxImageView.image = UIImage(named:"checked_box.png")!
            
            let dataDict = AllDataArray[indexPathh.row] as! [String:Any]
            let cat = dataDict["id"] as! String
            selectedGenderArray.append(cat)
        } else {
            cell.CheckBoxImageView.image = UIImage(named:"unchecked_box.png")!
            let dataDict = AllDataArray[indexPathh.row] as! [String:Any]
            let cat = dataDict["id"] as! String
            let index = selectedGenderArray.firstIndex(of: cat)
            selectedGenderArray.remove(at: index!)
        }
    }
    
    
    
    @objc func BisexualButtonAction(){
//        UIView.animate(withDuration: 0.4, animations: {
//            self.BisexualRadioImage.image = #imageLiteral(resourceName: "radio_on")
//           // self.TransDownImage.image = #imageLiteral(resourceName: "radio_off")
//        })
        if self.BisexualRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.BisexualRadioImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at: 5)
            selectedGenderArray.insert("", at: 5)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.BisexualRadioImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 5)
            selectedGenderArray.insert("Bisexual", at: 5)
        }
        
        
    }
    
    @objc func straightButtonAction(){
        if self.straightRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.straightRadioImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at: 2)
            selectedGenderArray.insert("", at: 2)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.straightRadioImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 2)
            selectedGenderArray.insert("Straight", at: 2)
        }
    }
    
    @objc func gayButtonAction(){
        if self.gayRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.gayRadioImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at:3)
            selectedGenderArray.insert("", at: 3)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.gayRadioImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 3)
            selectedGenderArray.insert("Gay", at: 3)
        }
    }
    
    @objc func TransTypeButtonAction(){
//        UIView.animate(withDuration: 0.4, animations: {
//           // self.BisexualRadioImage.image = #imageLiteral(resourceName: "radio_off")
//            self.TransDownImage.image = #imageLiteral(resourceName: "radio_on")
//        })
        if self.TransDownImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.TransDownImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at: 4)
            selectedGenderArray.insert("", at: 4)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.TransDownImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 4)
            selectedGenderArray.insert("Trans", at: 4)
        }
        
    }
     @objc func GenderButtonAction(){
        GenderType = "Male"
//        UIView.animate(withDuration: 0.4, animations: {
//            self.GenderDownImage.image = #imageLiteral(resourceName: "radio_on")
//          //  self.FemaleRadioImage.image = #imageLiteral(resourceName: "radio_off")
//        })
        if self.GenderDownImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.GenderDownImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at: 0)
            selectedGenderArray.insert("", at: 0)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.GenderDownImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 0)
            selectedGenderArray.insert("Male", at: 0)
        }
        
    }
    @objc func FemaleButtonAction(){
        GenderType = "Female"
//        UIView.animate(withDuration: 0.4, animations: {
//           // self.GenderDownImage.image = #imageLiteral(resourceName: "radio_off")
//            self.FemaleRadioImage.image = #imageLiteral(resourceName: "radio_on")
//        })
        if self.FemaleRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.FemaleRadioImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at: 1)
            selectedGenderArray.insert("", at: 1)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.FemaleRadioImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 1)
            selectedGenderArray.insert("Female", at: 1)
        }
        
    }
    @objc func OtherTypeButtonAction(){
        OthergenderType = "Straight"
//        UIView.animate(withDuration: 0.4, animations: {
//            self.OtherDownImage.image = #imageLiteral(resourceName: "radio_on")
//          //  self.BitransRadioImage.image = #imageLiteral(resourceName: "radio_off")
//        })
        if self.OtherDownImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.OtherDownImage.image = UIImage(named: "unchecked_box.png")
            })
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.OtherDownImage.image = UIImage(named: "checked_box.png")!
            })
        }
        
    }
    @objc func BitransButtonAction(){
        OthergenderType = "Gay/Bitrans"
        if self.BitransRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.BitransRadioImage.image = UIImage(named: "unchecked_box.png")
            })
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.BitransRadioImage.image = UIImage(named: "checked_box.png")!
            })
        }
//        UIView.animate(withDuration: 0.4, animations: {
//          //  self.OtherDownImage.image = #imageLiteral(resourceName: "radio_off")
//            self.BitransRadioImage.image = #imageLiteral(resourceName: "radio_on")
//        })
    }
    @objc func backButtonAction(sender: UIBarButtonItem){
        USER_DEFAULTS.set(0, forKey: "section")
        USER_DEFAULTS.set(0, forKey: "row")
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    @objc func SendGiftButtonAction(){
       if (textView.text == "Description" || textView.text.isEmpty == true) {
            Utility.sharedInstance.showAlert("", msg: "Please enter description.", controller: self)
        }else {
            SubmitCoupon(couponTitle: "",Description: textView.text!, categoryString: "")
        }
    }
    @objc func TraditionalButtonAction(){
        SelectedgenderType = "Traditional"
        if self.TraditionalRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.TraditionalRadioImage.image = UIImage(named: "unchecked_box.png")
            })
            selectedGenderArray.remove(at: 6)
            selectedGenderArray.insert("", at: 6)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.TraditionalRadioImage.image = UIImage(named: "checked_box.png")!
            })
            selectedGenderArray.remove(at: 6)
            selectedGenderArray.insert("Traditional", at: 6)
        }
//        UIView.animate(withDuration: 0.4, animations: {
//            self..image = #imageLiteral(resourceName: "radio_on")
//           // self.ExtremeRadioImage.image = #imageLiteral(resourceName: "radio_off")
//        })
    }
    @objc func ExtremeButtonAction(){
        SelectedgenderType = "Extreme"
        if self.ExtremeRadioImage.image == UIImage(named: "checked_box.png") {
            UIView.animate(withDuration: 0.4, animations: {
                self.ExtremeRadioImage.image = UIImage(named: "unchecked_box.png")
                // self.TraditionalRadioImage.image = #imageLiteral(resourceName: "radio_off")
            })
            selectedGenderArray.remove(at: 7)
            selectedGenderArray.insert("", at: 7)
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                self.ExtremeRadioImage.image = UIImage(named: "checked_box.png")!
                // self.TraditionalRadioImage.image = #imageLiteral(resourceName: "radio_off")
            })
            selectedGenderArray.remove(at: 7)
            selectedGenderArray.insert("Extreme", at: 7)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight), animated: true)
        } else if textField.tag == 2 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*5), animated: true)
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*3), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*4), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*5), animated: true)
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 2 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        
    }
    
    func GetCategory(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_categories")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        // let vc = SHowViewController()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print(self.AllDataArray)
                        DispatchQueue.main.async(execute: {
//                            self.categoryCollectionView.isHidden = false
                            
                        })
//                        self.categoryCollectionView.reloadData()
                    })
                }
                else {
                    //Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.categoryCollectionView.isHidden = true
                       // self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func
        SubmitCoupon(couponTitle:String,Description:String,categoryString:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        
        RappleActivityIndicatorView.startAnimating()
        
        let url = BASE_URL + "submit_coupons"
        
        // let imageData:Data = UIImagePNGRepresentation(pickedImage)!
        
        let parameters = [
            "user_id": USER_DEFAULTS.value(forKey: "user_id")!,
            "title":couponTitle,
            "description":Description,
            "category":categoryString
            
        ]
        
//        "gender":gender,
//        "body_type":bodyType,
//        "gender_type":genderType
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
        //    multipartFormData.append(UIImageJPEGRepresentation(image, 1)!, withName: "image", fileName: "swift_file.jpeg", mimeType: "image/png")
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
    }
    , usingThreshold:UInt64.init(),
           to: url, //URL Here
            method: .post,
            headers: nil, //pass header dictionary here
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")

                    upload.uploadProgress(closure: { (progress) in
                        print("something")
                    })
        
                    upload.responseJSON { response in
                        print("the resopnse code is : \(String(describing: response.response?.statusCode))")
                        print("the response is : \(response)")
                        
                        RappleActivityIndicatorView.stopAnimation()
                        
                        let AlertController = UIAlertController(title: "", message: "Coupon submitted successfully", preferredStyle: .alert)
                        let OkAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
                            USER_DEFAULTS.set(0, forKey: "section")
                            USER_DEFAULTS.set(3, forKey: "row")
                            self.navigationController?.pushViewController(MyCouponViewController(), animated: true)
                        }
                        AlertController.addAction(OkAction)
                        self.present(AlertController, animated: true, completion: nil)
                    }
                    break
                case .failure(let encodingError):
                    print("the error is  : \(encodingError.localizedDescription)")
                    
                    Utility.sharedInstance.showAlert("", msg: "Something wrong.", controller: self)
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var errorArr = [String : NSDictionary]()
                    let item1 = NSMutableDictionary()
                    item1.setObject("iOS", forKey: "From" as NSCopying)
                    item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                    item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                    item1.setObject(encodingError.localizedDescription, forKey: "error" as NSCopying)
                    errorArr["json"] = item1
                    var jsonData = NSData()
                    print(errorArr)
                    if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                        jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                        let jsondata = theJSONText
                        print("jsondata~~~~~~",jsondata)
                        callsaveExceptionLogApi(error: jsondata)
                    }
                    RappleActivityIndicatorView.stopAnimation()
                    
                    break
                }
        })
        
    }
    
    
    
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func buttomBorder(label: UILabel) -> UILabel {
        let frame = label.frame
        let bottomLayer = CALayer()
        bottomLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width - 2, height: 1)
        bottomLayer.backgroundColor = UIColor(displayP3Red: 27/255, green: 133/255, blue: 178/255, alpha: 1).cgColor
        label.layer.addSublayer(bottomLayer)
        //For Shadow
        label.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        label.layer.shadowOffset = CGSize(width: 0.0, height: 2.00)
        label.layer.shadowOpacity = 1.0
        label.layer.shadowRadius = 0.0
        label.layer.masksToBounds = false
        label.layer.cornerRadius = 4.0
        
        return label
        
    }
    
    func addDoneButtonOnKeyboard(textfieldd: UITextView) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        textView.resignFirstResponder()
    }
//    @objc func ChangeImageAction(){
//        let myPickerController = UIImagePickerController()
//        myPickerController.delegate = self
//        myPickerController.sourceType =  UIImagePickerControllerSourceType.photoLibrary
//        self.present(myPickerController, animated: true, completion: nil)
//    }
    
//    override  func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch:UITouch = touches.first!
//        if touch.view == UploadImageView {
//let actionSheetController: UIAlertController = UIAlertController(title: "Change Photo", message: "", preferredStyle: .actionSheet)
//
//    let action0: UIAlertAction = UIAlertAction(title: "Open camera", style: .default) { action -> Void in
//                let imagePicker = UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = .camera
//                self.present(imagePicker, animated: true, completion: nil)
//            }
//    let action1: UIAlertAction = UIAlertAction(title: "Open galary", style: .default) { action -> Void in
//
//                let myPickerController = UIImagePickerController()
//                myPickerController.delegate = self
//                myPickerController.sourceType =  UIImagePickerControllerSourceType.photoLibrary
//                self.present(myPickerController, animated: true, completion: nil)
//            }
//    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
//            }
//            actionSheetController.addAction(action0)
//            actionSheetController.addAction(action1)
//
//            actionSheetController.addAction(cancelAction)
//            self.present(actionSheetController, animated: true, completion: nil)
//        }
//    }
}

//extension SubmitCouponViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//
//        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            UploadImageView.image = pickedImage
//        }
//        dismiss(animated: true, completion: nil)
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        dismiss(animated: true, completion: nil)
//
//    }
//
//}

