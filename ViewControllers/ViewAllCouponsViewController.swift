//
//  ViewAllCouponsViewController.swift
//  Coupon Book
//
//  Created by Adarsh on 25/04/19.
//  Copyright © 2019 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SDWebImage
import MGStarRatingView
import MarqueeLabel
import UPCarouselFlowLayout

class userCell: UITableViewCell {
    var lblUserName:UILabel!
    var btnSend:UIButton!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        lblUserName = UILabel()
        lblUserName.frame = CGRect(x: 15, y: contentView.frame.minY+4, width: SWIDTH/2, height: 36)
        contentView.addSubview(lblUserName)

        btnSend = UIButton()
        btnSend.setButton(X: SWIDTH-150, Y: contentView.frame.minY+8, Width: 80, Height: 35, TextColor: .white, BackColor: .red, Title: "Send")
        btnSend.titleLabel?.textColor = UIColor.white
        btnSend.backgroundColor = UIColor.red
        btnSend.layer.cornerRadius = btnSend.frame.height/2
        btnSend.layer.borderColor = UIColor.white.cgColor
        btnSend.layer.borderWidth = 3.0
        btnSend.contentHorizontalAlignment = .center
        btnSend.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        contentView.addSubview(btnSend)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ViewAllCouponsViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource,StarRatingDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource  {

    var couponCollectionView: UICollectionView!
    var NoCouponsLabel: UILabel!
    var imageArray = [UIImage]()
    
    var AllDataArray = [Any]()
    var WalletAmount = ""
    
    var userDataArray = [Any]()
    
    var navBarHeight = CGFloat()
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    var BlackBackGround: UIView!
    
    
    var alertPopUpView: UIView!
    var ImageView = UIImageView()
    var lblAlert = UILabel()
    var lblAlert1 = UILabel()
    var lblAlert2 = UILabel()
    var lblAlert3 = UILabel()
    var btnRepotAnBloack = UIButton()
    var btnRepot = UIButton()
    var btnCancel = UIButton()
    var btnHere = UIButton()
    var lblborder = UILabel()
    var lblRemoveABlock = UILabel()
    var userTableview:UITableView = UITableView()
    var reuseIdentifire = "Cell"
    var userView:UIView!
    var lblSelectUser:UILabel!
    
    var coupenId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "View All Coupons"
        assignbackground()
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
        let sharebtn1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))
        sharebtn1.tintColor=UIColor.white
        navigationItem.leftBarButtonItems = [sharebtn1]
        
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(sendCouponCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
        // self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
        //  self.view.addSubview(FooterImage)
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction1))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        /***********************--NJ--*************************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        
        alertPopUpView = UIView()
        
        alertPopUpView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 220))
        alertPopUpView.layer.borderColor = UIColor.white.cgColor
        alertPopUpView.layer.borderWidth = 1.0
        alertPopUpView.layer.cornerRadius = 10
        alertPopUpView.clipsToBounds = true
        alertPopUpView.layer.shadowColor = UIColor.white.cgColor
        alertPopUpView.layer.shadowRadius = 5
        alertPopUpView.layer.shadowOpacity = 0.3
        alertPopUpView.backgroundColor = UIColor.black
//        ImageView.setImageView(X: 0, Y: 0, Width: alertPopUpView.frame.width, Height: alertPopUpView.frame.height, img: UIImage(named: "logo1.png")!)
//        ImageView.contentMode = UIViewContentMode.scaleAspectFit
//        ImageView.clipsToBounds = true
//        ImageView.alpha = 0.2
//        alertPopUpView.addSubview(ImageView)
        
        lblAlert.setLabel(X: 0, Y: 5, Width: alertPopUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "ALERT", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 14))
        lblAlert.font = UIFont(name: "Electrolize-Bold", size: 20)
//        lblAlert.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblAlert.adjustsFontSizeToFitWidth = true
        lblAlert.attributedText = NSAttributedString(string: "ALERT", attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        alertPopUpView.addSubview(lblAlert)
        
        lblAlert1.setLabel(X: 8, Y: lblAlert.frame.height+5, Width: alertPopUpView.frame.width-16, Height: 30, TextColor: .blue, BackColor: .clear, Text: "Breakups heppen.Are You Being Harassed?", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 16))
        lblAlert1.font = UIFont(name: "Electrolize-Bold", size: 10)
        lblAlert1.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        lblAlert1.adjustsFontSizeToFitWidth = true
        alertPopUpView.addSubview(lblAlert1)
        
        lblborder.setLabel(X: 0, Y: lblAlert1.frame.height+35, Width: alertPopUpView.frame.width, Height: 2, TextColor: .lightGray, BackColor: .clear, Text: "", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 18))
        lblborder.backgroundColor = UIColor.white
       // alertPopUpView.addSubview(lblborder)
        
        
        
        
        btnRepotAnBloack.setButton(X: 0, Y: lblAlert1.frame.height+25, Width: alertPopUpView.frame.width, Height: 40, TextColor: .white, BackColor: .clear, Title: "Flag = ")
        lblAlert1.font = UIFont(name: "Electrolize-Bold", size: 11)
        btnRepotAnBloack.addTarget(self,action:#selector(onClickReportAnBlockAlert),for:.touchUpInside)
        alertPopUpView.addSubview(btnRepotAnBloack)
        
        btnRepotAnBloack = UIButton()
        btnRepotAnBloack.setButton(X: alertPopUpView.frame.width-125, Y: lblAlert1.frame.height+38, Width: alertPopUpView.frame.width-280, Height: 22, TextColor: .white, BackColor: .clear, Title: "")
        btnRepotAnBloack.setImage(UIImage(named:"flag"), for: .normal)
        alertPopUpView.addSubview(btnRepotAnBloack)
        
        //HeadingLabel.frame.origin.y+HeadingLabel.frame.height-5
        lblAlert2.setLabel(X: 8, Y: btnRepotAnBloack.frame.origin.y + 20, Width: alertPopUpView.frame.width - 16, Height: 30, TextColor: .white, BackColor: .clear, Text: "Flagging a user = they cannot send you anymore coupons", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 16))
        lblAlert2.font = UIFont(name: "Electrolize-Regular", size: 8)
       // lblAlert2.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        lblAlert2.adjustsFontSizeToFitWidth = true
        //lblAlert2.numberOfLines = 2
        alertPopUpView.addSubview(lblAlert2)
        
        
        
        btnRepot.setButton(X: 0, Y: lblAlert2.frame.origin.y + 25, Width: alertPopUpView.frame.width, Height: 40, TextColor: .white, BackColor: .clear, Title: "Block = ")
        lblAlert1.font = UIFont(name: "Electrolize-Bold", size: 11)
        btnRepot.addTarget(self,action:#selector(onClickReportAlert),for:.touchUpInside)
        alertPopUpView.addSubview(btnRepot)
        
        btnRepot = UIButton()
        btnRepot.setButton(X: alertPopUpView.frame.width-118, Y: lblAlert2.frame.origin.y+36, Width: alertPopUpView.frame.width-280, Height: 20, TextColor: .white, BackColor: .clear, Title: "")
        btnRepot.setImage(UIImage(named:"block"), for: .normal)
        alertPopUpView.addSubview(btnRepot)
        
        lblAlert3.setLabel(X: 8, Y: btnRepot.frame.origin.y+15, Width: alertPopUpView.frame.width - 16, Height: 50, TextColor: .white, BackColor: .clear, Text: "Blocking a user = you must provide permission for them to send you coupons again", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 16))
        lblAlert3.font = UIFont(name: "Electrolize-Regular", size: 8)
//lblAlert3.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        lblAlert3.adjustsFontSizeToFitWidth = true
        //lblAlert3.numberOfLines = 2
        alertPopUpView.addSubview(lblAlert3)
        
        btnCancel.setButton(X: alertPopUpView.frame.width-40, Y: lblAlert.frame.origin.y, Width: 40*3/4, Height: 40*3/4, TextColor: .red, BackColor: .clear, Title: "X")
        btnCancel.addTarget(self,action:#selector(onClickCancelAlert),for:.touchUpInside)
        alertPopUpView.addSubview(btnCancel)
        
        lblRemoveABlock.setLabel(X: 0, Y: lblAlert3.frame.origin.y+lblAlert3.frame.height+5, Width: alertPopUpView.frame.width-20, Height: 25, TextColor: .white, BackColor: .clear, Text: "Remove A Block", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 8))
       // lblRemoveABlock.adjustsFontSizeToFitWidth = true
        alertPopUpView.addSubview(lblRemoveABlock)
        
        btnHere.setButton(X: alertPopUpView.frame.width-142, Y: lblAlert3.frame.origin.y+lblAlert3.frame.height+5, Width: 50, Height: 25, TextColor: UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0), BackColor: .clear, Title: "Here")
        btnHere.titleLabel?.font = UIFont(name: "Electrolize-Regular", size: 8)
        //btnHere.addTarget(self,action:#selector(onClickCancelAlert),for:.touchUpInside)
       // btnHere.titleLabel?.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        alertPopUpView.addSubview(btnHere)
        
        alertPopUpView.center = self.view.center
        alertPopUpView.isHidden = true
        self.view.addSubview(alertPopUpView)
        
        userView = UIView()
        userView = UIView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
        userView.backgroundColor = UIColor.darkGray
        self.view.addSubview(userView)
        
        lblSelectUser = UILabel()
        lblSelectUser.setLabel(X: SWIDTH/2-100, Y: 10, Width: 200, Height: 25, TextColor: .blue, BackColor: .clear, Text: "Select User", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 25))
        lblSelectUser.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        userView.addSubview(lblSelectUser)
        
        userTableview = UITableView(frame: CGRect(x: 30, y: lblSelectUser.frame.maxY+10, width: SWIDTH-60, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight-40))
        userTableview.delegate      =   self
        userTableview.dataSource    =   self
        userTableview.register(userCell.self, forCellReuseIdentifier: reuseIdentifire)
        userTableview.layer.cornerRadius = 10.0
        userTableview.backgroundColor = UIColor.clear
        userView.addSubview(self.userTableview)
        userView.isHidden = true
        
    }
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    @objc func backAction(){
        if (userView.isHidden == false) {
            userView.isHidden = true
        }else{
            navigationController?.pushViewController(HomeViewController(), animated: false)
        }
    }
    
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(sender:UIButton){
        print("shareAction")
        let buttonPosition = sender.convert(CGPoint.zero, to: self.couponCollectionView)
        let indexPath = self.couponCollectionView.indexPathForItem(at: buttonPosition)
        let dataDict = AllDataArray[indexPath?.row ?? 0] as! [String:Any]
        coupenId = dataDict["id"] as? String ?? ""
        get_ccbusers()
        //SendReferralCoupon(couponTitle: "TODAY OR TONIGHT", couponId: id, email: email, mobille: user_mobile, userID: user_id)
    }
    
    @objc func shareAction1(sender:UIButton){
        let shareText = "A user from the Couples Couponbook(CCB) app has send a naughty coupon.To open & view your coupon click : itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4"
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! sendCouponCollectionViewCell
        cell.CountLabel.isHidden = false
        cell.LabelByPost.isHidden = true
        cell.buttonViolation123.isHidden = true
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        let title = dataDict["coupon_title"] as? String ?? ""
        cell.HeadingLabel.text = title.uppercased()
        cell.CategaryLabel.text = "Today or TOnight"//dataDict["cat_name"] as? String ?? ""
        cell.lblUserName.text = dataDict["username"] as? String ?? ""
        let des = dataDict["description"] as? String ?? ""
        cell.LowerTextLabel.text = des.uppercased()
        
        cell.starRatingView.current = CGFloat(Float(((dataDict["rating"] as? NSString)?.integerValue)!))
        
        cell.DaysLabel.attributedText = NSAttributedString(string: "TODAY OR TONIGHT", attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        cell.DaysLabel.font = UIFont(name: "VerminVibes2Nightclub", size: 40)
        
        cell.starRatingView.isUserInteractionEnabled = true
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPREDCOLOR
        cell.backgroundColor = .clear
        
        cell.CategaryLabel.textColor = .white
        cell.CategaryLabel.type = .leftRight
        cell.CategaryLabel.speed = .rate(80)
        cell.CategaryLabel.fadeLength = 80.0
        cell.CategaryLabel.labelWillBeginScroll()
        cell.CategaryLabel.tag = indexPath.row
        cell.CategaryLabel.restartLabel()
        let like = dataDict["fav_status"] as?  String ?? ""
        if like == "Like"{
            cell.PostedByLabel.setImage(#imageLiteral(resourceName: "like-4"), for: .normal)
        }else{
            cell.PostedByLabel.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
        }
        btnRepot.tag = dataDict["id"] as? Int ?? 0
        btnRepotAnBloack.tag = dataDict["user_id"] as? Int ?? 0
        
        cell.PostedByLabel.tag = indexPath.row
        cell.PostedByLabel.addTarget(self, action: #selector(newFavorateCopoun), for: .touchUpInside)
//        cell.buttonViolation.tag = indexPath.row
        cell.buttonViolation.addTarget(self, action: #selector(buttonViolationAction), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? sendCouponCollectionViewCell else { return }
        cell.CategaryLabel.restartLabel()
    }
    @objc func newFavorateCopoun(sender: UIButton){
        var status = ""
        let dictData = AllDataArray[sender.tag] as! [String:Any]
        let Coupon_Id = dictData["id"] as? String ?? ""
        if sender.currentImage == #imageLiteral(resourceName: "like-4") {
            status = "Unlike"
            UnFavoriteCoupon(couponId: Coupon_Id, status: status)
        }else{
            status = "Like"
            FavoriteCoupon(couponId: Coupon_Id,status:status)
        }  //PostedByLabel.setImage(#imageLiteral(resourceName: "like-4"), for: .normal)
        
        
    }
    func UnFavoriteCoupon(couponId:String,status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL + "delete_favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=\(status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.getHundredCoupons()
                        Utility.sharedInstance.showDismissAlert("", msg: "\(msg)", controller: self)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func FavoriteCoupon(couponId:String,status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL + "favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=\(status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.getHundredCoupons()
                        Utility.sharedInstance.showDismissAlert("", msg: "\(msg)", controller: self)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    @objc func buttonViolationAction(sender:UIButton){
//        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
//        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
//            let dictData = self.AllDataArray[sender.tag] as! [String:Any]
//            let Coupon_Id = dictData["coupon_id"] as! String
//            self.GetRemoveCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String),Coupon_Id)
//        }
//        alertController.addAction(confirmAction)
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        present(alertController, animated: true, completion: nil)
        alertPopUpView.isHidden = false
        
    }
    
    @objc func onClickCancelAlert(sender:UIButton){
        alertPopUpView.isHidden = true
    }
    
    @objc func onClickReportAnBlockAlert(sender:UIButton){
        let dictData = self.AllDataArray[sender.tag] as! [String:Any]
        let Coupon_Id = dictData["user_id"] as! String
        print("Coupon_Id ",Coupon_Id)
        self.GetReportsAndBlockCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String),Coupon_Id)
        alertPopUpView.isHidden = true
    }
    
    @objc func onClickReportAlert(sender:UIButton){
        let dictData = self.AllDataArray[sender.tag] as! [String:Any]
        print("onClickReportAlert~~~",dictData["id"] as! String)
        let Coupon_Id = dictData["id"] as! String
        self.GetReportsCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String),Coupon_Id)
        alertPopUpView.isHidden = true
    }
    
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        
        let dataDict = AllDataArray[view.tag] as! [String:Any]
        
        print("~~~~~StarRatingValueChanged~~~~~~~~")
        submitRating(couponId: dataDict["id"] as! String, rating: value)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        WalletAmount = ""
        getHundredCoupons()
        GetWalletAmount()
    }
    func GetReportsCoupons(_ userId:String,_ couponId:String){
        //http://couplescouponbook.com/ccbadmin/Coupon_api/save_report_coupon
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_report_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!,couponId)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let parameters = "user_id= \(userId)&coupon_id=\(couponId)".data(using: .utf8)
        print("parameters :\(userId),\(couponId)")
        let postData = parameters
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary:\(dataDictionary)")
                let status = dataDictionary.object(forKey: "status") as! String
                print("status :\(status)")
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.getHundredCoupons()
                        Utility.sharedInstance.showAlert("Alert", msg: "Post Blocked", controller: self)
                    })
                }
                else {
                    RappleActivityIndicatorView.stopAnimation()
                    Utility.sharedInstance.showAlert("Alert", msg: dataDictionary.object(forKey: "message") as! String, controller: self)
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func GetReportsAndBlockCoupons(_ userId:String,_ couponId:String){
        //http://couplescouponbook.com/ccbadmin/Coupon_api/blockuser
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"blockuser")!
        print(USER_DEFAULTS.value(forKey: "user_id")!,couponId)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let parameters = "user_id= \(userId)&other_user_id=\(couponId)".data(using: .utf8)
        print("parameters :\(userId),\(couponId)")
        let postData = parameters
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary:\(dataDictionary)")
                let status = dataDictionary.object(forKey: "status") as! String
                print("status :\(status)")
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.getHundredCoupons()
                        Utility.sharedInstance.showAlert("Alert", msg: "Blocked & reported successfully", controller: self)
                    })
                }
                else {
                    RappleActivityIndicatorView.stopAnimation()
                    Utility.sharedInstance.showAlert("Alert", msg: dataDictionary.object(forKey: "message") as! String, controller: self)
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func getHundredCoupons(){
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        let url = URL(string: BASE_URL+"get_hundred_coupons")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            print("response~~~~~~~~~~",response)
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        USER_DEFAULTS.set("0", forKey: "redeem_count")
                        USER_DEFAULTS.set("0", forKey: "sent_coupon")
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print("AllDataArray~~~~~~",self.AllDataArray)
                        DispatchQueue.main.async(execute: {
                            self.couponCollectionView.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.couponCollectionView.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func submitRating(couponId:String,rating:CGFloat){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_rating")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&rating=\(rating)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                print("~~~~~StarRatingValueChanged~~~dataDictionary~~~~~",dataDictionary)
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        Utility.sharedInstance.showDismissAlert("", msg: "Rating submitted.", controller: self)
                        //RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray.removeAll()
                        self.getHundredCoupons()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var errorArr = [String : NSDictionary]()
                    let item1 = NSMutableDictionary()
                    item1.setObject("iOS", forKey: "From" as NSCopying)
                    item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                    item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                    item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                    errorArr["json"] = item1
                    var jsonData = NSData()
                    print(errorArr)
                    if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                        jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                        let jsondata = theJSONText
                        print("jsondata~~~~~~",jsondata)
                        callsaveExceptionLogApi(error: jsondata)
                    }
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func GetWalletAmount(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_wallet_amount")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.WalletAmount = dataDictionary.object(forKey: "amount") as! String
                    })
                }
                else {
                    // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func assignbackgroundd(){
        // let background = #imageLiteral(resourceName: "drawerpic")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        // imageView.image = nil
        /// imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
        imageView.image = UIImage(named: "background-1")!.resizableImage(withCapInsets: .zero)
    }
    
    func SendReferralCoupon(couponTitle:String,couponId:String,email:String,mobille:String,userID:String){
      //  print("SendReferralCoupon~~~~~~~")
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
         RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"refer_coupon")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "coupon_title=\(couponTitle)&user_id=\(userID)&coupon_id=\(couponId)&email=\(email)&mobile=\(mobille)".data(using: .utf8)
        request.httpBody = postData
        //print("SendReferralCoupon~~~~~~~",postData)
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
           // print("dataDictionary~~~~~~",response)
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                let dataDictionary = userObject as! NSDictionary
                let status = dataDictionary.object(forKey: "status") as! String
                let msg = dataDictionary.object(forKey: "message") as! String
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        print("dataDictionary~~~~~~",dataDictionary)
                        Utility.sharedInstance.showDismissAlert("", msg: "Success", controller: self)
                        RappleActivityIndicatorView.stopAnimation()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    //http://couplescouponbook.com/ccbadminnew/Coupon_api/get_ccbusers
    
    func get_ccbusers(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
         RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_ccbusers")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                print("~~~~~get_ccbusers~~~dataDictionary~~~~~",dataDictionary)
                self.userDataArray.removeAll()
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        self.userDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        self.userTableview.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                        self.userView.isHidden = false
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var errorArr = [String : NSDictionary]()
                    let item1 = NSMutableDictionary()
                    item1.setObject("iOS", forKey: "From" as NSCopying)
                    item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                    item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                    item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                    errorArr["json"] = item1
                    var jsonData = NSData()
                    print(errorArr)
                    if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                        jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                        let jsondata = theJSONText
                        print("jsondata~~~~~~",jsondata)
                        callsaveExceptionLogApi(error: jsondata)
                    }
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! userCell
        let dic = userDataArray[indexPath.row] as! [String:Any]
        let username = dic["username"] as! String
        cell.lblUserName.text = username
        cell.lblUserName.textColor = UIColor.white
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.btnSend.tag = indexPath.row
        cell.btnSend.addTarget(self, action: #selector(onClickSend), for: .touchUpInside)
        return cell
    }
    
    @objc func onClickSend(sender:UIButton){
        let shareText = "A user from the Couples Couponbook(CCB) app has sent a naughty coupon.To open & view your coupon click : itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4"
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.userTableview)
        let indexPath = self.userTableview.indexPathForRow(at: buttonPosition)
        let dataDict = userDataArray[indexPath?.row ?? 0] as! [String:Any]
        let user_id = dataDict["id"] as? String ?? ""
        let email = USER_DEFAULTS.value(forKey: "user_email") as! String//user_mobile
        let user_mobile = USER_DEFAULTS.value(forKey: "user_mobile") as! String
        SendReferralCoupon(couponTitle: "TODAY OR TONIGHT", couponId: coupenId, email: email, mobille: user_mobile, userID: user_id)
    }
    
}
