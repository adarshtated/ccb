//
//  HomeViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/22/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import SDWebImage
import RappleProgressHUD

class HomeViewController: SideMenuViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    //  var carousel: iCarousel!
    var NoCouponsLabel:UILabel!
    var DashBoardTableview: UITableView!
    //   var GenderTableView: UITableView!
    var GenderView: UIView!
    var HideGenderViewButton : UIButton!
    var imageArray = [UIImage]()
    var GenderArray = [String]()
    let TextFieldHeight = SHEIGHT/14
    var AllDataArray = [Any]()
    
    var agreementView: UIView!
    var agreementImage:UIImageView!
    var agreementLabel: UILabel!
    var agreementButton: UIButton!
    var scrollView: UIScrollView!
    var StatusChanged = ""
    //  let cellSpacingHeight: CGFloat = 5
    var navBarHeight = CGFloat()
    
    var BlackBackGround: UIView!
    var BookId = ""
    var CategoryName = ""
    let ImageView = UIImageView()
    let TitleLabel = UILabel()
    
    var privacyStatus = ""
    
    var DisclaimerPopUp: UIView!
    var PopUpLabelHeight = CGFloat()
    var EnterDetailsLabel: UILabel!
    var EnterDetailsLabel2: UILabel!
    var EnterDetailsLabel3: UILabel!
    var EnterDetailsLabel4: UILabel!
    var EnterDetailsLabel1: UILabel!
    var AcceptLabel: UILabel!
    var CancelLabel: UILabel!
    var AcceptButton:UIButton!
    var AcceptRadioImage: UIImageView!
    var CancelButton:UIButton!
    var CancelRadioImage:UIImageView!
    
    // ravi
    var logoImg = UIImageView(frame: CGRect(x: SWIDTH/2-SHEIGHT/14*3, y: 74, width: SHEIGHT/14*6, height: 150))
    var betaImg = UIImageView(frame: CGRect(x: 20, y: 10, width: 30, height: 30))
    var btnViewCoupons = UIButton()
    var CouponBackGoundImage:UIImageView!
    var couponBtn = UIButton()
    var sendGiftBtn = UIButton()
    var sendGiftLabel = UILabel()
    var reBuyBtn = UIButton()
    var submitBtn = UIButton()
    var viewButtons = UIView()
    var sentBtn = UIButton()
    var receivedBtn = UIButton()
    var favouiteBtn = UIButton()
    var sentLabel = UILabel()
    var receivedLabel = UILabel()
    var favouiteLabel = UILabel()
    var lineLabel = UILabel()
    var lineLabel1 = UILabel()
    var lineLabel2 = UILabel()
    var lineLabel3 = UILabel()
//    var lineLabel4 = UILabel()
//    var lineLabel5 = UILabel()
//    var acceptLabel = UILabel()
//    var termsLabel = UILabel()
//    var privcyLabel = UILabel()
//    var eulaLabel = UILabel()
//    var trmsSwitch : UISwitch!
//    var privcySwitch : UISwitch!
//    var eulaSwitch : UISwitch!
//    var NoLabel0 = UILabel()
//    var YesLabel0 = UILabel()
//    var NoLabel1 = UILabel()
//    var YesLabel1 = UILabel()
//    var NoLabel2 = UILabel()
//    var YesLabel2 = UILabel()
    var flag = false
    var commonHeight = 0
    var sentLabel1 = String()
    var recivedLabel = String()
    var favoriteLabel = String()
//    var statutoryLable = UILabel()
    
    // alert View ravi
    var commonView:UIView!
    var textViewTerm:UIView!
    var acceptBtn:UIButton!
    var textViewTrmText:String!
    var trmView:TermView?
    
    var seprator: UILabel!
    var floatView:UIView!
    var floatImage:UIImageView!
    var floatingButton = UIButton()
    var confirmAction:UIAlertAction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        self.title = "DASHBOARD"
        viewButtons.isHidden = true
        //self.view.backgroundColor = .black
        
        getUserProfile()
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        let sharebtn2 = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain, target: self, action: #selector(settingAction))
        sharebtn2.tintColor = UIColor.white
        let sharebtn1 = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItems = [sharebtn2,sharebtn]
        navigationItem.leftBarButtonItems = [sharebtn1]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.setUpView()
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        if privacyStatus == "Not Accept" {
            setDisclaimerPopUp()
        }
        
        
        
        
        
        
    }
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    func onToggleView(){
        scrollView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        
        
//        btnViewCoupons.setButtonDesboard(X: 20, Y: logoImg.frame.maxY + 20, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .blue, Title: "View Coupons")
        couponBtn.setButtonDesboard(X: 20, Y: btnViewCoupons.frame.maxY + 20, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .red, Title: "  Coupons")
        viewButtons.frame = CGRect(x: 0, y: couponBtn.frame.maxY+10, width: SWIDTH, height: CGFloat(commonHeight))
        sentBtn.setButton(X: 50, Y: 0, Width: SWIDTH/2, Height:viewButtons.frame.height/3.5 , TextColor: .white, BackColor: .clear, Title: "SENT COUPONS    ")
        sentBtn.titleLabel?.textAlignment = .left
        sentLabel.setLabel(X: sentBtn.frame.maxX+40, Y: 5, Width: 35, Height: 35, TextColor: .white, BackColor: .red, Text: sentLabel1, TextAlignment: .center, Font: .boldSystemFont(ofSize: 14))
        lineLabel.setLabel(X: 0, Y: sentLabel.frame.maxY+10, Width: viewButtons.frame.width, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
        receivedBtn.setButton(X: 50, Y: lineLabel.frame.maxY, Width: SWIDTH/2, Height:viewButtons.frame.height/3.5 , TextColor: .white, BackColor: .clear, Title: "RECEIVED COUPONS")
        receivedBtn.titleLabel?.textAlignment = .left
        receivedLabel.setLabel(X: receivedBtn.frame.maxX+40, Y: lineLabel.frame.maxY+5, Width: 35, Height: 35, TextColor: .white, BackColor: .red, Text: recivedLabel, TextAlignment: .center, Font: .boldSystemFont(ofSize: 14))
        lineLabel1.setLabel(X: 0, Y: receivedLabel.frame.maxY+10, Width: viewButtons.frame.width, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
        favouiteBtn.setButton(X: 50, Y: lineLabel1.frame.maxY, Width: SWIDTH/2, Height:viewButtons.frame.height/3.5 , TextColor: .white, BackColor: .clear, Title: "FAVORITE COUPONS")
        favouiteBtn.titleLabel?.textAlignment = .left
        favouiteLabel.setLabel(X: favouiteBtn.frame.maxX+40, Y: lineLabel1.frame.maxY+5, Width: 35, Height: 35, TextColor: .white, BackColor: .red, Text: favoriteLabel, TextAlignment: .center, Font: .boldSystemFont(ofSize: 14))
        lineLabel2.setLabel(X: 0, Y: favouiteLabel.frame.maxY+5, Width: viewButtons.frame.width, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
//        sendGiftBtn.setButtonDesboard(X: 20, Y: viewButtons.frame.maxY+20, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .red, Title: "  Send A Gift")
//        sendGiftBtn.frame = CGRect(x: 20, y: viewButtons.frame.maxY+10, width: SWIDTH/2.5, height: 40)
        //sendGiftLabel.setLabel(X: 50, Y: sendGiftBtn.frame.maxY+5, Width: SWIDTH/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "SEND AS A GIFT  ", TextAlignment: .center, Font: .boldSystemFont(ofSize: 14))
//        lineLabel3.setLabel(X: 0, Y: sendGiftBtn.frame.maxY+20, Width: SWIDTH, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
        reBuyBtn.setButtonDesboard(X: 20, Y: viewButtons.frame.maxY+10, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .red, Title: "  Re-Buy Coupons")
        reBuyBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        submitBtn.setButtonDesboard(X: 20, Y: reBuyBtn.frame.maxY+20, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: UIColor(displayP3Red: 218/255, green: 145/255, blue: 0/255, alpha: 1), Title: "  Submit A Coupon")
//        lineLabel4.setLabel(X: 0, Y: submitBtn.frame.maxY+20, Width: SWIDTH, Height: 2, TextColor: .white, BackColor: .white, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
//        acceptLabel.setLabel(X: 10, Y: lineLabel4.frame.maxY+15, Width: SWIDTH, Height: 40, TextColor: .white, BackColor: .clear, Text: "PLEASE REVIEW THE POLICIES BELOW:", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
//        acceptLabel.numberOfLines = 0
//        acceptLabel.sizeToFit()
////        print("switch accept label max \(acceptLabel.frame.maxY+10)")
//        termsLabel.setLabel(X: SWIDTH/9, Y: acceptLabel.frame.maxY+5, Width: SWIDTH*0.8/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "TERMS OF SERVICE  ", TextAlignment: .left, Font: .boldSystemFont(ofSize: 12))
//        trmsSwitch.frame = CGRect(x: termsLabel.frame.maxX+22, y: acceptLabel.frame.maxY+10, width: TextFieldHeight, height: TextFieldHeight/4)
//        YesLabel0.setLabel(X: trmsSwitch.frame.origin.x-8-SWIDTH/8, Y: acceptLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
//        NoLabel0.setLabel(X: trmsSwitch.frame.origin.x+trmsSwitch.frame.width+8, Y: acceptLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
//        privcyLabel.frame = CGRect(x: SWIDTH/9, y: termsLabel.frame.maxY+5, width: SWIDTH*0.8/2, height: 40)
//        privcySwitch.frame = CGRect(x: privcyLabel.frame.maxX+22, y: termsLabel.frame.maxY+10, width: TextFieldHeight, height: TextFieldHeight/4) //UISwitch(frame: CGRect(x: privcyLabel.frame.maxX+22, y:termsLabel.frame.maxY+10 , width: TextFieldHeight, height: TextFieldHeight/4))
//        YesLabel1.setLabel(X: privcySwitch.frame.origin.x-8-SWIDTH/8, Y: termsLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
//        NoLabel1.setLabel(X: privcySwitch.frame.origin.x+privcySwitch.frame.width+8, Y: termsLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
//        eulaLabel.setLabel(X: SWIDTH/9, Y: privcyLabel.frame.maxY+5, Width: SWIDTH*0.8/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "EULA  ", TextAlignment: .left, Font: .boldSystemFont(ofSize: 12))
//        eulaSwitch.frame = CGRect(x: eulaLabel.frame.maxX+22, y: privcyLabel.frame.maxY+10, width: TextFieldHeight, height: TextFieldHeight/4)
//        YesLabel2.setLabel(X: eulaSwitch.frame.origin.x-8-SWIDTH/8, Y: privcyLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
//        NoLabel2.setLabel(X: eulaSwitch.frame.origin.x+eulaSwitch.frame.width+8, Y: privcyLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
//        lineLabel5.setLabel(X: 0, Y: NoLabel2.frame.maxY+20, Width: SWIDTH, Height: 2, TextColor: .white, BackColor: .white, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
//        statutoryLable.setLabel(X: 0, Y: lineLabel5.frame.maxY+10, Width: SWIDTH, Height: 20, TextColor: .white, BackColor: .clear, Text: "App and it's contents are for 18 and up only. Use at own risk", TextAlignment: .center, Font: .boldSystemFont(ofSize: 13))
//        statutoryLable.numberOfLines = 0
//        statutoryLable.sizeToFit()
        
       
    }
    
    func setUpView(){
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        scrollView.contentSize.height = 800
        logoImg.image = UIImage(named: "Logo.png")
        //logoImg.alpha = 0.5
        logoImg.contentMode = .scaleAspectFit
        self.view.addSubview(scrollView)
        betaImg.image = UIImage(named: "Beta_icon.png")
       // betaImg.alpha = 0.5
        //self.view.addSubview(scrollView)
        print("logoImg~~~~~",logoImg.frame.width,logoImg.frame.height)
        //  button top coupon ----
        self.scrollView.addSubview(logoImg)
       // self.scrollView.addSubview(betaImg)
        btnViewCoupons.setButtonDesboard(X: 20, Y: logoImg.frame.maxY + 80, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .blue, Title: "  View Coupons")
        btnViewCoupons.addTarget(self, action: #selector(viewCouponAction), for: .touchUpInside)
        btnViewCoupons.contentHorizontalAlignment = .center
//        UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
//            self.btnViewCoupons.alpha = 0
//        }, completion: nil)
       // btnViewCoupons.contentHorizontalAlignment = .left
        self.scrollView.addSubview(btnViewCoupons)
        couponBtn.setButtonDesboard(X: 20, Y: btnViewCoupons.frame.maxY + 20, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .red, Title: "  Coupons")
        //couponBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        couponBtn.addTarget(self, action: #selector(couposAction), for: .touchUpInside)
//        couponBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        couponBtn.contentHorizontalAlignment = .center
        self.scrollView.addSubview(couponBtn)
        print(couponBtn)
        //  button top view show hide ----
        viewButtons.frame = CGRect(x: 0, y: couponBtn.frame.maxY+10, width: SWIDTH, height: CGFloat(commonHeight))
        viewButtons.backgroundColor = UIColor.clear
        self.scrollView.addSubview(viewButtons)
        print(viewButtons)
        //  button top view buttons ----
        sentBtn.setButton(X: 50, Y: 0, Width: SWIDTH/2, Height:viewButtons.frame.height/3.5 , TextColor: .white, BackColor: .clear, Title: "SENT COUPONS     ")
        //sentBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        sentBtn.addTarget(self, action: #selector(sentCouponAction), for: .touchUpInside)
        sentBtn.titleLabel?.textAlignment = .left
        sentBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        sentBtn.titleLabel?.minimumScaleFactor = 0.1
        sentBtn.contentHorizontalAlignment = .left
        self.viewButtons.addSubview(sentBtn)
        //  Label sentBtn ----- ----
        
        if let Sentcount = USER_DEFAULTS.value(forKey: "sent_coupon") as? String {
            if Sentcount == "" {
                sentLabel1 = "0"
            }
            else if Sentcount.count > 2 {
                sentLabel1 = "99+"
            } else {
                sentLabel1 = Sentcount
            }
        }
        sentLabel.setLabel(X: sentBtn.frame.maxX+40, Y: 5, Width: 35, Height: 35, TextColor: .white, BackColor: .red, Text: sentLabel1, TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        sentLabel.layer.cornerRadius = sentLabel.frame.height/2
        sentLabel.layer.masksToBounds = true
        self.viewButtons.addSubview(sentLabel)
        //  Label line first 1 ----- ----
        
        lineLabel.setLabel(X: 0, Y: sentLabel.frame.maxY+10, Width: viewButtons.frame.width, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
        //self.viewButtons.addSubview(lineLabel)
        //  Received button  ----- ----
        
        
        receivedBtn.setButton(X: 50, Y: lineLabel.frame.maxY, Width: SWIDTH/2, Height:viewButtons.frame.height/3.5 , TextColor: .white, BackColor: .clear, Title: "RECEIVED COUPONS")
        receivedBtn.addTarget(self, action: #selector(receivedAction), for: .touchUpInside)
        receivedBtn.titleLabel?.textAlignment = .left
       // receivedBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        receivedBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        receivedBtn.titleLabel?.minimumScaleFactor = 0.1
        receivedBtn.contentHorizontalAlignment = .left
        self.viewButtons.addSubview(receivedBtn)
        //  Recieved sentBtn ----- ----
        
        
        if let Rec_count = USER_DEFAULTS.value(forKey: "count") as? String {
            if Rec_count == "" {
                 recivedLabel = "0"
            }
            else if Rec_count.count > 2 {
                recivedLabel = "99+"
            } else {
                recivedLabel = Rec_count
            }
        } else {
            recivedLabel = "0"
        }
        receivedLabel.setLabel(X: receivedBtn.frame.maxX+40, Y: lineLabel.frame.maxY+5, Width: 35, Height: 35, TextColor: .white, BackColor: .red, Text: recivedLabel, TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        receivedLabel.layer.cornerRadius = receivedLabel.frame.height/2
        receivedLabel.layer.masksToBounds = true
        self.viewButtons.addSubview(receivedLabel)
        //  Label line first 2 ----- ----
       // lineLabel1.setLabel(X: 0, Y: receivedLabel.frame.maxY+10, Width: viewButtons.frame.width, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
        //self.viewButtons.addSubview(lineLabel1)
        
        //  Favorate button  ----- ----
        favouiteBtn.setButton(X: 50, Y: lineLabel1.frame.maxY, Width: SWIDTH/2, Height:viewButtons.frame.height/3.5 , TextColor: .white, BackColor: .clear, Title: "FAVORITE COUPONS")
        favouiteBtn.titleLabel?.textAlignment = .left
        favouiteBtn.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
//favouiteBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        favouiteBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        favouiteBtn.titleLabel?.minimumScaleFactor = 0.1
        favouiteBtn.contentHorizontalAlignment = .left
        self.viewButtons.addSubview(favouiteBtn)
        //  favouiteBtn sentBtn ----- ----
        
        if let fav_count = USER_DEFAULTS.value(forKey: "favorite_coupon_count") as? String {
            if fav_count == "" {
                favoriteLabel = "0"
            }
            else if fav_count.count > 2 {
                favoriteLabel = "99+"
            } else {
                favoriteLabel = fav_count
            }
        } else {
            favoriteLabel = "0"
        }
        
        favouiteLabel.setLabel(X: favouiteBtn.frame.maxX+40, Y: lineLabel1.frame.maxY+5, Width: 35, Height: 35, TextColor: .white, BackColor: .red, Text: favoriteLabel, TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        favouiteLabel.layer.cornerRadius = favouiteLabel.frame.height/2
        favouiteLabel.layer.masksToBounds = true
        self.viewButtons.addSubview(favouiteLabel)
        //  Label line first 2 ----- ----
       // lineLabel2.setLabel(X: 0, Y: favouiteLabel.frame.maxY+5, Width: viewButtons.frame.width, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
        //self.viewButtons.addSubview(lineLabel2)
        
        //  button Send Gift ----
        sendGiftBtn.setButtonDesboard(X: 20, Y: viewButtons.frame.maxY+10, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .red, Title: "  Send A Gift")
        //sendGiftBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        sendGiftBtn.addTarget(self, action: #selector(sendGiftAction1), for: .touchUpInside)
//        sendGiftBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        sendGiftBtn.contentHorizontalAlignment = .center
       // self.scrollView.addSubview(sendGiftBtn)
        //  Label Send Gift ----
//        sendGiftLabel.setLabel(X: 50, Y: sendGiftBtn.frame.maxY+5, Width: SWIDTH/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "SEND AS A GIFT  ", TextAlignment: .center, Font: .boldSystemFont(ofSize: 14))
//        self.scrollView.addSubview(sendGiftLabel)
        //  Label line  3 ----- ----
        lineLabel3.setLabel(X: 0, Y: sendGiftBtn.frame.maxY+20, Width: SWIDTH, Height: 2, TextColor: .white, BackColor: .gray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
       // self.scrollView.addSubview(lineLabel3)
        //  button ReBuy  ----
        reBuyBtn.setButtonDesboard(X: 20, Y: viewButtons.frame.maxY+10, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: .red, Title: "  Re-Buy Coupons")
        reBuyBtn.addTarget(self, action: #selector(reBuyAction), for: .touchUpInside)
        //reBuyBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
//        reBuyBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        reBuyBtn.contentHorizontalAlignment = .center
        self.scrollView.addSubview(reBuyBtn)
        //  button Submit  ----
//        submitBtn = UIButton()
        submitBtn.setButtonDesboard(X: 20, Y: reBuyBtn.frame.maxY+20, Width: SWIDTH/2.5, Height: 40, TextColor: .white, BackColor: UIColor(displayP3Red: 218/255, green: 145/255, blue: 0/255, alpha: 1), Title: "  Submit A Coupon")
        //submitBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        submitBtn.addTarget(self, action: #selector(submitCoupontAction), for: .touchUpInside)
        submitBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        submitBtn.contentHorizontalAlignment = .center
        self.scrollView.addSubview(submitBtn)
        
        commonView = UIView()
        commonView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        commonView.backgroundColor = UIColor.clear
        self.view.addSubview(commonView)
        
        textViewTerm = UITextView()
        textViewTerm.frame = CGRect(x: SWIDTH*0.075, y: SHEIGHT/7, width: SWIDTH*0.85, height: SHEIGHT/1.7)
        textViewTerm.backgroundColor = UIColor.black
        textViewTerm.tintColor = .white
        textViewTerm.layer.cornerRadius = 6
        textViewTerm.layer.borderWidth = 1.5
        commonView.isHidden = true
        textViewTerm.layer.borderColor = UIColor.white.cgColor
        commonView.addSubview(textViewTerm)
        
        acceptBtn = UIButton()
        acceptBtn.setButton(X: textViewTerm.frame.width-100, Y: textViewTerm.frame.height-30, Width: 100, Height: 30, TextColor: .white, BackColor: .blue, Title: "I ACCEPT")
        acceptBtn.layer.cornerRadius = 6
        acceptBtn.layer.masksToBounds = true
        acceptBtn.addTarget(self, action: #selector(acceptTerm), for: .touchUpInside)
        textViewTerm.addSubview(acceptBtn)
        
//        lineLabel5 = UILabel()
//        lineLabel5.setLabel(X: 0, Y: NoLabel2.frame.maxY+20, Width: SWIDTH, Height: 2, TextColor: .white, BackColor: .white, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 12))
//        self.scrollView.addSubview(lineLabel5)
//
//        statutoryLable = UILabel()
//        statutoryLable.setLabel(X: 0, Y: lineLabel5.frame.maxY+10, Width: SWIDTH, Height: 20, TextColor: .white, BackColor: .clear, Text: "App and it's contents are for 18 and up only. Use at own risk", TextAlignment: .center, Font: .systemFont(ofSize: 13))
//        statutoryLable.numberOfLines = 0
//        statutoryLable.sizeToFit()
//        statutoryLable.isUserInteractionEnabled = true
//        let gestu = UITapGestureRecognizer(target: self, action: #selector(statutoryAction))
//        statutoryLable.addGestureRecognizer(gestu)
//        self.scrollView.addSubview(statutoryLable)

//        statutoryLable
    }
    @objc func statutoryAction(){
        print("tap statutoryLable")
        if let customView = Bundle.main.loadNibNamed("StatuoryView", owner: self, options: nil)?.first as? StatuouryWarning {
            print("found view")
            customView.textView.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.showView.layer.borderWidth = 1.5
            customView.showView.layer.borderColor = UIColor.white.cgColor
            customView.acceptButton.layer.borderWidth = 1.5
            customView.acceptButton.layer.borderColor = UIColor.white.cgColor
            customView.declineButton.layer.borderWidth = 1.5
            customView.declineButton.layer.borderColor = UIColor.white.cgColor
            customView.declineButton.layer.cornerRadius = 10
            customView.declineButton.layer.masksToBounds = true
            customView.acceptButton.layer.cornerRadius = 10
            customView.acceptButton.layer.masksToBounds = true
//            customView.acceptButton.addTarget(self, action: #selector(acceptAction), for: .touchUpInside)
            customView.declineButton.addTarget(self, action: #selector(decineAction), for: .touchUpInside)
            self.view.addSubview(customView)
        }
    }
//    @objc func acceptAction(sender:UIButton){
//        print("acceptAction")
//
//    }
    @objc func decineAction(sender:UIButton){
        let AlertController = UIAlertController(title: "", message: "Sorry!! You are not eligible to use this app.", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "OK", style: .destructive) { (UIAlertAction) in
            exit(0)
        }
        AlertController.addAction(OkAction)
        self.present(AlertController, animated: true, completion: nil)
        print("decineAction")
//
    }
//
    @objc func acceptTerm(){
        print("acceptTerm")
        commonView.isHidden = true
    }

    @objc func termsLabelClicked(){
        if let customView = Bundle.main.loadNibNamed("TermsView1", owner: self, options: nil)?.first as? TermView {
            print("found view")
            customView.textView.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.textView.layer.borderWidth = 1.5
            customView.textView.layer.borderColor = UIColor.white.cgColor
            self.view.addSubview(customView)
        }
    }
    @objc func privcyLabelClicked(){
        print("privcyLabelClicked")
        if let customView = Bundle.main.loadNibNamed("PrivacyPolicyView", owner: self, options: nil)?.first as? PrivacyPolicy {
            print("found view")
            customView.textView.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.textView.layer.borderWidth = 1.5
            customView.textView.layer.borderColor = UIColor.white.cgColor
            
            self.view.addSubview(customView)
        }
    }
    @objc func eulaClicked(){
        print("eulaClicked")
        if let customView = Bundle.main.loadNibNamed("EULA", owner: self, options: nil)?.first as? ELUAClass {
            print("found view")
            customView.textViewElua.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.textViewElua.layer.borderWidth = 1.5
            customView.textViewElua.layer.borderColor = UIColor.white.cgColor
            
            self.view.addSubview(customView)
        }
    }
    @objc func AgeSwitchAction(){
        let AlertController = UIAlertController(title: "", message: "Sorry!! You are not eligible to use this app.", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "OK", style: .destructive) { (UIAlertAction) in
            exit(0)
        }
        AlertController.addAction(OkAction)
        self.present(AlertController, animated: true, completion: nil)
    }
    
    @objc func couposAction(){
        print("click")
        flag.toggle()
        guard !flag else {
            viewButtons.isHidden = false
            commonHeight = 145
            self.onToggleView()
            return }
        commonHeight = 0
        viewButtons.isHidden = true
        self.onToggleView()
    }
    @objc func sentCouponAction(){
        print("sentCouponAction")
        navigationController?.pushViewController(SendCouponsViewController(), animated: false)
    }
    @objc func viewCouponAction(){
        print("viewCouponAction")
        navigationController?.pushViewController(ViewAllCouponsViewController(), animated: false)
    }
    @objc func receivedAction(){
        print("receivedAction")
          navigationController?.pushViewController(ReferralCouponViewController(), animated: false)
    }
    @objc func favoriteAction(){
        print("favoriteAction")
         navigationController?.pushViewController(FavouriteCouponsViewController(), animated: false)
    }
    @objc func sendGiftAction1(){
        print("sendGiftAction")
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1444260084"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        }
//         navigationController?.pushViewController(SendGiftViewController(), animated: false)
    }
    @objc func reBuyAction(){
        print("reBuyAction")
        navigationController?.pushViewController(RepurchasedCouponsViewController(), animated: false)
    }
    @objc func submitCoupontAction(){
        print("submitCoupontAction")
        navigationController?.pushViewController(SubmitCouponViewController(), animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        StatusChanged = ""
        GetCategory()
        //  GetBooks(genderr: "", bodyType: "", genderType: "")
    }    
    @objc func agreementButtonAction(){
        self.navigationController?.pushViewController(userAgreementViewController(), animated: false)
    }
    
    @objc func floatingButtonAction(){
        let alertController = UIAlertController(title: "", message: "Have a Credit code ?", preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Enter code for voucher credit"
            textField.keyboardType = .numberPad
            textField.delegate = self
            textField.addTarget(self, action: #selector(self.textFieldDidChange), for: UIControlEvents.editingChanged)
        }
        confirmAction = UIAlertAction(title: "SUBMIT", style: .default) { [weak alertController] _ in
            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
            self.SubmitOTP(giftOTP: textField.text ?? "")
            
        }
        confirmAction.isEnabled = false
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    @objc func textFieldDidChange(sender:UITextField){
        if sender.text?.count ?? 0 > 3 {
            confirmAction.isEnabled = true
        } else {
            confirmAction.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if tableView == GenderTableView{
        //            return GenderArray.count
        //        }
        // else
        //{
        return AllDataArray.count
        //}
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //        if tableView == GenderTableView{
        //            return SHEIGHT/14
        //        }
        //        else {
        return SHEIGHT/12
        // }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        if tableView == GenderTableView{
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "gendercell", for: indexPath) as! SimpleLabelTableViewCell
        //
        //            cell.GenderTextLabel.text = GenderArray[indexPath.row]
        //            cell.GenderTextLabel.clipsToBounds = true
        //            cell.backgroundColor = .clear
        //            cell.selectionStyle = .none
        //            return cell
        //        }
        //            else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dashcell", for: indexPath) as! DashBoardTableViewCell
        
        cell.DownLoadButton.tag = indexPath.row
        cell.DownLoadButton.addTarget(self, action: #selector(DownloadButtonAction), for: .touchUpInside)
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.CategaryLabel.text = dataDict["category_name"] as? String ?? ""
        cell.ImageView.clipsToBounds = true
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
        // }
        // }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if tableView == GenderTableView {
        //            StatusChanged = ""
        //            if StatusChanged == "coupons"
        //            {
        //                if indexPath.row == 0 || indexPath.row == 1 {
        //                    GetCoupons(bookId: BookId, genderr: GenderArray[indexPath.row], bodyType: "", genderType: "")
        //                } else if indexPath.row == 2 || indexPath.row == 3 {
        //                    GetCoupons(bookId: BookId, genderr: "", bodyType: GenderArray[indexPath.row], genderType: "")
        //                } else {
        //                    GetCoupons(bookId: BookId, genderr: "", bodyType: "", genderType: GenderArray[indexPath.row])
        //                }
    }
    //            else {
    //                if indexPath.row == 0 || indexPath.row == 1 {
    //                    GetBooks(genderr: GenderArray[indexPath.row], bodyType: "", genderType: "")
    //                } else if indexPath.row == 2 || indexPath.row == 3 {
    //                    GetBooks(genderr: "", bodyType: GenderArray[indexPath.row], genderType: "")
    //                } else {
    //                    GetBooks(genderr: "", bodyType: "", genderType: GenderArray[indexPath.row])
    //                }
    //            }
    //            HideGenderViewButtonAction()
    //        }
    //    }
    func GetCategory(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_categories")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        // let vc = SHowViewController()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
//                        print(self.AllDataArray)
                        DispatchQueue.main.async(execute: {
//                            self.DashBoardTableview.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
//                        self.DashBoardTableview.reloadData()
                    })
                }
                else {
                    //Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
//                        self.DashBoardTableview.isHidden = true
                        self.NoCouponsLabel.isHidden = true
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func getUserProfile(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_user_profile")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "mobile") as? String, forKey: "user_mobile")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "name") as? String, forKey: "user_name")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "image") as? String, forKey: "user_image")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "email") as? String, forKey: "user_email")
                        
                    })
                }
                else {
                    //Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func updatePrivacyStatus(p_status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"privacy_and_policy_status")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&status=\(p_status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        if p_status == "Not Accept" {
                            self.BlackBackGround.isHidden = true
                            USER_DEFAULTS.set("no", forKey: "isloggedin")
                            USER_DEFAULTS.set("", forKey: "user_id")
                            USER_DEFAULTS.set("", forKey: "user_name")
                            USER_DEFAULTS.set("", forKey: "user_image")
                            USER_DEFAULTS.set("", forKey: "user_password")
                            USER_DEFAULTS.set("", forKey: "user_email")
                            self.navigationController?.pushViewController(ViewController(), animated: false)
                        } else {
                            USER_DEFAULTS.set("yes", forKey: "isloggedin")
                            self.BlackBackGround.isHidden = true
                            self.getUserProfile()
                        }
                        
                    })
                }
                else {
                    //Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    //    @objc func btnFilterAction(){
    //        UIView.animate(withDuration: 0.3, animations: {
    //            self.HideGenderViewButton.isHidden = false
    //            self.GenderView.frame = CGRect(x: SWIDTH/2-15, y: STATUSBARHEIGHT+self.navBarHeight, width: SWIDTH/2, height: (SHEIGHT/14)*CGFloat(self.GenderArray.count))
    //            self.GenderView.alpha = 1
    //            self.GenderTableView.frame = CGRect(x: 0, y: 0, width: self.GenderView.frame.width, height: self.GenderView.frame.height)
    //        })
    //        print("filter")
    //    }
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func settingAction(){
        print("settingAction")
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        UIView.animate(withDuration: 0.3) {
            self.sidebarView.frame=CGRect(x: -SWIDTH, y: 0, width: SWIDTH, height: self.sidebarView.frame.height)
        }
        navigationController?.pushViewController(MyProfileViewController(), animated: true)
    }
    @objc func shareAction(){
        let shareText = "Your admirer from the Couples Couponbook app has sent you a naughty coupon marked “For Your Eyes Only”. Enter the 4 digit code \(random()) to see your naughty coupon.itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4"
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
        ReferDetails(otp:random(), message:shareText)
    }
    @objc func DownloadButtonAction(sender: UIButton) {
        let vc = SHowViewController()
        print(sender.tag)
        
        StatusChanged = "coupons"
        let dictData = AllDataArray[sender.tag] as! [String:Any]
        print(dictData)
        //        BookId = dictData["id"] as! String
        //vc.BookId = dictData["id"] as! String
        CategoryName = dictData["category_name"] as! String
        vc.CategoryName = dictData["id"] as! String
        navigationController?.pushViewController(vc, animated: false)
        //  GetCoupons(bookId: BookId, genderr: "", bodyType: "", genderType: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func assignbackgroundd(){
        //let background = #imageLiteral(resourceName: "drawerpic")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = nil
        // imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    func setDisclaimerPopUp(){
        BlackBackGround = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        BlackBackGround.backgroundColor = UIColor(white: 0, alpha: 0.5)
        BlackBackGround.isHidden = false
        self.navigationController?.view.addSubview(BlackBackGround)
        /*********************--NJ--********************/
        DisclaimerPopUp = UIView(frame: CGRect(x: 20, y: SHEIGHT/3, width: SWIDTH-40, height: SHEIGHT/3))
        DisclaimerPopUp.layer.backgroundColor = UIColor.darkGray.cgColor
        DisclaimerPopUp.layer.cornerRadius = 8
        DisclaimerPopUp.layer.borderColor = UIColor.white.cgColor
        DisclaimerPopUp.layer.borderWidth = 1
        BlackBackGround.addSubview(DisclaimerPopUp)
        /*********************--NJ--********************/
        PopUpLabelHeight = DisclaimerPopUp.frame.height/9
        /*********************--NJ--********************/
        /*********************--NJ--********************/
        EnterDetailsLabel = UILabel()
        EnterDetailsLabel.setLabel(X: 15, Y: 20, Width: DisclaimerPopUp.frame.width-30, Height: PopUpLabelHeight/3+10, TextColor: .white, BackColor: .clear, Text: "Disclaimer", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        EnterDetailsLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        EnterDetailsLabel.attributedText = NSAttributedString(string: "Disclaimer", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        EnterDetailsLabel.adjustsFontSizeToFitWidth = true
        DisclaimerPopUp.addSubview(EnterDetailsLabel)
        /*********************--NJ--********************/
        EnterDetailsLabel1 = UILabel()
        EnterDetailsLabel1.setLabel(X: EnterDetailsLabel.frame.origin.x, Y: EnterDetailsLabel.frame.origin.y+EnterDetailsLabel.frame.height+4, Width: EnterDetailsLabel.frame.width, Height: PopUpLabelHeight, TextColor: .yellow, BackColor: .clear, Text: "I certify that I am at least 18 years of age", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        EnterDetailsLabel1.attributedText = NSAttributedString(string: "I certify that I am at least 18 years of age", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        EnterDetailsLabel1.font = UIFont(name: "Electrolize-Regular", size: 14)
        EnterDetailsLabel1.adjustsFontSizeToFitWidth = true
        DisclaimerPopUp.addSubview(EnterDetailsLabel1)
        /*********************--NJ--********************/
        EnterDetailsLabel2 = UILabel()
        EnterDetailsLabel2.setLabel(X: EnterDetailsLabel1.frame.origin.x, Y: EnterDetailsLabel1.frame.origin.y+EnterDetailsLabel1.frame.height+4, Width: EnterDetailsLabel.frame.width, Height: PopUpLabelHeight, TextColor: .white, BackColor: .clear, Text: "- Use of this application and all of its content is only for user 18 and up", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        EnterDetailsLabel2.font = UIFont(name: "Electrolize-Regular", size: 14)
        EnterDetailsLabel2.adjustsFontSizeToFitWidth = true
        EnterDetailsLabel2.numberOfLines = 2
        EnterDetailsLabel2.lineBreakMode = .byTruncatingTail
        DisclaimerPopUp.addSubview(EnterDetailsLabel2)
        /*********************--NJ--********************/
        EnterDetailsLabel3 = UILabel()
        EnterDetailsLabel3.setLabel(X: EnterDetailsLabel.frame.origin.x, Y: EnterDetailsLabel2.frame.origin.y+EnterDetailsLabel2.frame.height+4, Width: EnterDetailsLabel.frame.width, Height: PopUpLabelHeight, TextColor: .yellow, BackColor: .clear, Text: "Use of all coupon at your own risk", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        EnterDetailsLabel3.attributedText = NSAttributedString(string: "Use of all coupon at your own risk", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        EnterDetailsLabel3.font = UIFont(name: "Electrolize-Regular", size: 14)
        EnterDetailsLabel3.adjustsFontSizeToFitWidth = true
        DisclaimerPopUp.addSubview(EnterDetailsLabel3)
        /*********************--NJ--********************/
        EnterDetailsLabel4 = UILabel()
        EnterDetailsLabel4.setLabel(X: EnterDetailsLabel.frame.origin.x, Y: EnterDetailsLabel3.frame.origin.y+EnterDetailsLabel3.frame.height+4, Width: EnterDetailsLabel.frame.width, Height: PopUpLabelHeight, TextColor: .white, BackColor: .clear, Text: "- Use of all of coupons in the product are to be used at your own risk", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        EnterDetailsLabel4.font = UIFont(name: "Electrolize-Regular", size: 14)
        EnterDetailsLabel4.numberOfLines = 2
        EnterDetailsLabel4.lineBreakMode = .byTruncatingTail
        EnterDetailsLabel4.adjustsFontSizeToFitWidth = true
        DisclaimerPopUp.addSubview(EnterDetailsLabel4)
        /*********************--NJ--********************/
        AcceptRadioImage = UIImageView()
        AcceptRadioImage.setImageView(X: 30, Y: EnterDetailsLabel4.frame.origin.y+EnterDetailsLabel4.frame.height+PopUpLabelHeight*3/4+4, Width: PopUpLabelHeight/2, Height: PopUpLabelHeight/2, img: UIImage(named: "unchecked_box")!)
        AcceptRadioImage.contentMode = UIViewContentMode.scaleAspectFit
        DisclaimerPopUp.addSubview(AcceptRadioImage)
        /********************--NJ--*******************/
        AcceptLabel = UILabel()
        AcceptLabel.setLabel(X: AcceptRadioImage.frame.origin.x+AcceptRadioImage.frame.height+5, Y: AcceptRadioImage.frame.origin.y-PopUpLabelHeight/4, Width: SWIDTH/5, Height: PopUpLabelHeight, TextColor: .white, BackColor: .clear, Text: "Accept", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 14))
        AcceptLabel.adjustsFontSizeToFitWidth = true
        DisclaimerPopUp.addSubview(AcceptLabel)
        /********************--NJ--*******************/
        AcceptButton = UIButton()
        AcceptButton.setButton(X: AcceptRadioImage.frame.origin.x, Y: AcceptLabel.frame.origin.y, Width: AcceptRadioImage.frame.width+AcceptLabel.frame.width+5, Height: PopUpLabelHeight, TextColor: .white, BackColor: UIColor.clear, Title: "")
        AcceptButton.addTarget(self, action: #selector(AcceptButtonAction), for: .touchUpInside)
        DisclaimerPopUp.addSubview(AcceptButton)
        /*********************--NJ--********************/
        /******************--NJ--*********************/
        CancelRadioImage = UIImageView()
        CancelRadioImage.setImageView(X: DisclaimerPopUp.frame.width/2+30, Y: AcceptRadioImage.frame.origin.y, Width: AcceptRadioImage.frame.width, Height: AcceptRadioImage.frame.height, img: UIImage(named: "unchecked_box")!)
        CancelRadioImage.contentMode = UIViewContentMode.scaleAspectFit
        DisclaimerPopUp.addSubview(CancelRadioImage)
        /******************--NJ--*********************/
        CancelLabel = UILabel()
        CancelLabel.setLabel(X: CancelRadioImage.frame.origin.x+CancelRadioImage.frame.height+5, Y: CancelRadioImage.frame.origin.y-PopUpLabelHeight/4, Width: SWIDTH/5, Height: PopUpLabelHeight, TextColor: .white, BackColor: .clear, Text: "Decline", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 14))
        CancelLabel.adjustsFontSizeToFitWidth = true
        DisclaimerPopUp.addSubview(CancelLabel)
        /******************--NJ--*********************/
        CancelButton = UIButton()
        CancelButton.setButton(X: CancelRadioImage.frame.origin.x, Y: CancelLabel.frame.origin.y, Width: CancelRadioImage.frame.width+CancelLabel.frame.width+5, Height: PopUpLabelHeight, TextColor: .white, BackColor: UIColor.clear, Title: "")
        CancelButton.addTarget(self, action: #selector(DeclineButtonAction), for: .touchUpInside)
        DisclaimerPopUp.addSubview(CancelButton)
    }
    /*********************--NJ--********************/
    @objc func AcceptButtonAction(){
        UIView.animate(withDuration: 0.3, animations: {
            self.AcceptRadioImage.image = UIImage(named: "checked_box")!
            self.CancelRadioImage.image = UIImage(named: "unchecked_box")!
        })
        updatePrivacyStatus(p_status: "Accept")
    }
    @objc func DeclineButtonAction(){
        UIView.animate(withDuration: 0.3, animations: {
            self.AcceptRadioImage.image = UIImage(named: "unchecked_box")!
            self.CancelRadioImage.image = UIImage(named: "checked_box")!
        })
        updatePrivacyStatus(p_status: "Not Accept")
    }
    
    func SubmitOTP(giftOTP:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_gift_otp")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&gift_otp=\(giftOTP)&moblie=\(USER_DEFAULTS.value(forKey: "user_mobile")!)&email=\(USER_DEFAULTS.value(forKey: "user_email") ?? "")".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: msg, controller: self)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    
    
}

//https://adarshtated.com/dev/swamiji/api/saveExceptionLog
public func callsaveExceptionLogApi(error:String){
    RappleActivityIndicatorView.startAnimating()
    let url = URL(string: BASE_URL+"saveException")!
    print("error~~~~~~",error)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    let postData = "json=\(error)".data(using: .utf8)
    request.httpBody = postData
    URLSession.shared.dataTask(with: request) {
        (data, response, error) in
        print("response~~~~~~~~~~",response)
        if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
            
            let dataDictionary = userObject as! NSDictionary
            
            let status = dataDictionary.object(forKey: "status") as! NSNumber
            print("response~~~~~~~~~~",status)
            if status == 2000 {
                DispatchQueue.main.async(execute: {
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                })
                RappleActivityIndicatorView.stopAnimation()
            }
        }
        else {
            RappleActivityIndicatorView.stopAnimation()
        }
        }.resume()
}
