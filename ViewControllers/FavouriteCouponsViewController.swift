//
//  FavouriteCouponsViewController.swift
//  Coupon Book
//
//  Created by apple  on 12/12/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import SDWebImage
import MGStarRatingView
import MarqueeLabel
import UPCarouselFlowLayout


class FavouriteCouponsViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource,StarRatingDelegate,UITextFieldDelegate {
    
    var couponCollectionView : UICollectionView!
    
    var NoCouponsLabel:UILabel!
    
    var FavouriteCouponsTableview: UITableView!
    var imageArray = [UIImage]()
    var Status = "Like"
    var AllDataArray = [Any]()
    var navBarHeight = CGFloat()
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    var BlackBackGround: UIView!
    var BookId = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Favorite Coupons"
        
        assignbackground()
        self.view.backgroundColor = .black
        
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
        FavouriteCouponsTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
       // FavouriteCouponsTableview.delegate = self
       // FavouriteCouponsTableview.dataSource = self
        let sharebtn1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))
        sharebtn1.tintColor=UIColor.white
        navigationItem.leftBarButtonItems = [sharebtn1]
       
        FavouriteCouponsTableview.backgroundColor = .clear
        FavouriteCouponsTableview.bounces = false
        FavouriteCouponsTableview.separatorStyle = .none
        FavouriteCouponsTableview.isOpaque = false
        FavouriteCouponsTableview.register(FavouriteCouponsTableviewCell.self, forCellReuseIdentifier: "FavouriteCell")
       // self.view.addSubview(FavouriteCouponsTableview)
        
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(sendCouponCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        /***********************--NJ--*************************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
       // self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
       // self.view.addSubview(FooterImage)
        /******************--NJ--*********************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
    }
    @objc func backAction(){
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! sendCouponCollectionViewCell
        
        
        cell.CountLabel.isHidden = false
        cell.PostedByLabel.isHidden = true
        cell.shareButton.isHidden = true
        cell.LabelByPost.isHidden = false
        
        
        cell.lblUserName.isHidden = true
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = (dataDict["coupon_title"] as? String ?? "").uppercased()
//        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        cell.LowerTextLabel.text = (dataDict["description"] as? String ?? "").uppercased()
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        //cell.DaysLabel.text = dataDict["default_title"] as?  String ?? ""
        cell.DaysLabel.attributedText = NSAttributedString(string: "TODAY OR TONIGHT", attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        cell.DaysLabel.font = UIFont(name: "VerminVibes2Nightclub", size: 40)
        cell.starRatingView.isUserInteractionEnabled = true
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPLIGHTREDCOLOR
        cell.backgroundColor = .clear
        
        cell.btnlike.tag = indexPath.row
        cell.btnlike.isHidden = false
        cell.btnlike.addTarget(self, action: #selector(checkboxClicked), for: .touchUpInside)
        cell.backgroundColor = .clear
        cell.DaysLabel.text = dataDict["default_title"] as? String ?? ""
        
        cell.HeadingLabel.textColor = .white
        cell.HeadingLabel.type = .leftRight
        cell.HeadingLabel.speed = .rate(80)
        cell.HeadingLabel.fadeLength = 80.0
        cell.HeadingLabel.labelWillBeginScroll()
        cell.HeadingLabel.tag = indexPath.row
        cell.HeadingLabel.restartLabel()
        
//        cell.buttonViolation.tag = indexPath.row
//        cell.buttonViolation.addTarget(self, action: #selector(buttonViolationAction), for: .touchUpInside)
//
//        cell.buttonViolation.setImage(UIImage(named:""), for: .normal)
        cell.buttonViolation.isHidden = true
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? sendCouponCollectionViewCell else { return }
        cell.HeadingLabel.restartLabel()
    }
    
    
    @objc func buttonViolationAction(sender:UIButton){
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = self.AllDataArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            //  let Coupon_Title = dictData["coupon_title"] as! String
            self.removeFavoriteCoupons(couponId: Coupon_Id)
//            let RecEmail = Contact_Email
//            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
//            if let googleUrl = NSURL(string: googleUrlString) {
//                UIApplication.shared.openURL(googleUrl as URL)
//            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SHEIGHT/3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCouponsTableviewCell
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
        cell.PostedByLabel.text = "Posted by : \(dataDict["posted_by"] as? String ?? "" )"
        cell.CategaryLabel.text = "Category : \(dataDict["cat_name"] as? String ?? "")"
        cell.btnlike.tag = indexPath.row
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        //  cell.CountLabel.text = dataDict["re_redeem_counts"] as? String ?? ""
        cell.starRatingView.current = dataDict["rating"] as? NSNumber as! CGFloat
        cell.starRatingView.isUserInteractionEnabled = true
        cell.isUserInteractionEnabled = true
        cell.starRatingView.delegate = self
        cell.starRatingView.tag = indexPath.row
        cell.ImageView.clipsToBounds = true
        cell.RedView.backgroundColor = APPYELLOWCOLOR
        cell.btnlike.tag = indexPath.row
        cell.btnlike.addTarget(self, action: #selector(checkboxClicked), for: .touchUpInside)
        cell.backgroundColor = .clear
        cell.DayLabel.text = dataDict["default_title"] as? String ?? ""
        cell.selectionStyle = .none
       
        return cell
    }
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                       // Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
        //ReferDetails(otp:random(), message:shareText)
    }
    func FavoriteCoupon(couponId:String, status:String,tagg:Int){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=Unlike ".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        //RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Coupon Unlike successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    @objc func checkboxClicked(sender: UIButton) {
        print("buttonpressed")
        
        let dictData = AllDataArray[sender.tag] as! [String:Any]
        let Coupon_Id = dictData["coupon_id"] as! String
        removeFavoriteCoupons(couponId: Coupon_Id)
        
//        if sender.isSelected{
//
//            sender.isSelected = false
//            sender.setImage(#imageLiteral(resourceName: "like"), for: .normal)
//            let dictData = AllDataArray[sender.tag] as! [String:Any]
//            let Coupon_Id = dictData["coupon_id"] as! String
//            let Status = dictData["status"] as! String
//
//        }
//        else {
//            sender.isSelected = true
//            sender.setImage(#imageLiteral(resourceName: "like-5"), for: .normal)
//
//
//            //let Status = dictData["status"] as! String
//           // if Status == "Unlike"{
//           // FavoriteCoupon(couponId: Coupon_Id, status:Status, tagg:sender.tag)
//           // GetFavoriteCoupons()
//                sender.isHidden = true
//           // }
//        }
        
        
    }
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        
        let dataDict = AllDataArray[view.tag] as! [String:Any]
        
        
        submitRating(couponId: dataDict["coupon_id"] as! String, rating: value)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        GetFavoriteCoupons()
    }
    func GetFavoriteCoupons(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_favorite_coupons")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print(self.AllDataArray)
                        DispatchQueue.main.async(execute: {
                            self.couponCollectionView.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.couponCollectionView.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func removeFavoriteCoupons(couponId:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&status=Unlike".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    Utility.sharedInstance.showDismissAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray.removeAll()
                        self.GetFavoriteCoupons()
                        
                    })
                }
                else {
                      Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.FavouriteCouponsTableview.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func submitRating(couponId:String,rating:CGFloat){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_rating")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&coupon_id=\(couponId)&rating=\(rating)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        Utility.sharedInstance.showDismissAlert("", msg: "Rating submitted.", controller: self)
                        //RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray.removeAll()
                        self.GetFavoriteCoupons()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
}
