//
//  MyWalletViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/25/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD

class MyWalletViewController: SideMenuViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    var WalletAmount = ""
    
    var scrollView:UIScrollView!
    
    var UpperRedView:UIView!
    var GiftImage: UIImageView!
    var WalletAmountLabel:UILabel!
    var TotalGiftCreditLabel:UILabel!
    var AddCreditButton:UIButton!
    var BackButton : UIButton!
    
    var OtpScrollView: UIScrollView!
    var OtpTextfield: UITextField!
    var SendImage: UIImageView!
    var SendButton: UIButton!
    
    var PrevViewStatus = ""
    
    var TransactionTableView: UITableView!
    
    var AllDataArray = [Any]()
    var SortedDateArray = [String]()
    var DateArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetView()
        self.title = "MY WALLET"
        
        
        // get user Details
        getUserProfile()
        GetTransactions()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        USER_DEFAULTS.set(0, forKey: "section")
        USER_DEFAULTS.set(0, forKey: "row")
        if PrevViewStatus == "reredeem" {
            PrevViewStatus = ""
            navigationController?.popViewController(animated: false)
        } else {
            navigationController?.pushViewController(HomeViewController(), animated: false)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        GetWalletAmount()
    }
    
    func GetWalletAmount(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_wallet_amount")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
//                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.WalletAmount = dataDictionary.object(forKey: "amount") as? String ?? "0"
                        print(self.WalletAmount)
                        self.WalletAmountLabel.text = "$\(self.WalletAmount)"
                        
                    })
                }
                else {
                   // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func SubmitOTP(giftOTP:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"submit_gift_otp")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&gift_otp=\(giftOTP)&moblie=\(USER_DEFAULTS.value(forKey: "user_mobile")!)&email=\(USER_DEFAULTS.value(forKey: "user_email") ?? "")".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        self.OtpTextfield.text = ""
                        self.OtpTextfield.resignFirstResponder()
                        //RappleActivityIndicatorView.stopAnimation()
                        self.GetWalletAmount()
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func SetView(){
        self.view.backgroundColor = .black
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        scrollView.backgroundColor = .clear
        self.view.addSubview(scrollView)
        /******************--NJ--*********************/
        UpperRedView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT/2.8))
        UpperRedView.backgroundColor = APPREDCOLOR
        scrollView.addSubview(UpperRedView)
        /******************--NJ--*********************/
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        UpperRedView.addSubview(BackButton)
        /******************--NJ--*********************/
        GiftImage = UIImageView()
        GiftImage.setImageView(X: 10, Y: BackButton.frame.origin.y+BackButton.frame.height, Width: UpperRedView.frame.height/2, Height: UpperRedView.frame.height/2, img: #imageLiteral(resourceName: "gift5"))
        GiftImage.contentMode = UIViewContentMode.scaleAspectFit
        UpperRedView.addSubview(GiftImage)
        /******************--NJ--*********************/
        WalletAmountLabel = UILabel()
        WalletAmountLabel.setLabel(X: GiftImage.frame.width+GiftImage.frame.origin.x, Y: GiftImage.frame.origin.y+GiftImage.frame.height/3, Width: GiftImage.frame.width, Height: GiftImage.frame.height/4, TextColor: .white, BackColor: .clear, Text: "$ 0", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 35))
        WalletAmountLabel.adjustsFontSizeToFitWidth = true
        UpperRedView.addSubview(WalletAmountLabel)
        /******************--NJ--*********************/
        TotalGiftCreditLabel = UILabel()
        TotalGiftCreditLabel.setLabel(X: WalletAmountLabel.frame.origin.x, Y: WalletAmountLabel.frame.origin.y+WalletAmountLabel.frame.height, Width: SWIDTH/3.7, Height: GiftImage.frame.height/5, TextColor: .white, BackColor: .clear, Text: "Total Gift Credit", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
        TotalGiftCreditLabel.font = UIFont(name: "Electrolize-Regular", size: 16)
        TotalGiftCreditLabel.adjustsFontSizeToFitWidth = true
        UpperRedView.addSubview(TotalGiftCreditLabel)
        /******************--NJ--*********************/
        let yourAttributes : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16),
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        let attributeString = NSMutableAttributedString(string: "Add Credit",
                                                        attributes: yourAttributes)
        /******************--NJ--*********************/
        AddCreditButton = UIButton()
        AddCreditButton.setButton(X: TotalGiftCreditLabel.frame.origin.x+TotalGiftCreditLabel.frame.width+2, Y: TotalGiftCreditLabel.frame.origin.y, Width: SWIDTH/4, Height: TotalGiftCreditLabel.frame.height, TextColor: .white, BackColor: .clear, Title: "")
        AddCreditButton.setAttributedTitle(attributeString, for: .normal)
        AddCreditButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        AddCreditButton.titleLabel?.adjustsFontSizeToFitWidth = true
        AddCreditButton.titleLabel?.textAlignment = .left
        AddCreditButton.addTarget(self, action: #selector(AddCreditButtonAction), for: .touchUpInside)
        UpperRedView.addSubview(AddCreditButton)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        TransactionTableView = UITableView(frame: CGRect(x: 0, y: UpperRedView.frame.height, width: SWIDTH, height: SHEIGHT-UpperRedView.frame.height-SHEIGHT/14-STATUSBARHEIGHT))
        TransactionTableView.backgroundColor = .black
        TransactionTableView.delegate = self
        TransactionTableView.dataSource = self
        TransactionTableView.bounces = false
        TransactionTableView.isOpaque = false
        TransactionTableView.separatorStyle = .none
        TransactionTableView.register(TransactionTableViewCell.self, forCellReuseIdentifier: "cell")
        scrollView.addSubview(TransactionTableView)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        OtpScrollView = UIScrollView(frame: CGRect(x: 0, y: TransactionTableView.frame.origin.y+TransactionTableView.frame.height, width: SWIDTH, height: SHEIGHT/14))
        OtpScrollView.bounces = false
        OtpScrollView.backgroundColor = APPLIGHTGRAYCOLOR
        scrollView.addSubview(OtpScrollView)
        /******************--NJ--*********************/
        OtpTextfield = UITextField()
        OtpTextfield.setTextField(X: 15, Y: 4, Width: OtpScrollView.frame.width-SWIDTH/6, Height: SHEIGHT/14-8, TextColor: UIColor.black, BackColor: APPLIGHTGRAYCOLOR)
        OtpTextfield.delegate = self
        OtpTextfield.placeholder = "Enter OTP for Gift credit.."
        OtpScrollView.addSubview(OtpTextfield)
        /******************--NJ--*********************/
        UITextField.connectTextFields(fields: [OtpTextfield])
        /******************--NJ--*********************/
        SendButton = UIButton()
        SendButton.setButton(X: OtpTextfield.frame.origin.x+OtpTextfield.frame.width+5, Y: OtpTextfield.frame.origin.y, Width: OtpTextfield.frame.height, Height: OtpTextfield.frame.height, TextColor: .clear, BackColor: .clear, Title: "")
        SendButton.addTarget(self, action: #selector(SendButtonAction), for: .touchUpInside)
        SendButton.clipsToBounds = true
        OtpScrollView.addSubview(SendButton)
        /******************--NJ--*********************/
        SendImage = UIImageView()
        SendImage.setImageView(X: SendButton.frame.height/8, Y: SendButton.frame.height/8, Width: (SendButton.frame.height/8)*6, Height: (SendButton.frame.height/8)*6, img: UIImage(named: "sent-mail")!)
        SendImage.clipsToBounds = true
        SendButton.addSubview(SendImage)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DateArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SHEIGHT/7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TransactionTableViewCell
        
        let date = SortedDateArray[indexPath.row]
        let index = SortedDateArray.index(of: date)!
        
        let dict = AllDataArray[index] as! [String:Any]
        
        let typpe = dict["type"] as! String
        
        switch typpe {
        
        case "coupon":
            cell.AmountLabel.backgroundColor = APPSKYBLUECOLOR
            cell.AmountLabel.text = dict["amount"] as? String ?? ""
            cell.DateTimeLabel.text = dict["date"] as? String ?? ""
            cell.UpperDescriptionLAbel.text = "\(dict["title"] as? String ?? "") (Dr)"
            cell.MiddleLabel.text = dict["cat_name"] as? String ?? ""
            break
        case "transaction":
            cell.AmountLabel.backgroundColor = APPSKYBLUECOLOR
            cell.AmountLabel.text = dict["credit"] as? String ?? ""
            cell.DateTimeLabel.text = dict["date"] as? String ?? ""
            cell.UpperDescriptionLAbel.text = dict["title"] as? String ?? ""
            cell.MiddleLabel.text = dict["payment_id"] as? String ?? ""
            
            break
        case "gift":
            cell.AmountLabel.backgroundColor = APPPINKCOLOR
            cell.AmountLabel.text = dict["amount"] as? String ?? ""
            cell.DateTimeLabel.text = dict["date"] as? String ?? ""
            cell.UpperDescriptionLAbel.text = dict["title"] as? String ?? ""
            cell.MiddleLabel.text = dict["name"] as? String ?? ""
            break
        case "credit":
            cell.AmountLabel.backgroundColor = APPPINKCOLOR
            cell.AmountLabel.text = dict["amount"] as? String ?? ""
            cell.DateTimeLabel.text = dict["date"] as? String ?? ""
            cell.UpperDescriptionLAbel.text = dict["title"] as? String ?? ""
            cell.MiddleLabel.text = dict["name"] as? String ?? ""
            break
        default:
            print("Default")
        }
        
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    @objc func AddCreditButtonAction(){
        let vcc = AddMoneyViewController()
         vcc.WalletAmountt = WalletAmount
        navigationController?.pushViewController(vcc, animated: false)
    }
    
    @objc func SendButtonAction(){
        if OtpTextfield.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter OTP", controller: self)
        }
        else {
            
            SubmitOTP(giftOTP: OtpTextfield.text!)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.OtpScrollView.frame = CGRect(x: 0, y: SHEIGHT-330, width: SWIDTH, height: SHEIGHT/3)
        })
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.OtpScrollView.frame = CGRect(x: 0, y: self.TransactionTableView.frame.origin.y+self.TransactionTableView.frame.height, width: SWIDTH, height: SHEIGHT/14)
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
    }
    
    func getUserProfile(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
       // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_user_profile")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "mobile") as? String, forKey: "user_mobile")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "name") as? String, forKey: "user_name")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "image") as? String, forKey: "user_image")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "email") as? String, forKey: "user_email")
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func GetTransactions(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_details")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
//                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        if let C_DataArray = dataDictionary.object(forKey: "cdata") as? [Any] {
                            for c_index in 0..<C_DataArray.count {
                                let c_Dict = C_DataArray[c_index] as! [String:Any]
                                self.AllDataArray.append(c_Dict)
                                self.DateArray.append(c_Dict["date"] as! String)
                            }
                            
                        }
                        
                        if let T_DataArray = dataDictionary.object(forKey: "tdata") as? [Any] {
                            for t_index in 0..<T_DataArray.count {
                                let t_Dict = T_DataArray[t_index] as! [String:Any]
                                self.AllDataArray.append(t_Dict)
                                self.DateArray.append(t_Dict["date"] as! String)
                            }
                        }
                        if let G_DataArray = dataDictionary.object(forKey: "gdata") as? [Any] {
                            for g_index in 0..<G_DataArray.count {
                                let g_Dict = G_DataArray[g_index] as! [String:Any]
                                self.AllDataArray.append(g_Dict)
                                self.DateArray.append(g_Dict["date"] as! String)
                            }
                        }
                        if let Credit_DataArray = dataDictionary.object(forKey: "credit_data") as? [Any] {
                            for credit_index in 0..<Credit_DataArray.count {
                                let credit_Dict = Credit_DataArray[credit_index] as! [String:Any]
                                self.AllDataArray.append(credit_Dict)
                                self.DateArray.append(credit_Dict["date"] as! String)
                            }
                        }
                        let sortedDate = self.DateArray.sorted()//{ ($0["Time"])! > ($1["Time"])!}
                        
                        for index in (0..<sortedDate.count).reversed() {
                            self.SortedDateArray.append(sortedDate[index])
                        }
                        print(self.DateArray)
                        print(self.SortedDateArray)
                        print(self.AllDataArray)
                        
                        self.TransactionTableView.reloadData()
                    })
                }
                else {
                   // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
}


