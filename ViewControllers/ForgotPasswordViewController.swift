//
//  ForgotPasswordViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/22/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    
    var ForgotPasswordLAbel: UILabel!
    var TextLabel: UILabel!
    var ForgotText: UILabel!
    var EmailTextField: SkyFloatingLabelTextField!
    var ResetPasswordButton: UIButton!
    var CouponBackGoundImage:UIImageView!
    let TextFieldHeight = SHEIGHT/15
    
    var userId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.backgroundColor = .white
        
        //self.title = "Forgot Password"
        assignbackground()
        setView()
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        backButton.tintColor=UIColor.white
        self.navigationItem.leftBarButtonItem = backButton
        
        /*****************--NJ--*****************/
        
    }
    func assignbackground(){
        // let background = #imageLiteral(resourceName: "bklogin")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        //  imageView.image = background
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setView() {
//        CouponBackGoundImage = UIImageView()
//        CouponBackGoundImage.setImageView(X: 0, Y: 0, Width: SWIDTH, Height: SHEIGHT, img: #imageLiteral(resourceName: "new_background.png"))
//        CouponBackGoundImage.contentMode = .scaleToFill
//        self.view.addSubview(CouponBackGoundImage)
        
        
        /******************--NJ--*********************/
        ForgotPasswordLAbel = UILabel()
        ForgotPasswordLAbel.setLabel(X: SWIDTH/4, Y: TextFieldHeight*2, Width: SWIDTH/2, Height: TextFieldHeight*3/4, TextColor: .white, BackColor: .clear, Text: "Forgot Password?", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 25))
//        ForgotPasswordLAbel.font = UIFont(name: "Electrolize-Regular", size: 25)
        ForgotPasswordLAbel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(ForgotPasswordLAbel)
        /******************--NJ--*********************/
//        ForgotPasswordLAbel = UILabel()
//        ForgotPasswordLAbel.setLabel(X: SWIDTH/4, Y: TextFieldHeight*2, Width: SWIDTH/2, Height: TextFieldHeight*3/4, TextColor: .black, BackColor: .clear, Text: "Forgot Password?", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 17))
//        ForgotPasswordLAbel.font = UIFont(name: "Electrolize-Regular", size: 17)
//        ForgotPasswordLAbel.adjustsFontSizeToFitWidth = true
//        self.view.addSubview(ForgotPasswordLAbel)
        /******************--NJ--*********************/
        TextLabel = UILabel()
        TextLabel.setLabel(X: 20, Y: ForgotPasswordLAbel.frame.origin.y+ForgotPasswordLAbel.frame.height+TextFieldHeight/6, Width: SWIDTH-40, Height: TextFieldHeight*1.3, TextColor: .white, BackColor: .clear, Text: "We just need your registered Email address to send you password reset instructions.", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 14))
        TextLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        TextLabel.adjustsFontSizeToFitWidth = true
        TextLabel.numberOfLines = 0
        TextLabel.lineBreakMode = .byTruncatingTail
        self.view.addSubview(TextLabel)
        /******************--NJ--*********************/
        EmailTextField = SkyFloatingLabelTextFieldWithIcon()
        EmailTextField.setSkyLabelwithoutIcon(X: 20, Y: TextLabel.frame.origin.y+TextLabel.frame.height+TextFieldHeight/3, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Enter email", PlaceHolder: "Enter email", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1,lineColorr:.gray,SelectedlineColorr:.white, tagg: 1) //APPGREENCOLORs
        EmailTextField.delegate = self
        EmailTextField.keyboardType = .emailAddress
        self.view.addSubview(EmailTextField)
        UITextField.connectTextFields(fields: [EmailTextField])
        /******************--NJ--*********************/
        ResetPasswordButton = UIButton()
        ResetPasswordButton.setButton(X: 20, Y: EmailTextField.frame.origin.y+EmailTextField.frame.height+TextFieldHeight/2, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: APPPINKCOLOR, Title: "Reset Password")//APPGREENCOLOR
        ResetPasswordButton.addTarget(self, action: #selector(ResetPasswordButtonAction), for: .touchUpInside)
        ResetPasswordButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        ResetPasswordButton.titleLabel?.adjustsFontSizeToFitWidth = true
        ResetPasswordButton.layer.cornerRadius = 11
        self.view.addSubview(ResetPasswordButton)
        /******************--NJ--*********************/
        
        
        
    }
    
    @objc func ResetPasswordButtonAction(){
        EmailTextField.resignFirstResponder()
        if EmailTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter your email.", controller: self)
        } else if EmailTextField.text?.isValidEmail(testStr: EmailTextField.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid email.", controller: self)
        }
        else {
            ForgotPassword(emaill: EmailTextField.text!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        navigationController?.navigationBar.barTintColor = APPREDCOLOR
        //UINavigationBar.appearance().barTintColor = APPREDCOLOR
        self.navigationController?.isNavigationBarHidden = true
    }

    func ForgotPassword(emaill:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"forgot_password")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "email=\(emaill)&user_id=\(userId)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    print(dataDictionary)
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        let AlertController = UIAlertController(title: "", message: "Check your email..", preferredStyle: .alert)
                        let OkAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        AlertController.addAction(OkAction)
                        self.present(AlertController, animated: true, completion: nil)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject(url, forKey: "API Name" as NSCopying)
                item1.setObject(error!, forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
