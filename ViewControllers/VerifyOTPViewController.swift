//
//  VerifyOTPViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/23/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD

class VerifyOTPViewController: UIViewController,UITextFieldDelegate {

    var OTPImageView: UIImageView!
    var TextLabel: UILabel!
    var OTPTextField: SkyFloatingLabelTextField!
    var VerifyButton: UIButton!
    var ResendOTPButton: UIButton!
    var CouponBackGoundImage:UIImageView!
    var OTP = ""
    var userId = ""
    var userEmail = ""
    
    let TextFieldHeight = SHEIGHT/15
    
    var NAVBARHEIGHT = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Verify Code"
        /*****************--NJ--*****************/
        NAVBARHEIGHT = (self.navigationController?.navigationBar.frame.height ?? 0)
        /*****************--NJ--*****************/
        setView()
        /*****************--NJ--*****************/
        OTPTextField.delegate = self
        OTPTextField.text = OTP
        /*****************--NJ--*****************/
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        backButton.tintColor=UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationItem.leftBarButtonItem = backButton
        /*****************--NJ--*****************/
        
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setView() {
        
        /******************--NJ--*********************/
//        CouponBackGoundImage = UIImageView()
//        CouponBackGoundImage.setImageView(X: 0, Y: 0, Width: SWIDTH, Height: SHEIGHT, img: #imageLiteral(resourceName: "new_background.png"))
//        CouponBackGoundImage.contentMode = .scaleToFill
//        self.view.addSubview(CouponBackGoundImage)
        /******************--NJ--*********************/
        OTPImageView = UIImageView()
        OTPImageView.setImageView(X: SWIDTH/2-TextFieldHeight*3/4, Y: STATUSBARHEIGHT+NAVBARHEIGHT+TextFieldHeight, Width: TextFieldHeight*1.5, Height: TextFieldHeight*1.5, img: #imageLiteral(resourceName: "smartphone"))
        OTPImageView.contentMode = UIViewContentMode.scaleAspectFit
        self.view.addSubview(OTPImageView)
        /******************--NJ--*********************/
        TextLabel = UILabel()
        TextLabel.setLabel(X: 30, Y: OTPImageView.frame.origin.y+OTPImageView.frame.height+TextFieldHeight/2, Width: SWIDTH-60, Height: TextFieldHeight*1.3, TextColor: .darkGray, BackColor: .clear, Text: "Enter the 4 digit code we sent you via email to continue.", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 14))
        TextLabel.font = UIFont(name: "Electrolize-Regular", size: 14)
        TextLabel.adjustsFontSizeToFitWidth = true
        TextLabel.numberOfLines = 0
        TextLabel.lineBreakMode = .byTruncatingTail
        self.view.addSubview(TextLabel)
        /******************--NJ--*********************/
        OTPTextField = SkyFloatingLabelTextFieldWithIcon()
        OTPTextField.setSkyLabelwithoutIcon(X: 20, Y: TextLabel.frame.origin.y+TextLabel.frame.height+TextFieldHeight/2, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .black, BackColor: .clear, Title: "Enter 4 Digit Code Here", PlaceHolder: "Enter 4 Digit Code Here", TitleColor: .gray,selectedTitleColor:APPGREENCOLOR, LineHeight: 1,lineColorr:.gray,SelectedlineColorr:APPGREENCOLOR, tagg: 4)
        OTPTextField.keyboardType = .numberPad
        addDoneButtonOnKeyboard(textfieldd: OTPTextField)
        self.view.addSubview(OTPTextField)
        UITextField.connectTextFields(fields: [OTPTextField])
        /******************--NJ--*********************/
        VerifyButton = UIButton()
        VerifyButton.setButton(X: 20, Y: OTPTextField.frame.origin.y+OTPTextField.frame.height+TextFieldHeight/2, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: APPPINKCOLOR, Title: "Verify")
        VerifyButton.addTarget(self, action: #selector(VerifyButtonAction), for: .touchUpInside)
        VerifyButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        VerifyButton.titleLabel?.adjustsFontSizeToFitWidth = true
        VerifyButton.layer.cornerRadius = 11
        self.view.addSubview(VerifyButton)
        /******************--NJ--*********************/
        ResendOTPButton = UIButton()
        ResendOTPButton.setButton(X: SWIDTH/3, Y: VerifyButton.frame.origin.y+VerifyButton.frame.height+TextFieldHeight/2, Width: SWIDTH/3, Height: TextFieldHeight*3/4, TextColor: APPSKYBLUECOLOR, BackColor: .clear, Title: "Resend Code")
        ResendOTPButton.addTarget(self, action: #selector(ResendButtonAction), for: .touchUpInside)
        ResendOTPButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        ResendOTPButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.view.addSubview(ResendOTPButton)
    }
    
    @objc func ResendButtonAction(){
        resendOTP()
    }
    
    @objc func VerifyButtonAction(){
        OTPTextField.resignFirstResponder()
        if OTPTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter your OTP.", controller: self)
        } else if (OTPTextField.text?.count)! < 4 {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid OTP.", controller: self)
        }
        else {
            verifyOTP(otp: OTPTextField.text!)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
        navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        navigationController?.navigationBar.barTintColor = APPREDCOLOR
        //UINavigationBar.appearance().barTintColor = APPREDCOLOR
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        OTPTextField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 4 {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    
    func verifyOTP(otp:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"otp_verification")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let postData = "otp=\(otp)&user_id=\(userId)&device_id=\(USER_DEFAULTS.value(forKey: "device_token") ?? "")&device_type=Ios".data(using: .utf8)
        
        
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                       
                        let AlertController = UIAlertController(title: "", message: "Your account verified, now you can Login.", preferredStyle: .alert)
                        let OkAction = UIAlertAction(title: "Login", style: .default) { (UIAlertAction) in
                            self.navigationController?.pushViewController(ViewController(), animated: false)
                        }
                        AlertController.addAction(OkAction)
                        self.present(AlertController, animated: true, completion: nil)
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func resendOTP(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"resend_otp")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "email=\(userEmail)&user_id=\(userId)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "OTP sent.", controller: self)
                        let ottp = dataDictionary.object(forKey: "otp") as! NSNumber
                        self.OTPTextField.text = ottp.stringValue
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
}
