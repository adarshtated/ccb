import UIKit
import RappleProgressHUD

class NotificationsViewController: SideMenuViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    var AllDataArray = [Any]()
     var navBarHeight = CGFloat()
var NotificationTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Notifications"
        
        self.view.backgroundColor = .lightGray
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
        NotificationTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
        NotificationTableview.delegate = self
        NotificationTableview.dataSource = self
        NotificationTableview.backgroundColor = .clear
        NotificationTableview.bounces = false
        NotificationTableview.separatorStyle = .none
        NotificationTableview.isOpaque = false
        NotificationTableview.register(NotificationsTableViewCell.self, forCellReuseIdentifier: "Notificationcell")
        self.view.addSubview(NotificationTableview)
        
        getNotification()
        seenNotifications()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return AllDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return SHEIGHT/11
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notificationcell", for: indexPath) as! NotificationsTableViewCell
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        
        cell.upperText.text = dataDict["title"] as? String ?? ""
        cell.lowerText.text = dataDict["message"] as? String ?? ""
        
        cell.dateLabel.text = SplitDatefromString(dateString: dataDict["created"] as? String ?? "")
        cell.timeLabel.text = SplitTimefromString(dateString: dataDict["created"] as? String ?? "")
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        return cell
    }
    
    func SplitDatefromString(dateString:String) -> String {
        let getString = dateString.components(separatedBy: " ")
        guard getString.count != 0 else {
            return "0"
        }
        return getString[0]
    }
    func SplitTimefromString(dateString:String) -> String {
        let getString = dateString.components(separatedBy: " ")
        guard getString.count != 0 else {
            return "0"
        }
        return getString[1]
    }
    
    func getNotification(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_all_notification")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&email=\(USER_DEFAULTS.value(forKey: "user_email")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        self.NotificationTableview.isHidden = false
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print(self.AllDataArray)
                        self.NotificationTableview.reloadData()
                    })
                }
                else {
                    // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.NotificationTableview.isHidden = true
                        
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func seenNotifications(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"seen_notification")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&email=\(USER_DEFAULTS.value(forKey: "user_email")!)&status=Seen".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    RappleActivityIndicatorView.stopAnimation()
                }
                else {
                    
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
}
