//
//  PrivacyPolicyViewController.swift
//  Coupon Book
//
//  Created by apple  on 12/15/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
class PrivacyPolicyViewController: SideMenuViewController,UITextViewDelegate {
    
    var DisclaimerLabel: UILabel!
    var DisclaimerData: UILabel!
    var scrollView: UIScrollView!
    var BackButton : UIButton!
    var AllDataArray = [Any]()
    var PrivacyLabel: UILabel!
    var PrivacyPolicyLabel: UITextView!
    var PrivacyData: UILabel!
    let TextFieldHeight = SHEIGHT/15
    
    var topView:UIView!
    
    var navBarHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//self.title = "Privacy Policy"
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
       SetView()
        PrivacyPolicy()
    }
    func SetView(){
        topView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: STATUSBARHEIGHT+navBarHeight))
        topView.backgroundColor = .clear
        self.view.addSubview(topView)
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: topView.frame.height, width: SWIDTH, height: SHEIGHT-topView.frame.height))
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        /******************--NJ--*********************/
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        topView.addSubview(BackButton)
        /******************--NJ--*********************/
        PrivacyLabel = UILabel()
        PrivacyLabel.setLabel(X: 15, Y: 5, Width: SWIDTH/3, Height: TextFieldHeight, TextColor: .blue, BackColor: .clear, Text: "Privacy Policy:", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        PrivacyLabel.isHidden = true
        PrivacyLabel.font = UIFont(name: "Electrolize", size: 18)
        PrivacyLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        PrivacyLabel.attributedText = NSAttributedString(string: "Privacy Policy :", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        PrivacyLabel.adjustsFontSizeToFitWidth = true
        scrollView.addSubview(PrivacyLabel)
        /******************--NJ--*********************/
        PrivacyData = UILabel()
        PrivacyData.setLabel(X: PrivacyLabel.frame.origin.x, Y: PrivacyLabel.frame.origin.y+PrivacyLabel.frame.height+10, Width: SWIDTH-30, Height: 20, TextColor: .white, BackColor: .clear, Text: "PrivacyPolicy", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        PrivacyData.font = UIFont(name: "Electrolize-Regular", size: 15)
        PrivacyData.numberOfLines = 0
        PrivacyData.lineBreakMode = .byWordWrapping
        scrollView.addSubview(PrivacyData)
        PrivacyData.isHidden = true
        
        PrivacyPolicyLabel = UITextView()
        PrivacyPolicyLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        PrivacyPolicyLabel.isEditable = false
        PrivacyPolicyLabel.delegate = self
        PrivacyPolicyLabel.isHidden = true
        PrivacyPolicyLabel.isSelectable = false
        scrollView.addSubview(PrivacyPolicyLabel)
        /******************--NJ--*********************/
        DisclaimerLabel = UILabel()
        DisclaimerLabel.setLabel(X: PrivacyData.frame.origin.x, Y: PrivacyData.frame.origin.y+PrivacyData.frame.height+5, Width: SWIDTH/3, Height: TextFieldHeight, TextColor: .blue, BackColor: .clear, Text: "Disclaimer:", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        DisclaimerLabel.font = UIFont(name: "Electrolize", size: 18)
        DisclaimerLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        DisclaimerLabel.attributedText = NSAttributedString(string: "Disclaimer:", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        DisclaimerLabel.adjustsFontSizeToFitWidth = true
        DisclaimerLabel.isHidden = true
        scrollView.addSubview(DisclaimerLabel)
        /******************--NJ--*********************/
        DisclaimerData = UILabel()
        DisclaimerData.setLabel(X: DisclaimerLabel.frame.origin.x, Y: DisclaimerLabel.frame.origin.y+DisclaimerLabel.frame.height+5, Width: SWIDTH-30, Height: 20, TextColor: .white, BackColor: .clear, Text: "Disclaimer", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        DisclaimerData.font = UIFont(name: "Electrolize-Regular", size: 15)
        DisclaimerData.numberOfLines = 0
        DisclaimerData.lineBreakMode = .byWordWrapping
        DisclaimerData.isHidden = true
        scrollView.addSubview(DisclaimerData)
    }
    @objc func backButtonAction(sender: UIBarButtonItem){
        USER_DEFAULTS.set(0, forKey: "section")
        USER_DEFAULTS.set(0, forKey: "row")
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    func heightForView(text:String, font:UIFont, wwidth:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:wwidth, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    func PrivacyPolicy(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"privacy_and_policy")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
//        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
//        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                 DispatchQueue.main.async(execute: {
                if status == "TRUE" {
                    if let dataArrayy = dataDictionary.object(forKey: "data") as? [Any] {
                        let dataDict = dataArrayy[0] as! [String:Any]
                        let HtmlData = dataDict["privacy_and_policy"] as? String ?? ""
                        let disclaimer = dataDict["disclaimer"] as? String ?? ""
                        
                        let plainText = HtmlData.html2String
                        let plainDisText = disclaimer.html2String
                        
                        //let privacyHeight = self.heightForView(text: plainText, font: UIFont(name: "Electrolize-Regular", size: 15)!, wwidth: SWIDTH-30)
                        let disHeight = self.heightForView(text: plainDisText, font: UIFont(name: "Electrolize-Regular", size: 15)!, wwidth: SWIDTH-30)
                        
                        self.PrivacyPolicyLabel.text = plainText
                        
                        self.PrivacyPolicyLabel.isScrollEnabled = false
                        
                        self.PrivacyPolicyLabel.frame = CGRect(x: 15, y: self.PrivacyLabel.frame.origin.y+self.PrivacyLabel.frame.height+5, width: SWIDTH-30, height: self.PrivacyPolicyLabel.contentSize.height+50)
                        self.PrivacyPolicyLabel.sizeToFit()
                        
                        self.PrivacyData.isHidden = true
                        
                        self.PrivacyLabel.isHidden = false
                        self.PrivacyPolicyLabel.isHidden = false
                        self.DisclaimerLabel.isHidden = false
                        self.DisclaimerData.isHidden = false
                        
                        
                        
                        self.PrivacyPolicyLabel.textColor = .white
                        self.PrivacyPolicyLabel.backgroundColor = .clear
                        
                        self.DisclaimerLabel.frame = CGRect(x: self.PrivacyLabel.frame.origin.x, y: self.PrivacyPolicyLabel.frame.origin.y+self.PrivacyPolicyLabel.frame.height+5, width: SWIDTH/3, height: self.TextFieldHeight)
                        self.DisclaimerData.frame = CGRect(x: self.PrivacyLabel.frame.origin.x, y: self.DisclaimerLabel.frame.origin.y+self.DisclaimerLabel.frame.height+5, width: SWIDTH-30, height: disHeight)
                        self.DisclaimerData.text = plainDisText
                        
                        self.scrollView.contentSize = CGSize(width: SWIDTH, height: self.PrivacyLabel.frame.height+self.PrivacyPolicyLabel.frame.height+self.DisclaimerLabel.frame.height+self.DisclaimerData.frame.height+25)
                        
                        
                    }
                        RappleActivityIndicatorView.stopAnimation()
                }
                else {
                    RappleActivityIndicatorView.stopAnimation()
                }
                })
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
    }
    
}
//extension String {
//    var htmlToAttributedString: NSAttributedString? {
//        guard let data = data(using: .utf8) else { return NSAttributedString() }
//        do {
//            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            return NSAttributedString()
//        }
//    }
//    var htmlToString: String {
//        return htmlToAttributedString?.string ?? ""
//    }
//}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
