//
//  SendGiftViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/25/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import MessageUI
import SwiftKeychainWrapper


class SendGiftViewController: SideMenuViewController,UITextFieldDelegate {
    
    let TextFieldHeight = SHEIGHT/15
    
    var scrollView: UIScrollView!
    var BackButton : UIButton!
    var imageViewBack : UIImageView!
    var RecipientNameTextField: SkyFloatingLabelTextField!
    var MobileTextField:SkyFloatingLabelTextField!
    var AgeTextField: SkyFloatingLabelTextField!
    var EmailTextField: SkyFloatingLabelTextField!
    var SendGiftButton: UIButton!
    
    var GiftImage: UIImageView!
    var SendTextLabel: UILabel!
    var OnlyUserLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignbackground()
        SetView()

        // Do any additional setup after loading the view.
    }
    
    
    func SetView(){
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        /******************--NJ--*********************/
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        scrollView.addSubview(BackButton)
        
        /******************--NJ--*********************/
        GiftImage = UIImageView()
        GiftImage.setImageView(X: SWIDTH/2-(SHEIGHT/4.5)/2, Y: BackButton.frame.origin.y+BackButton.frame.height, Width: SHEIGHT/4.5, Height: SHEIGHT/4.5, img: #imageLiteral(resourceName: "gift5"))
        GiftImage.contentMode = UIViewContentMode.scaleAspectFit
        scrollView.addSubview(GiftImage)
        /*****************--NJ--*****************/
        SendTextLabel = UILabel()
        SendTextLabel.setLabel(X: SWIDTH/4, Y: GiftImage.frame.origin.y+GiftImage.frame.height+TextFieldHeight/8, Width: SWIDTH/2, Height: TextFieldHeight*3/4, TextColor: .white, BackColor: .clear, Text: "SEND AS A GIFT", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 23))
        SendTextLabel.font = UIFont(name: "Electrolize-Regular", size: 23)
        SendTextLabel.adjustsFontSizeToFitWidth = true
        scrollView.addSubview(SendTextLabel)
        /*****************--NJ--*****************/
        OnlyUserLabel = UILabel()
        OnlyUserLabel.setLabel(X: 15, Y: SendTextLabel.frame.origin.y+SendTextLabel.frame.height+TextFieldHeight/10, Width: SWIDTH-30, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Text: "Only users 18+ will be recieve the gift", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 19))
        OnlyUserLabel.font = UIFont(name: "Electrolize-Regular", size: 19)
        OnlyUserLabel.adjustsFontSizeToFitWidth = true
        OnlyUserLabel.numberOfLines = 0
        OnlyUserLabel.lineBreakMode = .byTruncatingTail
        scrollView.addSubview(OnlyUserLabel)
        /******************--NJ--*********************/
        RecipientNameTextField = SkyFloatingLabelTextField()
        RecipientNameTextField.setSkyLabelwithoutIcon(X: 20, Y: SHEIGHT/2, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Recipient Name", PlaceHolder: "Recipient Name", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1.5, lineColorr:.gray,SelectedlineColorr:.white, tagg: 1)
        RecipientNameTextField.delegate = self
        scrollView.addSubview(RecipientNameTextField)
        /******************--NJ--*********************/
        MobileTextField = SkyFloatingLabelTextField()
        MobileTextField.setSkyLabelwithoutIcon(X: 20, Y: RecipientNameTextField.frame.origin.y+RecipientNameTextField.frame.height+TextFieldHeight/3.5, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Mobile Number", PlaceHolder: "Mobile Number", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1.5,lineColorr:.gray,SelectedlineColorr:.white, tagg: 2)
        MobileTextField.delegate = self
        MobileTextField.keyboardType = .numberPad
        addDoneButtonOnKeyboard(textfieldd: MobileTextField)
        scrollView.addSubview(MobileTextField)
        /******************--NJ--*********************/
        AgeTextField = SkyFloatingLabelTextField()
        AgeTextField.setSkyLabelwithoutIcon(X: 20, Y: MobileTextField.frame.origin.y+MobileTextField.frame.height+TextFieldHeight/3.5, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Age", PlaceHolder: "Age", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1.5,lineColorr:.gray,SelectedlineColorr:.white, tagg: 3)
        AgeTextField.delegate = self
        AgeTextField.keyboardType = .numberPad
        addDoneButtonOnKeyboard(textfieldd: AgeTextField)
        scrollView.addSubview(AgeTextField)
        /******************--NJ--*********************/
        EmailTextField = SkyFloatingLabelTextField()
        EmailTextField.setSkyLabelwithoutIcon(X: 20, Y: AgeTextField.frame.origin.y+AgeTextField.frame.height+TextFieldHeight/3.5, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: .clear, Title: "Email", PlaceHolder: "Email", TitleColor: .gray,selectedTitleColor:.white, LineHeight: 1.5,lineColorr:.gray,SelectedlineColorr:.white, tagg: 4)
        EmailTextField.delegate = self
        EmailTextField.keyboardType = .emailAddress
        scrollView.addSubview(EmailTextField)
        /******************--NJ--*********************/
        SendGiftButton = UIButton()
        SendGiftButton.setButton(X: 20, Y: EmailTextField.frame.origin.y+EmailTextField.frame.height+TextFieldHeight*3/4, Width: SWIDTH-40, Height: TextFieldHeight, TextColor: .white, BackColor: APPPINKCOLOR, Title: "SEND GIFT")
        SendGiftButton.layer.cornerRadius = SendGiftButton.frame.height/2
        SendGiftButton.addTarget(self, action: #selector(SendGiftButtonAction), for: .touchUpInside)
        scrollView.addSubview(SendGiftButton)
        /******************--NJ--*********************/
        UITextField.connectTextFields(fields: [RecipientNameTextField])
        UITextField.connectTextFields(fields: [EmailTextField])
        /******************--NJ--*********************/
        
        
    }
    @objc func backButtonAction(sender: UIBarButtonItem){
        USER_DEFAULTS.set(0, forKey: "section")
        USER_DEFAULTS.set(0, forKey: "row")
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    @objc func SendGiftButtonAction(){
        
        if RecipientNameTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Recipient name.", controller: self)
        } else if MobileTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Mobile number.", controller: self)
        } else if (MobileTextField.text?.count)! < 8 || (MobileTextField.text?.count)! > 12 {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid Mobile number.", controller: self)
        } else if AgeTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Age.", controller: self)
        } else if Int(AgeTextField.text!)! < 18 {
            Utility.sharedInstance.showAlert("", msg: "Please enter age above 18.", controller: self)
        } else if EmailTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter Email.", controller: self)
        } else if EmailTextField.text?.isValidEmail(testStr: EmailTextField.text!) == false {
            Utility.sharedInstance.showAlert("", msg: "Please enter valid Email.", controller: self)
        } else {
            purchase_product_popup()
            //use this func after iap successful.
            //SendGift(name: RecipientNameTextField.text!, mobile: MobileTextField.text!, age: AgeTextField.text!, email: EmailTextField.text!)
        }
    }
    
    
    @objc func purchase_product_popup(){
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                print("purchased")
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
        let alertController = UIAlertController(title: "Make Purchase", message: "Are you sure you want to purchase this plan?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: self.purchase)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(OKAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func purchase(_:UIAlertAction){
        IAPHandler.shared.purchaseMyProduct(index: 0)
        //use this func after iap successful.
        //SendGift(name: RecipientNameTextField.text!, mobile: MobileTextField.text!, age: AgeTextField.text!, email: EmailTextField.text!)
        
        let saveSuccessful: Bool = KeychainWrapper.standard.set(0.99, forKey: "SendGift")
        
        
//        Add a string value to keychain:
//
//        let saveSuccessful: Bool = KeychainWrapper.standard.set("Some String", forKey: "myKey")
//        Retrieve a string value from keychain:
//
//        let retrievedString: String? = KeychainWrapper.standard.string(forKey: "myKey")
//        Remove a string value from keychain:
//
//        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "myKey")
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight), animated: true)
        } else if textField.tag == 2 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*2), animated: true)
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*3), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*4), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:TextFieldHeight*5), animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 2 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 3 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else if textField.tag == 4 {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 3 {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    func SendGift(name:String,mobile:String,age:String,email:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"send_gift")!
        
        print(String(format: "%.2f",GIFTCHARGE))
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&mobile=\(mobile)&email=\(email)&age=\(age)&name=\(name)&amount=\(String(format: "%.2f",GIFTCHARGE))".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        Utility.sharedInstance.showDismissAlert("", msg: "Success.", controller: self)
                        self.RecipientNameTextField.text = ""
                        self.MobileTextField.text = ""
                        self.AgeTextField.text = ""
                        self.EmailTextField.text = ""
                        
                        if let otp = dataDictionary.object(forKey: "otp") as? NSNumber {
                            //let username = dataDictionary.object(forKey: "username") as! String
//                            let number = "\(USER_DEFAULTS.value(forKey: "user_name") as? String ?? "") has sent you a gift - Please use OTP   for credit Visit Website www.thecouplebook.com for Download iOS/Android Application"
                            let number = "Congratulations!! \(USER_DEFAULTS.value(forKey: "user_name") as? String ?? "") sent you a voucher for the CouplesCouponbook.com mobile app. Download the app and use Voucher Code - \(otp) to get back your initial $2.99."
                            
                            guard let escapedBody = number.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
                                return
                            }
                            guard let messageURL = URL(string: "sms:\(mobile)&body=\(escapedBody)")
                                else { return }
                            if UIApplication.shared.canOpenURL(messageURL) {
                                UIApplication.shared.open(messageURL, options: [:], completionHandler: nil)
                            }
                        }
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func assignbackground(){
       
      // let background = #imageLiteral(resourceName: "background")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
       // imageView.image = background
        imageView.image = nil
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        MobileTextField.resignFirstResponder()
        AgeTextField.resignFirstResponder()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
