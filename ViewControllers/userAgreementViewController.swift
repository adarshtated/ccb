//
//  userAgreementViewController.swift
//  Coupon Book
//
//  Created by apple  on 1/30/19.
//  Copyright © 2019 Scientific web solution. All rights reserved.
//

import UIKit

class userAgreementViewController: UIViewController,UITextViewDelegate {
    
    var DisclaimerLabel: UILabel!
    var DisclaimerData: UILabel!
    var scrollView: UIScrollView!
    var BackButton : UIButton!
    var AllDataArray = [Any]()
    var PrivacyLabel: UILabel!
    var PrivacyPolicyLabel: UITextView!
    var PrivacyData: UILabel!
    let TextFieldHeight = SHEIGHT/15
    
    var topView:UIView!
    
    var navBarHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "Privacy Policy"
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
        SetView()
        setAgreement()
    }
    func SetView(){
        topView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: STATUSBARHEIGHT+navBarHeight))
        topView.backgroundColor = .clear
        self.view.addSubview(topView)
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: topView.frame.height, width: SWIDTH, height: SHEIGHT-topView.frame.height))
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        /******************--NJ--*********************/
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        topView.addSubview(BackButton)
        /******************--NJ--*********************/
        PrivacyLabel = UILabel()
        PrivacyLabel.setLabel(X: 15, Y: 5, Width: SWIDTH-30, Height: TextFieldHeight, TextColor: .blue, BackColor: .clear, Text: "End-User License Agreement (EULA) of Couples Couponbook", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 14))
        PrivacyLabel.isHidden = true
        PrivacyLabel.font = UIFont(name: "Electrolize", size: 18)
        PrivacyLabel.textColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.0)
        PrivacyLabel.attributedText = NSAttributedString(string: "End-User License Agreement (EULA) of Couples Couponbook", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        PrivacyLabel.adjustsFontSizeToFitWidth = true
        PrivacyLabel.numberOfLines = 0
        PrivacyLabel.lineBreakMode = .byTruncatingTail
        scrollView.addSubview(PrivacyLabel)
        /******************--NJ--*********************/
        PrivacyData = UILabel()
        PrivacyData.setLabel(X: PrivacyLabel.frame.origin.x, Y: PrivacyLabel.frame.origin.y+PrivacyLabel.frame.height+10, Width: SWIDTH-30, Height: 20, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 14))
        PrivacyData.font = UIFont(name: "Electrolize-Regular", size: 15)
        PrivacyData.numberOfLines = 0
        PrivacyData.lineBreakMode = .byWordWrapping
        scrollView.addSubview(PrivacyData)
        PrivacyData.isHidden = true
        
        PrivacyPolicyLabel = UITextView()
        PrivacyPolicyLabel.font = UIFont(name: "Electrolize-Regular", size: 15)
        PrivacyPolicyLabel.isEditable = false
        PrivacyPolicyLabel.delegate = self
        PrivacyPolicyLabel.isHidden = true
        PrivacyPolicyLabel.textAlignment = .justified
        PrivacyPolicyLabel.isSelectable = false
        scrollView.addSubview(PrivacyPolicyLabel)
        /******************--NJ--*********************/
    }
    @objc func backButtonAction(sender: UIBarButtonItem){
        USER_DEFAULTS.set(0, forKey: "section")
        USER_DEFAULTS.set(0, forKey: "row")
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    func heightForView(text:String, font:UIFont, wwidth:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:wwidth, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.isNavigationBarHidden = false
    }
    func setAgreement(){
       // DispatchQueue.main.async(execute: {
            let HtmlData = self.agreement
            let plainText = HtmlData.html2String
            self.PrivacyPolicyLabel.text = plainText
            self.PrivacyPolicyLabel.isScrollEnabled = false
            self.PrivacyPolicyLabel.frame = CGRect(x: 15, y: self.PrivacyLabel.frame.origin.y+self.PrivacyLabel.frame.height+5, width: SWIDTH-30, height: self.PrivacyPolicyLabel.contentSize.height+50)
            self.PrivacyPolicyLabel.sizeToFit()
            self.PrivacyData.isHidden = true
            self.PrivacyLabel.isHidden = false
            self.PrivacyPolicyLabel.isHidden = false
            self.PrivacyPolicyLabel.textColor = .white
            self.PrivacyPolicyLabel.backgroundColor = .clear
            self.scrollView.contentSize = CGSize(width: SWIDTH, height: self.PrivacyLabel.frame.height+self.PrivacyPolicyLabel.frame.height)
      //  })
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
    }
    
    let agreement = "This End-User License Agreement (EULA) is a legal agreement between you and Zenon Interactive LLC \nThis EULA agreement governs your acquisition and use of our Couples Couponbook software (Software) directly from Zenon Interactive LLC or indirectly through a Zenon Interactive LLC authorized reseller or distributor (a Reseller).\nPlease read this EULA agreement carefully before completing the installation process and using the Couples Couponbook software. It provides a license to use the Couples Couponbook software and contains warranty information and liability disclaimers.\nIf you register for a free trial of the Couples Couponbook software, this EULA agreement will also govern that trial. By clicking accept or installing and/or using the Couples Couponbook software, you are confirming your acceptance of the Software and agreeing to become bound by the terms of this EULA agreement.\nIf you are entering into this EULA agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity and its affiliates to these terms and conditions. If you do not have such authority or if you do not agree with the terms and conditions of this EULA agreement, do not install or use the Software, and you must not accept this EULA agreement.\nThis EULA agreement shall apply only to the Software supplied by Zenon Interactive LLC herewith regardless of whether other software is referred to or described herein. The terms also apply to any Zenon Interactive LLC updates, supplements, Internet-based services, and support services for the Software, unless other terms accompany those items on delivery. If so, those terms apply. This EULA was created by EULA Template for Couples Couponbook.\nLicense Grant\nZenon Interactive LLC hereby grants you a personal, non-transferable, non-exclusive licence to use the Couples Couponbook software on your devices in accordance with the terms of this EULA agreement.\nYou are permitted to load the Couples Couponbook software (for example a PC, laptop, mobile or tablet) under your control. You are responsible for ensuring your device meets the minimum requirements of the Couples Couponbook software.\nYou are not permitted to:\nEdit, alter, modify, adapt, translate or otherwise change the whole or any part of the Software nor permit the whole or any part of the Software to be combined with or become incorporated in any other software, nor decompile, disassemble or reverse engineer the Software or attempt to do any such things\nReproduce, copy, distribute, resell or otherwise use the Software for any commercial purpose\nAllow any third party to use the Software on behalf of or for the benefit of any third party\nUse the Software in any way which breaches any applicable local, national or international law\nuse the Software for any purpose that Zenon Interactive LLC considers is a breach of this EULA agreement\nIntellectual Property and Ownership\nZenon Interactive LLC shall at all times retain ownership of the Software as originally downloaded by you and all subsequent downloads of the Software by you. The Software (and the copyright, and other intellectual property rights of whatever nature in the Software, including any modifications made thereto) are and shall remain the property of Zenon Interactive LLC.\nZenon Interactive LLC reserves the right to grant licences to use the Software to third parties.\nTermination\nThis EULA agreement is effective from the date you first use the Software and shall continue until terminated. You may terminate it at any time upon written notice to Zenon Interactive LLC.\nIt will also terminate immediately if you fail to comply with any term of this EULA agreement. Upon such termination, the licenses granted by this EULA agreement will immediately terminate and you agree to stop all access and use of the Software. The provisions that by their nature continue and survive will survive any termination of this EULA agreement.\nGoverning Law\nThis EULA agreement, and any dispute arising out of or in connection with this EULA agreement, shall be governed by and construed in accordance with the laws of us.\nEnd-User License Agreement (EULA) of Couples Couponbook\nThis End-User License Agreement (EULA) is a legal agreement between you and Zenon Interactive LLC\nThis EULA agreement governs your acquisition and use of our Couples Couponbook software (Software) directly from Zenon Interactive LLC or indirectly through a Zenon Interactive LLC authorized reseller or distributor (a Reseller).</p><p>Please read this EULA agreement carefully before completing the installation process and using the Couples Couponbook software. It provides a license to use the Couples Couponbook</span> software and contains warranty information and liability disclaimers.</p><p>If you register for a free trial of the Couples Couponbook software, this EULA agreement will also govern that trial. By clicking accept or installing and/or using the Couples Couponbook software, you are confirming your acceptance of the Software and agreeing to become bound by the terms of this EULA agreement.</p><p>If you are entering into this EULA agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity and its affiliates to these terms and conditions. If you do not have such authority or if you do not agree with the terms and conditions of this EULA agreement, do not install or use the Software, and you must not accept this EULA agreement.</p><p>This EULA agreement shall apply only to the Software supplied by Zenon Interactive LLC herewith regardless of whether other software is referred to or described herein. The terms also apply to any Zenon Interactive LLC updates, supplements, Internet-based services, and support services for the Software, unless other terms accompany those items on delivery. If so, those terms apply. This EULA was created by https://eulatemplate.com EULA Template for Couples Couponbook.<h3>License Grant</h3><p>Zenon Interactive LLC hereby grants you a personal, non-transferable, non-exclusive licence to use the Couples Couponbook software on your devices in accordance with the terms of this EULA agreement.</p><p>You are permitted to load the Couples Couponbook software (for example a PC, laptop, mobile or tablet) under your control. You are responsible for ensuring your device meets the minimum requirements of the Couples Couponbook software.</p><p>You are not permitted to:</p><ul><li>Edit, alter, modify, adapt, translate or otherwise change the whole or any part of the Software nor permit the whole or any part of the Software to be combined with or become incorporated in any other software, nor decompile, disassemble or reverse engineer the Software or attempt to do any such things</li><li>Reproduce, copy, distribute, resell or otherwise use the Software for any commercial purpose</li><li>Allow any third party to use the Software on behalf of or for the benefit of any third party</li><li>Use the Software in any way which breaches any applicable local, national or international law</li><li>use the Software for any purpose that Zenon Interactive LLC considers is a breach of this EULA agreement</li></ul><h3>Intellectual Property and Ownership</h3><p>Zenon Interactive LLC shall at all times retain ownership of the Software as originally downloaded by you and all subsequent downloads of the Software by you. The Software (and the copyright, and other intellectual property rights of whatever nature in the Software, including any modifications made thereto) are and shall remain the property of Zenon Interactive LLC.</p><p>Zenon Interactive LLC reserves the right to grant licences to use the Software to third parties.</p><h3>Termination</h3><p>This EULA agreement is effective from the date you first use the Software and shall continue until terminated. You may terminate it at any time upon written notice to Zenon Interactive LLC.</p><p>It will also terminate immediately if you fail to comply with any term of this EULA agreement. Upon such termination, the licenses granted by this EULA agreement will immediately terminate and you agree to stop all access and use of the Software. The provisions that by their nature continue and survive will survive any termination of this EULA agreement.</p><h3>Governing Law</h3><p>This EULA agreement, and any dispute arising out of or in connection with this EULA agreement, shall be governed by and construed in accordance with the laws of country.</p>"
    
}
