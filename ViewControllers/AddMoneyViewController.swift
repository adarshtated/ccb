//
//  AddMoneyViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/27/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD

class AddMoneyViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate, PayPalPaymentDelegate {
    
    var payPalConfig = PayPalConfiguration()
    
    var WalletAmountt = ""
    
    var TopView: UIView!
    var BackButton : UIButton!
    var TitleLabel: UILabel!
    
    var scrollView: UIScrollView!
    var WalletDetailsView: UIView!
    var WalletBalanceLabel: UILabel!
    var WalletAmountLabel: UILabel!
    var WalletImageIcon: UIImageView!
    
    var AmountCollectionView: UICollectionView!
    var AmountTextField:SkyFloatingLabelTextField!
    var AddMoneyButton:UIButton!
    
    var navBarHeight = CGFloat()
    
    var AmountArray = [String]()
    
    var PaymentCompletePopUp : UIView!
    var PaypalImage: UIImageView!
    var SepLabel: UILabel!
    var SuccessImage: UIImageView!
    var SuccessAmountLabel: UILabel!
    var SuccessMessageLabel: UILabel!
    var LowerDetailsView: UIView!
    var WalletTransactionIdNAmeLabel: UILabel!
    var WalletTransactionIdLabel : UILabel!
    var DateNameLabel : UILabel!
    var DateTimeLabel : UILabel!
    var PaymentApproveLabel : UILabel!
    var OkButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        self.title = "Add Money to your wallet"
        self.view.backgroundColor = .black
        
        setUpView()
        
        AmountArray = ["$10","$15","$50","$100"]
        
//        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
//        backButton.tintColor=UIColor.black
//        self.navigationItem.leftBarButtonItem = backButton
        /*****************--NJ--*****************/
        
        self.configurePaypal(strMarchantName: "CouponBook")
        
        
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView(){
        /*****************--NJ--*****************/
        TopView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: STATUSBARHEIGHT+navBarHeight))
        TopView.backgroundColor = APPREDCOLOR
        self.view.addSubview(TopView)
        /******************--NJ--*********************/
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        TopView.addSubview(BackButton)
        /*****************--NJ--*****************/
        TitleLabel = UILabel()
        TitleLabel.setLabel(X: SWIDTH/4, Y: STATUSBARHEIGHT, Width: SWIDTH/2, Height: BackButton.frame.height, TextColor: .white, BackColor: .clear, Text: "Add Money to Your Wallet", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 22))
        TitleLabel.font = UIFont(name: "Electrolize-Regular", size: 22)
        TitleLabel.adjustsFontSizeToFitWidth = true
        TopView.addSubview(TitleLabel)
        /*****************--NJ--*****************/
        scrollView = UIScrollView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-navBarHeight-STATUSBARHEIGHT))
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        /*****************--NJ--*****************/
        /*****************--NJ--*****************/
        WalletDetailsView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT/10))
        WalletDetailsView.backgroundColor = APPREDCOLOR
        scrollView.addSubview(WalletDetailsView)
        /*****************--NJ--*****************/
        WalletBalanceLabel = UILabel()
        WalletBalanceLabel.setLabel(X: 20, Y: WalletDetailsView.frame.height/4, Width: WalletDetailsView.frame.width/2.7, Height: WalletDetailsView.frame.height/2, TextColor: UIColor.white, BackColor: .clear, Text: "Your Wallet Balance", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 17))
        WalletBalanceLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        WalletBalanceLabel.adjustsFontSizeToFitWidth = true
        WalletDetailsView.addSubview(WalletBalanceLabel)
        /*****************--NJ--*****************/
        WalletAmountLabel = UILabel()
        WalletAmountLabel.setLabel(X: WalletBalanceLabel.frame.origin.x+WalletBalanceLabel.frame.width+20, Y: WalletBalanceLabel.frame.origin.y, Width: WalletDetailsView.frame.width/6, Height: WalletDetailsView.frame.height/2, TextColor: UIColor.white, BackColor: .clear, Text: "$\(WalletAmountt)", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 21))
        WalletAmountLabel.adjustsFontSizeToFitWidth = true
        WalletDetailsView.addSubview(WalletAmountLabel)
        /*****************--NJ--*****************/
        WalletImageIcon = UIImageView()
        WalletImageIcon.setImageView(X: WalletDetailsView.frame.width-20-WalletDetailsView.frame.height/2, Y: WalletDetailsView.frame.height/4, Width: WalletDetailsView.frame.height/2, Height: WalletDetailsView.frame.height/2, img: #imageLiteral(resourceName: "addmoneyWallet"))
        WalletDetailsView.addSubview(WalletImageIcon)
        /*****************--NJ--*****************/
        /*****************--NJ--*****************/
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: (SWIDTH-30)/2, height: (SHEIGHT/3-20)/2)
        //layout.minimumLineSpacing = 0
        //layout.minimumInteritemSpacing = 0
        /*****************--NJ--*****************/
        AmountCollectionView = UICollectionView(frame: CGRect(x: 0, y: WalletDetailsView.frame.origin.y+WalletDetailsView.frame.height+10, width: SWIDTH, height: SHEIGHT/3), collectionViewLayout: layout)
        AmountCollectionView.dataSource = self
        AmountCollectionView.bounces = false
        AmountCollectionView.isScrollEnabled = false
        AmountCollectionView.delegate = self
        AmountCollectionView.register(AmountCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        AmountCollectionView.showsVerticalScrollIndicator = false
        AmountCollectionView.backgroundColor = UIColor.black
        scrollView.addSubview(AmountCollectionView)
        /*****************--NJ--*****************/
        /*****************--NJ--*****************/
        AmountTextField = SkyFloatingLabelTextField()
        AmountTextField.setSkyLabelwithoutIcon(X: 20, Y: AmountCollectionView.frame.origin.y+AmountCollectionView.frame.height*1.2, Width: SWIDTH-40, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Title: "Enter Amount", PlaceHolder: "Enter Amount", TitleColor: .white, selectedTitleColor: APPSKYBLUECOLOR, LineHeight: 1.3, lineColorr: .gray, SelectedlineColorr: APPSKYBLUECOLOR, tagg: 1)
        AmountTextField.selectedLineHeight = 2
        AmountTextField.keyboardType = .numberPad
        AmountTextField.delegate = self
        addDoneButtonOnKeyboard(textfieldd: AmountTextField)
        scrollView.addSubview(AmountTextField)
        /*****************--NJ--*****************/
        AddMoneyButton = UIButton()
        AddMoneyButton.setButton(X: 20, Y: AmountTextField.frame.origin.y+AmountTextField.frame.height*1.7, Width: AmountTextField.frame.width, Height: AmountTextField.frame.height, TextColor: .white, BackColor: APPSKYBLUECOLOR, Title: "ADD MONEY")
        AddMoneyButton.layer.cornerRadius = 10
        AddMoneyButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        AddMoneyButton.addTarget(self, action: #selector(AddMoneyButtonAction), for: .touchUpInside)
        scrollView.addSubview(AddMoneyButton)
        /*****************--NJ--*****************/
        /*****************--NJ--*****************/
        /*****************--NJ--*****************/
        PaymentCompletePopUp = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        PaymentCompletePopUp.backgroundColor = .black
        self.navigationController?.view.addSubview(PaymentCompletePopUp)
        /*****************--NJ--*****************/
        PaypalImage = UIImageView()
        PaypalImage.setImageView(X: SWIDTH/3, Y: 10, Width: SWIDTH/3, Height: SHEIGHT/14, img: #imageLiteral(resourceName: "paypal"))
        PaypalImage.contentMode = UIViewContentMode.scaleAspectFit
        PaypalImage.backgroundColor = .clear
        PaymentCompletePopUp.addSubview(PaypalImage)
        /*****************--NJ--*****************/
        SepLabel = UILabel()
        SepLabel.setLabel(X: 0, Y: PaypalImage.frame.origin.y+PaypalImage.frame.height+5, Width: SWIDTH, Height: 1.5, TextColor: .clear, BackColor: .lightGray, Text: "", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 12))
        SepLabel.font = UIFont(name: "Electrolize-Regular", size: 12)
        PaymentCompletePopUp.addSubview(SepLabel)
        /*****************--NJ--*****************/
        SuccessImage = UIImageView()
        SuccessImage.setImageView(X: SWIDTH/2-(SHEIGHT/10)/2, Y: PaypalImage.frame.origin.y+PaypalImage.frame.height+(SHEIGHT/10)*3/4, Width: SHEIGHT/10, Height: SHEIGHT/10, img: #imageLiteral(resourceName: "success"))
        SuccessImage.contentMode = UIViewContentMode.scaleAspectFit
        SuccessImage.backgroundColor = .clear
        PaymentCompletePopUp.addSubview(SuccessImage)
        /*****************--NJ--*****************/
        SuccessAmountLabel = UILabel()
        SuccessAmountLabel.setLabel(X: SWIDTH/3, Y: SuccessImage.frame.origin.y+SuccessImage.frame.height*1.2, Width: SWIDTH/3, Height: SHEIGHT/14, TextColor: .white, BackColor: .clear, Text: "$0", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 26))
        SuccessAmountLabel.font = UIFont(name: "Electrolize-Regular", size: 26)
        SuccessAmountLabel.adjustsFontSizeToFitWidth = true
        PaymentCompletePopUp.addSubview(SuccessAmountLabel)
        /*****************--NJ--*****************/
        SuccessMessageLabel = UILabel()
        SuccessMessageLabel.setLabel(X: SWIDTH/3, Y: SuccessAmountLabel.frame.origin.y+SuccessAmountLabel.frame.height, Width: SWIDTH/3, Height: SHEIGHT/14, TextColor: .white, BackColor: .clear, Text: "Payment Successful", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 24))
        SuccessMessageLabel.font = UIFont(name: "Electrolize-Regular", size: 24)
        SuccessMessageLabel.adjustsFontSizeToFitWidth = true
        PaymentCompletePopUp.addSubview(SuccessMessageLabel)
        /*****************--NJ--*****************/
        LowerDetailsView = UIView(frame: CGRect(x: 15, y: SuccessMessageLabel.frame.origin.y+SuccessMessageLabel.frame.height*1.4, width: SWIDTH-30, height: SHEIGHT/3.2))
        LowerDetailsView.layer.cornerRadius = 10
        LowerDetailsView.layer.borderColor = UIColor.lightGray.cgColor
        LowerDetailsView.layer.borderWidth = 1.5
        LowerDetailsView.backgroundColor = .clear
        LowerDetailsView.clipsToBounds = true
        PaymentCompletePopUp.addSubview(LowerDetailsView)
        /*****************--NJ--*****************/
        WalletTransactionIdNAmeLabel = UILabel()
        WalletTransactionIdNAmeLabel.setLabel(X: LowerDetailsView.frame.width/3, Y: (LowerDetailsView.frame.height/6)/2, Width: LowerDetailsView.frame.width/3, Height: LowerDetailsView.frame.height/6, TextColor: .white, BackColor: .clear, Text: "Wallet Txn. Id.", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        WalletTransactionIdNAmeLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        WalletTransactionIdNAmeLabel.adjustsFontSizeToFitWidth = true
       // LowerDetailsView.addSubview(WalletTransactionIdNAmeLabel)
        /*****************--NJ--*****************/
        WalletTransactionIdLabel = UILabel()
        WalletTransactionIdLabel.setLabel(X: LowerDetailsView.frame.width/4, Y: WalletTransactionIdNAmeLabel.frame.height/2+WalletTransactionIdNAmeLabel.frame.origin.y, Width: LowerDetailsView.frame.width/2, Height: LowerDetailsView.frame.height/6, TextColor: .gray, BackColor: .clear, Text: "00", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 19))
        WalletTransactionIdLabel.font = UIFont(name: "Electrolize-Regular", size: 19)
        WalletTransactionIdLabel.adjustsFontSizeToFitWidth = true
        LowerDetailsView.addSubview(WalletTransactionIdLabel)
        /*****************--NJ--*****************/
        DateNameLabel = UILabel()
        DateNameLabel.setLabel(X: LowerDetailsView.frame.width/3, Y: WalletTransactionIdLabel.frame.height+WalletTransactionIdLabel.frame.origin.y, Width: LowerDetailsView.frame.width/3, Height: LowerDetailsView.frame.height/6, TextColor: .white, BackColor: .clear, Text: "Date", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        DateNameLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        DateNameLabel.adjustsFontSizeToFitWidth = true
        LowerDetailsView.addSubview(DateNameLabel)
        /*****************--NJ--*****************/
        DateTimeLabel = UILabel()
        DateTimeLabel.setLabel(X: LowerDetailsView.frame.width/4, Y: DateNameLabel.frame.height+DateNameLabel.frame.origin.y, Width: LowerDetailsView.frame.width/2, Height: LowerDetailsView.frame.height/6, TextColor: .gray, BackColor: .clear, Text: "0000-00-00 00:00", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 19))
        DateTimeLabel.font = UIFont(name: "Electrolize-Regular", size: 19)
        DateTimeLabel.adjustsFontSizeToFitWidth = true
        LowerDetailsView.addSubview(DateTimeLabel)
        /*****************--NJ--*****************/
        PaymentApproveLabel = UILabel()
        PaymentApproveLabel.setLabel(X: LowerDetailsView.frame.width/4, Y: DateTimeLabel.frame.height+DateTimeLabel.frame.origin.y, Width: LowerDetailsView.frame.width/2, Height: LowerDetailsView.frame.height/6, TextColor: .gray, BackColor: .clear, Text: "approved", TextAlignment: .center, Font: UIFont.systemFont(ofSize: 19))
        PaymentApproveLabel.font = UIFont(name: "Electrolize-Regular", size: 19)
        PaymentApproveLabel.adjustsFontSizeToFitWidth = true
        LowerDetailsView.addSubview(PaymentApproveLabel)
        /*****************--NJ--*****************/
        /*****************--NJ--*****************/
        OkButton = UIButton()
        OkButton.setButton(X: SWIDTH/3, Y: LowerDetailsView.frame.origin.y+LowerDetailsView.frame.height+SHEIGHT/15, Width: SWIDTH/3, Height: SHEIGHT/13, TextColor: .white, BackColor: APPSKYBLUECOLOR, Title: "OK")
        OkButton.layer.cornerRadius = OkButton.frame.height/2
        OkButton.addTarget(self, action: #selector(OkButtonAction), for: .touchUpInside)
        PaymentCompletePopUp.addSubview(OkButton)
        /*****************--NJ--*****************/
        self.PaymentCompletePopUp.alpha = 0
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AmountArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AmountCollectionViewCell
        
        cell.AmountLabel.text = AmountArray[indexPath.row]
        cell.backgroundColor = .clear
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AmountTextField.text = (AmountArray[indexPath.row]).replacingOccurrences(of: "$", with: "")
    }
    
    @objc func AddMoneyButtonAction(){
        
        if AmountTextField.text?.isEmpty == true {
            Utility.sharedInstance.showAlert("", msg: "Please enter amount.", controller: self)
        }
        else {
            self.navigationController?.isNavigationBarHidden = true
            goforPayNow(totalAmount: AmountTextField.text!)
            
        }
        
    }
    
    
    func addDoneButtonOnKeyboard(textfieldd: UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneeButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfieldd.inputAccessoryView = doneToolbar
    }
    
    @objc func doneeButtonAction() {
        
        AmountTextField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x:0,y:SHEIGHT/7.5), animated: true)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
        navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        navigationController?.navigationBar.barTintColor = APPREDCOLOR
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    //It will provide access to the card too for the payment.
    
    func acceptCreditCards() -> Bool {
        
        return self.payPalConfig.acceptCreditCards
        
    }
    
    func setAcceptCreditCards(acceptCreditCards: Bool) {
        
        self.payPalConfig.acceptCreditCards = self.acceptCreditCards()
        
    }
    
    //PayPalPaymentDelegate methods
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        paymentViewController.dismiss(animated: true) { () -> Void in
            Utility.sharedInstance.showAlert("", msg: "Transaction Cancelled.", controller: self)
            print("and Dismissed")
            self.navigationController?.isNavigationBarHidden = true
        }
        print("Payment cancel")
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        self.navigationController?.isNavigationBarHidden = true
        paymentViewController.dismiss(animated: true) { () -> Void in
            print("and done")
            print("\(String(describing: completedPayment.softDescriptor))")
            
            let Datadict = completedPayment.confirmation as NSDictionary
            print((Datadict.value(forKey: "response") as AnyObject).value(forKey: "id") as! String)
            
            print(Datadict)
            
            if let t_Dict = Datadict.value(forKey: "response") as? [String:Any] {
                if let t_Id = t_Dict["id"] as? String {
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy/MM/dd HH:mm"
                    let someDateTime = formatter.string(from: Date())
                    
                    self.WalletTransactionIdLabel.text = t_Id
                    self.DateTimeLabel.text = someDateTime//(t_Dict["create_time"] as! String)
                    self.SuccessAmountLabel.text = "$" + self.AmountTextField.text!
                    
                    self.PayApi(payId: t_Id, currency_code: "USD", amount: self.AmountTextField.text!, date: self.DateTimeLabel.text!)
                }
             }
            
            
            UIView.animate(withDuration: 0.3, animations: {
                self.PaymentCompletePopUp.alpha = 1
            })
          //  self.navigationController?.popViewController(animated: true)
        }
        print("Paymane is going on")
    }
    
    @objc func OkButtonAction(){
        self.PaymentCompletePopUp.alpha = 0
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Set environment connection.
    
    var environment:String = PayPalEnvironmentNoNetwork {
        
        willSet(newEnvironment) {
            
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    //Configure paypal and set Marchant Name
    
    func configurePaypal(strMarchantName:String) {
        
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = self.acceptCreditCards();
        
        payPalConfig.merchantName = strMarchantName
        
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")! as URL
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")! as URL
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .payPal;
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        // self.environment = PayPalEnvironmentSandbox
        PayPalMobile.preconnect(withEnvironment: environment)
        
    }
    
    
    
    //Start Payment for selected shopping items
    
    func goforPayNow(totalAmount:String) {
        
        let Total : Float = (Float)((totalAmount as NSString).floatValue)
        
        let intTotal = Int(Total)
        
        let usdString : String = String("\(intTotal).00")
        
        let item1 = PayPalItem(name: "Title", withQuantity: 1, withPrice: NSDecimalNumber(string:usdString), withCurrency: "USD", withSku: "Hip-0037")
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        payPalConfig.acceptCreditCards = true
        
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        print(subtotal)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "CouponBook", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable)
        {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            print("Payment not processalbe: \(payment)")
        }
    }
    
    func PayApi(payId:String,currency_code:String,amount:String,date:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        // RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"payments")!

//        :2018-08-06 12:35
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&payment_id=\(payId)&currency_code=\(currency_code)&amount=\(amount)&payment_status=success&date=\(date)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
        
        
    }
    
    
}
