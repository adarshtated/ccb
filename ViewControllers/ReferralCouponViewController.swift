//
//  ReferralCouponViewController.swift
//  Coupon Book
//
//  Created by apple  on 11/14/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import UPCarouselFlowLayout

class ReferralCouponViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate {
    
    var PendingCollectionView : UICollectionView!
    var AcceptedCollectionView : UICollectionView!
    var DeclinedCollectionView : UICollectionView!
    
    var NoCouponsLabel:UILabel!
    
    var ReferralCouponTableview: UITableView!
    var imageArray = [UIImage]()
    
    var AllDataArray = [Any]()
    var referIDArray = [String]()
    var WalletAmount = ""
    var navBarHeight = CGFloat()
    var CouponStatus = ""
    var referId = ""
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    
    var ButtonView: UIView!
    var PendingButton: UIButton!
    var AcceptedButton: UIButton!
    var DeclinedButton: UIButton!
    
    var infoView: UIView!
    var infoImageView: UIImageView!
    
    var PendingScrollView: UIScrollView!
    var PendingShowing: Bool = true
    var AcceptedScrollView: UIScrollView!
    var AcceptedShowing: Bool = false
    var DeclinedScrollView: UIScrollView!
    var DeclinedShowing: Bool = false
    
    var AcceptedCouponTableview: UITableView!
    var DeclinedCouponTableview: UITableView!
    var slidingLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Received Coupon"
        
        assignbackgroundd()
//        self.view.backgroundColor = .black
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        let sharebtn1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))
        sharebtn1.tintColor=UIColor.white
        navigationItem.leftBarButtonItems = [sharebtn1]        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        imageArray = [#imageLiteral(resourceName: "c1"),#imageLiteral(resourceName: "c2"),#imageLiteral(resourceName: "c4")]
        
        /******************--NJ--******************/
        ButtonView = UIView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/14.5))
        ButtonView.backgroundColor = APPREDCOLOR
        self.view.addSubview(ButtonView)
        /******************--NJ--******************/
        PendingButton = UIButton()
        PendingButton.setButton(X: 0, Y: 0, Width: ButtonView.frame.width/3, Height: ButtonView.frame.height-2, TextColor: .white, BackColor: .clear, Title: "PENDING")
        PendingButton.addTarget(self, action: #selector(PendingButtonAction), for: .touchUpInside)
        ButtonView.addSubview(PendingButton)
        /******************--NJ--******************/
        /******************--NJ--******************/
        AcceptedButton = UIButton()
        AcceptedButton.setButton(X: ButtonView.frame.width/3, Y: 0, Width: ButtonView.frame.width/3, Height: ButtonView.frame.height-2, TextColor: .white, BackColor: .clear, Title: "ACCEPTED")
        AcceptedButton.addTarget(self, action: #selector(AcceptedButtonAction), for: .touchUpInside)
        ButtonView.addSubview(AcceptedButton)
        /******************--NJ--******************/
        /******************--NJ--******************/
        DeclinedButton = UIButton()
        DeclinedButton.setButton(X: 2*ButtonView.frame.width/3, Y: 0, Width: ButtonView.frame.width/3, Height: ButtonView.frame.height-2, TextColor: .white, BackColor: .clear, Title: "DECLINED")
        DeclinedButton.addTarget(self, action: #selector(DeclinedButtonAction), for: .touchUpInside)
        ButtonView.addSubview(DeclinedButton)
        /******************--NJ--******************/
        slidingLabel = UILabel()
        slidingLabel.setLabel(X: 0, Y: ButtonView.frame.height-2, Width: ButtonView.frame.width/3, Height: 2, TextColor: .clear, BackColor: .white, Text: "", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 12))
        ButtonView.addSubview(slidingLabel)
        /******************--NJ--******************/
        /******************--NJ--******************/
        PendingScrollView = UIScrollView(frame: CGRect(x: 0, y: ButtonView.frame.origin.y+ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-ButtonView.frame.origin.y-ButtonView.frame.height))
        PendingScrollView.backgroundColor = .clear
        PendingScrollView.bounces = false
        self.view.addSubview(PendingScrollView)
        /******************--NJ--******************/
        /******************--NJ--******************/
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/2.5)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        PendingCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: PendingScrollView.frame.height), collectionViewLayout: layout)
        PendingCollectionView.backgroundColor = .clear
        PendingCollectionView.delegate = self
        PendingCollectionView.dataSource = self
        PendingCollectionView.register(ReferralCouponCollectionViewCell.self, forCellWithReuseIdentifier: "pendingcell")
        PendingScrollView.addSubview(PendingCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--******************/
        ReferralCouponTableview = UITableView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: PendingScrollView.frame.height))
       // ReferralCouponTableview.delegate = self
       // ReferralCouponTableview.dataSource = self
        ReferralCouponTableview.backgroundColor = .clear
        ReferralCouponTableview.bounces = false
        ReferralCouponTableview.separatorStyle = .none
        ReferralCouponTableview.isOpaque = false
        ReferralCouponTableview.register(ReferralCouponTableViewCell.self, forCellReuseIdentifier: "couponcell")
       // PendingScrollView.addSubview(ReferralCouponTableview)
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT/2.9))
        HeaderImage.image = UIImage(named: "bottom_layer")
      //  PendingScrollView.addSubview(HeaderImage)
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        /***********************--NJ--*************************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: 2*PendingScrollView.frame.height/3, width: SWIDTH, height: SHEIGHT/2.9))
        FooterImage.image = UIImage(named: "top_layer")
      //  PendingScrollView.addSubview(FooterImage)
        /******************--NJ--*********************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        /******************--NJ--*********************/
        /******************--NJ--******************/
        AcceptedScrollView = UIScrollView(frame: CGRect(x: SWIDTH, y: ButtonView.frame.origin.y+ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-ButtonView.frame.origin.y-ButtonView.frame.height))
        AcceptedScrollView.backgroundColor = .clear
        AcceptedScrollView.bounces = false
        self.view.addSubview(AcceptedScrollView)
        /***********************--NJ--*************************/
        let layout1 = UPCarouselFlowLayout()
        layout1.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout1.scrollDirection = .vertical
        layout1.minimumLineSpacing = 0
        layout1.minimumInteritemSpacing = 0
        AcceptedCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: AcceptedScrollView.frame.height), collectionViewLayout: layout1)
        AcceptedCollectionView.backgroundColor = .clear
        AcceptedCollectionView.delegate = self
        AcceptedCollectionView.dataSource = self
        AcceptedCollectionView.register(ReferralCouponCollectionViewCell.self, forCellWithReuseIdentifier: "acc_cell")
        AcceptedScrollView.addSubview(AcceptedCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--******************/
        AcceptedCouponTableview = UITableView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: PendingScrollView.frame.height))
       // AcceptedCouponTableview.delegate = self
       // AcceptedCouponTableview.dataSource = self
        AcceptedCouponTableview.backgroundColor = .clear
        AcceptedCouponTableview.bounces = false
        AcceptedCouponTableview.separatorStyle = .none
        AcceptedCouponTableview.isOpaque = false
        AcceptedCouponTableview.register(ReferralCouponTableViewCell.self, forCellReuseIdentifier: "acc_couponcell")
       // AcceptedScrollView.addSubview(AcceptedCouponTableview)
        /******************--NJ--******************/
        /******************--NJ--******************/
        DeclinedScrollView = UIScrollView(frame: CGRect(x: SWIDTH, y: ButtonView.frame.origin.y+ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-ButtonView.frame.origin.y-ButtonView.frame.height))
        DeclinedScrollView.backgroundColor = .clear
        DeclinedScrollView.bounces = false
        self.view.addSubview(DeclinedScrollView)
        /***********************--NJ--*************************/
        let layout2 = UPCarouselFlowLayout()
        layout2.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout2.scrollDirection = .vertical
        layout2.minimumLineSpacing = 0
        layout2.minimumInteritemSpacing = 0
        DeclinedCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: DeclinedScrollView.frame.height), collectionViewLayout: layout2)
        DeclinedCollectionView.backgroundColor = .clear
        DeclinedCollectionView.delegate = self
        DeclinedCollectionView.dataSource = self
        DeclinedCollectionView.register(ReferralCouponCollectionViewCell.self, forCellWithReuseIdentifier: "dec_cell")
        DeclinedScrollView.addSubview(DeclinedCollectionView)
        /***********************--NJ--*************************/
        /******************--NJ--******************/
        DeclinedCouponTableview = UITableView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: PendingScrollView.frame.height))
       // DeclinedCouponTableview.delegate = self
       // DeclinedCouponTableview.dataSource = self
        DeclinedCouponTableview.backgroundColor = .clear
        DeclinedCouponTableview.bounces = false
        DeclinedCouponTableview.separatorStyle = .none
        DeclinedCouponTableview.isOpaque = false
        DeclinedCouponTableview.register(ReferralCouponTableViewCell.self, forCellReuseIdentifier: "dec_couponcell")
      //  DeclinedScrollView.addSubview(DeclinedCouponTableview)
        /******************--NJ--******************/
        /******************--NJ--*********************/
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipeRight.direction = UISwipeGestureRecognizerDirection.right
//        self.view.addGestureRecognizer(swipeRight)
//        /******************--NJ--*********************/
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
//        self.view.addGestureRecognizer(swipeLeft)
        /******************--NJ--*********************/
        /******************--NJ--*********************/
        
//        infoView = UIView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
//        infoView.backgroundColor = UIColor.clear
//        self.view.addSubview(infoView)
        
        infoImageView = UIImageView()
        infoImageView.setImageView(X: 20, Y: ButtonView.frame.maxY+8, Width: SWIDTH-40, Height: SHEIGHT/2, img: UIImage(named: "showInfo.png")!)
        //infoImageView.center = self.view.center
        self.view.addSubview(infoImageView)
        infoImageView.isHidden = true
        
    }
    @objc func backAction(){
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == PendingCollectionView {
            var couponArray = [String]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Pending" {
                        couponArray.append(couponStatus)
                    }
                }
                return couponArray.count
            } else {
                return 0
            }
        } else if collectionView == AcceptedCollectionView {
            var couponArray = [String]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Accept" {
                        couponArray.append(couponStatus)
                    }
                }
                return couponArray.count
            } else {
                return 0
            }
        } else {
            var couponArray = [String]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Decline" || couponStatus == "Reject" {
                        couponArray.append(couponStatus)
                    }
                }
                return couponArray.count
            } else {
                return 0
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == PendingCollectionView {
            
            var couponArray = [Any]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Pending" {
                        couponArray.append(dataDict)
                        referIDArray.append(dataDict["refer_id"] as? String ?? "" )
                    }
                }
            }
            
            let dataDict = couponArray[indexPath.row] as! [String:Any]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pendingcell", for: indexPath) as! ReferralCouponCollectionViewCell
            
            cell.CountLabel.isHidden = false
            cell.CouponStatus.isHidden = true
            cell.AcceptButton.isHidden = true//false
            cell.RejectButton.isHidden = true//false
            cell.PostedByLabel.isHidden = true
            cell.buttonViolation.isHidden = false
            cell.acceptButton.isHidden = false
            cell.rejectButton.isHidden = false
            cell.infoButton.isHidden = false
            
            cell.DaysLabel.attributedText = NSAttributedString(string: "TODAY OR TONIGHT", attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
            cell.PostedByLabel.text = "\(dataDict["book_name"] as? String ?? "" ) Posted by: \(dataDict["posted_by"] as? String ?? "" )"
            cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
            cell.HeadingLabel.text = (dataDict["description"] as? String ?? "").uppercased()
            cell.NameLabel.text = dataDict["ref_name"] as? String ?? ""
            cell.EmailLabel.text = dataDict["ref_email"] as? String ?? ""
            cell.ImageView.clipsToBounds = true
            cell.RedView.backgroundColor = APPYELLOWCOLOR
            cell.backgroundColor = .clear
            cell.cellView.tag = indexPath.row
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeRight.delegate = self
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            cell.cellView.addGestureRecognizer(swipeRight)
            /******************--NJ--*********************/
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture1))
            swipeLeft.direction = UISwipeGestureRecognizerDirection.left
            cell.cellView.addGestureRecognizer(swipeLeft)
            swipeLeft.delegate = self
            cell.CategaryLabel.textColor = .white
            cell.CategaryLabel.type = .leftRight
            cell.CategaryLabel.speed = .rate(80)
            cell.CategaryLabel.fadeLength = 80.0
            cell.CategaryLabel.labelWillBeginScroll()
            cell.CategaryLabel.tag = indexPath.row
            cell.CategaryLabel.restartLabel()
        
            //cell.AcceptButton.tag = indexPath.row
//            cell.AcceptButton.addTarget(self, action: #selector(AcceptButtonAction), for: .touchUpInside)
//            cell.RejectButton.tag = indexPath.row
//            cell.RejectButton.addTarget(self, action: #selector(RejectButtonAction), for: .touchUpInside)
            
            cell.buttonViolation.tag = indexPath.row
            cell.buttonViolation.addTarget(self, action: #selector(buttonViolationActionPending), for: .touchUpInside)
            cell.acceptButton.tag = indexPath.row
            cell.rejectButton.tag = indexPath.row
            cell.infoButton.tag = indexPath.row
            cell.acceptButton.addTarget(self, action: #selector(onClickAccept), for: .touchUpInside)
            cell.rejectButton.addTarget(self, action: #selector(onClickReject), for: .touchUpInside)
            cell.infoButton.addTarget(self, action: #selector(onClickInfo), for: .touchUpInside)
            
            return cell
            
        } else if collectionView == AcceptedCollectionView {
            
            var couponArray = [Any]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Accept" {
                        couponArray.append(dataDict)
                    }
                }
            }
            let dataDict = couponArray[indexPath.row] as! [String:Any]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "acc_cell", for: indexPath) as! ReferralCouponCollectionViewCell
            
            cell.CountLabel.isHidden = false
            cell.CouponStatus.isHidden = true
            cell.CouponStatus.text = "ACCEPTED"
            cell.CouponStatus.textColor = APPGREENCOLOR
            cell.AcceptButton.isHidden = true
            cell.RejectButton.isHidden = true
            cell.PostedByLabel.isHidden = true
            cell.ImageView.isHidden = false
            cell.PostedByLabel.isHidden = true
            cell.likeButton.isHidden = true
            cell.buttonViolation.isHidden = true
            cell.bottomView.isHidden = true
            cell.acceptButton.isHidden = true
            cell.rejectButton.isHidden = true
            cell.infoButton.isHidden = true
            cell.DaysLabel.attributedText = NSAttributedString(string: "TODAY OR TONIGHT", attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])//(dataDict["coupon_title"] as? String ?? "").uppercased()
            cell.PostedByLabel.text = "\(dataDict["book_name"] as? String ?? "" ) Posted by: \(dataDict["posted_by"] as? String ?? "" )"
            cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
            
            cell.HeadingLabel.text = (dataDict["description"] as? String ?? "").uppercased()
            
            cell.NameLabel.text = dataDict["senderusername"] as? String ?? ""
            cell.EmailLabel.text = dataDict["senderemail"] as? String ?? ""
            cell.ImageView.clipsToBounds = true
            cell.RedView.backgroundColor = APPREDCOLOR
            cell.backgroundColor = .clear
            
            cell.CategaryLabel.textColor = .white
            cell.CategaryLabel.type = .leftRight
            cell.CategaryLabel.speed = .rate(80)
            cell.CategaryLabel.fadeLength = 80.0
            cell.CategaryLabel.labelWillBeginScroll()
            cell.CategaryLabel.tag = indexPath.row
            cell.CategaryLabel.restartLabel()
            cell.buttonViolation.tag = indexPath.row
            cell.buttonViolation.addTarget(self, action: #selector(buttonViolationActionAccept), for: .touchUpInside)
            
            return cell
            
        } else if collectionView == DeclinedCollectionView  {
            
            var couponArray = [Any]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Pending" {
                    } else if couponStatus == "Accept" {
                    } else {
                        couponArray.append(dataDict)
                    }
                }
            }
            let dataDict = couponArray[indexPath.row] as! [String:Any]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dec_cell", for: indexPath) as! ReferralCouponCollectionViewCell
            
            cell.CountLabel.isHidden = false
            cell.CouponStatus.isHidden = true
            cell.CouponStatus.text = "DECLINED"
            cell.CouponStatus.textColor = APPREDCOLOR
            cell.AcceptButton.isHidden = true
            cell.RejectButton.isHidden = true
            cell.PostedByLabel.isHidden = true
            cell.ImageView.isHidden = false
            cell.PostedByLabel.isHidden = true
            cell.likeButton.isHidden = true
            cell.buttonViolation.isHidden = true
            cell.bottomView.isHidden = true
            cell.acceptButton.isHidden = true
            cell.rejectButton.isHidden = true
            cell.infoButton.isHidden = true
            
            cell.DaysLabel.attributedText = NSAttributedString(string: "TODAY OR TONIGHT", attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
            cell.PostedByLabel.text = "\(dataDict["book_name"] as? String ?? "" ) Posted by: \(dataDict["posted_by"] as? String ?? "" )"
            cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
            cell.HeadingLabel.text = (dataDict["description"] as? String ?? "").uppercased()
            cell.NameLabel.text = dataDict["senderusername"] as? String ?? ""
            cell.EmailLabel.text = dataDict["senderemail"] as? String ?? ""
            cell.ImageView.clipsToBounds = true
            cell.RedView.backgroundColor = APPREDCOLOR
            cell.backgroundColor = .clear
            
            cell.CategaryLabel.textColor = .white
            cell.CategaryLabel.type = .leftRight
            cell.CategaryLabel.speed = .rate(80)
            cell.CategaryLabel.fadeLength = 80.0
            cell.CategaryLabel.labelWillBeginScroll()
            cell.CategaryLabel.tag = indexPath.row
            cell.CategaryLabel.restartLabel()
            cell.buttonViolation.tag = indexPath.row
            cell.buttonViolation.addTarget(self, action: #selector(buttonViolationActionDecline), for: .touchUpInside)
            return cell
        } else {
            let cell = UICollectionViewCell()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == PendingCollectionView {
            guard let cell = cell as? ReferralCouponCollectionViewCell else { return }
            cell.CategaryLabel.restartLabel()
        } else if collectionView == PendingCollectionView {
            guard let cell = cell as? ReferralCouponCollectionViewCell else { return }
            cell.CategaryLabel.restartLabel()
        } else if collectionView == PendingCollectionView {
            guard let cell = cell as? ReferralCouponCollectionViewCell else { return }
            cell.CategaryLabel.restartLabel()
        }
    }
    
    @objc func onClickAccept(sender: UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.PendingCollectionView)
        let indexPath = self.PendingCollectionView.indexPathForItem(at: buttonPosition)
        guard let index = indexPath?[1] else { return  }
        acceptRejectAction(referrId: referIDArray[Int(index)], statuss: "Accept")
    }
    @objc func onClickReject(sender: UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.PendingCollectionView)
        let indexPath = self.PendingCollectionView.indexPathForItem(at: buttonPosition)
        guard let index = indexPath?[1] else { return  }
        acceptRejectAction(referrId: referIDArray[Int(index)], statuss: "Reject")
    }
    @objc func onClickInfo(sender: UIButton){
        if (infoImageView.isHidden == true) {
            infoImageView.isHidden = false
        }else{
            infoImageView.isHidden = true
        }
        
    }

    
    @objc func buttonViolationActionPending(sender:UIButton){
        
        var couponArray = [Any]()
        if (AllDataArray.count) > 0 {
            for index in 0..<AllDataArray.count {
                let dataDict = AllDataArray[index] as! [String:Any]
                let couponStatus = dataDict["coupon_status"] as? String ?? ""
                if couponStatus == "Pending" {
                    couponArray.append(dataDict)
                }
            }
        }
        
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = couponArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            //  let Coupon_Title = dictData["coupon_title"] as! String
            self.GetRemoveCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String), Coupon_Id)
//            let RecEmail = Contact_Email
//            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
//            if let googleUrl = NSURL(string: googleUrlString) {
//                UIApplication.shared.openURL(googleUrl as URL)
//            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func buttonViolationActionAccept(sender:UIButton){
        
        var couponArray = [Any]()
        if (AllDataArray.count) > 0 {
            for index in 0..<AllDataArray.count {
                let dataDict = AllDataArray[index] as! [String:Any]
                let couponStatus = dataDict["coupon_status"] as? String ?? ""
                if couponStatus == "Accept" {
                    couponArray.append(dataDict)
                }
            }
        }
        
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = couponArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            //  let Coupon_Title = dictData["coupon_title"] as! String
            self.GetRemoveCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String), Coupon_Id)
//            let RecEmail = Contact_Email
//            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
//            if let googleUrl = NSURL(string: googleUrlString) {
//                UIApplication.shared.openURL(googleUrl as URL)
//            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func buttonViolationActionDecline(sender:UIButton){
        
        var couponArray = [Any]()
        if (AllDataArray.count) > 0 {
            for index in 0..<AllDataArray.count {
                let dataDict = AllDataArray[index] as! [String:Any]
                let couponStatus = dataDict["coupon_status"] as? String ?? ""
                if couponStatus == "Pending" {
                } else if couponStatus == "Accept" {
                } else {
                    couponArray.append(dataDict)
                }
            }
        }
        
        let alertController = UIAlertController(title: "", message: "Report as a Violation.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            let dictData = couponArray[sender.tag] as! [String:Any]
            let Coupon_Id = dictData["coupon_id"] as! String
            self.GetRemoveCoupons((USER_DEFAULTS.value(forKey: "user_id") as! String), Coupon_Id)
            //  let Coupon_Title = dictData["coupon_title"] as! String
            
//            let RecEmail = Contact_Email
//            // let Dict = "Coupon%20Name%20:%20\(Coupon_Title),%0D%0ACoupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let Dict = "Coupon%20Id%20:%20\(Coupon_Id),%0D%0ASender%20:%20\(USER_DEFAULTS.value(forKey: "user_id") ?? "")"
//
//            let googleUrlString = "googlegmail:///co?to=\(RecEmail)&subject=Report%20as%20a%20Violation&body=\(Dict)"
//            if let googleUrl = NSURL(string: googleUrlString) {
//                UIApplication.shared.openURL(googleUrl as URL)
//            }
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    func GetRemoveCoupons(_ userId:String,_ couponId:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"delete_favorite_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let parameters = "user_id= \(userId)&coupon_id=\(couponId)".data(using: .utf8)
        print("parameters :\(userId),\(couponId)")
        let postData = parameters
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                let dataDictionary = userObject as! NSDictionary
                print("dataDictionary:\(dataDictionary)")
                let status = dataDictionary.object(forKey: "status") as! String
                print("status :\(status)")
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
//                        self.GetRedeemedCoupons()
                        self.AllDataArray.removeAll()
                        self.referIDArray.removeAll()
                        self.GetReferralCoupons()
                        Utility.sharedInstance.showAlert("Alert", msg: "Post Blocked", controller: self)
                       
                    })
                }
                else {
                    RappleActivityIndicatorView.stopAnimation()
                      Utility.sharedInstance.showAlert("Alert", msg: dataDictionary.object(forKey: "message") as! String, controller: self)                    
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ReferralCouponTableview {
            var couponArray = [String]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Pending" {
                        couponArray.append(couponStatus)
                    }
                }
                return couponArray.count
            } else {
                return 0
            }
        } else if tableView == AcceptedCouponTableview {
            var couponArray = [String]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Accept" {
                        couponArray.append(couponStatus)
                    }
                }
                return couponArray.count
            } else {
                return 0
            }
        } else {
            var couponArray = [String]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Decline" || couponStatus == "Reject" {
                        couponArray.append(couponStatus)
                    }
                }
                return couponArray.count
            } else {
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SHEIGHT/2.9
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == ReferralCouponTableview {
            
            var couponArray = [Any]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Pending" {
                        couponArray.append(dataDict)
                        referIDArray.append(dataDict["refer_id"] as? String ?? "" )
                    }
                }
            }
            
            let dataDict = couponArray[indexPath.row] as! [String:Any]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "couponcell", for: indexPath) as! ReferralCouponTableViewCell
            cell.CountLabel.isHidden = false
            cell.CouponStatus.isHidden = true
            cell.AcceptButton.isHidden = false
            cell.RejectButton.isHidden = false
            
            
            //        if dataDict["image"] as? String != "" {
            //            cell.ImageView.sd_setImage(with: URL(string: dataDict["image"] as! String), placeholderImage: #imageLiteral(resourceName: "c1"))
            //        }
            cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
            cell.PostedByLabel.text = "\(dataDict["book_name"] as? String ?? "" ) Posted by: \(dataDict["posted_by"] as? String ?? "" )"
            cell.CategaryLabel.text = "Category : \(dataDict["cat_name"] as? String ?? "")"
            cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
            cell.NameLabel.text = dataDict["ref_name"] as? String ?? ""
            cell.EmailLabel.text = dataDict["ref_email"] as? String ?? ""
            cell.ImageView.clipsToBounds = true
            cell.RedView.backgroundColor = APPYELLOWCOLOR
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
//            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//            swipeRight.direction = UISwipeGestureRecognizerDirection.right
//            self.view.addGestureRecognizer(swipeRight)
//                    /******************--NJ--*********************/
//            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//            swipeLeft.direction = UISwipeGestureRecognizerDirection.left
//            self.view.addGestureRecognizer(swipeLeft)
            cell.AcceptButton.tag = indexPath.row
//            cell.AcceptButton.addTarget(self, action: #selector(AcceptButtonAction), for: .touchUpInside)
            cell.RejectButton.tag = indexPath.row
//            cell.RejectButton.addTarget(self, action: #selector(RejectButtonAction), for: .touchUpInside)
            
            return cell
        } else if tableView == AcceptedCouponTableview {
            
            var couponArray = [Any]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Accept" {
                        couponArray.append(dataDict)
                    }
                }
            }
            let dataDict = couponArray[indexPath.row] as! [String:Any]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "acc_couponcell", for: indexPath) as! ReferralCouponTableViewCell
            
            cell.CountLabel.isHidden = false
            cell.CouponStatus.isHidden = false
            cell.CouponStatus.text = "ACCEPTED"
            cell.CouponStatus.textColor = APPGREENCOLOR
            cell.AcceptButton.isHidden = true
            cell.RejectButton.isHidden = true
            
            
            
            //        if dataDict["image"] as? String != "" {
            //            cell.ImageView.sd_setImage(with: URL(string: dataDict["image"] as! String), placeholderImage: #imageLiteral(resourceName: "c1"))
            //        }
            cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
            cell.PostedByLabel.text = "\(dataDict["book_name"] as? String ?? "" ) Posted by: \(dataDict["posted_by"] as? String ?? "" )"
            cell.CategaryLabel.text = "Category : \(dataDict["cat_name"] as? String ?? "")"
            cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
            cell.NameLabel.text = dataDict["ref_name"] as? String ?? ""
            cell.EmailLabel.text = dataDict["ref_email"] as? String ?? ""
            cell.ImageView.clipsToBounds = true
            cell.RedView.backgroundColor = APPYELLOWCOLOR
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            
            return cell
        } else if tableView == DeclinedCouponTableview {
            
            var couponArray = [Any]()
            if (AllDataArray.count) > 0 {
                for index in 0..<AllDataArray.count {
                    let dataDict = AllDataArray[index] as! [String:Any]
                    let couponStatus = dataDict["coupon_status"] as? String ?? ""
                    if couponStatus == "Pending" {
                    } else if couponStatus == "Accept" {
                    } else {
                        couponArray.append(dataDict)
                    }
                }
            }
            
            let dataDict = couponArray[indexPath.row] as! [String:Any]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "dec_couponcell", for: indexPath) as! ReferralCouponTableViewCell
            
            cell.CountLabel.isHidden = false
            cell.CouponStatus.isHidden = false
            cell.CouponStatus.text = "DECLINED"
            cell.CouponStatus.textColor = APPREDCOLOR
            cell.AcceptButton.isHidden = true
            cell.RejectButton.isHidden = true
            
            
            //let dataDict = AllDataArray[indexPath.row] as! [String:Any]
            //        if dataDict["image"] as? String != "" {
            //            cell.ImageView.sd_setImage(with: URL(string: dataDict["image"] as! String), placeholderImage: #imageLiteral(resourceName: "c1"))
            //        }
            cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
            cell.PostedByLabel.text = "\(dataDict["book_name"] as? String ?? "" ) Posted by: \(dataDict["posted_by"] as? String ?? "" )"
            cell.CategaryLabel.text = "Category : \(dataDict["cat_name"] as? String ?? "")"
            cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
            cell.NameLabel.text = dataDict["ref_name"] as? String ?? ""
            cell.EmailLabel.text = dataDict["ref_email"] as? String ?? ""
            cell.ImageView.clipsToBounds = true
            cell.RedView.backgroundColor = APPYELLOWCOLOR
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            
            
            
            return cell
        } else {
            let celll = UITableViewCell()
            return celll
        }
        
        
    }
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
       // ReferDetails(otp:random(), message:shareText)
    }
     func RejectButtonAction(sender: UIButton) {
        acceptRejectAction(referrId: referIDArray[sender.tag], statuss: "Reject")
    }
    func AcceptButtonAction(sender: UIButton!) {
        acceptRejectAction(referrId: referIDArray[sender.tag], statuss: "Accept")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        setNotificationSeen()
        GetReferralCoupons()
    }
    
    func GetReferralCoupons(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_refered_coupons")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&mobile=\(USER_DEFAULTS.value(forKey: "user_mobile") ?? "")&email=\(USER_DEFAULTS.value(forKey: "user_email") ?? "")".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "coupon_status") as? String, forKey: "coupon_status")
                        print(self.AllDataArray)
                        DispatchQueue.main.async(execute: {
                            self.ReferralCouponTableview.isHidden = false
                            self.NoCouponsLabel.isHidden = true
                        })
                        self.PendingCollectionView.reloadData()
                        self.AcceptedCollectionView.reloadData()
                        self.DeclinedCollectionView.reloadData()
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                        self.ReferralCouponTableview.isHidden = true
                        self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func accept_and_reject_coupon(referid:String, status:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
             Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
         RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"accept_and_reject_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&refer_id=\(referid)&status=\(status)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        //  self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        self.ReferralCouponTableview.reloadData()
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    func acceptRejectAction(referrId:String,statuss:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()

        let url = URL(string: BASE_URL+"accept_and_reject_coupon")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&refer_id=\(referrId)&status=\(statuss)".data(using: .utf8)
        request.httpBody = postData

        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {

                let dataDictionary = userObject as! NSDictionary

                let status = dataDictionary.object(forKey: "status") as! String

                let msg = dataDictionary.object(forKey: "message") as! String

                if status == "TRUE" {

                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray.removeAll()
                        self.referIDArray.removeAll()
                        if statuss == "Accept"{
                            let renderer = UIGraphicsImageRenderer(size: self.view.bounds.size)
                            let image = renderer.image { ctx in
                                self.view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
                            }
                            print("image ---- ",image)
                            let view_c = UIView(frame: CGRect(x: UIScreen.main.bounds.width - 200, y: STATUSBARHEIGHT+(SHEIGHT/14.5), width: 200, height: 300))
                            
                            let image2 = UIImage.imageWithView(self.view)
                            
                            let imgview = UIImageView.init(image: image2)
                            imgview.contentMode = .scaleToFill
                            imgview.frame = CGRect(x: 0, y: 0, width: 200, height: 300)
                            view_c.addSubview(imgview)
                            view_c.backgroundColor = UIColor.blue
                            self.view.addSubview(view_c)
                            Toast.show(message: "ACCEPTED", controller: self, left: 200, colorSelect: .green)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                                // code to remove your view
                                view_c.removeFromSuperview()
                            }
//                            Toast.showScreenImage(controller: self, img:image )
                            
                        }else{
                           Toast.show(message: "DECLINED", controller: self, left: SWIDTH, colorSelect: .red)
//                            img = self.view.capture()!
                        }
                        self.GetReferralCoupons()
                        
                    })
                }
                else {
                      Utility.sharedInstance.showAlert("", msg: msg, controller: self)

                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var errorArr = [String : NSDictionary]()
                let item1 = NSMutableDictionary()
                item1.setObject("iOS", forKey: "From" as NSCopying)
                item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                item1.setObject("\(url)", forKey: "API Name" as NSCopying)
                item1.setObject(error?.localizedDescription ?? "", forKey: "error" as NSCopying)
                errorArr["json"] = item1
                var jsonData = NSData()
                print(errorArr)
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                    jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                    let jsondata = theJSONText
                    print("jsondata~~~~~~",jsondata)
                    callsaveExceptionLogApi(error: jsondata)
                }
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    func setNotificationSeen(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_refered_notification")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&mobile=\(USER_DEFAULTS.value(forKey: "user_mobile") ?? "")&email=\(USER_DEFAULTS.value(forKey: "user_email") ?? "")&status=Seen".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                //let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        USER_DEFAULTS.set("0", forKey: "count")
                        
                    })
                }
                else {
                    //  Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func assignbackgroundd(){
        // let background = #imageLiteral(resourceName: "drawerpic")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        // imageView.image = background
        imageView.center = view.center
        imageView.image = UIImage(named: "bg.jpg")
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
        //imageView.image = UIImage(named: "background-1")!.resizableImage(withCapInsets: .zero)
        //        let BlurView = UIView()
        //        BlurView.frame = CGRect(x: imageView.frame.origin.x, y: imageView.frame.origin.y, width: imageView.frame.width, height: imageView.frame.height)
        //        BlurView.backgroundColor = UIColor(white: 0, alpha: 0.8)
        //        BlurView.clipsToBounds = true
        //        BlurView.layer.cornerRadius = 10
        //        imageView.addSubview(BlurView)
    }
    
    
//    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
//
//        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
//
//            switch swipeGesture.direction {
//            case UISwipeGestureRecognizerDirection.right:
//                print("Swiped right")
//
//                if PendingShowing == true {
//                    print("pending Showing")
//
//                }
//                else if AcceptedShowing == true {
//
//                    PendingShowing = true
//                    AcceptedShowing = false
//                    DeclinedShowing = false
//
//                    UIView.animate(withDuration: 0.4, animations: {
//
//                        self.PendingScrollView.alpha = 1
//                        self.AcceptedScrollView.alpha = 0
//                        self.DeclinedScrollView.alpha = 0
//
//                        self.PendingScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.AcceptedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.DeclinedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//                        self.slidingLabel.frame = CGRect(x: 0, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
//
//                    })
//                }
//                else if DeclinedShowing == true {
//
//                    PendingShowing = false
//                    AcceptedShowing = true
//                    DeclinedShowing = false
//
//                    UIView.animate(withDuration: 0.4, animations: {
//
//                        self.PendingScrollView.alpha = 0
//                        self.AcceptedScrollView.alpha = 1
//                        self.DeclinedScrollView.alpha = 0
//
//                        self.PendingScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.AcceptedScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.DeclinedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.slidingLabel.frame = CGRect(x: self.ButtonView.frame.width/3, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
//
//                    })
//
//                }
//
//
//            case UISwipeGestureRecognizerDirection.down:
//                print("Swiped down")
//            case UISwipeGestureRecognizerDirection.left:
//                print("Swiped left")
//
//                if PendingShowing == true {
//
//                    PendingShowing = false
//                    AcceptedShowing = true
//                    DeclinedShowing = false
//
//                    UIView.animate(withDuration: 0.4, animations: {
//
//                        self.PendingScrollView.alpha = 0
//                        self.AcceptedScrollView.alpha = 1
//                        self.DeclinedScrollView.alpha = 0
//
//                        self.PendingScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.AcceptedScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.DeclinedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.slidingLabel.frame = CGRect(x: self.ButtonView.frame.width/3, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
//
//                    })
//                }
//                else if AcceptedShowing == true {
//
//                    PendingShowing = false
//                    AcceptedShowing = false
//                    DeclinedShowing = true
//
//                    UIView.animate(withDuration: 0.4, animations: {
//
//                        self.PendingScrollView.alpha = 0
//                        self.AcceptedScrollView.alpha = 0
//                        self.DeclinedScrollView.alpha = 1
//
//                        self.PendingScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.AcceptedScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//
//                        self.DeclinedScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
//                        self.slidingLabel.frame = CGRect(x: 2*self.ButtonView.frame.width/3, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
//                    })
//
//                }
//                else if DeclinedShowing == true {
//                    print("declined Showing")
//                }
//
//            case UISwipeGestureRecognizerDirection.up:
//                print("Swiped up")
//            default:
//                break
//            }
//        }
//    }
   
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer){
        let pointInCollectionView = gesture.location(in: PendingCollectionView)
        let indexPath = PendingCollectionView.indexPathForItem(at: pointInCollectionView)
        guard let index = indexPath?[1] else { return  }
        acceptRejectAction(referrId: referIDArray[Int(index)], statuss: "Accept")
    }
    @objc func respondToSwipeGesture1(gesture: UIGestureRecognizer){
        let pointInCollectionView = gesture.location(in: PendingCollectionView)
        let indexPath = PendingCollectionView.indexPathForItem(at: pointInCollectionView)
        guard let index = indexPath?[1] else { return  }
        acceptRejectAction(referrId: referIDArray[Int(index)], statuss: "Reject")
    }
    
    @objc func PendingButtonAction(){
        
        PendingShowing = true
        AcceptedShowing = false
        DeclinedShowing = false
        
        UIView.animate(withDuration: 0.4, animations: {
            
            self.PendingScrollView.alpha = 1
            self.AcceptedScrollView.alpha = 0
            self.DeclinedScrollView.alpha = 0
            
            self.PendingScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.AcceptedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.DeclinedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            self.slidingLabel.frame = CGRect(x: 0, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
            
        })
        
    }
    
    @objc func AcceptedButtonAction(){
        
        PendingShowing = false
        AcceptedShowing = true
        DeclinedShowing = false
        
        UIView.animate(withDuration: 0.4, animations: {
            
            self.PendingScrollView.alpha = 0
            self.AcceptedScrollView.alpha = 1
            self.DeclinedScrollView.alpha = 0
            
            self.PendingScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.AcceptedScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.DeclinedScrollView.frame = CGRect(x: SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.slidingLabel.frame = CGRect(x: self.ButtonView.frame.width/3, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
            
        })
        
    }
    
    
    @objc func DeclinedButtonAction(){
        
        PendingShowing = false
        AcceptedShowing = false
        DeclinedShowing = true
        
        UIView.animate(withDuration: 0.4, animations: {
            
            
            self.PendingScrollView.alpha = 0
            self.AcceptedScrollView.alpha = 0
            self.DeclinedScrollView.alpha = 1
            
            self.PendingScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.AcceptedScrollView.frame = CGRect(x: -SWIDTH, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.DeclinedScrollView.frame = CGRect(x: 0, y: self.ButtonView.frame.origin.y+self.ButtonView.frame.height, width: SWIDTH, height: SHEIGHT-self.ButtonView.frame.origin.y-self.ButtonView.frame.height)
            
            self.slidingLabel.frame = CGRect(x: 2*self.ButtonView.frame.width/3, y: self.ButtonView.frame.height-2, width: self.ButtonView.frame.width/3, height: 2)
        })
        
    }
    
    
}
class Toast {
    static func showScreenImage(controller: UIViewController,img:UIImage) {
        let toastContainer = UIView()//UIView(frame: CGRect())
        toastContainer.frame = CGRect(x: 15, y: controller.view.frame.height/5, width: controller.view.frame.width-30, height: controller.view.frame.height-30)
        toastContainer.backgroundColor = UIColor.clear
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 25;
        toastContainer.layer.borderWidth = 1.5
        toastContainer.layer.borderColor = UIColor.white.cgColor
        toastContainer.clipsToBounds  =  true
        let toastImg = UIImageView()//UILabel(frame: CGRect())
        toastImg.frame = CGRect(x: 0, y: 0, width: toastContainer.frame.width, height: toastContainer.frame.height)
        
        toastContainer.addSubview(toastImg)
        controller.view.addSubview(toastContainer)
        
        
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.10, delay: 1.5, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    
    }
    static func show(message: String, controller: UIViewController,left:CGFloat,colorSelect:UIColor) {
        let toastContainer = UIView()//UIView(frame: CGRect())
        toastContainer.frame = CGRect(x: left/2-100, y: controller.view.frame.height/5, width: 200, height: 50)
        toastContainer.backgroundColor = colorSelect.withAlphaComponent(0.8)
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 25;
        toastContainer.layer.borderWidth = 1.5
        toastContainer.layer.borderColor = UIColor.white.cgColor
        toastContainer.clipsToBounds  =  true
        
        let toastLabel = UILabel()//UILabel(frame: CGRect())
        toastLabel.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font.withSize(20.0)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        toastContainer.addSubview(toastLabel)
        controller.view.addSubview(toastContainer)
        
//        toastLabel.translatesAutoresizingMaskIntoConstraints = false
//        toastContainer.translatesAutoresizingMaskIntoConstraints = false
//
//        let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 15)
//        let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -15)
//        let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
//        let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
//        toastContainer.addConstraints([a1, a2, a3, a4])
//
//        let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller.view, attribute: .leading, multiplier: 1, constant: 65)
//        let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller.view, attribute: .trailing, multiplier: 1, constant: -65)
//        let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller.view, attribute: .bottom, multiplier: 1, constant: -75)
//        controller.view.addConstraints([c1, c2, c3])
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    }
}
extension UIView {
    func capture() -> UIImage? {
        var image: UIImage?
        
        if #available(iOS 10.0, *) {
            let format = UIGraphicsImageRendererFormat()
            format.opaque = isOpaque
            let renderer = UIGraphicsImageRenderer(size: frame.size, format: format)
            image = renderer.image { context in
                drawHierarchy(in: frame, afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(frame.size, isOpaque, UIScreen.main.scale)
            drawHierarchy(in: frame, afterScreenUpdates: true)
            image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        return image
    }
}
extension UIImage {
    class func imageWithView(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0)
        defer { UIGraphicsEndImageContext() }
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }
}
