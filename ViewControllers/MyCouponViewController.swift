//
//  MyCouponViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/25/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import RappleProgressHUD
import MarqueeLabel
import UPCarouselFlowLayout

class MyCouponViewController: SideMenuViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var couponCollectionView : UICollectionView!
    
    var NoCouponsLabel:UILabel!
    
    var CouponsTableview: UITableView!
    var imageArray = [UIImage]()
    
    var AllDataArray = [Any]()
    
    var navBarHeight = CGFloat()
    var HeaderImage: UIImageView!
    var FooterImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SUBMITTED COUPONS"
        
       // assignbackgroundd()
        self.view.backgroundColor = .black
        
        navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
         let sharebtn1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        sharebtn1.tintColor = UIColor.white
        navigationItem.leftBarButtonItems = [sharebtn1]
        
        CouponsTableview = UITableView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight))
       // CouponsTableview.delegate = self
      //  CouponsTableview.dataSource = self
        CouponsTableview.backgroundColor = .clear
        CouponsTableview.bounces = false
        CouponsTableview.separatorStyle = .none
        CouponsTableview.isOpaque = false
        CouponsTableview.register(MyCouponsTableViewCell.self, forCellReuseIdentifier: "couponcell")
      //  self.view.addSubview(CouponsTableview)
        /******************--NJ--*********************/
        HeaderImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT/2))
        HeaderImage.image = UIImage(named: "bottom_layer")
      //  self.view.addSubview(HeaderImage)
        /******************--NJ--*********************/
        FooterImage = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight+HeaderImage.frame.height, width: SWIDTH, height: SHEIGHT/2))
        FooterImage.image = UIImage(named: "top_layer")
      //  self.view.addSubview(FooterImage)
        /******************--NJ--*********************/
        let sharebtn = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(shareAction))
        sharebtn.tintColor = UIColor.white
        navigationItem.rightBarButtonItems = [sharebtn]
        /***********************--NJ--*************************/
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height/3)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        couponCollectionView = UICollectionView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+navBarHeight, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-navBarHeight), collectionViewLayout: layout)
        couponCollectionView.backgroundColor = .clear
        couponCollectionView.delegate = self
        couponCollectionView.dataSource = self
        couponCollectionView.register(submittedCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(couponCollectionView)
        /***********************--NJ--*************************/
         /******************--NJ--*********************/
        NoCouponsLabel = UILabel()
        NoCouponsLabel.setLabel(X: SWIDTH/4, Y: SHEIGHT/2-(SHEIGHT/13)/2, Width: SWIDTH/2, Height: SHEIGHT/13, TextColor: .white, BackColor: .clear, Text: "No Coupons available..", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 17))
        NoCouponsLabel.font = UIFont(name: "Electrolize-Regular", size: 17)
        NoCouponsLabel.isHidden = true
        NoCouponsLabel.adjustsFontSizeToFitWidth = true
        self.view.addSubview(NoCouponsLabel)
        
    }
    func ReferDetails(otp:String, message:String){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"save_refer_details")!
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "message=\(message)&user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)&otp=\(otp)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        Utility.sharedInstance.showDismissAlert("", msg: "Message send successfully.", controller: self)
                        self.AllDataArray.removeAll()
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    func random() -> String {
        var result = ""
        repeat {
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4 || Int(result)! < 1000
        print(result)
        return result
    }
    @objc func shareAction(){
        let shareText = "One of our Ambassadors has sent you a code to download the CouplesCouponBook.com mobile app. Please use the Code \(random()) to begin sending naughty coupons to your lover."
        let activityvc = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        activityvc.popoverPresentationController?.sourceView = self.view
        self.present(activityvc, animated: true, completion: nil)
        ReferDetails(otp:random(), message:shareText)
    }
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! submittedCollectionViewCell
        
        
        cell.CountLabel.isHidden = false
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
        cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
        cell.PostedByLabel.text = "Posted by : \(dataDict["posted_by"] as? String ?? "admin" )"
        cell.PostedByLabel.textAlignment = .left
        cell.CategaryLabel.text = dataDict["cat_name"] as? String ?? ""
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        cell.DaysLabel.text = dataDict["default_title"] as?  String ?? ""
        cell.ImageView.clipsToBounds = true
        
        if dataDict["status"] as! String == "Not-approved" {
            cell.RedView.backgroundColor = .lightGray
            cell.currentStatusLabel.text = "In Process"
            cell.HeadingLabel.textColor = .lightGray
            cell.LowerTextLabel.textColor = .lightGray
            cell.PostedByLabel.textColor = .lightGray
        }
        else {
            cell.RedView.backgroundColor = APPREDCOLOR
            cell.currentStatusLabel.text = "APPROVED"
            cell.HeadingLabel.textColor = .white
            cell.LowerTextLabel.textColor = .white
            cell.PostedByLabel.textColor = .white
        }
    
        cell.backgroundColor = .clear
        
        cell.CategaryLabel.textColor = .white
        cell.CategaryLabel.type = .leftRight
        cell.CategaryLabel.speed = .rate(80)
        cell.CategaryLabel.fadeLength = 80.0
        cell.CategaryLabel.labelWillBeginScroll()
        cell.CategaryLabel.tag = indexPath.row
        cell.CategaryLabel.restartLabel()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? submittedCollectionViewCell else { return }
        cell.CategaryLabel.restartLabel()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SHEIGHT/3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponcell", for: indexPath) as! MyCouponsTableViewCell
        cell.ImageView.clipsToBounds = true
        
        let dataDict = AllDataArray[indexPath.row] as! [String:Any]
//        if dataDict["image"] as? String != "" {
//            cell.ImageView.sd_setImage(with: URL(string: dataDict["image"] as! String), placeholderImage: #imageLiteral(resourceName: "c1"))
//        }
        cell.layer.cornerRadius = 2
        cell.HeadingLabel.text = dataDict["coupon_title"] as? String ?? ""
        cell.PostedByLabel.text = "Posted by: \(dataDict["posted_by"] as? String ?? "" )"
        
        cell.CategaryLabel.text = "Category : \(dataDict["cat_name"] as? String ?? "")"
        cell.LowerTextLabel.text = dataDict["description"] as? String ?? ""
        
        if dataDict["status"] as! String == "Not-approved" {
            cell.BlurView.backgroundColor = UIColor(white: 0, alpha: 0.4)
            cell.RedView.backgroundColor = .lightGray
            cell.StatusLabel.text = "In Process"
        }
        else {
            cell.BlurView.backgroundColor = UIColor(white: 0, alpha: 0.4)
            cell.RedView.backgroundColor = APPREDCOLOR
            cell.StatusLabel.text = "APPROVED"
        }
        
        
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AllDataArray.removeAll()
        GetMyCoupons()
    }
    
    
    func GetMyCoupons(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_my_coupons")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
               // let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        self.NoCouponsLabel.isHidden = true
                        self.couponCollectionView.isHidden = false
                        RappleActivityIndicatorView.stopAnimation()
                        self.AllDataArray = dataDictionary.object(forKey: "data") as! [Any]
                        print(self.AllDataArray)
                        self.couponCollectionView.reloadData()
                    })
                }
                else {
                   // Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    DispatchQueue.main.async(execute: {
                    self.couponCollectionView.isHidden = true
                    self.NoCouponsLabel.isHidden = false
                    })
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func assignbackgroundd(){
       // let background = #imageLiteral(resourceName: "drawerpic")
        var imageView : UIImageView!
        imageView = UIImageView(frame: CGRect(x: 0, y: STATUSBARHEIGHT+20, width: SWIDTH, height: SHEIGHT-20-STATUSBARHEIGHT))
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
       // imageView.image = nil
       // imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
       imageView.image = UIImage(named: "background-1")!.resizableImage(withCapInsets: .zero)
//        let BlurView = UIView()
//        BlurView.frame = CGRect(x: imageView.frame.origin.x, y: imageView.frame.origin.y, width: imageView.frame.width, height: imageView.frame.height)
//        BlurView.backgroundColor = UIColor(white: 0, alpha: 0.8)
//        BlurView.clipsToBounds = true
//        BlurView.layer.cornerRadius = 10
//        imageView.addSubview(BlurView)
    }
    
}
