//
//  MyProfileViewController.swift
//  Coupon Book
//
//  Created by apple  on 10/26/18.
//  Copyright © 2018 Scientific web solution. All rights reserved.
//

import UIKit
import SDWebImage
import RappleProgressHUD

class SettingViewController: UIViewController {
    
    var CouponBookImage: UIImageView!
    var settinfImage: UIImageView!
    var BackButton : UIButton!
    
    var LabelHeight = SHEIGHT/13
    var scrollView = UIScrollView()
    var UserProfileImageView: UIImageView!
    var userNAmeTittle: UILabel!
    var UserNAmeLabel:UILabel!
    var userEmailTittle: UILabel!
    var UserEmailLabel: UILabel!
    var UserMobileTittle: UILabel!
    var UserMobileNoLabel: UILabel!
    var EditProfileButton: UIButton!
    var EditIcon: UIImageView!
    var setting:UILabel!
    var fristLine:UILabel!
    var secondLine:UILabel!
    var thirdLine:UILabel!
    var fristAgeLabel:UILabel!
    var fristSwitchYes:UILabel!
    var fristSwitch:UISwitch!
    var lastTittle:UILabel!
    var lastTittle0:UILabel!
    
    var termsLabel = UILabel()
    var privcyLabel = UILabel()
    var eulaLabel = UILabel()
    var trmsSwitch : UISwitch!
    var privcySwitch : UISwitch!
    var eulaSwitch : UISwitch!
    var NoLabel0 = UILabel()
    var YesLabel0 = UILabel()
    var NoLabel1 = UILabel()
    var YesLabel1 = UILabel()
    var NoLabel2 = UILabel()
    var YesLabel2 = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        //self.view.backgroundColor = .black//APPGRAYCOLOR
        assignbackground()
        SetupView()
        
        print("SHEIGHT~~~~~~~",SHEIGHT)
    }
    
    //Assign backGround
    func assignbackground(){
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = UIImage.init(named: "bg.jpg")
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
    
    @objc func backButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: true)
//        navigationController?.pushViewController(HomeViewController(), animated: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getUserProfile()
    }
    var imgUrl:String!
    func getUserProfile(){
        
        if  Reachability.isConnectedToNetwork() == false
        {
            Utility.sharedInstance.showAlert("Alert", msg: "Internet Connection not Available!", controller: self)
            return
        }
        RappleActivityIndicatorView.startAnimating()
        
        let url = URL(string: BASE_URL+"get_user_profile")!
        
        print(USER_DEFAULTS.value(forKey: "user_id")!)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postData = "user_id=\(USER_DEFAULTS.value(forKey: "user_id")!)".data(using: .utf8)
        request.httpBody = postData
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                let dataDictionary = userObject as! NSDictionary
                
                let status = dataDictionary.object(forKey: "status") as! String
                
                let msg = dataDictionary.object(forKey: "message") as! String
                
                if status == "TRUE" {
                    
                    DispatchQueue.main.async(execute: {
                        RappleActivityIndicatorView.stopAnimation()
                        
                        if dataDictionary.object(forKey: "image") as? String != "" {
                            self.imgUrl = dataDictionary["image"] as? String
                            //self.CouponBookImage.sd_setImage(with: URL(string: dataDictionary["image"] as! String), placeholderImage: #imageLiteral(resourceName: "user"))
                        }
                        let trimmedString = "\(dataDictionary.object(forKey: "email") as? String ?? "")".trimmingCharacters(in: .whitespaces)
                        let trimmedString1 = "\(dataDictionary.object(forKey: "name") as? String ?? "")".trimmingCharacters(in: .whitespaces)
                        let trimmedString2 = "\(dataDictionary.object(forKey: "mobile") as? String ?? "")".trimmingCharacters(in: .whitespaces)
                        
                        self.UserNAmeLabel.text = " \(trimmedString1)"
                        self.UserEmailLabel.text = " \(trimmedString)"
                        self.UserMobileNoLabel.text = " \(trimmedString2)"
                        
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "mobile") as? String, forKey: "user_mobile")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "name") as? String, forKey: "user_name")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "image") as? String, forKey: "user_image")
                        USER_DEFAULTS.set(dataDictionary.object(forKey: "email") as? String, forKey: "user_email")
                        
                    })
                }
                else {
                    Utility.sharedInstance.showAlert("", msg: msg, controller: self)
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            else {
                Utility.sharedInstance.showAlert("", msg: "Something went wrong..", controller: self)
                RappleActivityIndicatorView.stopAnimation()
            }
            }.resume()
    }
    
    
    
    func SetupView(){
        
        /******************--NJ--*********************/
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT))
        scrollView.backgroundColor = .clear
        scrollView.bounces = false
        scrollView.contentSize.height = 750
        
        self.view.addSubview(scrollView)
        BackButton = UIButton()
        BackButton.setButton(X: 20, Y: STATUSBARHEIGHT+5, Width: 35, Height: 35, TextColor: UIColor.clear, BackColor: UIColor.clear, Title: "")
        BackButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        BackButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        BackButton.tintColor=UIColor.white
        self.scrollView.addSubview(BackButton)
        /*****************--NJ--*****************/
        CouponBookImage = UIImageView()//*1.3
        CouponBookImage.setImageView(X: 8, Y: STATUSBARHEIGHT+LabelHeight-10, Width: SWIDTH-16, Height: SHEIGHT/8, img: UIImage(named: "Logo.png")!)
        //CouponBookImage.alpha = 0.5
        self.scrollView.addSubview(CouponBookImage)
        
        /******************--NJ--*********************/
        //        UserProfileImageView = UIImageView()
        //        UserProfileImageView.setImageView(X: SWIDTH/2-LabelHeight*1.1, Y: CouponBookImage.frame.origin.y+CouponBookImage.frame.height+LabelHeight, Width: LabelHeight*2.2, Height: LabelHeight*2.2, img: #imageLiteral(resourceName: "user"))
        //        UserProfileImageView.contentMode = UIViewContentMode.scaleAspectFill
        //        UserProfileImageView.layer.cornerRadius = UserProfileImageView.frame.height/2
        //        UserProfileImageView.layer.borderColor = UIColor.lightGray.cgColor
        //        UserProfileImageView.layer.borderWidth = 0.5
        //        UserProfileImageView.clipsToBounds = true
        //        self.view.addSubview(UserProfileImageView)
        /******************--NJ--*********************/
        settinfImage = UIImageView()//*1.3
        settinfImage.setImageView(X: 8, Y: CouponBookImage.frame.maxY+40, Width: SWIDTH-16, Height: LabelHeight/2+10, img: UIImage(named: "Settings.png")!)
        settinfImage.contentMode  = .scaleAspectFit
        self.scrollView.addSubview(settinfImage)
        
//        setting = UILabel()
//        setting.setLabel(X: 50, Y: CouponBookImage.frame.maxY, Width: SWIDTH-100, Height: LabelHeight+10, TextColor: UIColor.init(red: 217/255, green: 134/255, blue: 35/255, alpha: 1.0), BackColor: .clear, Text: "Settings", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 18))
//        setting.font = UIFont(name: "NellaSueDEMO", size: 40)
//        self.scrollView.addSubview(setting)
        
        userNAmeTittle = UILabel()
        userNAmeTittle.setLabel(X: 10, Y: settinfImage.frame.maxY+5, Width: 150, Height: LabelHeight-20, TextColor: .white, BackColor: .clear, Text: " NAME", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 18))
        userNAmeTittle.adjustsFontSizeToFitWidth = true
        userNAmeTittle.minimumScaleFactor = 0.1
        self.scrollView.addSubview(userNAmeTittle)
        
        UserNAmeLabel = UILabel()
        UserNAmeLabel.setLabel(X: SWIDTH/4.5, Y: settinfImage.frame.maxY+5, Width: SWIDTH-SWIDTH/3.5, Height: LabelHeight-20, TextColor: .white, BackColor: UIColor(displayP3Red: 60/255, green: 57/255, blue: 57/255, alpha: 1), Text: " User Name", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 20))
        UserNAmeLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        
        UserNAmeLabel.adjustsFontSizeToFitWidth = true
        UserNAmeLabel.layer.cornerRadius = 6
        UserNAmeLabel.layer.masksToBounds = true
        self.scrollView.addSubview(UserNAmeLabel)
        /******************--NJ--*********************/
        userEmailTittle = UILabel()
        userEmailTittle.setLabel(X: 10, Y: UserNAmeLabel.frame.maxY+5, Width: 150, Height: LabelHeight-20, TextColor: .white, BackColor: .clear, Text: " EMAIL", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 18))
        userEmailTittle.adjustsFontSizeToFitWidth = true
        userEmailTittle.minimumScaleFactor = 0.1
        self.scrollView.addSubview(userEmailTittle)
        
        UserEmailLabel = UILabel()
        UserEmailLabel.setLabel(X: SWIDTH/4.5, Y: UserNAmeLabel.frame.maxY+5, Width: SWIDTH-SWIDTH/3.5, Height: LabelHeight-20, TextColor: .white, BackColor: UIColor(displayP3Red: 60/255, green: 57/255, blue: 57/255, alpha: 1), Text: " user@gmail.com", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 20))
        UserEmailLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        UserEmailLabel.adjustsFontSizeToFitWidth = true
        UserEmailLabel.layer.cornerRadius = 6
        UserEmailLabel.layer.masksToBounds = true
        self.scrollView.addSubview(UserEmailLabel)
        /******************--NJ--*********************/
        
        UserMobileTittle = UILabel()
        UserMobileTittle.setLabel(X: 10, Y: UserEmailLabel.frame.maxY+5, Width: 150, Height: LabelHeight-20, TextColor: .white, BackColor: .clear, Text: " PHONE", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 18))
        UserMobileTittle.adjustsFontSizeToFitWidth = true
        UserMobileTittle.minimumScaleFactor = 0.1
        self.scrollView.addSubview(UserMobileTittle)
        
        UserMobileNoLabel = UILabel()
        UserMobileNoLabel.setLabel(X: SWIDTH/4.5, Y: UserEmailLabel.frame.maxY+5, Width: SWIDTH-SWIDTH/3.5, Height: LabelHeight-20, TextColor: .white, BackColor: UIColor(displayP3Red: 60/255, green: 57/255, blue: 57/255, alpha: 1), Text: "1234567890", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 20))
        UserMobileNoLabel.font = UIFont(name: "Electrolize-Regular", size: 20)
        UserMobileNoLabel.adjustsFontSizeToFitWidth = true
        UserMobileNoLabel.layer.cornerRadius = 6
        UserMobileNoLabel.layer.masksToBounds = true
        self.scrollView.addSubview(UserMobileNoLabel)
        /******************--NJ--*********************/
        fristLine = UILabel()
        fristLine.setLabel(X: 0, Y: UserMobileNoLabel.frame.maxY+15, Width: SWIDTH, Height: 1.5, TextColor: .white, BackColor: .white, Text: "", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 20))
        self.scrollView.addSubview(fristLine)
        /******************--FristLabel Are You 18 or Older--*********************/
        fristAgeLabel = UILabel()
        fristAgeLabel.setLabel(X: SWIDTH/2-SWIDTH/4, Y: fristLine.frame.maxY+20, Width: SWIDTH/2, Height: 20, TextColor: .white, BackColor: .clear, Text: "Are You 18 or Older", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 22))
        fristAgeLabel.adjustsFontSizeToFitWidth = true
        fristAgeLabel = buttomBorder(label: fristAgeLabel)
        self.scrollView.addSubview(fristAgeLabel)
        /******************--FristSwitch Yes/No--*********************/
        fristSwitch = UISwitch(frame: CGRect(x:SWIDTH/2-20 , y: fristAgeLabel.frame.origin.y+25, width: 40, height: 30))
        fristSwitch.isOn = false
        fristSwitch.tintColor = .red
        fristSwitch.thumbTintColor = .red
        //fristSwitch.addTarget(self, action: #selector(fristSwitchAction), for: .touchUpInside)
        self.scrollView.addSubview(fristSwitch)
        /******************--FristSwitch Yes--*********************/
        fristSwitchYes = UILabel()
        fristSwitchYes.setLabel(X: SWIDTH/2-60, Y: fristSwitch.frame.origin.y+5, Width: 40, Height: 20, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 18))
        fristSwitchYes.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(fristSwitchYes)
        /******************--FristSwitch Yes--*********************/
        fristAgeLabel = UILabel()
        fristAgeLabel.setLabel(X: SWIDTH/2+30, Y: fristSwitch.frame.origin.y+5, Width: 40, Height: 20, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 18))
        fristAgeLabel.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(fristAgeLabel)
        
        /******************--facebook UIButton--*********************/
        secondLine = UILabel()
        secondLine.setLabel(X: 0, Y: fristAgeLabel.frame.maxY+20, Width: SWIDTH, Height: 1.5, TextColor: .white, BackColor: .white, Text: "", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 20))
        self.scrollView.addSubview(secondLine)
        /******************--NJ--*********************/
        termsLabel.setLabel(X: SWIDTH/9, Y: secondLine.frame.maxY+5, Width: SWIDTH*0.8/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "TERMS OF SERVICE  ", TextAlignment: .left, Font: .boldSystemFont(ofSize: 12))
        
        self.scrollView.addSubview(termsLabel)
        termsLabel.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(termsLabelClicked))
        termsLabel.addGestureRecognizer(gesture)
        /******************--NJ-trmsSwitch-****/
        
        //        print("setupview accept label max \(acceptLabel.frame.maxY+10)")
        trmsSwitch = UISwitch(frame: CGRect(x: termsLabel.frame.maxX+22, y:secondLine.frame.maxY+10 , width: 44, height: 44))
        trmsSwitch.isOn = false
        trmsSwitch.tintColor = .red
        trmsSwitch.thumbTintColor = .red
       // trmsSwitch.addTarget(self, action: #selector(fristSwitchAction), for: .touchUpInside)
        self.scrollView.addSubview(trmsSwitch)
        /******************--NJ--*********************/
        YesLabel0.setLabel(X: trmsSwitch.frame.origin.x-8-SWIDTH/8, Y: secondLine.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
        YesLabel0.font = UIFont(name: "Electrolize-Regular", size: 16)
        YesLabel0.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(YesLabel0)
        /******************--NJ--*********************/
        NoLabel0.setLabel(X: trmsSwitch.frame.origin.x+trmsSwitch.frame.width+8, Y: secondLine.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
        NoLabel0.font = UIFont(name: "Electrolize-Regular", size: 16)
        NoLabel0.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(NoLabel0)
        //  Label Do you want Accept  ----- ----
        privcyLabel.setLabel(X: SWIDTH/9, Y: termsLabel.frame.maxY+5, Width: SWIDTH*0.8/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "PRIVACY POLICIES  ", TextAlignment: .left, Font: .boldSystemFont(ofSize: 12))
        self.scrollView.addSubview(privcyLabel)
        privcyLabel.isUserInteractionEnabled = true
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(privcyLabelClicked))
        privcyLabel.addGestureRecognizer(gesture1)
        /******************--NJ-trmsSwitch-****/
        privcySwitch = UISwitch(frame: CGRect(x: privcyLabel.frame.maxX+22, y:termsLabel.frame.maxY+10 , width: 44, height: 44))
        privcySwitch.isOn = false
        privcySwitch.tintColor = .red
        privcySwitch.thumbTintColor = .red
       // privcySwitch.addTarget(self, action: #selector(fristSwitchAction), for: .touchUpInside)
        self.scrollView.addSubview(privcySwitch)
        /******************--NJ--*********************/
        YesLabel1.setLabel(X: privcySwitch.frame.origin.x-8-SWIDTH/8, Y: termsLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
        YesLabel1.font = UIFont(name: "Electrolize-Regular", size: 16)
        YesLabel1.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(YesLabel1)
        /******************--NJ--*********************/
        NoLabel1.setLabel(X: privcySwitch.frame.origin.x+privcySwitch.frame.width+8, Y: termsLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
        NoLabel1.font = UIFont(name: "Electrolize-Regular", size: 16)
        NoLabel1.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(NoLabel1)
        
        //  Label Do you want Accept  ----- ----
        eulaLabel.setLabel(X: SWIDTH/9, Y: privcyLabel.frame.maxY+5, Width: SWIDTH*0.8/2, Height: 40, TextColor: .white, BackColor: .clear, Text: "EULA  ", TextAlignment: .left, Font: .boldSystemFont(ofSize: 12))
        self.scrollView.addSubview(eulaLabel)
        eulaLabel.isUserInteractionEnabled = true
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(eulaClicked))
        eulaLabel.addGestureRecognizer(gesture2)
        /******************--NJ-trmsSwitch-****/
        eulaSwitch = UISwitch(frame: CGRect(x: eulaLabel.frame.maxX+22, y:privcyLabel.frame.maxY+10 , width: 44, height: 44))
        eulaSwitch.isOn = false
        eulaSwitch.tintColor = .red
        eulaSwitch.thumbTintColor = .red
        //eulaSwitch.addTarget(self, action: #selector(fristSwitchAction), for: .touchUpInside)
        self.scrollView.addSubview(eulaSwitch)
        /******************--NJ--*********************/
        YesLabel2.setLabel(X: eulaSwitch.frame.origin.x-8-SWIDTH/8, Y: privcyLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "No", TextAlignment: .right, Font: UIFont.systemFont(ofSize: 16))
        YesLabel2.font = UIFont(name: "Electrolize-Regular", size: 16)
        YesLabel2.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(YesLabel2)
        /******************--NJ--*********************/
        NoLabel2.setLabel(X: eulaSwitch.frame.origin.x+eulaSwitch.frame.width+8, Y: privcyLabel.frame.maxY+5, Width: SWIDTH/8, Height: 40, TextColor: .white, BackColor: .clear, Text: "Yes", TextAlignment: .left, Font: UIFont.systemFont(ofSize: 16))
        NoLabel2.font = UIFont(name: "Electrolize-Regular", size: 16)
        NoLabel2.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(NoLabel2)
        /******************--NJ--*********************/
        thirdLine = UILabel()
        thirdLine.setLabel(X: 0, Y: NoLabel2.frame.maxY+5, Width: SWIDTH, Height: 1.5, TextColor: .white, BackColor: .white, Text: "", TextAlignment: .left, Font: UIFont.boldSystemFont(ofSize: 20))
        self.scrollView.addSubview(thirdLine)
        //scrollViewlastTittle
        /******************--NJ--*********************/
        lastTittle = UILabel()
      //  lastTittle.setLabel(X: 0, Y: thirdLine.frame.maxY+5, Width: SWIDTH, Height: 40, TextColor: .white, BackColor: .clear, Text: "THIS APP IS FOR ADULT USE ONLY. USE AT OWN RISK", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        //        lastTittle
        /******************--NJ--*********************/
        lastTittle0 = UILabel()
        lastTittle0.setLabel(X: 0, Y: thirdLine.frame.maxY+10, Width: SWIDTH, Height: 42, TextColor: .white, BackColor: .clear, Text: "THIS APP IS FOR ADULT USE ONLY.\nUSE AT OWN RISK", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        lastTittle0.numberOfLines = 2
        lastTittle0.adjustsFontSizeToFitWidth = true
        self.scrollView.addSubview(lastTittle0)
        
        lastTittle.setLabel(X: SWIDTH/4, Y: lastTittle0.frame.maxY+10, Width: SWIDTH/2, Height: 40, TextColor: .white, BackColor: .green, Text: "Continue", TextAlignment: .center, Font: UIFont.boldSystemFont(ofSize: 20))
        lastTittle.numberOfLines = 0
        lastTittle.layer.cornerRadius = 20
        lastTittle.layer.masksToBounds = true
        lastTittle.layer.borderWidth = 1.5
        lastTittle.layer.borderColor = UIColor.red.cgColor
        lastTittle.adjustsFontSizeToFitWidth = true
        lastTittle.isUserInteractionEnabled = true
        let gesture12 = UITapGestureRecognizer(target: self, action: #selector(btnContine))
        lastTittle.addGestureRecognizer(gesture12)
        self.scrollView.addSubview(lastTittle)
        /******************--NJ--*********************/
//        EditProfileButton = UIButton()
//        EditProfileButton.setButton(X: SWIDTH-LabelHeight-15, Y: CouponBookImage.frame.origin.y-LabelHeight*3/4, Width: LabelHeight*3/4, Height: LabelHeight*3/4, TextColor: .clear, BackColor: .clear, Title: "")
//        EditProfileButton.addTarget(self, action: #selector(EditProfileButtonAction), for: .touchUpInside)
//        self.view.addSubview(EditProfileButton)
//        /******************--NJ--*********************/
//        EditIcon = UIImageView()
//        EditIcon.setImageView(X: EditProfileButton.frame.height/6, Y: EditProfileButton.frame.height/6, Width: (EditProfileButton.frame.height/6)*4, Height: (EditProfileButton.frame.height/6)*4, img: #imageLiteral(resourceName: "edit"))
//        EditIcon.contentMode = UIViewContentMode.scaleAspectFit
//        EditIcon.clipsToBounds = true
//        EditProfileButton.addSubview(EditIcon)
        /******************--NJ--*********************/
        
        /******************--NJ--*********************/
        
        
        
        
    }
    
    @objc func btnContine(){
        if fristSwitch.isOn == false{
            Utility.sharedInstance.showAlert("", msg: "Please accept all the Points", controller: self)
            return
        }else if trmsSwitch.isOn == false{
            Utility.sharedInstance.showAlert("", msg: "Please accept all the Points", controller: self)
            return
        }else if privcySwitch.isOn == false{
            Utility.sharedInstance.showAlert("", msg: "Please accept all the Points", controller: self)
            return
        }else if eulaSwitch.isOn == false{
            Utility.sharedInstance.showAlert("", msg: "Please accept all the Points", controller: self)
            return
        }else{
            print("Home")
            let vc = HomeViewController()
          //  vc.privacyStatus = dataDictionary.object(forKey: "privacy_status") as? String ?? ""
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    @objc func termsLabelClicked(){
        if let customView = Bundle.main.loadNibNamed("TermsView1", owner: self, options: nil)?.first as? TermView {
            print("found view")
            customView.textView.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.textView.layer.borderWidth = 1.5
            customView.textView.layer.borderColor = UIColor.white.cgColor
            self.view.addSubview(customView)
        }
    }
    @objc func privcyLabelClicked(){
        print("privcyLabelClicked")
        if let customView = Bundle.main.loadNibNamed("PrivacyPolicyView", owner: self, options: nil)?.first as? PrivacyPolicy {
            print("found view")
            customView.textView.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.textView.layer.borderWidth = 1.5
            customView.textView.layer.borderColor = UIColor.white.cgColor
            
            self.view.addSubview(customView)
        }
    }
    
    @objc func eulaClicked(){
        print("eulaClicked")
        if let customView = Bundle.main.loadNibNamed("EULA", owner: self, options: nil)?.first as? ELUAClass {
            print("found view")
            customView.textViewElua.setContentOffset(CGPoint.zero, animated: false)
            customView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            customView.textViewElua.layer.borderWidth = 1.5
            customView.textViewElua.layer.borderColor = UIColor.white.cgColor
            
            self.view.addSubview(customView)
        }
    }
    
    @objc func fristSwitchAction(){
        let AlertController = UIAlertController(title: "", message: "Sorry!! You are not eligible to use this app.", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "OK", style: .destructive) { (UIAlertAction) in
            exit(0)
        }
        AlertController.addAction(OkAction)
        self.present(AlertController, animated: true, completion: nil)
    }
    @objc func EditProfileButtonAction(){
        
        let vc = EditProfileViewController()
        vc.userId = ""
        vc.userName = UserNAmeLabel.text!
        vc.userEmail = UserEmailLabel.text!
        vc.userMobile = UserMobileNoLabel.text!
        vc.userImageStr = self.imgUrl//UserProfileImageView.image!
        navigationController?.pushViewController(vc, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
        navigationController?.navigationBar.barTintColor = APPGRAYCOLOR
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        navigationController?.navigationBar.barTintColor = APPREDCOLOR
        //UINavigationBar.appearance().barTintColor = APPREDCOLOR
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func buttomBorder(label: UILabel) -> UILabel {
        let frame = label.frame
        let bottomLayer = CALayer()
        bottomLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width - 2, height: 1)
        bottomLayer.backgroundColor = UIColor.lightGray.cgColor
        label.layer.addSublayer(bottomLayer)
        //For Shadow
        label.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        label.layer.shadowOffset = CGSize(width: 0.0, height: 2.00)
        label.layer.shadowOpacity = 1.0
        label.layer.shadowRadius = 0.0
        label.layer.masksToBounds = false
        label.layer.cornerRadius = 4.0
        
        return label
        
    }
}
